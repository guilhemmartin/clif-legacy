/*
 * CLIF is a Load Injection Framework
 * Copyright (C) 2004, 2005, 2006 France Telecom
 * Copyright (C) 2013, 2016, 2019 Orange SA
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 *
 * Contact: clif@ow2.org
 */

package org.ow2.isac.plugin.httpinjector;

import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.apache.commons.httpclient.Credentials;
import org.apache.commons.httpclient.DefaultHttpMethodRetryHandler;
import org.apache.commons.httpclient.Header;
import org.apache.commons.httpclient.HostConfiguration;
import org.apache.commons.httpclient.HttpClient;
import org.apache.commons.httpclient.SimpleHttpConnectionManager;
import org.apache.commons.httpclient.UsernamePasswordCredentials;
import org.apache.commons.httpclient.params.HttpMethodParams;
import org.apache.log4j.BasicConfigurator;
import org.apache.log4j.varia.NullAppender;
import org.ow2.clif.scenario.isac.exception.IsacRuntimeException;
import org.ow2.clif.scenario.isac.plugin.ControlAction;
import org.ow2.clif.scenario.isac.plugin.DataProvider;
import org.ow2.clif.scenario.isac.plugin.SampleAction;
import org.ow2.clif.scenario.isac.plugin.TestAction;
import org.ow2.clif.scenario.isac.plugin.SessionObjectAction;
import org.ow2.clif.scenario.isac.util.ParameterParser;
import org.ow2.clif.storage.api.ActionEvent;
import org.ow2.isac.plugin.httpinjector.actions.HttpInjectorSamples;
import org.ow2.isac.plugin.httpinjector.actions.HttpInjectorTests;
import org.ow2.isac.plugin.httpinjector.tools.ParameterConstants;

/**
 * This class is the implementation of a session object for the HTTP injector
 * plugin.
 *
 * @author JC Meillaud
 * @author A Peyrard
 * @author Emmanuel Varoquaux
 * @author Bruno Dillenseger
 */
public class SessionObject
		implements
			SampleAction,
			TestAction,
			ControlAction,
			SessionObjectAction,
			DataProvider
{ 
	static
	{
		//PropertyConfigurator.configure(FileName.LOG4J_PROPERTIES);
		BasicConfigurator.configure(new NullAppender());
	}

	public static final int SAMPLE_GET = 0;
	public static final int SAMPLE_POST = 1;
	public static final int SAMPLE_MULTIPARTPOST = 2;
	public static final int SAMPLE_HEAD = 3;
	public static final int SAMPLE_OPTIONS = 4;
	public static final int SAMPLE_PUT = 5;
	public static final int SAMPLE_DELETE = 6;

	public static final int TEST_IS404RESPONSE = 0;
	public static final int TEST_ISSTATUSCODERESPONSE = 1;
	public static final int TEST_ISHEADERVALUE = 2;
	public static final int TEST_HASHEADER = 3;
	public static final int TEST_HASNOHEADER = 4;
	public static final int TEST_STATUSISONEOF = 5;
	public static final int TEST_STATUSISNOTONEOF = 6;

	public static final int CONTROL_SETCONNECTIONTIMEOUT = 0;
	public static final int CONTROL_SETRESPONSETIMEOUT = 1;
	public static final int CONTROL_SETERRORRESPONSECODES = 2;

	public static final String HEADER_VARIABLE_PREFIX = "#";
	public static final String STATUS_CODE_VAR = "!";

	// int representing the last errorcode received
	private int lastStatusCode = -1;

	private Header[] lastHeader = null;

	private String lastResponseBody = null;

	// http client will be used to execute the http methods
	private HttpClient httpClient;

	// host and port for the proxy
	private String proxyHost;

	private String proxyPort;

	private String proxyUserName;

	private String proxyUserPass;
	
	private String userAgent;

	private boolean loadFrames = false;
	private boolean loadImages = false;
	private boolean loadCss = false;
	private boolean loadJs = false;
	private boolean preemptiveAuthentication;
	private boolean singleCookieHeader = false;
	private List<String> errorResponseCodes;

	private Map<String,String> variables;

	/** if false, response times are measured in microseconds instead of milliseconds */
	private boolean unit_ms = true;

	/**
	 * Constructor
	 *
	 * @param params
	 *            defining the httpclient
	 */
	public SessionObject(Map<String,String> params)
	{
		String unit = ParameterParser.getRadioGroup(params.get(ParameterConstants.RESPONSE_TIME_UNIT));
		unit_ms = (unit == null) || unit.equals("") || unit.equals("millisecond");
		userAgent = params.get(ParameterConstants.USERAGENT);
		if (userAgent == null || userAgent.length() == 0)
		{
			userAgent = ParameterConstants.DEFAULTUSERAGENT;
		}
		String proxyHost = params.get(ParameterConstants.PROXYHOST);
		String proxyPort = params.get(ParameterConstants.PROXYPORT);
		String inDepthLoadStr = params.get(ParameterConstants.INDEPTHLOAD);
		String proxyUserName = params.get(ParameterConstants.PROXYUSERNAME);
		String proxyUserPass = params.get(ParameterConstants.PROXYUSERPASS);
		String cookieHeader = ParameterParser.getRadioGroup(params.get(ParameterConstants.COOKIEHEADER));
		String preemptiveAuthenticationStr = params.get(
			ParameterConstants.PREEMPTIVEAUTHENTICATION);
		// init variables to EMPTY if they are null
		this.proxyPort = (proxyPort != null)
			? proxyPort
			: ParameterConstants.EMPTY;
		this.proxyHost = (proxyHost != null)
			? proxyHost
			: ParameterConstants.EMPTY;
		this.proxyUserName = (proxyUserName != null)
			? proxyUserName
			: ParameterConstants.EMPTY;
		this.proxyUserPass = (proxyUserPass != null)
			? proxyUserPass
			: ParameterConstants.EMPTY;
		// init boolean attributes
		if (ParameterParser.getCheckBox(inDepthLoadStr).contains(ParameterConstants.ENABLED))
		{
			// for backward compatibility
			this.loadFrames = true;
			this.loadImages = true;
		}
		else
		{
			this.loadFrames = ParameterParser.getCheckBox(inDepthLoadStr).contains(ParameterConstants.LOAD_FRAMES);
			this.loadImages = ParameterParser.getCheckBox(inDepthLoadStr).contains(ParameterConstants.LOAD_IMAGES);
			this.loadCss = ParameterParser.getCheckBox(inDepthLoadStr).contains(ParameterConstants.LOAD_CSS);
			this.loadJs = ParameterParser.getCheckBox(inDepthLoadStr).contains(ParameterConstants.LOAD_JS);
		}
		this.preemptiveAuthentication =
			ParameterParser.getCheckBox(preemptiveAuthenticationStr).contains(ParameterConstants.ENABLED);
		if (cookieHeader.equals(ParameterConstants.SINGLECOOKIEHEADER))
		{
			this.singleCookieHeader = true;
		}
		else if (cookieHeader.equals(ParameterConstants.MULTIPLECOOKIEHEADERS))
		{
			this.singleCookieHeader = false;
		}
		else if (cookieHeader.isEmpty())
		{
			this.singleCookieHeader = false; // for backward compatibility
		}
		else
		{
			throw new IsacRuntimeException(
				"Invalid value \"" + cookieHeader
				+ "\" for import parameter \"" + ParameterConstants.COOKIEHEADER
				+ "\" in ISAC plug-in HttpInjector.");
		}
	}

	/**
	 * Constructor used in cloneObject method
	 *
	 * @param so
	 *            The session object to clone
	 */
	private SessionObject(SessionObject so) {
		this.unit_ms = so.unit_ms;
		this.userAgent = so.userAgent;
		this.proxyHost = so.getProxyHost();
		this.proxyPort = so.getProxyPort();
		this.proxyUserName = so.getProxyUserName();
		this.proxyUserPass = so.getProxyUserPass();
		this.preemptiveAuthentication = so.preemptiveAuthenticationEnabled();
		this.singleCookieHeader = so.singleCookieHeader;
		this.loadFrames = so.loadFramesEnabled();
		this.loadImages = so.loadImagesEnabled();
		this.loadCss = so.loadCssEnabled();
		this.loadJs = so.loadJsEnabled();
		this.variables = new HashMap<String,String>();

		// init the http client
/*
 * MultiThreadedHttpConnectionManager in HttpClient v3 is incompatible with Isac
 * execution engine because it keeps track of threads going through, and then
 * calls "interrupt()" on them later on. This disturbs pool thread management by
 * IsacExtendedEngine (typically, ISAC scenarios never terminate).
 * So, we use SimpleHttpConnectionManager instead.
 */
//		this.httpClient = new HttpClient(new MultiThreadedHttpConnectionManager());
		this.httpClient = new HttpClient(new SimpleHttpConnectionManager());
		// if the proxy has been configured set it to
		// the connection manager
		try
		{
			if (!proxyHost.equals("") && !proxyPort.equals(""))
			{
				setProxy(proxyHost, (new Integer(proxyPort)).intValue());
			}
			if (!proxyUserName.equals("") && !proxyUserPass.equals(""))
			{
				setProxyAuthentication(proxyUserName, proxyUserPass);
			}
			else if (System.getProperty("http.proxyUserName") != null)
			{
				setProxyAuthentication(
					System.getProperty("http.proxyUserName"),
					System.getProperty("http.proxyPassword"));
			}
			httpClient.getParams().setParameter(
				HttpMethodParams.RETRY_HANDLER,
				new DefaultHttpMethodRetryHandler(0, false));
			httpClient.getParams().setAuthenticationPreemptive(preemptiveAuthentication);
			httpClient.getParams().getDefaults().setBooleanParameter(
				HttpMethodParams.SINGLE_COOKIE_HEADER,
				singleCookieHeader);
		}
		catch (Exception e)
		{
			throw new IsacRuntimeException(
				"Unable to set the Session Object for ISAC HttpInjector plug-in", e);
		}
	}


	public ActionEvent doSample(int number, Map<String,String> params, ActionEvent report)
	{
		if (!params.get(ParameterConstants.URI).equals(ParameterConstants.EMPTY))
		{
			switch (number) {
				case SAMPLE_GET :
					return HttpInjectorSamples.doGet(this, report, params);
				case SAMPLE_POST :
					return HttpInjectorSamples.doPost(this, report, params);
				case SAMPLE_MULTIPARTPOST :
					return HttpInjectorSamples.doMultiPost(this, report, params);
				case SAMPLE_HEAD :
					return HttpInjectorSamples.doHead(this, report, params);
				case SAMPLE_OPTIONS :
					return HttpInjectorSamples.doOptions(this, report, params);
				case SAMPLE_PUT :
					return HttpInjectorSamples.doPut(this, report, params);
				case SAMPLE_DELETE :
					return HttpInjectorSamples.doDelete(this, report, params);
				default :
					throw new Error("Unable to find this sample in HttpInjector ISAC plugin: " + number);
			}
		}
		else
		{
			throw new IsacRuntimeException("HttpInjector ISAC plug-in: no URI is defined for sample " + number);
		}
	}


	public boolean doTest(int number, Map<String,String> params)
	{
		switch (number)
		{
			case TEST_IS404RESPONSE :
				return HttpInjectorTests.is404Response(this);
			case TEST_ISSTATUSCODERESPONSE :
			{
				int statusCode = new Integer(params.get(ParameterConstants.STATUSCODE)).intValue();
				return
					statusCode != -1
					&& HttpInjectorTests.isStatusCodeResponse(this, statusCode);
			}
			case TEST_ISHEADERVALUE :
			{
				String headerType = params.get(ParameterConstants.HEADERPARAMETERTYPE);
				String headerValue = params.get(ParameterConstants.HEADERPARAMETERVALUE);
				String equality = ParameterParser.getRadioGroup(
					params.get(ParameterConstants.EQUALITY_OPERATOR));
				if (equality == null || equality.length() == 0)
				{
					equality = ParameterConstants.EQUALITY_EQUAL;
				}
				if (equality.equalsIgnoreCase(ParameterConstants.EQUALITY_EQUAL))
				{
					return
						headerType != null
						&& headerValue != null
						&& HttpInjectorTests.isHeaderValue(this, headerType, headerValue);
				}
				else
				{
					return
						headerType == null
						|| headerValue == null
						|| ! HttpInjectorTests.isHeaderValue(this, headerType, headerValue);
				}
			}
			case TEST_HASHEADER :
			{
				String headerType = params.get(ParameterConstants.HEADERPARAMETERTYPE);
				return
					headerType != null
					&& HttpInjectorTests.hasHeader(this, headerType);
			}
			case TEST_HASNOHEADER :
			{
				String headerType = params.get(ParameterConstants.HEADERPARAMETERTYPE);
				return
					headerType == null
					|| ! HttpInjectorTests.hasHeader(this, headerType);
			}
			case TEST_STATUSISONEOF:
			{
				Collection<String> codes = ParameterParser.getNField(
					params.get(ParameterConstants.STATUS_CODES));
				return HttpInjectorTests.statusIsOneOf(this, codes);
			}
			case TEST_STATUSISNOTONEOF:
			{
				Collection<String> codes = ParameterParser.getNField(
					params.get(ParameterConstants.STATUS_CODES));
				return ! HttpInjectorTests.statusIsOneOf(this, codes);
			}
			default :
				throw new IsacRuntimeException(
					"Unable to find this test in HttpInjector plugin: "
					+ number);
		}
	}


	/////////////////////////////
	// ControlAction interface //
	/////////////////////////////


	public void doControl(int number, Map<String,String> params)
	{
		switch (number)
		{
			case CONTROL_SETCONNECTIONTIMEOUT:
				httpClient.getHttpConnectionManager().getParams().setConnectionTimeout(
					Integer.parseInt(params.get(ParameterConstants.CONTROL_SETCONNECTIONTIMEOUT_TIMEOUT)));
			break;
			case CONTROL_SETRESPONSETIMEOUT:
				httpClient.getParams().setSoTimeout(
					Integer.parseInt(params.get(ParameterConstants.CONTROL_SETRESPONSETIMEOUT_TIMEOUT)));
			break;
			case CONTROL_SETERRORRESPONSECODES:
				errorResponseCodes = ParameterParser.getNField(
					params.get(ParameterConstants.CONTROL_SETERRORRESPONSECODES)); 
			break;
			default:
				throw new IsacRuntimeException(
					"Unable to find control #" + number + " in HttpInjector plugin.");
		}
	}


	/**
	 * Method which configure the proxy
	 *
	 * @param host
	 *            The hostname of the proxy server
	 * @param port
	 *            The port name to go through the proxy
	 */
	private void setProxy(String host, int port) {
		// configure the proxy
		HostConfiguration hostConfiguration = new HostConfiguration();
		hostConfiguration.setProxy(host, port);
		// add it to the connection manager
		this.httpClient.setHostConfiguration(hostConfiguration);
	}

	private void setProxyAuthentication(String userName, String userPass)
	{
		Credentials customCrds = new UsernamePasswordCredentials(userName, userPass);
		this.httpClient.getState().setProxyCredentials(null, customCrds);
	}

	/////////////////////////////////////////
	// SessionObjectAction implementation
	/////////////////////////////////////////

	/**
	 * @see org.ow2.clif.scenario.isac.plugin.SessionObjectAction#createNewSessionObject()
	 */
	public Object createNewSessionObject() {
		return new SessionObject(this);
	}

	/**
	 * @see org.ow2.clif.scenario.isac.plugin.SessionObjectAction#close()
	 */
	public void close()
	{
		this.httpClient.getState().clear();
	}

	/**
	 * @see org.ow2.clif.scenario.isac.plugin.SessionObjectAction#reset()
	 */
	public void reset()
	{
		this.lastStatusCode = -1;
		this.lastHeader = null;
		this.lastResponseBody = null;
		this.httpClient.getState().clear();
	}

	////////////////////////////
	// DataProvider interface //
	////////////////////////////

	public String doGet(String variable)
	{
		if (variable.startsWith(HEADER_VARIABLE_PREFIX))
		{
			variable = variable.substring(1);
			for (Header head : getLastHeader())
			{
				if (head.getName().equalsIgnoreCase(variable))
				{
					return head.getValue();
				}
			}
			return "";
		}
		else if (variable.equals(STATUS_CODE_VAR))
		{
			if (lastStatusCode != -1)
			{
				return String.valueOf(lastStatusCode);
			}
			else
			{
				return "";
			}
		}
		else
		{
			return variables.get(variable);
		}
	}

	///////////////////////////////////////////////////
	// Attribute getters and setters
	///////////////////////////////////////////////////

	/**
	 * @return Returns the httpClient.
	 */
	public HttpClient getHttpClient() {
		return httpClient;
	}

	/**
	 * @return Returns the proxy host.
	 */
	public String getProxyHost() {
		return proxyHost;
	}

	/**
	 * @return Returns the proxy port.
	 */
	public String getProxyPort() {
		return proxyPort;
	}

	/**
	 * @return Returns the proxyUserName.
	 */
	public String getProxyUserName() {
		return proxyUserName;
	}

	/**
	 * @param proxyUserName
	 *            The proxyUserName to set.
	 */
	public void setProxyUserName(String proxyUserName) {
		this.proxyUserName = proxyUserName;
	}

	/**
	 * @return Returns the proxyUserPass.
	 */
	public String getProxyUserPass() {
		return proxyUserPass;
	}

	/**
	 * @param proxyUserPass
	 *            The proxyUserPass to set.
	 */
	public void setProxyUserPass(String proxyUserPass) {
		this.proxyUserPass = proxyUserPass;
	}

	/**
	 * @return true if preemptive authentication is enabled, false otherwise
	 */
	public boolean preemptiveAuthenticationEnabled() {
		return preemptiveAuthentication;
	}


	/**
	 * @return true if frames must be loaded, false otherwise
	 */
	public boolean loadFramesEnabled()
	{
		return loadFrames;
	}


	/**
	 * @return true if images must be loaded, false otherwise
	 */
	public boolean loadImagesEnabled()
	{
		return loadImages;
	}


	/**
	 * @return true if style files (.css) must be loaded, false otherwise
	 */
	public boolean loadCssEnabled()
	{
		return loadCss;
	}


	/**
	 * @return true if scripts (.js) must be loaded, false otherwise
	 */
	public boolean loadJsEnabled()
	{
		return loadJs;
	}


	/**
	 * @return true if the content of the loaded HTML must be parsed to
	 * identify and load external resources, such as javascript (.js),
	 * styles (.css), images, or frames.
	 */
	public boolean inDepthLoadEnabled()
	{
		return loadJs || loadCss || loadFrames || loadImages;
	}


	/**
	 * @param authenticationPreemptive
	 *            The authenticationPreemptive to set.
	 */
	public void enablePreemptiveAuthentication(boolean authenticationPreemptive) {
		this.preemptiveAuthentication = authenticationPreemptive;
	}


	/**
	 * Get the HTTP status code of the last executed HTTP method
	 *
	 * @return the last HTTP response status code,
	 * or -1 if the latest method execution did not complete.
	 */
	public int getLastStatusCode() {
		return lastStatusCode;
	}

	/**
	 * Method to set the StatusCode of the lastMethod executed
	 *
	 * @param lastStatusCode
	 *            The lastStatusCode to set.
	 */
	public void setLastStatusCode(int lastStatusCode) {
		this.lastStatusCode = lastStatusCode;
	}

	/**
	 * Method to get the Headers from the last method executed
	 *
	 * @return Returns the lastHeader.
	 */
	public Header[] getLastHeader() {
		return lastHeader;
	}

	/**
	 * Method to set the Headers of the last method executed
	 *
	 * @param lastHeader
	 *            The lastHeader to set.
	 */
	public void setLastHeader(Header[] lastHeader) {
		this.lastHeader = lastHeader;
	}

	/**
	 * Method to get the Response from the last method executed
	 *
	 * @return Returns the last response body.
	 */
	public String getLastResponseBody() {
		return lastResponseBody;
	}
	/**
	 * Method to set the Response body of the last method executed
	 *
	 * @param lastResponseBody the body part of the last response received
	 */
	public void setLastResponseBody(String lastResponseBody) {
		this.lastResponseBody = lastResponseBody;
	}
	
	public String getUserAgent()
	{
		return userAgent;
	}

	public void setVariable(String name, String value)
	{
		variables.put(name, value);
	}

	public void clearVariable(String name)
	{
		variables.remove(name);
	}

	public boolean unitIsMicros()
	{
		return ! unit_ms;
	}

	public boolean isSuccessful(int code)
	{
		if (errorResponseCodes != null)
		{
			String codeStr = String.valueOf(code);
			for (String errorCode : errorResponseCodes)
			{
				if (codeStr.startsWith(errorCode))
				{
					return false;
				}
			}
		}
		return true;
	}
}
