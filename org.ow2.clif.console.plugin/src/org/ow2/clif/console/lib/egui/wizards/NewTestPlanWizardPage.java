/*
 * CLIF is a Load Injection Framework
 * Copyright 2005 Manuel Azema, Tsirimiaina Andrianavonimiarina Jaona
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 *
 * CLIF $Name: not supported by cvs2svn $
 *
 * Contact: clif@ow2.org
 */

package org.ow2.clif.console.lib.egui.wizards;

import org.eclipse.core.resources.IContainer;
import org.eclipse.core.resources.IFile;
import org.eclipse.core.resources.IResource;
import org.eclipse.core.resources.IWorkspaceRoot;
import org.eclipse.core.resources.ResourcesPlugin;
import org.eclipse.core.runtime.Path;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.jface.wizard.WizardPage;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.ModifyEvent;
import org.eclipse.swt.events.ModifyListener;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Text;
import org.eclipse.ui.dialogs.ContainerSelectionDialog;

/**
 * The "New" wizard page allows setting the container for
 * the new test plan file as well as the file name. The page
 * will only accept file name without the extension or
 * with the .ctp extension.
 * The wizard checks if container exists and file does not exist.
 * 
 * @author Tsirimiaina Andrianavonimiarina Jaona
 */

public class NewTestPlanWizardPage extends WizardPage {
    
    /* Textfields */
    private Text containerText;
    private Text fileText;
    
    /* Selection in the workbench. */
    private ISelection selection;
    
    /**
     * Construct a new NewTestPlanWizardPage.
     * 
     * @param selection use for container selected
     */
    public NewTestPlanWizardPage(ISelection selection) {
        super("chooseFileNamePage");
        setTitle("Create a new CLIF Test Plan");
        setDescription("This wizard creates a new CLIF Test Plan with *.ctp extension that can be opened by a Test Plan editor.");
        this.selection = selection;
    }
    
    /**
     * Create page contents (2 labels and textfields and 1 button).
     * 
     * @param parent the parent composite
     * @see org.eclipse.jface.dialogs.IDialogPage#createControl(Composite)
     */
    public void createControl(Composite parent) {
        Composite container = new Composite(parent, SWT.NULL);
        GridLayout layout = new GridLayout();
        container.setLayout(layout);
        layout.numColumns = 3;
        layout.verticalSpacing = 9;
        
        Label label = new Label(container, SWT.NULL);
        label.setText("&Container:");
        containerText = new Text(container, SWT.BORDER | SWT.SINGLE);
        GridData gd = new GridData(GridData.FILL_HORIZONTAL);
        containerText.setLayoutData(gd);
        containerText.addModifyListener(new ModifyListener() {
            public void modifyText(ModifyEvent e) {
                dialogChanged();
            }
        });
        
        Button button = new Button(container, SWT.PUSH);
        button.setText("Browse...");
        button.addSelectionListener(new SelectionAdapter() {
            public void widgetSelected(SelectionEvent e) {
                handleBrowse();
            }
        });
        
        label = new Label(container, SWT.NULL);
        label.setText("&File name:");
        fileText = new Text(container, SWT.BORDER | SWT.SINGLE);
        gd = new GridData(GridData.FILL_HORIZONTAL);
        fileText.setLayoutData(gd);
        fileText.addModifyListener(new ModifyListener() {
            public void modifyText(ModifyEvent e) {
                dialogChanged();
            }
        });
        
        initialize();
        dialogChanged();
        setControl(container);
    }
    
    /**
     * Tests if the current workbench selection is a suitable
     * container to use.
     */	
    private void initialize() {
        if (selection!=null && selection.isEmpty()==false && selection instanceof IStructuredSelection) {
            IStructuredSelection ssel = (IStructuredSelection)selection;
            if (ssel.size()>1) return;
            Object obj = ssel.getFirstElement();
            if (obj instanceof IResource) {
                IContainer container;
                if (obj instanceof IContainer)
                    container = (IContainer)obj;
                else
                    container = ((IResource)obj).getParent();
                containerText.setText(container.getFullPath().toString());
            }
        }
        fileText.setText("new_test_plan.ctp");
    }
    
    /**
     * Uses the standard container selection dialog to
     * choose the new value for the container field.
     */
    private void handleBrowse() {
        ContainerSelectionDialog dialog =
            new ContainerSelectionDialog(
                    getShell(),
                    ResourcesPlugin.getWorkspace().getRoot(),
                    false,
            "Select new file container");
        if (dialog.open() == ContainerSelectionDialog.OK) {
            Object[] result = dialog.getResult();
            if (result.length == 1) {
                containerText.setText(((Path)result[0]).toOSString());
            }
        }
    }
    
    /**
     * Ensures that both text fields are set and valid.
     * Container name is valid if it exists.
     * File name is valid if it does not exist.
     */
    private void dialogChanged() {
        String containerName = getContainerName();
        String fileName = getFileName();
        
        if (containerName.length() == 0) {
            updateStatus("File container must be specified");
            return;
        }
        if (fileName.length() == 0) {
            updateStatus("File name must be specified");
            return;
        }
        
        // Test file extension
        int dotLoc = fileName.lastIndexOf('.');
        if (dotLoc != -1) {
            String ext = fileName.substring(dotLoc + 1);
            if (ext.equalsIgnoreCase("ctp") == false) {
                updateStatus("File extension must be \"ctp\"");
                return;
            }
        }
        else {
            // If no extension, test with the .ctp extension
            fileName = fileName + ".ctp";
        }
        
        //Test if container exist
        IWorkspaceRoot root = ResourcesPlugin.getWorkspace().getRoot();
        IResource resource = root.findMember(new Path(containerName));
        if (resource == null || !resource.exists() || !(resource instanceof IContainer)) {
            updateStatus("Invalid container");
            return;
        }		
        
        //Test if file not exist
        IContainer container = (IContainer) resource;
        IFile file = container.getFile(new Path(fileName));
        if (file.exists()) {
            updateStatus("File already exist");
            return;				
        }
        
        updateStatus(null);
    }
    
    /**
     * Show error message and allow "Finish" action or not.
     * 
     * @param message the message to display if error
     */
    private void updateStatus(String message) {
        setErrorMessage(message);
        setPageComplete(message == null);
    }
    
    /**
     * Return the container name.
     * 
     * @return the container name
     */
    public String getContainerName() {
        return containerText.getText();
    }
    
    /**
     * Return the file name.
     * 
     * @return the file name
     */
    public String getFileName() {
        return fileText.getText();
    }
}