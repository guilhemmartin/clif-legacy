/*
 * CLIF is a Load Injection Framework
 * Copyright (C) 2005, 2011 France Telecom R&D
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 *
 * Contact: clif@ow2.org
 */

package org.ow2.clif.console.lib.egui.tree;

import java.util.HashMap;
import java.util.Map;
import org.eclipse.jface.resource.ImageDescriptor;
import org.eclipse.jface.viewers.LabelProvider;
import org.eclipse.swt.graphics.Image;
import org.ow2.clif.console.lib.egui.ClifConsolePlugin;

/**
 * Provides label and decoration for clif tree viewer 
 * @author Joan Chaumont
 *
 */
public class ClifTreeLabelProvider extends LabelProvider {
    
    private Map<ImageDescriptor,Image> imageCache = new HashMap<ImageDescriptor,Image>(2);
    
    /**
     * Get text to write in clif tree viewer
     * @param obj object to display
     */
    public String getText(Object obj) {
        return obj.toString();
    }
    
    /**
     * Get image to display in clif tree viewer depending of object : 
     * blade (injector or probe) or server.
     * @param obj object to display
     */
    public Image getImage(Object obj) {
        ImageDescriptor descriptor = null;
        
        if (obj instanceof ClifTreeParent) {
            descriptor = ClifConsolePlugin.imageDescriptorFromPlugin(
                    ClifConsolePlugin.PLUGIN_ID,"icons/servers.png");
        }
        else if (obj instanceof ClifTreeObject) {
            if(((ClifTreeObject)obj).getName().startsWith("injector")) {
                descriptor = ClifConsolePlugin.imageDescriptorFromPlugin(
                        ClifConsolePlugin.PLUGIN_ID,"icons/injector.png"); 
            }
            else { 
                descriptor = ClifConsolePlugin.imageDescriptorFromPlugin(
                        ClifConsolePlugin.PLUGIN_ID,"icons/probe.png");
            }
        }
        
        if(descriptor == null) {
            return null;
        }
        
        Image image = imageCache.get(descriptor);
        if (image == null) {
            image = descriptor.createImage();
            imageCache.put(descriptor, image);
        }   
        return image;
    }
}
