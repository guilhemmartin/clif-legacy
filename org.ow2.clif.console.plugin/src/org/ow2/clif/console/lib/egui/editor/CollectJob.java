/*
 * CLIF is a Load Injection Framework
 * Copyright (C) 2005, 2012 France Telecom R&D
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 *
 * Contact: clif@ow2.org
 */

package org.ow2.clif.console.lib.egui.editor;

import java.io.File;
import java.io.PrintStream;
import org.eclipse.core.resources.IProject;
import org.eclipse.core.resources.IWorkspaceRoot;
import org.eclipse.core.resources.ResourcesPlugin;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Status;
import org.eclipse.core.runtime.jobs.Job;
import org.ow2.clif.storage.api.CollectListener;
import org.ow2.clif.storage.api.StorageRead;
import org.ow2.clif.supervisor.api.ClifException;
import org.ow2.clif.analyze.lib.quickstat.QuickstatProgress;
import org.ow2.clif.analyze.lib.quickstat.TestStats;
import org.ow2.clif.console.lib.batch.BatchUtil;
import org.ow2.clif.console.lib.egui.ClifConsolePlugin;
import org.ow2.clif.deploy.ClifAppFacade;

/**
 * Collecting measures and generating a simple statistical report,
 * with progress monitoring (in Progress view).
 * 
 * @author Bruno Dillenseger
 */
public class CollectJob
	extends Job
	implements CollectListener, QuickstatProgress
{
	protected IProgressMonitor monitor;
	protected boolean canceled = false;
	protected int fullSize;
	protected int fullProgress;
	protected int bladeProgress;
	protected ClifAppFacade clifApp;
	private String[] bladesId;

	/**
	 * Creates a new job for collecting measures of the latest test, and
	 * generating a simple statistical report.
	 * 
	 * @param clifApp
	 *            clifApp used by this test
	 * @param bladesId
	 *            blades id to collect
	 */
	public CollectJob(ClifAppFacade clifApp, String[] bladesId)
	{
		super("Collect");
		this.clifApp = clifApp;
		this.bladesId = bladesId;
	}

	/**
	 * Generates the quickstats report from the latest test execution
	 * available from the Storage component.
	 * @param out the output stream where to print the report
	 * @throws ClifException could not compute statistics or print the report
	 */
	private void report(PrintStream out) throws ClifException
	{
		try
		{
			StorageRead storage = (StorageRead)clifApp.getComponentByName("storage").getFcInterface(StorageRead.STORAGE_READ);
			TestStats stats = new TestStats(storage);
			stats.compute(this);
			stats.report(out);
		}
		catch (Exception ex)
		{
			throw new ClifException("Could not generate quick statistical report.", ex);
		}
	}

	@Override
	/**
	 * Collect measures from the latest test execution and
	 * generates the quickstats report to a file in the stats directory
	 * of current test project. Terminates by refreshing the workspace. 
	 * @param monitor supposed to be provided by the Progress View
	 */
	protected IStatus run(IProgressMonitor monitor)
	{
		this.monitor = monitor;
		// collect measures
		if (clifApp.collect(bladesId, this) != BatchUtil.SUCCESS)
		{
			return new Status(
				Status.WARNING,
				ClifConsolePlugin.PLUGIN_ID,
				"Collect failed.");
		}
		else if (canceled)
		{
			return Status.CANCEL_STATUS;
		}
		// generate quickstats report
		else
		{
			PrintStream output = null;
			File toDir = new File(System.getProperty("clif.eclipse.statdir"), clifApp.getCurrentTestId());
			File csvFile = new File(toDir, "quickstats.csv");
			try
			{
				toDir.mkdirs();
				output = new PrintStream(csvFile);
				report(output);
			}
			catch (Exception ex)
			{
				return new Status(
					Status.WARNING,
					ClifConsolePlugin.PLUGIN_ID,
					"Generation of the quick statistical report failed.",
					ex);
			}
			finally
			{
				if (output != null)
				{
					output.close();
				}
				if (csvFile.exists() && csvFile.length() == 0 && csvFile.canWrite())
				{
					csvFile.delete();
				}
			}
			// refresh workspace
			try
			{
				IWorkspaceRoot root = ResourcesPlugin.getWorkspace().getRoot();
				root.refreshLocal(IProject.DEPTH_INFINITE, null);
			}
			catch (CoreException ex)
			{
				return new Status(
					Status.WARNING,
					ClifConsolePlugin.PLUGIN_ID,
					"Workspace refresh failed.",
					ex);
			}
			return Status.OK_STATUS;
		}
	}

	///////////////////////////////
	// CollectListener interface //
	///////////////////////////////

	public void collectStart(String testId, long size)
	{
		fullSize = (int)(size >> 10);
		fullProgress = 0;
		bladeProgress = 0;
		monitor.beginTask("Collecting " + testId, fullSize);
	}

	public void bladeCollectStart(String bladeId, long size)
	{
		fullProgress += bladeProgress;
		bladeProgress = 0;
		monitor.subTask((size >> 10) + "KB from " + bladeId);
	}

	public void progress(String bladeId, long done)
	{
		if (monitor.isCanceled())
		{
			canceled = true;
		}
		int doneKB = (int)(done >> 10);
		monitor.worked(doneKB - bladeProgress);
		bladeProgress = doneKB;
	}

	public void done()
	{
		monitor.done();
	}

	public boolean isCanceled()
	{
		return canceled;
	}

	public boolean isCanceled(String bladeId)
	{
		return canceled;
	}

	/////////////////////////////////
	// QuickstatProgress interface //
	/////////////////////////////////

	public void quickstatStarted(String testId, int size)
	{
		monitor.beginTask("Generating statistics for test " + testId, size);
		fullSize = size;
		fullProgress = 0;
	}

	public void quickstatProgress(String bladeId, int done)
	{
		if (monitor.isCanceled())
		{
			canceled = true;
		}
		monitor.subTask("Analyzing injector " + bladeId);
		monitor.worked(done - (int)fullProgress);
		fullProgress = done;
	}

	public void quickstatComplete()
	{
		monitor.worked(fullSize - fullProgress);
		monitor.done();
	}

	public boolean quickstatIsCanceled()
	{
		return canceled;
	}
}
