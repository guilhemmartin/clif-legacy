/*
 * CLIF is a Load Injection Framework
 * Copyright (C) 2010 France Telecom R&D
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 *
 * Contact: clif@ow2.org
 */

package org.ow2.clif.console.lib.egui.wizards.clifProject;

import org.eclipse.jface.wizard.WizardPage;
import org.eclipse.swt.widgets.Composite;

/**
 * Page of the New CLIF project Wizard, used to set custom system properties
 * 
 * @author Bruno Dillenseger
 */
public class CustomPage extends WizardPage
{
	private InteractionManager interaction;

	public CustomPage()
	{
		super("wizardPage");
		setTitle("Custom Configuration");
		setDescription("User-specific Java system properties.");
	}

	/**
	 * Creates and initializes controls through InterationManager
	 */
	public void createControl(Composite parent)
	{
		interaction = new InteractionManager(this,null);
		Composite comp = interaction.createCustomContents(parent);
		interaction.initializeCustom();
		setControl(comp);
	}
}
