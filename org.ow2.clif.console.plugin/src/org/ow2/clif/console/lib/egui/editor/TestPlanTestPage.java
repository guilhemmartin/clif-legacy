/*
 * CLIF is a Load Injection Framework
 * Copyright 2005 Manuel Azema, Tsirimiaina Andrianavonimiarina Jaona
 * Copyright (C) 2009-2012 France Telecom
 * Copyright (C) 2014 Orange
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 *
 * Contact: clif@ow2.org
 */

package org.ow2.clif.console.lib.egui.editor;

import org.eclipse.core.resources.IProject;
import org.eclipse.core.resources.IWorkspaceRoot;
import org.eclipse.core.resources.ResourcesPlugin;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.MultiStatus;
import org.eclipse.core.runtime.Status;
import org.eclipse.jface.dialogs.ErrorDialog;
import org.eclipse.jface.dialogs.InputDialog;
import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.jface.window.Window;
import org.eclipse.jface.wizard.WizardDialog;
import org.eclipse.swt.SWT;
import org.eclipse.swt.SWTException;
import org.eclipse.swt.events.MouseEvent;
import org.eclipse.swt.events.MouseListener;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.SelectionListener;
import org.eclipse.swt.graphics.Color;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.MessageBox;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.ui.IWorkbenchPage;
import org.eclipse.ui.IWorkbenchWindow;
import org.eclipse.ui.PartInitException;
import org.eclipse.ui.PlatformUI;
import org.eclipse.ui.forms.IManagedForm;
import org.eclipse.ui.forms.editor.FormEditor;
import org.eclipse.ui.forms.editor.FormPage;
import org.eclipse.ui.forms.widgets.FormToolkit;
import org.eclipse.ui.forms.widgets.ScrolledForm;
import org.eclipse.ui.forms.widgets.Section;
import org.ow2.clif.console.lib.ClifDeployDefinition;
import org.ow2.clif.console.lib.egui.ClifConsolePerspective;
import org.ow2.clif.console.lib.egui.ClifConsolePlugin;
import org.ow2.clif.console.lib.egui.monitor.MonitorView;
import org.ow2.clif.console.lib.egui.wizards.ParamWizard;
import org.ow2.clif.deploy.ClifAppFacade;
import org.ow2.clif.deploy.DeployObservation;
import org.ow2.clif.storage.api.AlarmEvent;
import org.ow2.clif.supervisor.api.BladeState;
import org.ow2.clif.supervisor.api.ClifException;
import org.ow2.clif.supervisor.lib.AlarmObservation;
import org.ow2.clif.supervisor.lib.BladeObservation;
import org.ow2.clif.util.ThrowableHelper;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Observable;
import java.util.Observer;
import java.util.Map.Entry;

/**
 * Test Plan Editor page to execute tests.<br/>
 * Selected blades are executed.<br/>
 * Test commands are:<br/>
 * <ul>
 * <li>Initialize: initialize blades;</li>
 * <li>Start: start test;</li>
 * <li>Suspend/Resume: suspend/resume test;</li>
 * <li>Stop: stop test;</li>
 * <li>Collect: After execution, collect results;</li>
 * <li>Parameters: gets and possibly changes some blades parameters at runtime.</li>
 * </ul>
 * An executed test plan can be executed again after a new initialization.<br/>
 * An executing test plan can't be changed.
 * A copy of the test plan is created and opened.
 *
 * @author Manuel AZEMA
 * @author Joan Chaumont
 * @author Florian Francheteau
 * @author Bruno Dillenseger
 */
public class TestPlanTestPage extends FormPage implements Observer {

	/* used to get Workbench */
	private static IWorkbenchWindow activeWorkbenchWindow = PlatformUI.getWorkbench().getActiveWorkbenchWindow();
	/* Used display */
	private Display display;

	/* Test Id of the last test executed. */
	private String testId;
	/* The deployed test plan. */
	protected Map<String, ClifDeployDefinition> deployedTestPlan;
	/* The test plan displayed */
	private TestPlanObservable testPlan;
	/* Tab folder to display test plan blades sort by class. */
	private TestPlanVisualDisplay tableBlade;
	/* The clifApplication of this test plan */
	private ClifAppFacade clifApp;
	/* bladeState references the current state of each blade */
	private Map<ClifDeployDefinition, BladeState> bladeState = new HashMap<ClifDeployDefinition, BladeState>();

	/* Blades global state. */
	private BladeState globalState;
	/* Show blades global state. */
	private Label bladeStateLabel;

	/* Actions buttons. */
	private Button bSelect, bDeselect, bChange,
			bInitialize, bStart, bSuspendResume,
			bStop, bCollect;

	/* used to manage changes in the MonitorView's Table */
	private MonitorView monitorView;

	/**
	 * Construct a test page to select blades and execute test plan.
	 *
	 * @param editor   parent form editor
	 * @param testPlan the test plan used by this page
	 * @param id	   page id
	 * @param title	page title
	 */
	public TestPlanTestPage(FormEditor editor, TestPlanObservable testPlan, String id, String title) {
		super(editor, id, title);

		this.testPlan = testPlan;
		this.globalState = BladeState.UNDEPLOYED;
	}

	/**
	 * Construct a test page to select blades and execute test plan.
	 *
	 * @param testPlan the test plan used by this page
	 * @param id	   page id
	 * @param title	page title
	 */
	public TestPlanTestPage(TestPlanObservable testPlan, String id, String title) {
		super(id, title);

		this.testPlan = testPlan;
		this.globalState = BladeState.UNDEPLOYED;
	}

	/**
	 * Create page contents.<br/>
	 * Add a blades display tab folder and commands buttons to control test execution.
	 */
	protected void createFormContent(IManagedForm managedForm) {
		FormToolkit toolkit = managedForm.getToolkit();
		ScrolledForm form = managedForm.getForm();
		form.setText("Test Commands");
		form.getBody().setLayout(new GridLayout());

		/* Create "Injectors and probes" section */
		Section section = toolkit.createSection(
			form.getBody(),
			Section.DESCRIPTION | Section.EXPANDED);
		section.setText("Injectors and probes");
		section.setDescription("All injectors and probes in the test plan");
		toolkit.createCompositeSeparator(section);
		section.marginWidth = 10;
		section.marginHeight = 5;
		section.setLayoutData(new GridData(GridData.FILL_BOTH));

		/* Composite client for injectors and probes table */
		Composite sectionClient = toolkit.createComposite(section, SWT.WRAP);
		GridLayout layout = new GridLayout();
		layout.numColumns = 2;
		layout.marginWidth = 2;
		layout.marginHeight = 2;
		sectionClient.setLayout(layout);

		display = section.getDisplay();

		/* Listener used for checkEvent */
		MouseListener checkListener = new MouseListener() {
			public void mouseDoubleClick(MouseEvent e) {
				Shell s = getEditorSite().getShell();
				String id = tableBlade.getSelectedBlade();
				if (id != null) {
					Map<String, Serializable> params = clifApp.getCurrentParameters(id);
					if (params != null) {
						WizardDialog dialog = new WizardDialog(s,
								new ParamWizard(params, clifApp, id));
						dialog.open();
					} else {
						MessageDialog.openWarning(s, "Warning", "There is no parameter for this blade.");
					}
				} else {
					MessageDialog.openInformation(s, "Information", "No blade selected!");
				}
			}

			public void mouseDown(MouseEvent e) {
			}

			public void mouseUp(MouseEvent e) {
				if (!globalState.equals(BladeState.UNDEPLOYED)) {
					if (tableBlade.selectedTestPlan() != null) {
						globalState = clifApp.getGlobalState(tableBlade.selectedTestPlan());
					} else {
						globalState = BladeState.NONE;
					}
					showGlobalState();
					updateCommands();
				}
			}
		};

		/* Injectors and probes tab folder */
		tableBlade = new TestPlanVisualDisplay(sectionClient, testPlan.getTestPlan(), managedForm,
				getEditor().getEditorInput(), SWT.BORDER | SWT.FLAT,
				null, checkListener, true, true);

		/* Edit buttons composite */
		Composite selectButtons = toolkit.createComposite(sectionClient, SWT.WRAP);
		selectButtons.setLayout(new GridLayout());
		selectButtons.setLayoutData(new GridData(GridData.VERTICAL_ALIGN_BEGINNING));

		/* "Select all blades" button */
		bSelect = toolkit.createButton(selectButtons, "Select All", SWT.PUSH);
		bSelect.setToolTipText("Select all probes and injectors to deploy");
		GridData gd = new GridData(GridData.FILL_HORIZONTAL);
		bSelect.setLayoutData(gd);
		bSelect.addSelectionListener(actionSelectAll);

		/* "Deselect all blades" button */
		bDeselect = toolkit.createButton(selectButtons, "Deselect All", SWT.PUSH);
		bDeselect.setToolTipText("Deselect all probes and injectors to deploy");
		gd = new GridData(GridData.FILL_HORIZONTAL);
		bDeselect.setLayoutData(gd);
		bDeselect.addSelectionListener(actionDeselectAll);

		/* Information global blades state */
		Color red = form.getForm().getBody().getDisplay().getSystemColor(SWT.COLOR_RED);
		toolkit.createLabel(selectButtons, "Global state:");
		bladeStateLabel = toolkit.createLabel(selectButtons, globalState.toString());
		bladeStateLabel.setForeground(red);

		section.setClient(sectionClient);
		toolkit.paintBordersFor(sectionClient);

		/* Test commands buttons composite */
		Composite commands = toolkit.createComposite(form.getBody(), SWT.WRAP);
		commands.setLayout(new GridLayout(8, true));

		/* Initialize button */
		bInitialize = toolkit.createButton(commands, "Initialize", SWT.PUSH);
		bInitialize.setToolTipText("Initialize deployed probes and injectors");
		gd = new GridData(GridData.FILL_HORIZONTAL);
		bInitialize.setLayoutData(gd);
		bInitialize.addSelectionListener(actionInitialize);

		/* Start button */
		bStart = toolkit.createButton(commands, "Start", SWT.PUSH);
		bStart.setToolTipText("Start test");
		gd = new GridData(GridData.FILL_HORIZONTAL);
		bStart.setLayoutData(gd);
		bStart.addSelectionListener(actionStart);

		/* Suspend/Resume button */
		bSuspendResume = toolkit.createButton(commands, "Suspend", SWT.PUSH);
		bSuspendResume.setToolTipText("Suspend/Resume test");
		gd = new GridData(GridData.FILL_HORIZONTAL);
		bSuspendResume.setLayoutData(gd);
		bSuspendResume.addSelectionListener(actionSuspendResume);

		/* Stop button */
		bStop = toolkit.createButton(commands, "Stop", SWT.PUSH);
		bStop.setToolTipText("Stop test");
		gd = new GridData(GridData.FILL_HORIZONTAL);
		bStop.setLayoutData(gd);
		bStop.addSelectionListener(actionStop);

		/* Collect button */
		bCollect = toolkit.createButton(commands, "Collect", SWT.PUSH);
		bCollect.setToolTipText("Collect results");
		gd = new GridData(GridData.FILL_HORIZONTAL);
		bCollect.setLayoutData(gd);
		bCollect.addSelectionListener(actionCollect);

		/* "Change parameters" button */
		bChange = toolkit.createButton(commands, "Parameters", SWT.PUSH);
		bChange.setToolTipText("Change parameters for selected blade");
		gd = new GridData(GridData.FILL_HORIZONTAL);
		bChange.setLayoutData(gd);
		bChange.addSelectionListener(actionChange);

		tableBlade.selectAll();
		updateCommands();
		clifApp = ClifConsolePlugin.getDefault().getClifApp();
		clifApp.addObserver(this);
		testPlan.addObserver(this);
		if (globalState == BladeState.UNDEPLOYED)
		{
			try
			{
				deploy();
			}
			catch (ClifException ex)
			{
				globalState = BladeState.UNDEPLOYED;
				displayDetailedError(
					IStatus.ERROR,
					"Deployment Failure",
					"Test plan deployment failed",
					ex);
			}
		}
		else
		{
			setChanged();
		}
	}

	public boolean isIdle()
	{
		return
			globalState.equals(BladeState.ABORTED)
			|| globalState.equals(BladeState.COMPLETED)
			|| globalState.equals(BladeState.STOPPED)
			|| globalState.equals(BladeState.DEPLOYED)
			|| globalState.equals(BladeState.UNDEPLOYED);
	}

	/**
	 * Get table blade viewer
	 *
	 * @return Returns the tableBlade.
	 */
	public TestPlanVisualDisplay getTableBlade() {
		return tableBlade;
	}

	/**
	 * Set the table blade viewer
	 *
	 * @param tableBlade The tableBlade to set.
	 */
	public void setTableBlade(TestPlanVisualDisplay tableBlade) {
		this.tableBlade = tableBlade;
	}

	/**
	 * Stop execution if needed.
	 *
	 * @see org.eclipse.ui.forms.editor.FormPage#dispose()
	 */
	public void dispose() {
		if (clifApp != null) {
			tableBlade.dispose();
			clifApp.deleteObserver(this);
			testPlan.deleteObserver(this);
		}
		super.dispose();
	}


	/**
	 * Update test plan display TabOrder.
	 */
	public void refresh() {
		if (tableBlade != null) {
			tableBlade.removeAllTab();
			tableBlade.createTabByClass();
		}
	}

	/**
	 * If a change has been made restart started clifApp
	 */
	public void setChanged() {
		globalState = BladeState.UNDEPLOYED;
		showGlobalState();
		updateCommands();
	}


	//////////////////////////////////
	// Commands for blade selection //
	//////////////////////////////////

	/**
	 * Select all blades for deploy.
	 */
	private SelectionListener actionSelectAll = new SelectionAdapter() {
		public void widgetSelected(SelectionEvent event) {
			tableBlade.selectAll();
			globalState = clifApp.getGlobalState(tableBlade.selectedTestPlan());
			showGlobalState();
			updateCommands();
		}
	};

	/**
	 * Deselect all blades for deploy.
	 */
	private SelectionListener actionDeselectAll = new SelectionAdapter() {
		public void widgetSelected(SelectionEvent event) {
			globalState = BladeState.NONE;
			tableBlade.deselectAll();
			updateCommands();
		}
	};

	/**
	 * Change common parameters for selected blades.
	 */
	private SelectionListener actionChange = new SelectionAdapter() {
		public void widgetSelected(SelectionEvent event) {

			/* All parameters (identifiers and values) of a single blade*/
			Map<String, Serializable> allParams;

			/* Common parameters (identifiers and different values) of selected blades*/
			Map<String, List<String>> commonParams;

			/* All parameters (identifiers and values) of selected blades*/
			List<Map<String, Serializable>> listParams = new ArrayList<Map<String, Serializable>>();

			/* List of selected blades */
			List<String> checkedBlades;

			String ids = null;
			Shell s = getEditorSite().getShell();

			checkedBlades = tableBlade.getCheckedBlades();

			if (checkedBlades.size() != 0) {
				/* if only one blade is selected */
				if (checkedBlades.size() == 1) {
					ids = checkedBlades.get(0);
					allParams = clifApp.getCurrentParameters(ids);
					if (allParams != null) {
						WizardDialog dialog = new WizardDialog(s,
								new ParamWizard(allParams, clifApp, ids));
						dialog.open();
					} else {
						MessageDialog.openWarning(s, "Warning", "There is no parameter for this blade.");
					}
				}
				/* if several blades are selected */
				else {
					for (String cBlade : checkedBlades) {
						if (ids == null) {
							ids = cBlade;
						} else {
							ids = ids + ", " + cBlade;
						}
						allParams = clifApp.getCurrentParameters(cBlade);
						listParams.add(allParams);
					}
					commonParams = getCommonParams(listParams);
					if (commonParams.size() != 0) {
						WizardDialog dialog = new WizardDialog(s,
								new ParamWizard(commonParams, clifApp, checkedBlades, ids));
						dialog.open();
					} else {
						MessageDialog.openWarning(s, "Warning", "There is no common parameters for these checked blades.");
					}
				}
			} else {
				MessageDialog.openInformation(s, "Information", "No blade selected!");
			}
		}
	};

	/**
	 * Deploys the test plan defined by this editor page.
	 * @throws ClifException
	 */
	private void deploy()
		throws ClifException
	{
		deployedTestPlan = tableBlade.getTestPlan();
		((TestPlanEditor) getEditor()).setEditable(false);
		tableBlade.initBladeStates();
		for (ClifDeployDefinition def : deployedTestPlan.values())
		{
			bladeState.put(def, BladeState.UNDEPLOYED);
		}
		tableBlade.selectAll();
		globalState = BladeState.DEPLOYING;
		showGlobalState();
		clifApp.deploy(deployedTestPlan, ClifConsolePlugin.getDefault().getRegistry(false));
	}

	/**
	 * Initializes deployed blades.
	 */
	private SelectionListener actionInitialize = new SelectionAdapter() {
		public void widgetSelected(SelectionEvent event) {
			if (clifApp != null) {
				try {
					String defValue = getEditor().getTitle().replaceFirst(".ctp", "");
					tableBlade.selectAll();

					InputDialog dialog = new InputDialog(getEditorSite().getShell(), "Test id",
							"Enter test id name :", defValue, null);
					int res = dialog.open();

					if (res == Window.CANCEL) {
						return;
					}

					globalState = BladeState.INITIALIZING;
					showGlobalState();
					testId = (!dialog.getValue().equals("")) ? dialog.getValue() : defValue;

					clifApp.init(testId);
					IWorkspaceRoot root = ResourcesPlugin.getWorkspace().getRoot();
					root.refreshLocal(IProject.DEPTH_INFINITE, null);
				}
				catch (Exception ex) {
					ex.printStackTrace(System.err);
					MessageDialog.openError(
						getEditorSite().getShell(),
						"Can't initialize test plan",
						ThrowableHelper.getMessages(ex));
				}
			}
		}
	};

	/**
	 * Starts test execution.
	 */
	private SelectionListener actionStart = new SelectionAdapter() {
		public void widgetSelected(SelectionEvent event) {
			if (clifApp != null) {
				globalState = BladeState.STARTING;
				showGlobalState();
				clifApp.start(tableBlade.selectedTestPlan());
			}
		}
	};

	/**
	 * Suspends or resumes test execution.
	 */
	private SelectionListener actionSuspendResume = new SelectionAdapter() {
		public void widgetSelected(SelectionEvent event) {
			if (clifApp != null) {
				if (bSuspendResume.getText().equals("Suspend")) {
					globalState = BladeState.SUSPENDING;
					showGlobalState();
					clifApp.suspend(tableBlade.selectedTestPlan());
					bSuspendResume.setText("Resume");
				} else {
					globalState = BladeState.RESUMING;
					showGlobalState();
					clifApp.resume(tableBlade.selectedTestPlan());
					bSuspendResume.setText("Suspend");
				}
			}
		}
	};

	/**
	 * Stops test execution.
	 */
	private SelectionListener actionStop = new SelectionAdapter() {
		public void widgetSelected(SelectionEvent event) {
			globalState = BladeState.STOPPING;
			showGlobalState();
			clifApp.stop(null);
			((TestPlanEditor) getEditor()).setEditable(true);
		}
	};

	/**
	 * When test execution is stopped, collects all results.
	 */
	private SelectionListener actionCollect = new SelectionAdapter() {
		public void widgetSelected(SelectionEvent event) {
			// nothing to collect if clif app is not deployed or test not initialized 
			if (clifApp != null && monitorView != null)
			{
				try
				{
					activeWorkbenchWindow
						.getActivePage()
						.showView("org.eclipse.ui.views.ProgressView");
				}
				catch (Exception ex)
				{
					System.err.println("Warning: could not show the Progress view.");
					ex.printStackTrace(System.err);
				}
				// collect and quickstat report generation (monitoring in Progress view)
				CollectJob job = new CollectJob(clifApp, tableBlade.selectedTestPlan());
				job.schedule();
			}
		}
	};

	/**
	 * Enables/disables test command buttons with respect to
	 * blades states and life cycle.
	 */
	private void updateCommands() {
		((TestPlanEditor) getEditor()).setEditable(false);		
		try {
			if (globalState.equals(BladeState.INCOHERENT)) {
				bInitialize.setEnabled(false);
				bStart.setEnabled(false);
				bSuspendResume.setEnabled(false);
				bStop.setEnabled(true);
				bCollect.setEnabled(false);
				((TestPlanEditor) getEditor()).setEditable(true);
			} else if (globalState.equals(BladeState.UNDEPLOYED)) {
				bSelect.setEnabled(false);
				bDeselect.setEnabled(false);
				bInitialize.setEnabled(false);
				bStart.setEnabled(false);
				bSuspendResume.setEnabled(false);
				bStop.setEnabled(false);
				bCollect.setEnabled(false);
				bChange.setEnabled(false);
				((TestPlanEditor) getEditor()).setEditable(true);
			} else if (globalState.equals(BladeState.DEPLOYED)) {
				bSelect.setEnabled(true);
				bDeselect.setEnabled(true);
				bInitialize.setEnabled(true);
				bStop.setEnabled(true);
				bChange.setEnabled(true);
			} else if (globalState.equals(BladeState.INITIALIZED)) {
				bInitialize.setEnabled(false);
				bStart.setEnabled(true);
				bSuspendResume.setEnabled(false);
				bStop.setEnabled(true);
				bCollect.setEnabled(false);
			} else if (globalState.equals(BladeState.RUNNING)) {
				bSuspendResume.setText("Suspend");
				bInitialize.setEnabled(false);
				bStart.setEnabled(false);
				bSuspendResume.setEnabled(true);
				bStop.setEnabled(true);
				bCollect.setEnabled(false);
			} else if (globalState.equals(BladeState.SUSPENDED)) {
				bSuspendResume.setText("Resume");
				bInitialize.setEnabled(false);
				bStart.setEnabled(false);
				bSuspendResume.setEnabled(true);
				bStop.setEnabled(true);
				bCollect.setEnabled(false);
			} else if (
				globalState.equals(BladeState.STOPPED)
				|| globalState.equals(BladeState.ABORTED)
				|| globalState.equals(BladeState.COMPLETED))
			{
				if (clifApp.getGlobalState(null).equals(BladeState.STOPPED)
					|| clifApp.getGlobalState(null).equals(BladeState.ABORTED)
					|| clifApp.getGlobalState(null).equals(BladeState.COMPLETED))
				{
					bInitialize.setEnabled(true);
				}
				bStart.setEnabled(false);
				bSuspendResume.setEnabled(false);
				bSuspendResume.setText("Suspend");
				bStop.setEnabled(false);
				bCollect.setEnabled(true);
				((TestPlanEditor) getEditor()).setEditable(true);
			} else {
				bInitialize.setEnabled(false);
				bStart.setEnabled(false);
				bSuspendResume.setEnabled(false);
				bStop.setEnabled(false);
				bCollect.setEnabled(false);
				((TestPlanEditor) getEditor()).setEditable(true);
			}
		} catch (SWTException e) {
			e.printStackTrace(System.err);
		}
	}

	/**
	 * Displays the global test plan state information.
	 */
	private void showGlobalState()
	{
		if (! bladeStateLabel.isDisposed())
		{
			try {
				if (globalState.equals(BladeState.INCOHERENT)) {
					bladeStateLabel.setText("None");
				} else {
					bladeStateLabel.setText(globalState.toString());
				}
			} catch (SWTException e) {
				e.printStackTrace(System.err);
			}
		}
	}

	////////////////////////
	// Observer interface //
	////////////////////////

	/**
	 * Receives alarms, deployment information and blade state changes
	 * from the supervisor.
	 */
	public void update(Observable supervisor, Object observation) {		
		if (observation instanceof String) {
			tableBlade.setSelection((String) observation, false);
			return;
		}

		/* If blade state changed */
		else if (observation instanceof BladeObservation) {
			final String id = ((BladeObservation) observation).getBladeId();
			final BladeState state = ((BladeObservation) observation).getState();
			try {
				display.syncExec(new Runnable() {
					public void run() {
						if (! tableBlade.isDisposed())
						{
							tableBlade.setBladeState(id, state);
						}
					}
				});
			} catch (SWTException e) {
				e.printStackTrace(System.err);
				return;
			}

			if (!state.equals(BladeState.DEPLOYED)) {
				try {
					display.syncExec(new Runnable() {
						public void run() {
							if (! tableBlade.isDisposed())
							{
								String[] blades = tableBlade.selectedTestPlan();
								if (blades == null) {
									globalState = BladeState.INCOHERENT;
								} else {
									globalState = clifApp.getGlobalState(tableBlade.selectedTestPlan());
								}
								showGlobalState();
							}
						}
					});
				} catch (SWTException e) {
					e.printStackTrace(System.err);
					return;
				}
			}

			/* Try opening monitor view if needed
					* and add execution tab.*/
			try
			{				
				display.syncExec(new Runnable() {
					public void run()
					{
						if (! tableBlade.isDisposed()
							&& clifApp.getGlobalState(tableBlade.selectedTestPlan()).equals(BladeState.INITIALIZED))
						{
							/* page is used to get MonitorView */
							IWorkbenchPage page = activeWorkbenchWindow.getActivePage();
							if (page != null)
							{
								try
								{
									monitorView = (MonitorView) page.showView(ClifConsolePerspective.ID_MONITOR_VIEW);
								}
								catch (PartInitException e)
								{
									MessageDialog.openError(getEditorSite().getShell(),
										"Can't show monitor view", "Problem with monitor view");
								}
								monitorView.addExecutionTab(deployedTestPlan, testId, clifApp);
							}
						}
					}
				});
			}
			catch (SWTException e)
			{
				e.printStackTrace(System.err);
			}
		}
		else if (observation instanceof DeployObservation)
		{
			final DeployObservation obs = (DeployObservation) observation;
			if (obs.isSuccessful())
			{
				// clifApp.addObserver(this);
				if (obs.getException() == null)
				{
					displayInformationMessage("Information", "Test plan successfully deployed");
				}
				else
				{
					displayDetailedError(
						IStatus.WARNING,
						"Information",
						"Test plan partially deployed, with some deployment errors",
						obs.getException());
				}
				globalState = BladeState.DEPLOYED;
			}
			else
			{
				globalState = BladeState.UNDEPLOYED;
				displayDetailedError(
					IStatus.ERROR,
					"Deployment Failure",
					"Test plan deployment failed",
					obs.getException());
			}
		}
		else if (observation instanceof AlarmObservation)
		{
			final AlarmObservation alarmObs = (AlarmObservation) observation;
			BladeState state = clifApp.getGlobalState(new String[]{alarmObs.getBladeId()});

			if (state.equals(BladeState.DEPLOYED)
				|| state.equals(BladeState.DEPLOYING)
				|| state.equals(BladeState.INITIALIZING)
				|| state.equals(BladeState.ABORTED))
			{
				showPopupAlarm(alarmObs);
			}
		}
		try
		{
			display.syncExec(new Runnable() {
				public void run() {
					updateCommands();
					showGlobalState();
				}
			});
		}
		catch (SWTException e)
		{
			e.printStackTrace(System.err);
		}
	}

	/**
	 * Display the error message in a scrollable dialog
	 *
	 * @param dialogTitle The title of the frame
	 * @param title	   The title of the message
	 * @param e		   The exception to display
	 */
	private void displayDetailedError(final int statusCode, final String title, final String message, final Throwable ex)
	{
		try
		{
			display.syncExec(new Runnable() {
				public void run()
				{
					ex.printStackTrace(System.err);
					MultiStatus status = new MultiStatus(
						ClifConsolePlugin.PLUGIN_ID,
						0,
						ThrowableHelper.getMessages(ex),
						null);
					for (String line : ThrowableHelper.getStackTraceLines(ex))
					{
						status.add(new Status(
							IStatus.ERROR,
							ClifConsolePlugin.PLUGIN_ID,
							line,
							null));
					}
					ErrorDialog.openError(
						getEditorSite().getShell(),
						title,
						message,
						status);
				}
			});
		}
		catch (SWTException e)
		{
			e.printStackTrace(System.err);
		}
	}

	/**
	 * Display information
	 *
	 * @param dialogTitle The title of the frame
	 * @param message	   The message
	 */
	private void displayInformationMessage(final String dialogTitle, final String message) {
		try {
			display.asyncExec(new Runnable() {
				public void run() {
					MessageBox sd = new MessageBox(getEditorSite().getShell());
					sd.setMessage(message);
					sd.setText(dialogTitle);
					sd.open();
				}
			});
		} catch (SWTException e) {
			e.printStackTrace(System.err);
		}
	}

	/**
	 * Opens a pop-up for alarms thrown before tests start
	 * (Other alarms are added in monitor view)
	 */
	private void showPopupAlarm(final AlarmObservation alarmObs)
	{
		try
		{
			if (alarmObs.getAlarm().getFieldValue(AlarmEvent.ARGUMENT_FIELD) instanceof Throwable)
			{
				displayDetailedError(
					IStatus.WARNING,
					"Alarm from " + alarmObs.getBladeId(),
					alarmObs.getMessage(),
					(Throwable)alarmObs.getAlarm().getFieldValue(AlarmEvent.ARGUMENT_FIELD));
			}
			else
			{
				displayInformationMessage(
					"Alarm from " + alarmObs.getBladeId(),
					alarmObs.getMessage());
				
			}
		}
		catch (SWTException ex)
		{
			ex.printStackTrace(System.err);
		}
	}

	/**
	 * Gets common parameters and values from a list of blade parameters
	 *
	 * @param listParams list of blade parameters
	 * @return Parameters with their types and different values for each type
	 */
	private Map<String, List<String>> getCommonParams(List<Map<String, Serializable>> listParams) {

		boolean commonParamOK;
		String key;
		String searchedKey;
		int i;

		Map<String, List<String>> commonParams = new HashMap<String, List<String>>();

		/* Loop scanning parameters of the first blade */
		for (Entry<String, Serializable> currentEntry : listParams.get(0).entrySet()) {
			commonParamOK = true;
			String commonParamKey = null;
			List<String> commonParamsValues = new ArrayList<String>();
			commonParamsValues.clear();
			key = currentEntry.getKey();
			/* Loop scanning other blades */
			for (i = 1; i < listParams.size() && commonParamOK; i++) {
				commonParamOK = false;
				/* Loop scanning parameters of another blade */
				for (Entry<String, Serializable> searchedEntry : listParams.get(i).entrySet()) {
					searchedKey = searchedEntry.getKey();
					if (searchedKey.equals(key)) {
						commonParamsValues.add(searchedEntry.getValue().toString());
						commonParamKey = key;
						commonParamOK = true;
					}
				}
			}
			if (i == listParams.size() && commonParamOK && commonParamKey != null) {
				commonParamsValues.add((String) currentEntry.getValue());
				commonParams.put(commonParamKey, commonParamsValues);
			}
		}
		return commonParams;
	}
}
