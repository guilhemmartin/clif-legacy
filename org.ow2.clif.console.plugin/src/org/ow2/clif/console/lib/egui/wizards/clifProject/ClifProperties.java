/*
 * CLIF is a Load Injection Framework
 * Copyright (C) 2009 France Telecom R&D
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 *
 * CLIF $Name: not supported by cvs2svn $
 *
 * Contact: clif@ow2.org
 */

package org.ow2.clif.console.lib.egui.wizards.clifProject;

import org.eclipse.core.resources.IProject;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.ui.dialogs.PropertyPage;

/**
 * Main properties page of CLIF properties. 
 * Used to choose folders where statistics and reports 
 * data will be stored
 * 
 * @author Florian Francheteau
 */
public class ClifProperties extends PropertyPage{
	
	private InteractionManager interaction;

	/**
	 * Creates and initializes controls through InterationManager
	 */
	@Override
    protected Control createContents(Composite parent) {
		noDefaultAndApplyButton();
		interaction = new InteractionManager(this, (IProject) getElement());
		interaction.createMainContents(parent);
		if (!interaction.getInitialized()){
			interaction.initialize();
		}
		interaction.initializeMainContents();
		return null;
    }
	
	/**
	 * applies modifications when user clicks on OK
	 */
	@Override
	public boolean performOk() {
		interaction.setInitialized(false);
		try {
			interaction.apply();
		}
		catch (Exception e) {
			interaction.catchException(e);
		}
		return super.performOk();
	}
	
	/**
	 * cancels modifications when user clicks on Cancel
	 */
	public boolean performCancel() {
		interaction.setInitialized(false);
		return super.performCancel();
	}
}
