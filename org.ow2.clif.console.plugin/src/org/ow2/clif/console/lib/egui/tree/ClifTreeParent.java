/*
 * CLIF is a Load Injection Framework
 * Copyright (C) 2005, 2011 France Telecom R&D
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 *
 * Contact: clif@ow2.org
 */

package org.ow2.clif.console.lib.egui.tree;

import java.util.ArrayList;

/**
 * Clif tree object whose can have children
 * @author Joan Chaumont
 * @author Bruno Dillenseger
 */
public class ClifTreeParent extends ClifTreeObject {
    
    private ArrayList<ClifTreeObject> children;
    
    /**
     * Simple constructor with object's name
     * @param name
     */
    public ClifTreeParent (String name){
        super(name);
        children = new ArrayList<ClifTreeObject>();
    }
    
    /**
     * Add a child.
     * @param child the child to add to clif tree
     */
    public void addChild (ClifTreeObject child){
        children.add(child);
        child.setParent(this);
    }
    
    /**
     * Remove child.
     * @param child the child to remove from clif tree
     */
    public void removeChild (ClifTreeObject child){
        children.remove(child);
        child.setParent(null);
    }
    
    /**
     * Return array of children.
     * @return ClifTreeObject[] array of children
     */
    public ClifTreeObject[] getChildren (){
        return children.toArray(new ClifTreeObject[children.size()]);
    }
    
    /**
     * Test if this object have at least one child.
     * @return boolean true if this object has children
     */
    public boolean hasChildren(){
        return children.size()>0;
    }
}
