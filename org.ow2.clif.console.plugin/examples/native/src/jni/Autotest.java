/*
* CLIF is a Load Injection Framework
* Copyright (C) 2004, 2005 France Telecom R&D
*
* This library is free software; you can redistribute it and/or
* modify it under the terms of the GNU Lesser General Public
* License as published by the Free Software Foundation; either
* version 2 of the License, or (at your option) any later version.
*
* This library is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
* Lesser General Public License for more details.
*
* You should have received a copy of the GNU Lesser General Public
* License along with this library; if not, write to the Free Software
* Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*
* CLIF $Name: not supported by cvs2svn $
*
* Contact: clif@ow2.org
*/

package jni;


import org.ow2.clif.scenario.multithread.MTScenario;
import org.ow2.clif.scenario.multithread.MTScenarioSession;
import org.ow2.clif.storage.api.ActionEvent;
import org.ow2.clif.supervisor.api.ClifException;
import java.util.StringTokenizer;


/**
 * Example of MTScenario utilization, performing dummy sleep actions implemented by native code.
 * Action response time is computed in micro-seconds (instead of milliseconds)
 * by the native code.
 *
 * @author Bruno Dillenseger
 */
public class Autotest extends MTScenario
{
	static {
		System.loadLibrary("autotest");
	}
	public long arg_sleep_ms = 0;


	/**
	 * @param sessionId the session identifier
	 * @param arg should contain a single integer parameter giving the sleep duration of each action
	 * @return a new Autotest session
	 */
	public MTScenarioSession newSession(int sessionId, String arg)
		throws ClifException
	{
		try
		{
			StringTokenizer parser = new StringTokenizer(arg);
			arg_sleep_ms = Integer.parseInt(parser.nextToken());
		}
		catch (Exception ex)
		{
			throw new ClifException(
				"jni.Autotest requires 4 arguments:\n\t<number of concurrent threads>\n\t<test duration in seconds>\n\t<ramp-up duration in seconds>\n\t<iteration sleep duration in ms>",
				ex);
		}
		return new Session();
	}


	public native ActionEvent native_action(ActionEvent report);


	class Session implements MTScenarioSession
	{
		public ActionEvent action(ActionEvent report)
		{
			report.setDate(System.currentTimeMillis());
			// native_action() sets the report's duration attribute in micro-seconds
			report = Autotest.this.native_action(report);
			report.type = "native sleep action";
			report.comment = arg_sleep_ms + " ms";
			report.successful = true;
			return report;
		}
	}
}
