/*
* CLIF is a Load Injection Framework
* Copyright (C) 2011 France Telecom R&D
*
* This library is free software; you can redistribute it and/or
* modify it under the terms of the GNU Lesser General Public
* License as published by the Free Software Foundation; either
* version 2 of the License, or (at your option) any later version.
*
* This library is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
* Lesser General Public License for more details.
*
* You should have received a copy of the GNU Lesser General Public
* License along with this library; if not, write to the Free Software
* Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*
* Contact: clif@ow2.org
*/

package org.ow2.clif.scenario.util;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;
import java.util.StringTokenizer;
import org.objectweb.fractal.api.Component;
import org.objectweb.fractal.api.Interface;
import org.objectweb.fractal.api.NoSuchInterfaceException;
import org.objectweb.fractal.api.control.BindingController;
import org.objectweb.fractal.api.control.ContentController;
import org.objectweb.fractal.api.control.IllegalBindingException;
import org.objectweb.fractal.api.control.IllegalLifeCycleException;
import org.objectweb.fractal.api.control.NameController;
import org.objectweb.fractal.util.Fractal;
import org.ow2.clif.datacollector.api.DataCollectorWrite;
import org.ow2.clif.deploy.ClifRegistry;
import org.ow2.clif.server.api.BladeControl;
import org.ow2.clif.server.api.BladeInsertResponse;
import org.ow2.clif.server.api.Synchronizer;
import org.ow2.clif.server.lib.SynchronizerImpl;
import org.ow2.clif.storage.api.AlarmEvent;
import org.ow2.clif.supervisor.api.ClifException;

/**
 * Implementation of a centralized synchronization BladeInsert component to
 * provide synchronization support among distributed scenarios.
 * Each instance provides synchronization service for a given "domain", whose name
 * must be given as first blade argument. This domain name is used to bind this insert
 * in the CLIF registry, prefixed by the value of attribute Synchronizer.SYNCHRONIZER.
 * The execution duration in seconds must be given as second blade argument.
 *
 * @author Bruno Dillenseger
 */
public class SynchroBladeInsert
	extends SynchronizerImpl
	implements BindingController, BladeControl, Runnable
{
	// name of synchronization domain
	private String domain;
	// map of predefined rendez-vous indexed by their lock name
	private Map<String,Long> predefinedRendezVous = new HashMap<String,Long>();
	// this blade identifier
	private String bladeId;
	// reference to the DataCollector Fractal interface
	private DataCollectorWrite dataCollectorWrite;
	// reference to the BladeInsertResponse Fractal interface
	private BladeInsertResponse bladeInsertResponse;
	// pseudo-start time (discarding suspension periods), or elapsed time if suspended 
	private long baseTime_ms = 0;
	// actual, global execution time before completion (suspension periods dismissed)
	private long duration_ms;
	// the blade is stopped (in the sense of blade lifecycle)
	private volatile boolean stopped;
	// the blade is suspended (in the sense of blade lifecycle)
	private volatile boolean suspended;
	// manage the execution duration
	private Thread timeOut;


	////////////////////////////
	// BladeControl interface //
	////////////////////////////


	public void changeParameter(String parameter, Serializable value)
	{
	}

	public Map<String,Serializable> getCurrentParameters()
	{
		return null;
	}

	public String getId()
	{
		return bladeId;
	}

	/**
	 * Binds this Insert component in the CLIF/Fractal registry
	 * using the provided domain name. Any previous name binding is
	 * overridden.
	 * @param argument the synchronization domain name.
	 */
	public void setArgument(String argument) throws ClifException
	{
		// arguments parsing
		StringTokenizer parser = new StringTokenizer(argument, " ");
		try
		{
			domain = parser.nextToken();
			duration_ms = 1000 * Integer.parseInt(parser.nextToken());
		}
		catch (Exception ex)
		{
			throw new ClifException("2 arguments required: domain duration_in_seconds");
		}
		predefinedRendezVous.clear();
		try
		{
			while (parser.hasMoreTokens())
			{
				String[] prv = parser.nextToken().split("=");
				predefinedRendezVous.put(prv[0], Long.parseLong(prv[1]));
			}
		}
		catch (Exception ex)
		{
			throw new ClifException("syntax error in rendez-vous optional arguments (lock=n expected).", ex);
		}
		// register the component implemented by this object in the CLIF/Fractal registry
		try
		{
			ClifRegistry registry = new ClifRegistry(false);
			ContentController contentCtrl = Fractal.getContentController(
				Fractal.getSuperController(
					((Interface)bladeInsertResponse).getFcItfOwner()).getFcSuperComponents()[0]);
			Component[] comps = contentCtrl.getFcSubComponents();
			for (Component c : comps)
			{
				NameController nameCtrl = Fractal.getNameController(c);
				if (nameCtrl.getFcName().equals("insert"))
				{
					registry.rebind(Synchronizer.SYNCHRONIZER + "/" + domain, c);
				}
			}
		}
		catch (Exception e)
		{
			throw new ClifException("Can't register synchronization domain " + domain + " in CLIF registry.", e);
		}
	}


	public void setId(String id)
	{
		bladeId = id;
	}


	public void init(Serializable testId)
	{
		stopped = false;
		suspended = false;
		timeOut = new Thread(this);
		reset();
		for (Map.Entry<String,Long> prv : predefinedRendezVous.entrySet())
		{
			setRendezVous(prv.getKey(), prv.getValue());
		}
	}


	public void start()
	{
		stopped = false;
		baseTime_ms = System.currentTimeMillis();
		timeOut.start();
	}


	public void stop()
	{
		stopped = true;
		timeOut.interrupt();
	}


	public void suspend()
	{
		suspended = true;
		baseTime_ms = System.currentTimeMillis() - baseTime_ms;
		timeOut.interrupt();
	}


	public void resume()
	{
		baseTime_ms = System.currentTimeMillis() - baseTime_ms;
		suspended = false;
		timeOut.interrupt();
	}


	public void join()
	{
		// no implementation because join() is caught/implemented by the blade insert adapter
		throw new Error("A blade insert's join() method should never be called (call its adapter's join() method instead)");
	}


	/////////////////////////////////
	// BindingController interface //
	/////////////////////////////////


	public void bindFc(String clientItfName, Object serverItf)
		throws
			NoSuchInterfaceException,
			IllegalBindingException,
			IllegalLifeCycleException
	{
		if (clientItfName.equals(DataCollectorWrite.DATA_COLLECTOR_WRITE))
		{
			dataCollectorWrite = (DataCollectorWrite)serverItf;
		}
		else if (clientItfName.equals(BladeInsertResponse.BLADE_INSERT_RESPONSE))
		{
			bladeInsertResponse = (BladeInsertResponse)serverItf;
		}
		else
		{
			throw new NoSuchInterfaceException(
				"No such interface: " + clientItfName);
		}
	}

	public String[] listFc()
	{
		return new String[] {
			DataCollectorWrite.DATA_COLLECTOR_WRITE,
			BladeInsertResponse.BLADE_INSERT_RESPONSE };
	}

	public Object lookupFc(String itfName)
		throws
			NoSuchInterfaceException
	{
		if (itfName.equals(DataCollectorWrite.DATA_COLLECTOR_WRITE))
		{
			return dataCollectorWrite;
		}
		else if (itfName.equals(BladeInsertResponse.BLADE_INSERT_RESPONSE))
		{
			return bladeInsertResponse;
		}
		else
		{
			throw new NoSuchInterfaceException("Component " + this + "does not have " + itfName + " client interface.");
		}
	}

	public void unbindFc(String clientItfName)
		throws
			NoSuchInterfaceException,
			IllegalBindingException,
			IllegalLifeCycleException
	{
		if (clientItfName.equals(DataCollectorWrite.DATA_COLLECTOR_WRITE))
		{
			dataCollectorWrite = null;
		}
		else if (clientItfName.equals(BladeInsertResponse.BLADE_INSERT_RESPONSE))
		{
			bladeInsertResponse = null;
		}
		else
		{
			throw new NoSuchInterfaceException(
				"No such interface: " + clientItfName);
		}
	}


	////////////////////////
	// Runnable interface //
	////////////////////////


	/**
	 * Timer to manage the execution duration.
	 * The duration excludes the blade suspension periods.
	 */
	public void run()
	{
		while (!stopped && (suspended || duration_ms > System.currentTimeMillis() - baseTime_ms))
		{
			try
			{
				synchronized(timeOut)
				{
					while (suspended)
					{
						timeOut.wait();
					}
					if (! stopped)
					{
						long sleep = duration_ms - System.currentTimeMillis() + baseTime_ms;
						if (sleep > 0)
						{
							Thread.sleep(sleep);
						}
					}
				}
			}
			catch (InterruptedException ex)
			{
			}
		}
		if (! stopped)
		{
			bladeInsertResponse.completed();
		}
	}


	//////////////////////////////////////////
	// refinement of class SynchronizerImpl //
	//////////////////////////////////////////


	/**
	 * Raise an alarm on each first lock notify 
	 */
	@Override
	protected void newLock(String lockName)
	{
		bladeInsertResponse.alarm(new AlarmEvent(
			System.currentTimeMillis(),
			AlarmEvent.INFO,
			"first notification of lock " + lockName));
	}
}
