/*
* CLIF is a Load Injection Framework
* Copyright (C) 2004, 2012 France Telecom
*
* This library is free software; you can redistribute it and/or
* modify it under the terms of the GNU Lesser General Public
* License as published by the Free Software Foundation; either
* version 2 of the License, or (at your option) any later version.
*
* This library is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
* Lesser General Public License for more details.
*
* You should have received a copy of the GNU Lesser General Public
* License along with this library; if not, write to the Free Software
* Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*
* Contact: clif@ow2.org
*/
package org.ow2.clif.scenario.isac.util;

import org.ow2.clif.supervisor.api.ClifException;

/**
 * Utility class for transmitting a reference to a boolean.
 * Provides a utility method to set the boolean value
 * from a string representation.
 * 
 * @author JC Meillaud
 * @author A Peyrard
 * @author Bruno Dillenseger
 */
public class BooleanHolder {
	boolean booleanValue ;
	
	public BooleanHolder() {
		// set a default value
		this.booleanValue = true; 
	}
	
	public BooleanHolder(boolean b) {
		this.booleanValue = b ;
	}

	@Override
	public String toString()
	{
		return String.valueOf(getBooleanValue());
	}

	////////////////////////
	// getter and setters //
	////////////////////////

	/**
	 * @return Returns the booleanValue.
	 */
	public boolean getBooleanValue() {
		return booleanValue;
	}

	/**
	 * @param booleanValue The booleanValue to set.
	 */
	public void setBooleanValue(boolean booleanValue) {
		this.booleanValue = booleanValue;
	}

	/**
	 * Sets this boolean value according to a string
	 * representation of a boolean. 
	 * @param value the case-insensitive string
	 * representation of a boolean.
	 * Valid strings for true are: true, yes, on, 1
	 * Valid strings for false are: false, no, off, 0
	 * @throws ClifException if the provided string 
	 */
	public void setBooleanValue(String value)
	throws ClifException
	{
		if (
			value.equalsIgnoreCase("true")
			|| value.equalsIgnoreCase("yes")
			|| value.equalsIgnoreCase("on")
			|| value.equalsIgnoreCase("1"))
		{
			setBooleanValue(true);
		}
		else if (
			value.equalsIgnoreCase("false")
			|| value.equalsIgnoreCase("no")
			|| value.equalsIgnoreCase("off")
			|| value.equalsIgnoreCase("0"))
		{
			setBooleanValue(false);
		}
		else
		{
			throw new ClifException("Invalid boolean value: " + value);
		}
	}
}
