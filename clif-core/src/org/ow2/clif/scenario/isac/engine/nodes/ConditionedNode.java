/*
 * CLIF is a Load Injection Framework
 * Copyright (C) 2005 France Telecom R&D
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 *
 * CLIF
 *
 * Contact: clif@ow2.org
 */
package org.ow2.clif.scenario.isac.engine.nodes;

import java.util.ArrayList;
import java.util.ListIterator;

import org.jdom.Attribute;
import org.jdom.Element;

/**
 * @author Emmanuel Varoquaux
 */
/*
 * This is the parent class for IfElement, WhileElement and PreemptiveElement.
 */
abstract class ConditionedNode implements InstructionNode {
	public ConditionNode	condition;

	protected ArrayList<InstructionNode> analyseConditionedBlock(Element element)
			throws NodeException {
		ListIterator i;
		Element e;
		String name;

		i = element.getAttributes().listIterator();
		if (i.hasNext())
			throw new UnexpectedAttributeException(((Attribute)i.next())
					.getName());

		i = element.getChildren().listIterator();

		if (!i.hasNext())
			throw new MissingElementException("condition");
		e = (Element)i.next();
		name = e.getName();
		if (!name.equals("condition"))
			throw new BadElementException(name, "condition");
		condition = new ConditionNode(e);
		return Util.analyseBlock(i);
	}
}
