/*
 * CLIF is a Load Injection Framework Copyright (C) 2005 France Telecom R&D
 * 
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2 of the License, or (at your option) any
 * later version.
 * 
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with this library; if not, write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 * 
 * CLIF
 * 
 * Contact: clif@ow2.org
 */
package org.ow2.clif.scenario.isac.engine.nodes;

import java.util.ArrayList;
import java.util.Map;
import java.util.Stack;

import org.jdom.Element;
import org.ow2.clif.scenario.isac.engine.PlugIn;
import org.ow2.clif.scenario.isac.engine.instructions.GotoInstruction;
import org.ow2.clif.scenario.isac.engine.instructions.Instruction;
import org.ow2.clif.scenario.isac.engine.instructions.TestInstruction;

/**
 * @author Emmanuel Varoquaux
 */
public class WhileNode extends ConditionedNode {
	public ArrayList<InstructionNode>	instructions;

	public WhileNode(Element element) throws NodeException {
		instructions = analyseConditionedBlock(element);
	}
	
	@Override
	public StringBuilder indentedToString(int indent, StringBuilder str) {
		Util.stringIndent(indent, str);
		System.out.print("while: ");
		condition.toString();
		for (int i = 0, n = instructions.size(); i < n; i++)
		{
			instructions.get(i).indentedToString(indent + 1, str);
		}
		return str;
	}

	@Override
	public void compile(Map<String,PlugIn> plugIns, ArrayList<Instruction> code, Stack<TestInstruction> conditions) throws Exception {
		int l;
		TestInstruction t;

		l = code.size();
		condition.checkConditions(code, conditions);
		code.add(t = new TestInstruction(condition.use, condition.getMethodNumber(plugIns),
				condition.split(condition.params), 0));
		for (int i = 0, n = instructions.size(); i < n; i++) {
			instructions.get(i).compile(plugIns, code, conditions);
		}
		code.add(new GotoInstruction(l));
		t.labelFalse = code.size();
	}
}
