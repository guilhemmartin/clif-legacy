/*
 * CLIF is a Load Injection Framework
 * Copyright (C) 2005, 2008, 2009-2012 France Telecom R&D
 * Copyright (C) 2015 Orange
 * 
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2 of the License, or (at your option) any
 * later version.
 * 
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with this library; if not, write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 * 
 * Contact: clif@ow2.org
 */
package org.ow2.clif.scenario.isac.engine;

import java.io.FileNotFoundException;
import java.io.Serializable;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.ListIterator;
import java.util.Map;
import java.util.NoSuchElementException;
import java.util.StringTokenizer;
import org.objectweb.fractal.api.NoSuchInterfaceException;
import org.objectweb.fractal.api.control.BindingController;
import org.objectweb.fractal.api.control.IllegalBindingException;
import org.objectweb.fractal.api.control.IllegalLifeCycleException;
import org.ow2.clif.datacollector.api.DataCollectorWrite;
import org.ow2.clif.datacollector.lib.GenericFilter;
import org.ow2.clif.scenario.isac.util.BooleanHolder;
import org.ow2.clif.server.api.BladeControl;
import org.ow2.clif.server.api.BladeInsertResponse;
import org.ow2.clif.server.util.EventStorageState;
import org.ow2.clif.storage.api.ActionEvent;
import org.ow2.clif.storage.api.AlarmEvent;
import org.ow2.clif.supervisor.api.ClifException;

/**
 * This is the main class of the Isac extended engine.
 * Each instance creates a pool of threads to execute jobs
 * (sort of scenario slices), a scheduler to dispatch jobs
 * and apply load profiles, a clock to handle real time
 * (despite possible suspend periods), a timer for periodically
 * updating the load profiles, and an engine supervisor to manage
 * the end of scenario execution.
 *
 * @author Emmanuel Varoquaux
 * @author Bruno Dillenseger
 */
public class IsacExtendedEngine
	implements BladeControl, BindingController
{
	/**
	 * This is a class for the thread group to which the engine's threads belong.
	 * It overrides the uncaughtException method of ThreadGroup.
	 */
	private class InternalThreadGroup extends ThreadGroup {
		public InternalThreadGroup() {
			super("Engine thread group");
		}

		/**
		 * Aborts the activity of the engine in case of uncaught exception in the thread group.
		 * Adds an AlarmEvent in the data collector of the engine.
		 */
		public void uncaughtException(Thread t, Throwable e) {
			String message = "Exception in thread " + t.getName();
			System.err.println("Fatal error: " + message);
			e.printStackTrace(System.err);
			for (Group i : groupMap.values())
			{
				i.abort();
			}
			bladeInsertResponse.alarm(new AlarmEvent(
				System.currentTimeMillis(),
				AlarmEvent.FATAL,
				new Error(message, e)));
		}
	}

	/* Interfaces names */
	private static final String[]	interfaceNames				= new String[] {
			DataCollectorWrite.DATA_COLLECTOR_WRITE,
			BladeInsertResponse.BLADE_INSERT_RESPONSE			};

	/* States of the engine */
	private static final int		UNINITIALIZED				= 0;
	private static final int		INITIALIZED					= 1;
	private static final int		RUNNING						= 2;
	private static final int		SUSPENDED					= 3;

	/* Properties */
	/* NOTE ! default values are also to maintain in dist/clif.props and dist/clif.props.template */
	private static final String		THREADS_PROPERTY			= "threads";
	private static final int		THREADS_DEFAULT				= 100;
	private volatile int			threads;
	private static final String		GROUP_PERIOD_PROPERTY		= "groupperiod";
	private static final long		GROUP_PERIOD_DEFAULT		= 100;
	private long					groupPeriod;
	private static final String		SCHEDULER_PERIOD_PROPERTY	= "schedulerperiod";
	private static final long		SCHEDULER_PERIOD_DEFAULT	= 100;
	private long					schedulerPeriod;
	private static final String		JOB_DELAY_PROPERTY			= "jobdelay";
	private static final long		JOB_DELAY_DEFAULT			= -1;
	private long					jobDelay;

	/* Bound interfaces */
	private DataCollectorWrite		dataCollectorWrite;
	private BladeInsertResponse		bladeInsertResponse;

	/* Fields */
	private String					id;
	private Scenario				scenario;
	private final BooleanHolder		storeLifeCycleEvents		= new BooleanHolder(true);
	private final BooleanHolder		storeAlarmEvents			= new BooleanHolder(true);
	private String					storeActionEvents			= "true";
	private final Map<String,Object> eventStorageStatesMap = new HashMap<String,Object>();

	/* State of the engine */
	private int						state;

	/* Runtime objects */
	private Clock					clock;
	private Scheduler				scheduler;
	private InternalThreadGroup		threadGroup;
	private Map<String, Group>		groupMap;
	private List<ExecutionThread>	executionThreads;
	private volatile int activeThreads; // number of execution threads whose run() method has not completed yet
	private Timer					timer;
	private Supervisor				supervisor;
	private AsyncAlarmForwarder	asyncBladeInsertResponse;

	/** The following properties can be set in clif.props :
	 * - clif.isac.threads
	 * - clif.isac.groupperiod
	 * - clif.isac.schedulerperiod
	 * - clif.isac.jobdelay
	 * - clif.store.action
	 * - clif.store.lifecycle
	 * - clif.store.alarms
	 * Otherwise, use default values.
	 */
	public IsacExtendedEngine()
	{
		state = UNINITIALIZED;
		try {
			setProperty(THREADS_PROPERTY, System.getProperty("clif.isac." + THREADS_PROPERTY));
		}
		catch (Exception e) {
			threads = THREADS_DEFAULT;
		}
		try {
			setProperty(GROUP_PERIOD_PROPERTY, System.getProperty("clif.isac." + GROUP_PERIOD_PROPERTY));
		}
		catch (Exception e) {
			groupPeriod = GROUP_PERIOD_DEFAULT;
		}
		try {
			setProperty(SCHEDULER_PERIOD_PROPERTY, System.getProperty("clif.isac." + SCHEDULER_PERIOD_PROPERTY));
		}
		catch (Exception e) {
			schedulerPeriod = SCHEDULER_PERIOD_DEFAULT;
		}
		try {
			setProperty(JOB_DELAY_PROPERTY, System.getProperty("clif.isac." + JOB_DELAY_PROPERTY));
		}
		catch (Exception e) {
			jobDelay = JOB_DELAY_DEFAULT;
		}
		try
		{
			storeActionEvents = System.getProperty("clif.store.action", "true");
		}
		catch (Exception ex)
		{
			System.err.println("Warning: ignored incorrect setting for clif.store.action property: " + ex);
		}
		try
		{
			storeLifeCycleEvents.setBooleanValue(System.getProperty("clif.store.lifecycle", "true"));
		}
		catch (ClifException ex)
		{
			System.err.println("Warning: ignored incorrect setting for clif.store.lifecycle property: " + ex);
		}
		try
		{
			storeAlarmEvents.setBooleanValue(System.getProperty("clif.store.alarm", "true"));
		}
		catch (ClifException ex)
		{
			System.err.println("Warning: ignored incorrect setting for clif.store.alarm property: " + ex);
		}
		eventStorageStatesMap.put("store-lifecycle-events", storeLifeCycleEvents);
		eventStorageStatesMap.put("store-alarm-events", storeAlarmEvents);
		eventStorageStatesMap.put("store-" + ActionEvent.EVENT_TYPE_LABEL + "-events", storeActionEvents);
	}

	/*
	 * Various private methods
	 */

	private void doBindFc(String clientItfName, Object serverItf)
		throws NoSuchInterfaceException, IllegalBindingException
	{
		if (clientItfName.equals(DataCollectorWrite.DATA_COLLECTOR_WRITE)) {
			try {
				dataCollectorWrite = (DataCollectorWrite)serverItf;
			}
			catch (ClassCastException e) {
				throw new IllegalBindingException(DataCollectorWrite.class
						.getName()
						+ " expected");
			}
		}
		else if (clientItfName
				.equals(BladeInsertResponse.BLADE_INSERT_RESPONSE)) {
			try {
				bladeInsertResponse = (BladeInsertResponse)serverItf;
			}
			catch (ClassCastException e) {
				throw new IllegalBindingException(BladeInsertResponse.class
						.getName()
						+ " expected");
			}
		}
		else {
			throw new NoSuchInterfaceException("No such interface: "
					+ clientItfName);
		}
	}

	private void setProperty(String name, String value) throws ClifException
	{
		if (EventStorageState.setEventStorageState(eventStorageStatesMap, name, value))
		{}
		else if (name.equals(THREADS_PROPERTY))
		{
			int i;
			try {
				i = Integer.parseInt(value);
				if (i <= 0)
					throw new NumberFormatException();
			}
			catch (NumberFormatException e) {
				throw new ClifException("Invalid value for parameter "
						+ THREADS_PROPERTY + ": " + value
						+ ": positive integer expected.");
			}
			threads = i;
		}
		else if (name.equals(GROUP_PERIOD_PROPERTY))
		{
			long l;
			try {
				l = Long.parseLong(value);
				if (l <= 0)
					throw new NumberFormatException();
			}
			catch (NumberFormatException e) {
				throw new ClifException("Invalid value for parameter "
						+ GROUP_PERIOD_PROPERTY + ": " + value
						+ ": positive long integer expected.");
			}
			groupPeriod = l;
		}
		else if (name.equals(SCHEDULER_PERIOD_PROPERTY))
		{
			long l;
			try {
				l = Long.parseLong(value);
				if (l <= 0)
					throw new NumberFormatException();
			}
			catch (NumberFormatException e) {
				throw new ClifException("Invalid value for parameter "
						+ SCHEDULER_PERIOD_PROPERTY + ": " + value
						+ ": positive long integer expected.");
			}
			schedulerPeriod = l;
			return;
		}
		else if (name.equals(JOB_DELAY_PROPERTY)) {
			long l;
			try {
				l = Long.parseLong(value);
			}
			catch (NumberFormatException e) {
				throw new ClifException("Invalid value for parameter "
						+ JOB_DELAY_PROPERTY + ": " + value
						+ ": positive long integer expected.");
			}
			jobDelay = l;
		}
		else
		{
			throw new ClifException("Invalid parameter: " + name);
		}
	}

	/* Releases runtime objects. */
	private void cleanUp() {
		clock = null;
		threadGroup = null;
		timer = null;
		scheduler = null;
		groupMap = null;
		executionThreads = null;
		supervisor = null;
		state = UNINITIALIZED;
		System.gc();
	}

	/**
	 * Called by the timer to update the number of vUsers.
	 */
	public void updatePopulation()
	{
		for (Group gr : groupMap.values())
		{
			gr.balance();
		}
	}

	synchronized void threadIsBorn()
	{
		++activeThreads;
	}

	synchronized void threadIsDead()
	{
		if (--activeThreads == 0)
		{
			notifyAll();
		}
	}

	void threadIsIdle() throws InterruptedException
	{
		Thread.sleep(schedulerPeriod);
	}


	/**
	 * Safe engine stop, either because of scenario completion or abortion,
	 * or because of manual/external stop invocation. 
	 * @param internalCall true if this method is called by the engine itself
	 * (scenario completion or abortion), or false if it is called by external
	 * control (manual stop).
	 */
	synchronized void doStop(boolean internalCall)
	{
		/* Interrupt threads managed by the engine: execution threads, timer and alarm forwarder */
		threadGroup.interrupt();

		/* wait for actual threads termination */
		try
		{
			timer.join();
		}
		catch (InterruptedException ex)
		{
			System.err.println("Unexpected: thread interrupted while waiting for the ISAC engine timer to complete.");
		}
		try
		{
			asyncBladeInsertResponse.join();
		}
		catch (InterruptedException ex)
		{
			System.err.println("Unexpected: thread interrupted while waiting for the ISAC engine alarm forwarder to complete.");
		}
		while (activeThreads > 0)
		{
			try
			{
				wait();
			}
			catch (InterruptedException ex)
			{
			}
		}
		/* Stop clock */
		clock.stop();
		/* Close session objects */
		scheduler.destroy();
		/* Give engine termination status if internal call */
		if (internalCall)
		{
			boolean status = true;
			for (Group i : groupMap.values())
			{
				if (! i.getStatus())
				{
					status = false;
				}
			}
			if (status)
			{
				bladeInsertResponse.completed();
			}
			else
			{
				bladeInsertResponse.aborted();
			}
		}
		/* release threads waiting for the engine termination (join() invocations) */
		notifyAll();
		/* Release unused references */
		cleanUp();
	}

	////////////////////////////
	// interface BladeControl //
	////////////////////////////

	/**
	 * @param arg unused
	 */
	public synchronized void init(Serializable arg) throws ClifException {
		if (state != UNINITIALIZED) {
			return;
		}

		if (scenario == null)
			throw new ClifException("Invalid scenario");

		try {
			dataCollectorWrite.setFilter(
				new GenericFilter(
					storeActionEvents,
					storeAlarmEvents.getBooleanValue(),
					storeLifeCycleEvents.getBooleanValue(),
					false));
			/* Initialize runtime objects */
			clock = new Clock();
			threadGroup = new InternalThreadGroup();
			threadGroup.setDaemon(true);
			asyncBladeInsertResponse = new AsyncAlarmForwarder(threadGroup, bladeInsertResponse);
			timer = new Timer(threadGroup, clock);
			scheduler = new Scheduler(
				clock,
				asyncBladeInsertResponse,
				scenario.getSpecimenMap(),
				jobDelay);
			String[] behaviors = scenario.listBehaviors();
			supervisor = new Supervisor(this, behaviors.length);
			groupMap = new HashMap<String,Group>(behaviors.length);
			for (int i = 0; i < behaviors.length; i++)
			{
				Group group = scenario.getGroup(
					behaviors[i],
					supervisor,
					clock,
					scheduler);
				groupMap.put(behaviors[i], group);
			}
			timer.add(this, "updatePopulation", groupPeriod);
			executionThreads = new LinkedList<ExecutionThread>();
			for (int i = 0; i < threads; i++)
			{
				executionThreads.add(
					new ExecutionThread(
						threadGroup,
						clock,
						scheduler,
						dataCollectorWrite,
						asyncBladeInsertResponse,
						this));
			}
		}
		catch (Exception e) {
			cleanUp();
			throw new ClifException(e);
		}
		state = INITIALIZED;
	}

	public synchronized void start() {
		if (state != INITIALIZED) {
			return;
		}

		clock.start();
		for (ListIterator<ExecutionThread> i = executionThreads.listIterator(); i.hasNext();)
			i.next().start();
		timer.start();

		state = RUNNING;
	}

	public synchronized void stop() {
		if (state != RUNNING && state != SUSPENDED) {
			return;
		}

		doStop(false);
	}

	public synchronized void suspend() {
		if (state != RUNNING) {
			return;
		}

		clock.stop();

		state = SUSPENDED;
	}

	public synchronized void resume() {
		if (state != SUSPENDED) {
			return;
		}

		clock.start();

		state = RUNNING;
	}

	public synchronized void join() {
		if (state != RUNNING && state != SUSPENDED) {
			return;
		}

		while (activeThreads > 0)
		{
			try
			{
				wait();
			}
			catch (InterruptedException e)
			{
				Thread.currentThread().interrupt();
			}
		}
	}

	/**
	 * @param argument The argument is expected to have the following format:
	 *   filename [param=value]*
	 * <br/>where:
	 * </ul>
	 *   <li>filename is the name of the scenario filename (XML file)</li>
	 *   <li>param is one of "threads", "groupperiod", "schedulerperiod", "jobdelay",
	 *   "store-action-events", "store-alarm-events" or "store-lifecycle-events"</li>
	 *   <li>value is a positive integer or a boolean string (on/yes/true or off/no/false)
	 *   depending on the parameter</li>
	 * </ul>
	 */
	public synchronized void setArgument(String argument) throws ClifException {
		if (state != UNINITIALIZED) {
			return;
		}

		if (argument == null || argument.trim().length() == 0)
		{
			throw new ClifException("Invalid test plan: argument is missing for injector " + getId());
		}
		StringTokenizer st = new StringTokenizer(argument);
		String scenarioFileName = null;
		try
		{
			scenarioFileName = st.nextToken();
			scenario = new Scenario(scenarioFileName);
		}
		catch (NoSuchElementException e) {
			throw new ClifException("Scenario file name is missing for injector " + getId());
		}
		catch (FileNotFoundException e)
		{
			throw new ClifException("Scenario file " + scenarioFileName + " not found for injector " + getId());
		}
		catch (Throwable e)
		{
			throw new ClifException("Error in scenario file " + scenarioFileName + " for injector " + getId(), e);
		}

		String str;
		String[] tmp;
		while (st.hasMoreTokens())
		{
			str = st.nextToken();
			tmp = str.split("=");
			if (tmp.length != 2)
			{
				throw new ClifException("Syntax error");
			}
			setProperty(tmp[0].trim(), tmp[1].trim());
		}
	}

	public synchronized void setId(String id) {
		this.id = id;
	}

	public synchronized String getId() {
		return id;
	}

	/* The parameter argument is expected to have one of the following values,
	 * each of them implying a specific format for the value argument :
	 * - "population". This sets the population of a group to a constant value.
	 *   This has no effect if the group has finished its load profile.
	 *   The value argument must be a String, with the following syntax :
	 *   "behaviorId=population", where behaviorId represents the id of the behavior which
	 *   population is to set, and population is a positive integer.
	 * - one of the property names, as in setArgument. The value argument must be a String.
	 *   See setArgument.
	 */
	public synchronized void changeParameter(String parameter,
			Serializable value) throws ClifException {
		if (!(value instanceof String))
			throw new ClifException("Invalid parameter value: must be a String");

		if (EventStorageState.setEventStorageState(eventStorageStatesMap,
				parameter, value)) {
			dataCollectorWrite.setFilter(new GenericFilter(storeActionEvents,
					storeAlarmEvents.getBooleanValue(),
					storeLifeCycleEvents.getBooleanValue(), false));
			return;
		}
		if (parameter.equals("population")) {
			if (state == UNINITIALIZED) {
				return;
			}
			String[] behaviorLoads = ((String)value).split(";");
			for (int i = 0; i < behaviorLoads.length; i++) {
				String[] tmp = behaviorLoads[i].split("=");
				if (tmp.length < 1)
					throw new ClifException("Syntax error in parameter value");
				tmp[0] = tmp[0].trim();
				if (!groupMap.containsKey(tmp[0]))
					throw new ClifException(tmp[0] + ": no such behavior");
				if (tmp.length == 1)
					continue;
				if (tmp.length == 2) {
					tmp[1] = tmp[1].trim();
					if (!tmp[1].equals("")) {
						try {
							groupMap.get(tmp[0]).setLoad(Integer.parseInt(tmp[1]));
						}
						catch (NumberFormatException e) {
							throw new ClifException(
									"Syntax error in parameter value");
						}
						catch (Exception e) {
							throw new ClifException(e);
						}
					}
					else
						continue;
				}
				else
					throw new ClifException("Syntax error in parameter value");
			}
			return;
		}

		setProperty(parameter, (String)value);

		if (state != RUNNING && state != SUSPENDED)
			return;

		if (parameter.equals(THREADS_PROPERTY)) {
			int diff = threads - executionThreads.size();
			if (diff < 0) {
				while (diff++ < 0) {
					ExecutionThread et = executionThreads.remove(0);
					et.interrupt();
					/* NOTE !!! It could seem logical to wait for the
					 * interrupted threads to finish, but actually it may take
					 * a significant time. The console expects this method to
					 * return quickly. So we do not ensure that the threads DO
					 * finish. */
				}
			}
			else if (diff > 0) {
				while (diff-- > 0) {
					ExecutionThread et = new ExecutionThread(threadGroup,
							clock, scheduler, dataCollectorWrite,
							asyncBladeInsertResponse, this);
					executionThreads.add(et);
					et.start();
				}
			}
			return;
		}
		if (parameter.equals(GROUP_PERIOD_PROPERTY)) {
			for (Iterator<Group> i = groupMap.values().iterator(); i.hasNext();)
				timer.setPeriod(i.next(), "balance", groupPeriod);
			return;
		}
		if (parameter.equals(JOB_DELAY_PROPERTY)) {
			scheduler.setJobDelay(jobDelay);
			asyncBladeInsertResponse.clear();
			return;
		}
	}

	public Map<String,Serializable> getCurrentParameters() {
		Map<String,Serializable> parameters = new HashMap<String,Serializable>();

		if (state != UNINITIALIZED) {
			String str;
			Map.Entry<String,Group> entry;
			int load;
			str = "";
			for (Iterator<Map.Entry<String,Group>> i = groupMap.entrySet().iterator(); i.hasNext();) {
				entry = i.next();
				str += entry.getKey() + "=";
				if ((load = entry.getValue().getLoad()) >= 0)
					str += load;
				if (i.hasNext())
					str += "; ";
			}
			parameters.put("population", str);
		}
		parameters.put(THREADS_PROPERTY, new Integer(threads).toString());
		parameters.put(GROUP_PERIOD_PROPERTY, new Long(groupPeriod).toString());
		parameters.put(SCHEDULER_PERIOD_PROPERTY, new Long(schedulerPeriod)
				.toString());
		parameters.put(JOB_DELAY_PROPERTY, new Long(jobDelay).toString());
		EventStorageState.putEventStorageStates(parameters,
				eventStorageStatesMap);
		return parameters;
	}

	/////////////////////////////////
	// interface BindingController //
	/////////////////////////////////

	public synchronized String[] listFc() {
		return interfaceNames;
	}

	public synchronized Object lookupFc(String clientItfName)
			throws NoSuchInterfaceException {
		if (clientItfName.equals(DataCollectorWrite.DATA_COLLECTOR_WRITE)) {
			return dataCollectorWrite;
		}
		else if (clientItfName
				.equals(BladeInsertResponse.BLADE_INSERT_RESPONSE)) {
			return bladeInsertResponse;
		}
		else {
			throw new NoSuchInterfaceException("No such interface: "
					+ clientItfName);
		}
	}

	public synchronized void bindFc(String clientItfName, Object serverItf)
			throws NoSuchInterfaceException, IllegalBindingException,
			IllegalLifeCycleException {
		doBindFc(clientItfName, serverItf);
	}

	public synchronized void unbindFc(String clientItfName)
			throws NoSuchInterfaceException, IllegalBindingException,
			IllegalLifeCycleException {
		doBindFc(clientItfName, null);
	}
}
