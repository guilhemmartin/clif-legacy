/*
 * CLIF is a Load Injection Framework
 * Copyright (C) 2005,2007 France Telecom R&D
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 *
 * CLIF
 *
 * Contact: clif@ow2.org
 */
package org.ow2.clif.scenario.isac.engine.instructions;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

/**
 * @author Emmanuel Varoquaux
 * @author Bruno Dillenseger
 */
public class ParamsHolder
{
	private Map<String,PlugInParamPart[]> splitParams = null;
	private Map<String,String> plainParams = null;

	public ParamsHolder(Map<String,PlugInParamPart[]> params)
	{
		Map<String,String> tmp = new HashMap<String,String>();

		for (
			Iterator<Map.Entry<String,PlugInParamPart[]>> i = params.entrySet().iterator();
			i.hasNext();)
		{
			Map.Entry<String,PlugInParamPart[]> entry = i.next();
			switch (entry.getValue().length)
			{
			case 0:
				tmp.put(entry.getKey(), "");
				continue;
			case 1:
				if (entry.getValue()[0].type == PlugInParamPart.STRING) {
					tmp.put(entry.getKey(), entry.getValue()[0].str);
					continue;
				}
			default:
				splitParams = params;
				return;
			}
		}
		plainParams = tmp;
	}

	public Map<String,PlugInParamPart[]> getSplitParams()
	{
		return splitParams;
	}

	public Map<String,String> getPlainParams()
	{
		return plainParams;
	}

	public boolean isSplit()
	{
		return splitParams != null;
	}

	@Override
	public String toString()
	{
		return isSplit() ? splitParams.toString() : plainParams.toString();
	}	
}
