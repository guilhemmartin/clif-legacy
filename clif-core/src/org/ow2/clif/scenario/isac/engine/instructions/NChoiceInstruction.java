/*
 * CLIF is a Load Injection Framework Copyright (C) 2005 France Telecom R&D
 * 
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2 of the License, or (at your option) any
 * later version.
 * 
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with this library; if not, write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 * 
 * CLIF
 * 
 * Contact: clif@ow2.org
 */
package org.ow2.clif.scenario.isac.engine.instructions;

import java.util.ArrayList;

/**
 * @author Emmanuel Varoquaux
 */
public class NChoiceInstruction extends Instruction {
	public static class Choice {
		public int	proba;
		public int	label;

		public Choice(int proba, int label) {
			this.proba = proba;
			this.label = label;
		}
	}

	public int total;
	public ArrayList<Choice> choices;

	public NChoiceInstruction(int total, ArrayList<Choice> choices) {
		this.total = total;
		this.choices = choices;
	}

	@Override
	public int getType() {
		return NCHOICE;
	}

	@Override
	public String toString() {
		String res;

		res = "NCHOICE\t" + total;
		for (int i = 0, n = choices.size(); i < n; i++) {
			Choice c = choices.get(i);
			res += ", (" + c.proba + ", " + c.label + ")";
		}
		return res;
	}
}
