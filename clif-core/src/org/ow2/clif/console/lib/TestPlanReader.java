/*
* CLIF is a Load Injection Framework
* Copyright (C) 2004-2005, 2012-2013 France Telecom R&D
*
* This library is free software; you can redistribute it and/or
* modify it under the terms of the GNU Lesser General Public
* License as published by the Free Software Foundation; either
* version 2 of the License, or (at your option) any later version.
*
* This library is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
* Lesser General Public License for more details.
*
* You should have received a copy of the GNU Lesser General Public
* License along with this library; if not, write to the Free Software
* Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*
* Contact: clif@ow2.org
*/

package org.ow2.clif.console.lib;


import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Collection;
import java.util.HashSet;
import java.util.Map;
import java.util.HashMap;
import java.util.Properties;
import java.util.Set;


/**
 * CLIF test plan reader. Reads test plan definitions defined as
 * Java properties, from an input stream (typically a CLIF .ctp file).
 * 
 * @author Bruno Dillenseger
 */
abstract public class TestPlanReader
{
	static public final String BLADE_PROP = "blade";
	static public final String SERVER_PROP = "server";
	static public final String INJECTOR_PROP = "injector";
	static public final String PROBE_PROP = "probe";
	static public final String ARGUMENT_PROP = "argument";
	static public final String COMMENT_PROP = "comment";
	static public final String ID_PROP = "id";
	static public final String FILE_EXT = ".ctp";

	/**
	 * Simple command that prints the names of all CLIF servers involved in some test plans
	 * Names are printed only once whatever the number of occurrences in the test plans.
	 * Each printed line contains a single CLIF server name.
	 * @param args the names of the test plan files
	 */
	static public void main(String[] args)
		throws IOException
	{
		Set<String> allNames = new HashSet<String>();
		for (String tpf : args)
		{
			allNames.addAll(getServers(new FileInputStream(tpf)));
		}
		for (String name : allNames)
		{
			System.out.println(name);
		}
	}

	/**
	 * Reads a test plan definition from the provided input stream,
	 * and returns the set of names of CLIF servers involved in this
	 * test plan.
	 * @param in the input stream to read the test plan from
	 * @return the set of CLIF server's names
	 * @throws IOException
	 */
	static public Set<String> getServers(InputStream in)
		throws IOException
	{
		return getServers(readFromProp(in).values());
	}

	/**
	 * Get the set of names of CLIF servers involved in the provided collection
	 * of deployment definitions.
	 * @param definitions the collection of deployment definitions to analyze
	 * @return the set of CLIF server's names
	 */
	static public Set<String> getServers(Collection<ClifDeployDefinition> definitions)
	{
		Set<String> result = new HashSet<String>(definitions.size());
		for (ClifDeployDefinition def : definitions)
		{
			result.add(def.getServerName());
		}
		return result;
	}

	/**
	 * Reads a CLIF test plan definition from the provided input stream,
	 * and returns a test plan object, i.e. a Map with blade identifiers
	 * as keys, and associated deployment definitions as values.
	 * @param in the input stream to read the test plan definition from
	 * @return the test plan Map
	 * @throws IOException an error occurred while reading the input stream
	 */
	static public Map<String, ClifDeployDefinition> readFromProp(InputStream in)
		throws IOException
	{
		Map<String, ClifDeployDefinition> testPlan = new HashMap<String, ClifDeployDefinition>();
		Properties props = new Properties();
		props.load(in);
		int n = 0;
		String prefix = BLADE_PROP + "." + n + ".";
		Map<String, String> context;
		while (props.getProperty(prefix + ID_PROP) != null)
		{
			context = new HashMap<String, String>();
			if (props.containsKey(prefix + INJECTOR_PROP))
			{
				context.put(
					"insert",
					props.getProperty(prefix + INJECTOR_PROP));
				context.put(
					"datacollector",
					"org.ow2.clif.datacollector.lib.InjectorDataCollector");
			}
			else if (props.containsKey(prefix + PROBE_PROP))
			{
				String probeName = props.getProperty(prefix + PROBE_PROP);
				context.put("insert", probeName);
				context.put(
					"datacollector",
					probeName.substring(0, probeName.lastIndexOf('.')) + ".DataCollector");
			}
			else
			{
				throw new IOException("Bad test plan file format");
			}
			testPlan.put(
				props.getProperty(prefix + ID_PROP),
				new ClifDeployDefinition(
					props.getProperty(prefix + SERVER_PROP),
					"org.ow2.clif.server.lib.Blade",
					context,
					props.getProperty(prefix + ARGUMENT_PROP),
					props.getProperty(prefix + COMMENT_PROP),
					props.containsKey(prefix + PROBE_PROP)));
			prefix = BLADE_PROP + "." + ++n + ".";
		}
		return testPlan;
	}
}
