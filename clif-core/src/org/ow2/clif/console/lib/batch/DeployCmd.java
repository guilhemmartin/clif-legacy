/*
 * CLIF is a Load Injection Framework
 * Copyright (C) 2005-2006, 2008, 2011 France Telecom
 * Copyright (C) 2017 Orange SA
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 *
 * Contact: clif@ow2.org
 */

package org.ow2.clif.console.lib.batch;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Map;
import java.util.Observable;
import java.util.Observer;
import org.ow2.clif.console.lib.ClifDeployDefinition;
import org.ow2.clif.console.lib.TestPlanReader;
import org.ow2.clif.deploy.ClifAppFacade;
import org.ow2.clif.deploy.ClifRegistry;
import org.ow2.clif.deploy.DeployObservation;
import org.ow2.clif.server.lib.ClifServerImpl;
import org.ow2.clif.util.ExecutionContext;


/**
 * Batch command to deploy a test plan as defined in a given test plan file.
 * The deployed test plan is given a name (registered in the Registry) for
 * further reference with other batch commands. A CLIF registry is created,
 * as well as a default CLIF server, when no one can be found. 
 * 
 * @author Joan Chaumont
 * @author Bruno Dillenseger
 */
public class DeployCmd
{
	static private boolean deployOK = true;

	/**
	 * @see #run(String, String)
	 * @param args args[0] is the name for the deployed test plan,
	 * args[1] is the file name of the test plan definition to be deployed
	 */
	static public void main(String[] args)
	{
		if (System.getSecurityManager() == null)
		{
			System.setSecurityManager(new SecurityManager());
		}
		if (args.length != 2)
		{
			BatchUtil.usage("expected arguments: <deployed test plan name> <test plan definition file>");
		}
		ExecutionContext.init("./");
		int res = run(args[0], args[1]); 
		if (res != BatchUtil.SUCCESS)
		{
			System.exit(res);
		}
	}


	/**
	 * Deploys a test plan (i.e. a set of blades among CLIF servers) according to the given test plan definition.
	 * @param deployName name for the new deployed test plan
	 * @param fileName file name of the test plan definition to be deployed
	 * @return command status code (@see BatchUtil)
	 */
	static public int run(String deployName, String fileName)
	{
		try
		{
			ClifRegistry clifReg = null;
			// try to create a registry
	        try
	        {
	            clifReg = new ClifRegistry(true);
	            System.out.println("Created " + clifReg);
	        }
	        catch (Throwable ex)
	        {
	        	// otherwise, try to connect to an existing registry
	            try
	            {
	                clifReg = new ClifRegistry(false);
	                System.out.println("Connected to " + clifReg);
	            }
	            catch (Throwable th)
	            {
	                System.out.println("Failed getting a registry.");
	                System.err.println("Please check your configuration with regard to registry host and port number.");
					return BatchUtil.ERR_REGISTRY;
				}
			}

			/* creates the default CLIF server */
			try
			{
				ClifServerImpl.create(ExecutionContext.DEFAULT_SERVER, clifReg);
			}
			catch (Exception ex)
			{
				System.err.println("fatal: could not register local host CLIF server");
				ex.printStackTrace(System.err);
				return BatchUtil.ERR_REGISTRY;
			}

			/* test plan deployment */
			System.out.println("Deploying from " + fileName + " test plan definition...");
			File ctpFile = new File(fileName);
			Map<String,ClifDeployDefinition> definitions = TestPlanReader.readFromProp(new FileInputStream(ctpFile));
			ClifAppFacade clifApp = new ClifAppFacade(deployName, BatchUtil.CLIF_APPLICATION);
			clifApp.addObserver(
				new Observer()
				{
					public void update(Observable clifApp, Object observation)
					{
						if (observation instanceof DeployObservation
							&& !((DeployObservation)observation).isSuccessful())
						{
							System.out.println(observation);
							deployOK = false;
						}
					}
				});
			clifApp.syncDeploy(definitions, clifReg);
			if (deployOK)
			{
				clifReg.bindClifApp(deployName, clifApp.getClifApp());
				System.out.println("Test plan " + deployName + " is deployed.");
				return BatchUtil.SUCCESS;
			}
			else
			{
				System.out.println("Test plan " + deployName + " deployment failed.");
				return BatchUtil.ERR_DEPLOY;
			}
		}
		catch (FileNotFoundException fileEx)
		{
			System.err.println("Test plan file " + fileName + " not found");
			fileEx.printStackTrace(System.err);
			return BatchUtil.ERR_ARGS;
		}
		catch (IOException ioEx)
		{
			System.err.println("Invalid test plan file " + fileName);
			ioEx.printStackTrace(System.err);
			return BatchUtil.ERR_ARGS;
		}
		catch (Exception ex)
		{
			System.err.println("Execution problem while deploying");
			ex.printStackTrace(System.err);
			return BatchUtil.ERR_EXEC;
		}
	}
}
