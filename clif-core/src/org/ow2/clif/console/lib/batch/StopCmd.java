/*
* CLIF is a Load Injection Framework
* Copyright (C) 2005, 2006 France Telecom
*
* This library is free software; you can redistribute it and/or
* modify it under the terms of the GNU Lesser General Public
* License as published by the Free Software Foundation; either
* version 2 of the License, or (at your option) any later version.
*
* This library is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
* Lesser General Public License for more details.
*
* You should have received a copy of the GNU Lesser General Public
* License along with this library; if not, write to the Free Software
* Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*
* CLIF $Name: not supported by cvs2svn $
*
* Contact: clif@ow2.org
*/

package org.ow2.clif.console.lib.batch;

import org.ow2.clif.deploy.ClifAppFacade;
import org.ow2.clif.supervisor.api.BladeState;
import org.ow2.clif.util.ExecutionContext;

/**
 * Batch command to stop a test execution, or just a subset of its blades.
 * 
 * @author Joan Chaumont
 * @author Bruno Dillenseger
 */
public class StopCmd
{
	/**
	 * Stops a test execution, or just a subset of its blades.
	 * @param args args[0] is the name of the running test plan,
	 * args[1] is optional, and may contain a list of blade identifiers
	 * separated by : character bladeId1:bladeId2:...bladeIdn
	 */
	public static void main(String[] args) 
	{
		if(args.length < 1)
		{
		    BatchUtil.usage("arguments expected: <name of running test plan> [<blade id1>:<blade id2>:...<blade idn>]");
		}
		if (System.getSecurityManager() == null)
		{
			System.setSecurityManager(new SecurityManager());
		}
		ExecutionContext.init("./");
		System.exit(run(args[0], BatchUtil.getSelBlades(args, 1)));
	}


	/**
	 * Stops a test execution, or just a subset of its blades.
	 * @param testPlan the name of the running test plan
	 * @param blades
	 * @return array of blade identifiers designating blades that must be stopped
	 * in the given test plan, or null to stop all blades in the test plan.
	 */
	static public int run(String testPlan, String[] blades)
	{
		try
		{
		    ClifAppFacade clifApp = BatchUtil.getClifAppFacade(testPlan);
		    if(clifApp == null)
		    {
		        System.err.println("Unknown test plan");
		        return BatchUtil.ERR_DEPLOY;
		    }
			int res = clifApp.stop(blades);
            if (res == BatchUtil.SUCCESS)
            {
                res = clifApp.waitForState(blades, BladeState.STOPPED);
                if (res == BatchUtil.SUCCESS)
                {
                    System.out.println("Stopped");
                    return BatchUtil.SUCCESS;
                }
            }
            System.err.println("Blades are not in a stoppable state");
            return BatchUtil.ERR_LIFECYCLE;
		}
		catch (Exception ex)
		{
		    System.err.println("Execution problem while stopping");
		    ex.printStackTrace(System.err);
		    return BatchUtil.ERR_EXEC;
		}
	}
}
