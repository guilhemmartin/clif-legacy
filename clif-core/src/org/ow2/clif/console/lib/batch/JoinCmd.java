/*
* CLIF is a Load Injection Framework
* Copyright (C) 2005, 2006 France Telecom
*
* This library is free software; you can redistribute it and/or
* modify it under the terms of the GNU Lesser General Public
* License as published by the Free Software Foundation; either
* version 2 of the License, or (at your option) any later version.
*
* This library is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
* Lesser General Public License for more details.
*
* You should have received a copy of the GNU Lesser General Public
* License along with this library; if not, write to the Free Software
* Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*
* CLIF $Name: not supported by cvs2svn $
*
* Contact: clif@ow2.org
*/

package org.ow2.clif.console.lib.batch;

import org.ow2.clif.deploy.ClifAppFacade;
import org.ow2.clif.util.ExecutionContext;

/**
 * Batch command to wait until all blades of a given running test,
 * or just a subset of them, terminate.
 * @author Joan Chaumont
 * @author Bruno Dillenseger
 */
public class JoinCmd
{
	/**
	 * Blocks until all blades of a given running test, or just a subset of them, terminate.
	 * @param args args[0] is the name of the target running test plan;
	 * args[1] is optional, giving the list of blade identifiers to wait for termination;
	 * when this argument is omitted, all blades from the given test plan are waited for termination.
	 */
	public static void main(String[] args)
	{
		if (args.length < 1)
		{
			BatchUtil.usage("arguments expected: <name of running test plan> [<bladeId1:bladeId2:...bladeIdn>]");
		}
		if (System.getSecurityManager() == null)
		{
			System.setSecurityManager(new SecurityManager());
		}
		ExecutionContext.init("./");
		System.exit(run(args[0], BatchUtil.getSelBlades(args, 1)));
	}


	/**
	 * Blocks until all blades of a given running test, or just a subset of them, terminate.
	 * @param testPlan the name of the target test plan
	 * @param blades array of identifiers of blades to wait for termination.
	 * A null value means all blades from the given test plan are concerned.
	 * @return execution status code
	 */
	static public int run(String testPlan, String[] blades)
	{
		try
		{
			ClifAppFacade clifApp = BatchUtil.getClifAppFacade(testPlan);
			if (clifApp == null)
			{
				System.err.println("Test undeployed");
				return BatchUtil.ERR_DEPLOY;
			}
			int res = clifApp.join(blades);
			if (res == BatchUtil.ERR_LIFECYCLE)
			{
				System.err.println("Blades are not running");
				return BatchUtil.ERR_LIFECYCLE;
			}
			System.out.println("Join successful");
			return BatchUtil.SUCCESS;
		}
		catch (Exception ex)
		{
			System.err.println("Execution problem while joining");
			ex.printStackTrace(System.err);
			return BatchUtil.ERR_EXEC;
		}
	}
}
