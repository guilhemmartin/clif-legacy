/*
* CLIF is a Load Injection Framework
* Copyright (C) 2003-2004 France Telecom R&D
* Copyright (C) 2003 INRIA
*
* This library is free software; you can redistribute it and/or
* modify it under the terms of the GNU Lesser General Public
* License as published by the Free Software Foundation; either
* version 2 of the License, or (at your option) any later version.
*
* This library is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
* Lesser General Public License for more details.
*
* You should have received a copy of the GNU Lesser General Public
* License along with this library; if not, write to the Free Software
* Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*
* CLIF $Name: not supported by cvs2svn $
*
* Contact: clif@ow2.org
*/

/**
 * Interface to control a Blade component, or a Blade Insert component
 * @author Julien Buret
 * @author Nicolas Droze
 * @author Bruno Dillenseger
 */
package org.ow2.clif.server.api;


import java.io.Serializable;
import java.util.Map;

import org.ow2.clif.supervisor.api.ClifException;


public interface BladeControl extends ActivityControl
{
	static public final String BLADE_CONTROL = "Blade control";
	static public final String BLADE_INSERT_CONTROL= "Blade insert control";

	/**
	 * Sets the scenario argument string.
	 */
	public void setArgument(String argument) throws ClifException;
    
	/**
	 * Sets a unique identifier string to this blade
	 */
	public void setId(String id);


	/**
	 * @return the blade unique identifier
	 */
	 public String getId();
	 
	/**
	 * Sets the parameter <code>parameter</code> of this blade with the value
	 * <code>value</code>.
	 * 
	 * This method allows to change parameters independently of the activity of
	 * the blade. However, some parameters can depend on the state of the blade.   
	 */
	public void changeParameter(String parameter, Serializable value) throws ClifException;
	
	/**
	 * Returns a <code>Map</code> which maps the defined parameters to their values.
	 */
	public Map<String,Serializable> getCurrentParameters();
}
