/*
* CLIF is a Load Injection Framework
* Copyright (C) 2011 France Telecom R&D
*
* This library is free software; you can redistribute it and/or
* modify it under the terms of the GNU Lesser General Public
* License as published by the Free Software Foundation; either
* version 2 of the License, or (at your option) any later version.
*
* This library is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
* Lesser General Public License for more details.
*
* You should have received a copy of the GNU Lesser General Public
* License along with this library; if not, write to the Free Software
* Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*
* Contact: clif@ow2.org
*/

package org.ow2.clif.server.api;

/**
 * Support for multiple synchronization locks designated by names,
 * possibly with counters for the number of lock notifications.
 * 
 * @author Bruno Dillenseger
 */
public interface Synchronizer
{
	// Fractal interface name
	static public final String SYNCHRONIZER = "Synchronizer";

	/**
	 * Set a rendez-vous for a given lock, setting the minimum
	 * number of notifications that must be reached to complete
	 * the rendez-vous.
	 * @param lockName the lock name
	 * @param count the minimum number of notifications to wait for
	 */
	public void setRendezVous(String lockName, long count);

	/**
	 * Wait until a rendez-vous is complete, i.e. the minimum number
	 * of notifications has been reached for this lock.
	 * @param lockName the lock name
	 * @param timeoutMs time out in ms. 0 means infinite time out.
	 * A negative value is not acceptable.
	 * @return false in case of time out, true otherwise
	 * @throws InterruptedException if any thread interrupted the current
	 * thread before or while the current thread was waiting for a notification
	 * @throws IllegalArgumentException if the value of timeout is negative. 
	 */
	public boolean getRendezVous(String lockName, long timeoutMs)
		throws InterruptedException, IllegalArgumentException;

	/**
	 * Wait until the specified lock is notified at least once.
	 * @param lockName the lock name
	 * @throws InterruptedException the current thread has been interrupted
	 * while waiting for the lock notification, causing this method to exit
	 * with no lock notification.
	 */
	public void wait(String lockName) throws InterruptedException;

	/**
	 * Wait until the specified lock is notified at least once,
	 * or the specified time out is reached
	 * @param lockName the lock name
	 * @param timeout_ms time out in ms. 0 means infinite time out.
	 * A negative value is not acceptable.
	 * @return false in case of time out, true if the otherwise
	 * @throws InterruptedException if any thread interrupted the current
	 * thread before or while the current thread was waiting for a notification
	 * @throws IllegalArgumentException if the value of timeout is negative. 
	 */
	public boolean wait(String lockName, long timeout_ms)
		throws InterruptedException, IllegalArgumentException;

	/**
	 * Wait until the specified lock is notified at least the specified
	 * number of times, or the specified time out is reached
	 * @param lockName the lock name
	 * @param timeout_ms time out in ms. 0 means infinite time out.
	 * A negative value is not acceptable.
	 * @param occurrences the minimum number of notifications to wait for
	 * @return false in case of time out, true otherwise
	 * @throws InterruptedException if any thread interrupted the current
	 * thread before or while the current thread was waiting for a notification
	 * @throws IllegalArgumentException if the value of timeout is negative. 
	 */
	public boolean wait(String lockName, long timeout_ms, long occurrences)
		throws InterruptedException, IllegalArgumentException;

	/**
	 * Notify the specified lock, incrementing a notification counter.
	 * @param lockName the lock name
	 */
	public void notify(String lockName);

	/**
	 * Test if a lock has been notified at least once
	 * @param lockName the lock name
	 * @return true if the lock has been notified, false otherwise
	 */
	public boolean wasNotified(String lockName);

	/**
	 * Test if a lock has been notified at least the specified
	 * number of times.
	 * @param lockName the lock name
	 * @param occurrences the minimum number of notifications
	 * @return true if the lock has been notified at least the
	 * specified number of times, false otherwise
	 */
	public boolean wasNotified(String lockName, long occurrences);

	/**
	 * Get the number of notifications issued for the specified lock
	 * @param lockName the lock name
	 * @return the number of notifications issued for the specified lock.
	 * 0 means this lock is unknown by this synchronizer.  
	 */
	public long getCount(String lockName);

	/**
	 * Clear all locks supported by this synchronizer.
	 */
	public void reset();

	/**
	 * Clear one lock.
	 * @param lockName the lock name
	 * @return the number of notifications issued on the
	 * specified lock. 0 means this lock is unknown by this
	 * synchronizer.  
	 */
	public long clear(String lockName);
}
