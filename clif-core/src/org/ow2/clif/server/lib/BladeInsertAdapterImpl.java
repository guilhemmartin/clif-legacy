/*
* CLIF is a Load Injection Framework
* Copyright (C) 2003-2004, 2007, 2011 France Telecom R&D
* Copyright (C) 2003 INRIA
*
* This library is free software; you can redistribute it and/or
* modify it under the terms of the GNU Lesser General Public
* License as published by the Free Software Foundation; either
* version 2 of the License, or (at your option) any later version.
*
* This library is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
* Lesser General Public License for more details.
*
* You should have received a copy of the GNU Lesser General Public
* License along with this library; if not, write to the Free Software
* Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*
* Contact: clif@ow2.org
*/

package org.ow2.clif.server.lib;

import java.io.Serializable;
import java.util.Map;
import org.objectweb.fractal.api.control.BindingController;
import org.ow2.clif.datacollector.api.DataCollectorWrite;
import org.ow2.clif.server.api.BladeControl;
import org.ow2.clif.server.api.BladeInsertResponse;
import org.ow2.clif.storage.api.AlarmEvent;
import org.ow2.clif.storage.api.LifeCycleEvent;
import org.ow2.clif.storage.api.StorageProxyAdmin;
import org.ow2.clif.supervisor.api.BladeState;
import org.ow2.clif.supervisor.api.ClifException;
import org.ow2.clif.supervisor.api.SupervisorInfo;

/**
 * Implementation of a Blade Insert Adapter component (kind of an asynchronous wrapper to the
 * Blade Insert component).
 * @author Julien Buret
 * @author Nicolas Droze
 * @author Bruno Dillenseger
 */
public class BladeInsertAdapterImpl
	implements
		BladeControl,
		BladeInsertResponse,
		BindingController,
		Runnable
{
	static final String[] interfaceNames = new String[] {
		SupervisorInfo.SUPERVISOR_INFO,
		BladeControl.BLADE_INSERT_CONTROL,
		StorageProxyAdmin.STORAGEPROXY_ADMIN,
		DataCollectorWrite.DATA_COLLECTOR_WRITE };
	static final int OP_IDLE = 0;
	static final int OP_INIT = 1;
	static final int OP_START = 2;
	static final int OP_STOP = 3;
	static final int OP_SUSPEND = 4;
	static final int OP_RESUME = 5;
	static final int OP_TERM = 6;

	volatile int current_op = -1;
	Serializable current_testId = null;
	volatile Thread op_thread = null;

	private SupervisorInfo sis;
	private BladeControl bladeCtl;
	private StorageProxyAdmin spa;
	private DataCollectorWrite dcw;
	private BladeState bladeState;
	private String bladeId;


	public BladeInsertAdapterImpl()
	{
		current_op = OP_IDLE;
		bladeState = BladeState.DEPLOYED;
		op_thread = new Thread(this, "Blade adapter control");
		op_thread.start();
	}

	////////////////////////
	// interface Runnable //
	////////////////////////

	/**
	 * handles asynchronous calls on control operations, enforcing mutual exclusion
	 */
	public void run()
	{
		synchronized (Thread.currentThread())
		{
			while (op_thread != null)
			{
				switch (current_op)
				{
					case OP_IDLE:
						try
						{
							op_thread.wait();
						}
						catch (InterruptedException ex)
						{
							ex.printStackTrace(System.err);
						}
						break;
					case OP_INIT:
						if (bladeState == BladeState.DEPLOYED
							|| bladeState == BladeState.COMPLETED
							|| bladeState == BladeState.STOPPED
							|| bladeState == BladeState.ABORTED)
						{
							do_init();
						}
						current_op = OP_IDLE;
						op_thread.notify();
						break;
					case OP_START:
						if (bladeState == BladeState.INITIALIZED)
						{
							do_start();
						}
						current_op = OP_IDLE;
						op_thread.notify();
						break;
					case OP_STOP:
						if (bladeState == BladeState.DEPLOYED
						    || bladeState == BladeState.INITIALIZED
							|| bladeState == BladeState.SUSPENDED
							|| bladeState == BladeState.RUNNING)
						{
							do_stop(bladeState != BladeState.DEPLOYED);
						}
						current_op = OP_IDLE;
						op_thread.notify();
						break;
					case OP_SUSPEND:
						if (bladeState == BladeState.RUNNING)
						{
							do_suspend();
						}
						current_op = OP_IDLE;
						op_thread.notify();
						break;
					case OP_RESUME:
						if (bladeState == BladeState.SUSPENDED)
						{
							do_resume();
						}
						current_op = OP_IDLE;
						op_thread.notify();
						break;
					default:
						throw new Error("unexpected operation id: " + current_op);
				}
			}
			current_op = OP_TERM;
			Thread.currentThread().notify();
		}
	}

/* LifeCycleController removed
	public synchronized void startFc()
	{
		current_op = OP_IDLE;
		bladeState = BladeState.DEPLOYED;
		op_thread = new Thread(this, "Blade adapter control");
		op_thread.start();
	}


	public synchronized void stopFc()
	{
		if (op_thread != null)
		{
			Object lock = op_thread;
			synchronized (lock)
			{
				op_thread = null;
				lock.notifyAll();
				if (current_op != OP_TERM)
				{
					try
					{
						lock.wait();
					}
					catch (InterruptedException ex)
					{
						ex.printStackTrace(System.err);
					}
				}
			}
		}
	}
*/

	/////////////////////////////////
	// interface BindingController //
	/////////////////////////////////


	public Object lookupFc(String clientItfName)
	{
		if (clientItfName.equals(SupervisorInfo.SUPERVISOR_INFO))
		{
			return sis;
		}
		else if (clientItfName.equals(BladeControl.BLADE_INSERT_CONTROL))
		{
			return bladeCtl;
		}
		else if (clientItfName.startsWith(StorageProxyAdmin.STORAGEPROXY_ADMIN))
		{
			return spa;
		}
		else if (clientItfName.equals(DataCollectorWrite.DATA_COLLECTOR_WRITE))
		{
			return dcw;
		}
		else
		{
			return null;
		}
	}


	public void bindFc(String clientItfName, Object serverItf)
	{
		if (clientItfName.equals(SupervisorInfo.SUPERVISOR_INFO))
		{
			sis = (SupervisorInfo) serverItf;
		}
		else if (clientItfName.equals(BladeControl.BLADE_INSERT_CONTROL))
		{
			bladeCtl = (BladeControl) serverItf;
		}
		else if (clientItfName.startsWith(StorageProxyAdmin.STORAGEPROXY_ADMIN))
		{
			spa = (StorageProxyAdmin) serverItf;
		}
		else if (clientItfName.equals(DataCollectorWrite.DATA_COLLECTOR_WRITE))
		{
			dcw = (DataCollectorWrite) serverItf;
		}
	}


	public void unbindFc(String clientItfName)
	{
		if (clientItfName.equals(SupervisorInfo.SUPERVISOR_INFO))
		{
			sis = null;
		}
		else if (clientItfName.equals(BladeControl.BLADE_INSERT_CONTROL))
		{
			bladeCtl = null;
		}
		else if (clientItfName.startsWith(StorageProxyAdmin.STORAGEPROXY_ADMIN))
		{
			spa = null;
		}
		else if (clientItfName.equals(DataCollectorWrite.DATA_COLLECTOR_WRITE))
		{
			dcw = null;
		}
	}


	public String[] listFc()
	{
		return interfaceNames;
	}


	////////////////////////////
	// interface BladeControl //
	////////////////////////////


	/**
	 * Sets blade argument.
	 */
	public void setArgument(String argument) throws ClifException
	{
		bladeCtl.setArgument(argument);
	}


	/**
	 * Sets the blade identifier.
	 */
	public void setId(String id)
	{
		bladeId = id;
		spa.init(id);
		bladeCtl.setId(bladeId);
	}


	/**
	 * @return the blade identifier
	 */
	public String getId()
	{
		return bladeId;
	}


	/**
	 * asynchronously initialize the blade
	 * @param testId should contain a unique test identifier
	 */
	public synchronized void init(Serializable testId)
	{
		if (op_thread != null)
		{
			synchronized(op_thread)
			{
				while (current_op != OP_IDLE)
				{
					try
					{
						op_thread.wait();
					}
					catch (InterruptedException ex)
					{
						ex.printStackTrace(System.err);
					}
				}
				current_op = OP_INIT;
				current_testId = testId;
				op_thread.notifyAll();
			}
		}
	}


	public void do_init()
	{
		sis.setBladeState(bladeId, BladeState.INITIALIZING);
		try
		{
			spa.newTest(current_testId);
			dcw.init(current_testId, bladeId);
			dcw.add(new LifeCycleEvent(System.currentTimeMillis(), BladeState.INITIALIZING));
			bladeCtl.init(current_testId);
			bladeState = BladeState.INITIALIZED;
			dcw.add(new LifeCycleEvent(System.currentTimeMillis(), BladeState.INITIALIZED));
			System.gc();
			sis.setBladeState(bladeId, bladeState);
		}
		catch (Throwable ex)
		{
			AlarmEvent alarm = new AlarmEvent(
				System.currentTimeMillis(),
				AlarmEvent.ERROR,
				ex);
			dcw.add(alarm);
			sis.alarm(bladeId, alarm);
			aborted();
		}
	}


	/**
	 * asynchronously starts the blade
	 */
	public synchronized void start()
	{
		if (op_thread != null)
		{
			synchronized(op_thread)
			{
				while (current_op != OP_IDLE)
				{
					try
					{
						op_thread.wait();
					}
					catch (InterruptedException ex)
					{
						ex.printStackTrace(System.err);
					}
				}
				current_op = OP_START;
				op_thread.notifyAll();
			}
		}
	}


	public void do_start()
	{
		sis.setBladeState(bladeId, BladeState.STARTING);
		try
		{
			dcw.add(new LifeCycleEvent(System.currentTimeMillis(), BladeState.STARTING));
			bladeCtl.start();
			bladeState = BladeState.RUNNING;
			dcw.add(new LifeCycleEvent(System.currentTimeMillis(), BladeState.RUNNING));
			sis.setBladeState(bladeId, bladeState);
		}
		catch (Throwable e)
		{
			e.printStackTrace(System.err);
			aborted();
		}
	}


	/**
	 * asynchronously stops the blade
	 */
	public synchronized void stop()
	{
		if (op_thread != null)
		{
			synchronized(op_thread)
			{
				while (current_op != OP_IDLE)
				{
					try
					{
						op_thread.wait();
					}
					catch (InterruptedException ex)
					{
						ex.printStackTrace(System.err);
					}
				}
				current_op = OP_STOP;
				op_thread.notifyAll();
			}
		}
	}


	protected void do_stop(boolean initialized)
	{
		sis.setBladeState(bladeId, BladeState.STOPPING);
		try
		{
			if (initialized)
			{
				dcw.add(new LifeCycleEvent(System.currentTimeMillis(), BladeState.STOPPING));
				bladeCtl.stop();
			}
			bladeState = BladeState.STOPPED;
			if (initialized)
			{
				dcw.add(new LifeCycleEvent(System.currentTimeMillis(), BladeState.STOPPED));
				dcw.terminate();
				spa.closeTest();
			}
			sis.setBladeState(bladeId, bladeState);
		}
		catch (Throwable e)
		{
			e.printStackTrace(System.err);
			aborted();
		}
	}


	/**
	 * asynchronously suspends the blade
	 */
	public synchronized void suspend()
	{
		if (op_thread != null)
		{
			synchronized(op_thread)
			{
				while (current_op != OP_IDLE)
				{
					try
					{
						op_thread.wait();
					}
					catch (InterruptedException ex)
					{
						ex.printStackTrace(System.err);
					}
				}
				current_op = OP_SUSPEND;
				op_thread.notifyAll();
			}
		}
	}


	protected void do_suspend()
	{
		sis.setBladeState(bladeId, BladeState.SUSPENDING);
		try
		{
			dcw.add(new LifeCycleEvent(System.currentTimeMillis(), BladeState.SUSPENDING));
			bladeCtl.suspend();
			bladeState = BladeState.SUSPENDED;
			dcw.add(new LifeCycleEvent(System.currentTimeMillis(), BladeState.SUSPENDED));
			sis.setBladeState(bladeId, bladeState);
		}
		catch (Throwable e)
		{
			e.printStackTrace(System.err);
			aborted();
		}
	}


	/**
	 * asynchronously resumes the blade
	 */
	public synchronized void resume()
	{
		if (op_thread != null)
		{
			synchronized(op_thread)
			{
				while (current_op != OP_IDLE)
				{
					try
					{
						op_thread.wait();
					}
					catch (InterruptedException ex)
					{
						ex.printStackTrace(System.err);
					}
				}
				current_op = OP_RESUME;
				op_thread.notifyAll();
			}
		}
	}


	protected void do_resume()
	{
		sis.setBladeState(bladeId, BladeState.RESUMING);
		try
		{
			dcw.add(new LifeCycleEvent(System.currentTimeMillis(), BladeState.RESUMING));
			bladeCtl.resume();
			bladeState = BladeState.RUNNING;
			dcw.add(new LifeCycleEvent(System.currentTimeMillis(), BladeState.RUNNING));
			sis.setBladeState(bladeId, bladeState);
		}
		catch (Throwable e)
		{
			e.printStackTrace(System.err);
			aborted();
		}
	}


	/**
	 * Waits for the end of the activity
	 */
	public void join()
	{
		Object lock = op_thread;
		if (lock != null)
		{
			synchronized(lock)
			{
				while (
					bladeState != BladeState.DEPLOYED
					&& bladeState != BladeState.COMPLETED
					&& bladeState != BladeState.STOPPED
					&& bladeState != BladeState.ABORTED)
				{
					try
					{
						lock.wait();
					}
					catch (InterruptedException ex)
					{
						ex.printStackTrace(System.err);
					}
				}
			}
		}
	}

	/**
	 * asynchronously changes a parameter of the blade
	 */
	public synchronized void changeParameter(String parameter, Serializable value) throws ClifException {
		bladeCtl.changeParameter(parameter, value);
	}

	/**
	 * asynchronously gets parameters of the blade
	 */
	public synchronized Map<String,Serializable> getCurrentParameters() {
		return bladeCtl.getCurrentParameters();
	}

	///////////////////////////////////
	// interface BladeInsertResponse //
	///////////////////////////////////


	public void aborted()
	{
		Object lock = op_thread;
		if (lock != null)
		{
			synchronized(lock)
			{
				bladeState = BladeState.ABORTED;
				lock.notifyAll();
			}
		}
		dcw.add(new LifeCycleEvent(System.currentTimeMillis(), BladeState.ABORTED));
		dcw.terminate();
		spa.closeTest();
		sis.setBladeState(bladeId, bladeState);
	}


	public void completed()
	{
		Object lock = op_thread;
		if (lock != null)
		{
			synchronized(lock)
			{
				bladeState = BladeState.COMPLETED;
				lock.notifyAll();
			}
		}
		dcw.add(new LifeCycleEvent(System.currentTimeMillis(), BladeState.COMPLETED));
		dcw.terminate();
		spa.closeTest();
		sis.setBladeState(bladeId, bladeState);
	}
	
	
	public void alarm(AlarmEvent alarm)
	{
		dcw.add(alarm);
		sis.alarm(bladeId, alarm);
	}
}
