/*
* CLIF is a Load Injection Framework
* Copyright (C) 2005 France Telecom R&D
* Copyright (C) 2014 Orange
*
* This library is free software; you can redistribute it and/or
* modify it under the terms of the GNU Lesser General Public
* License as published by the Free Software Foundation; either
* version 2 of the License, or (at your option) any later version.
*
* This library is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
* Lesser General Public License for more details.
*
* You should have received a copy of the GNU Lesser General Public
* License along with this library; if not, write to the Free Software
* Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*
* Contact: clif@ow2.org
*/

package org.ow2.clif.probe.jmx_jvm;


import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import javax.management.MBeanServerConnection;
import javax.management.ObjectName;
import javax.management.remote.JMXConnector;
import javax.management.remote.JMXConnectorFactory;
import javax.management.remote.JMXServiceURL;
import javax.naming.Context;
import org.ow2.clif.probe.util.AbstractDumbInsert;
import org.ow2.clif.storage.api.ProbeEvent;
import org.ow2.clif.supervisor.api.ClifException;
import org.ow2.clif.util.ClifClassLoader;


/**
 * Insert for JMX JVM probe
 * @author Cyrille Puget
 * @author Bruno Dillenseger
 */
public class Insert_JRockit5 extends AbstractDumbInsert
{
	// metrics retrieved
	private long committed = 0;
	private long used = 0;
	private long max = 0;
	// application server properties
	private String user = null;
	private String password = null;
	private String protocol = null;
	private String hostname = null;
	private int port;
	private String jndiroot = null;
	private String protocol_provider_package = null;
	private String mbeanservername = null;
	private String location = null;
	
	private MBeanServerConnection connection = null;
	private ObjectName jRockitRuntime = null;
	
	//private CompositeDataSupport heapMemoryUsage = null;
	
	public Insert_JRockit5() throws Exception
	{
		super();
		
		eventStorageStatesMap.put("store-lifecycle-events", storeLifeCycleEvents);
		eventStorageStatesMap.put("store-alarm-events", storeAlarmEvents);
		eventStorageStatesMap.put("store-" + JMX_JVMEvent.EVENT_TYPE_LABEL + "-events", storeProbeEvents);
	}

	@Override
	public ProbeEvent doProbe()
	{
		try {
			// retrieve memory information and convert from bytes to mega bytes
			committed = ((Long) connection.getAttribute(jRockitRuntime, "HeapSizeCurrent")).longValue() >> 20;
			max = ((Long) connection.getAttribute(jRockitRuntime, "HeapSizeMax")).longValue() >> 20;
			used = committed - (((Long) connection.getAttribute(jRockitRuntime, "HeapFreeCurrent")).longValue() >> 20);
		} catch (Exception e) {
			System.out.println("Error when configuring the parameters of the jmx jvm probe: " + e.getClass().getName() + " " + e.getLocalizedMessage());
		}
		
		long[] values = new long[3];
		// free memory (MB)
		values[0] = committed - used;
		// used memory %"
		values[1] = 100 * used/committed;
		// free usable memory %
		values[2] = 100 * (max - used)/max;
		return new JMX_JVMEvent(System.currentTimeMillis(), values);
	}

	
	@Override
	public void setExtraArguments(List<String> arg_probe_config) throws ClifException {
		
		// retrieve the third argument of the 'class' arguments, should be the property file 
		String propertiesFileName = arg_probe_config.get(0);
		Properties props = new Properties();
		try {
			props.load(ClifClassLoader.getClassLoader().getResourceAsStream(propertiesFileName));
		} catch (IOException ioe) {
			System.out.println("Error during initialization of the jmx_jvm properties: " + ioe.getClass().getName() + " " + ioe.getLocalizedMessage());
		}
		user = props.getProperty("server.user.name");
		password = props.getProperty("server.user.password");
		protocol = props.getProperty("server.connection.protocol");
		hostname = props.getProperty("server.host.name");
		port = Integer.valueOf(props.getProperty("server.host.port")).intValue();
		jndiroot = props.getProperty("server.jmx.connection.jndiroot");
		protocol_provider_package = props.getProperty("server.jmx.protocol_provider_package");
		location = props.getProperty("server.jmx.location");
		mbeanservername = props.getProperty("server.jmx.mbeanservername");
		
		try {
			// Create a JMX Service URL
			JMXServiceURL serviceURL = new JMXServiceURL(protocol, hostname, port, jndiroot + hostname + ":" +port + "/" + mbeanservername);
			
	    	// Setup connection properties
			Map<String,String> h = new HashMap<String,String>();
	    	h.put(Context.SECURITY_PRINCIPAL, user);
	    	h.put(Context.SECURITY_CREDENTIALS, password);
	    	h.put(JMXConnectorFactory.PROTOCOL_PROVIDER_PACKAGES, protocol_provider_package);
	
	    	// Create a connection to the DomainRuntimeMBeanServer
	    	JMXConnector connector = JMXConnectorFactory.connect(serviceURL, h);
	    	// Get the DomainRuntimeMBeanServer
	    	connection = connector.getMBeanServerConnection();
	    	// Get the JRockitRuntime MBean
	    	jRockitRuntime = new ObjectName("com.bea:Name="+location+",ServerRuntime="+location+",Location="+location+",Type=JRockitRuntime");
		} catch (Exception e) {
			System.out.println("Error when configuring the parameters of the jmx jvm probe: " + e.getClass().getName() + " " + e.getLocalizedMessage());
		}
	}

	@Override
	public String getHelpMessage()
	{
		return super.getHelpMessage() + " <configuration file>";
	}
}
