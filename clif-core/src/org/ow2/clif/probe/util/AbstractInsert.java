/*
* CLIF is a Load Injection Framework
* Copyright (C) 2004 France Telecom R&D
*
* This library is free software; you can redistribute it and/or
* modify it under the terms of the GNU Lesser General Public
* License as published by the Free Software Foundation; either
* version 2 of the License, or (at your option) any later version.
*
* This library is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
* Lesser General Public License for more details.
*
* You should have received a copy of the GNU Lesser General Public
* License along with this library; if not, write to the Free Software
* Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*
* CLIF $Name: not supported by cvs2svn $
*
* Contact: clif@ow2.org
*/

package org.ow2.clif.probe.util;


/**
 * This abstract implementation of a blade insert component manages a single thread,
 * doing a periodic task (probing) with a given period (in milliseconds) and during
 * an given actual time. It provides a strict interpretation of the blade lifecycle
 * with regard to the ActivityControl interface.
 * @author Bruno Dillenseger
 */
abstract public class AbstractInsert
	extends AbstractDumbInsert
{


	//////////////////////////////////////////////
	// implementation of interface BladeControl //
	//////////////////////////////////////////////


	@Override
	public void start()
	{
		synchronized(activity_lock)
		{
			started = true;
			activity_lock.notify();
		}
	}


	@Override
	public void stop()
	{
		synchronized (activity_lock)
		{
			stopped = true;
			poller.interrupt();
		}
		if (suspended)
		{
			resume();
		}
		synchronized (activity_lock)
		{
			if (started && ! terminated)
			{
				try
				{
					activity_lock.wait();
				}
				catch (InterruptedException ex)
				{
				}
			}
		}
	}


	@Override
	public void suspend()
	{
		synchronized (activity_lock)
		{
			suspended = true;
			poller.interrupt();
		}
	}


	@Override
	public void resume()
	{
		synchronized (activity_lock)
		{
			suspended = false;
			activity_lock.notify();
		}
	}


	////////////////////////
	// Runnable interface //
	////////////////////////


	@Override
	public void run()
	{
		try
		{
			synchronized (activity_lock)
			{
				if (! started)
				{
					activity_lock.wait();
				}
				baseTime_ms = System.currentTimeMillis();
			}
		}
		catch (InterruptedException ex)
		{
		}
		while (
			! stopped
			&& arg_duration_ms > System.currentTimeMillis() - baseTime_ms)
		{
			try
			{
				synchronized (activity_lock)
				{
					if (suspended)
					{
						baseTime_ms = System.currentTimeMillis() - baseTime_ms;
						activity_lock.wait();
						baseTime_ms = System.currentTimeMillis() - baseTime_ms;
					}
				}
				synchronized (activity_lock)
				{
					if (!stopped
						&& arg_duration_ms > System.currentTimeMillis() - baseTime_ms)
					{
						try
						{
							dcw.add(doProbe());
						}
						catch (Exception ex)
						{
						}
					}
				}
				long time = System.currentTimeMillis();
				if (arg_duration_ms > time - baseTime_ms + arg_period_ms)
				{
					Thread.sleep(arg_period_ms);
				}
				else
				{
					Thread.sleep(arg_duration_ms - time + baseTime_ms);
				}
			}
			catch (InterruptedException ex)
			{
			}
		}
		synchronized (activity_lock)
		{
			terminated = true;
			if (! stopped)
			{
				bir.completed();
			}
			else
			{
				activity_lock.notify();
			}
		}
	}
}
