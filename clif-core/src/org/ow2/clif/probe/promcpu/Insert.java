/*
* CLIF is a Load Injection Framework
* Copyright (C) 2020 Orange
*
* This library is free software; you can redistribute it and/or
* modify it under the terms of the GNU Lesser General Public
* License as published by the Free Software Foundation; either
* version 2 of the License, or (at your option) any later version.
*
* This library is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
* Lesser General Public License for more details.
*
* You should have received a copy of the GNU Lesser General Public
* License along with this library; if not, write to the Free Software
* Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*
* Contact: clif@ow2.org
*/

package org.ow2.clif.probe.promcpu;

import org.ow2.clif.probe.util.AbstractDumbInsert;
import org.ow2.clif.storage.api.ProbeEvent;
import org.ow2.clif.supervisor.api.ClifException;
import org.ow2.clif.util.ClifClassLoader;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.List;
import java.util.Properties;

import com.jayway.jsonpath.DocumentContext;
import com.jayway.jsonpath.JsonPath;

/**
 * Insert for CPU Prometheus probe based on Prometheus API
 * 
 * @author Bruno Dillenseger
 * @author Guilhem Martin
 */
public class Insert extends AbstractDumbInsert {

	// Input parameters needed for Prometheus probe

	// Hardcoded parameter
	private final String promQlQueryTemplate = "api/v1/query?query=100-(avg+by(hostname)(irate(node_cpu_seconds_total{mode=\"idle\",__MONITOREDHOSTFILTER__}[__AVERAGEDURATION__])))*100";

	// Parameters retrieved from the prometheus.properties file
	private String endpoint = null;
	private String averageDuration = null;

	// Parameters retrieved from the command line
	private String monitoredHostFilter = null;

	// Parameters computed on above parameters
	private String urlToCall = null;

	public Insert() throws ClifException {
		super();

		eventStorageStatesMap.put("store-lifecycle-events", storeLifeCycleEvents);
		eventStorageStatesMap.put("store-alarm-events", storeAlarmEvents);
		eventStorageStatesMap.put("store-" + PromCpuEvent.EVENT_TYPE_LABEL + "-events", storeProbeEvents);
	}

	/**
	 * 
	 * Example of monitored host filter as the 4th arguments (or 2nd extra arguments)
	 * hostname="myhostname"
	 * setname="mysetname"
	 * 
	 * 
	 * @param arg_probe_config
	 * @throws ClifException
	 */

	@Override
	public void setExtraArguments(List<String> arg_probe_config) throws ClifException {

		if (arg_probe_config.size() == 2) {
			try {
				// retrieve the third argument of the 'class' arguments, should be the property
				// file
				String propertiesFileName = arg_probe_config.get(0);

				// retrieve the fourth argument of the 'class' arguments, should be the
				// monitored host filter
				monitoredHostFilter = arg_probe_config.get(1);

				Properties props = new Properties();
				try {
					props.load(ClifClassLoader.getClassLoader().getResourceAsStream(propertiesFileName));
				} catch (IOException ioe) {
					throw new ClifException(
							"Could not get Prometheus probe's configuration from file \"" + propertiesFileName + "\".",
							ioe);
				}
				endpoint = props.getProperty("server.endpoint");
				averageDuration = props.getProperty("promql.averageduration");

				urlToCall = buildApiUrl();
			} catch (Exception e) {
				throw new ClifException("Could not load the extra argument for Prometheus CLIF probe.", e);
			}
		} else {
			throw new ClifException("Expected 2 arguments, got " + arg_probe_config.size() + " arguments for Prometheus CLIF probe.");
		}
	}

	/**
	 * 
	 * Build API URL from the endpoint and a Prometheus Query template, retrieving
	 * values from properties file
	 * 
	 * Example of built URL:
	 * "http://<Prometheus IP>:9090/api/v1/query?query=100-(avg+by(hostname)(irate(node_cpu_seconds_total{mode=%22idle%22,hostname=%22<name of the host>%22}[5m])))*100");
	 * Returns CPU activity as a percentage only for the host dvcasdb01.
	 * 
	 * @return URL
	 */
	private String buildApiUrl() {

		String promQlQuery = promQlQueryTemplate;

		promQlQuery = promQlQuery.replace("__MONITOREDHOSTFILTER__", monitoredHostFilter);
		promQlQuery = promQlQuery.replace("__AVERAGEDURATION__", averageDuration);

		return endpoint + "/" + promQlQuery;
	}

	/**
	 * 
	 * Performs API call to Prometheus and returns CPU
	 * 
	 */
	private int callPrometheusToGetCpu() throws ClifException {

		try {

			URL url = new URL(urlToCall);

			HttpURLConnection con = (HttpURLConnection) url.openConnection();
			con.setRequestMethod("GET");

			int status = con.getResponseCode();
			System.out.println("Response code from server: " + status);

			BufferedReader in = new BufferedReader(new InputStreamReader(con.getInputStream()));
			String inputLine;
			StringBuffer jsonPrometheusDataHttpResponse = new StringBuffer();
			while ((inputLine = in.readLine()) != null) {
				jsonPrometheusDataHttpResponse.append(inputLine);
			}
			in.close();

			System.out.println("Réponse http: " + jsonPrometheusDataHttpResponse);

			DocumentContext jsonPrometheusContext = JsonPath.parse(jsonPrometheusDataHttpResponse.toString());

			String jsonpathprometheusCPUPath = "$.data.result[0].value[1]";

			// String to double to round integer
			String prometheusCpu = jsonPrometheusContext.read(jsonpathprometheusCPUPath);
			double cpuDouble = Double.parseDouble(prometheusCpu);
			int cpu = (int) Math.round(cpuDouble);

			return cpu;

		} catch (Exception e) {
			throw new ClifException("Could not initialize Prometheus CPU probe on endpoint: \"" + urlToCall + "\"", e);
		}
	}

	@Override
	public ProbeEvent doProbe() {
		PromCpuEvent event = null;
		try {

			long[] values = new long[1];

			values[0] = callPrometheusToGetCpu();

			event = new PromCpuEvent(System.currentTimeMillis(), values);
		} catch (Exception ex) {
			ex.printStackTrace(System.err);
		}
		return event;
	}

	/**
	 * Gives probe help. This super implementation mentions the 2 first In addition
	 * of the 2 base first arguments (polling period and execution time), for
	 * Prometheus CLIF probe, 2 more arguments are required: the path to the
	 * properties file and the Prometheus monitored host filter
	 * 
	 * @return a message giving basic help to use this probe
	 */
	@Override
	public String getHelpMessage() {
		return super.getHelpMessage() + " <properties file path>" + " <Prometheus monitored filter criteria host - example: hostname=\"myhostname\" or setname=\"mysetname\" ; don't forget the double quote surrounding the filter value>";
	}

	static public void main(String args[]) throws Exception {
		Insert prometheusProbe = new Insert();

		prometheusProbe.setArgument(String.join(" ", args));

		while (true) {
			ProbeEvent measurement = prometheusProbe.doProbe();
			System.out.println(measurement);
			Thread.sleep(1000);
		}
	}
}
