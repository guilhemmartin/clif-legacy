/*
* CLIF is a Load Injection Framework
* Copyright (C) 2014 Orange
*
* This library is free software; you can redistribute it and/or
* modify it under the terms of the GNU Lesser General Public
* License as published by the Free Software Foundation; either
* version 2 of the License, or (at your option) any later version.
*
* This library is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
* Lesser General Public License for more details.
*
* You should have received a copy of the GNU Lesser General Public
* License along with this library; if not, write to the Free Software
* Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*
* Contact: clif@ow2.org
*/

package org.ow2.clif.probe.network;

import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;
import org.hyperic.sigar.NetInterfaceConfig;
import org.hyperic.sigar.NetInterfaceStat;
import org.hyperic.sigar.Sigar;
import org.hyperic.sigar.SigarException;
import org.ow2.clif.probe.util.AbstractDumbInsert;
import org.ow2.clif.probe.util.SigarUtil;
import org.ow2.clif.storage.api.ProbeEvent;
import org.ow2.clif.supervisor.api.ClifException;


/**
 * Insert for Network probe based on SIGAR
 * @author Bruno Dillenseger
 */
public class Insert extends AbstractDumbInsert
{
	private NetInterfaceStat probe = new NetInterfaceStat();
	private String itfName = null;
	private long prevTxPackets = -1;
	private long prevTxBytes = -1;
	private long prevTxErrors = -1;
	private long prevTxDrops = -1;
	private long prevTxOverruns = -1;
	private long prevTxCollisions = -1;
	private long prevRxPackets = -1;
	private long prevRxBytes = -1;
	private long prevRxErrors = -1;
	private long prevRxDrops = -1;
	private long prevRxOverruns = -1;
	private long prevTime = -1;


	public Insert()
	throws ClifException
	{
		super();
		eventStorageStatesMap.put("store-lifecycle-events", storeLifeCycleEvents);
		eventStorageStatesMap.put("store-alarm-events", storeAlarmEvents);
		eventStorageStatesMap.put("store-" + NetworkEvent.EVENT_TYPE_LABEL + "-events", storeProbeEvents);
	}


	@Override
	public String getHelpMessage()
	{
		return super.getHelpMessage()
			+ " <network interface name, IP address or description>\n"
			+ "configured interfaces: " + getConfiguredInterfacesAsString();
	}


	private List<String> getConfiguredInterfacesAsString()
	{
		List<String> result = new LinkedList<String>();
		try
		{
			Sigar sigar = SigarUtil.getSigar();
	       	String[] names = sigar.getNetInterfaceList();
			for (String itf : names)
	       	{
	       		NetInterfaceConfig conf = sigar.getNetInterfaceConfig(itf);
	       		if (! conf.getAddress().equals("0.0.0.0"))
	       		{
	       			result.add(
	       				itf + " "
	       				+ conf.getAddress() + " "
	       				+ conf.getDescription().replace(" ", ""));
	       		}
	       	}
		}
		catch (SigarException ex)
		{
			ex.printStackTrace(System.err);
			result = null;
		}
		return result;
	}


	@Override
	protected void setExtraArguments(List<String> args)
	throws ClifException
	{
        if (! args.isEmpty())
        {
            String arg = args.get(0);
        	try
        	{
	           	Sigar sigar = SigarUtil.getSigar();
	           	String[] names = sigar.getNetInterfaceList();
           		for (int i=0 ; i<names.length && itfName == null ; ++i)
           		{
           			if (names[i].equals(arg))
           			{
           				itfName = names[i];
           			}
           			else
           			{
           				NetInterfaceConfig conf = sigar.getNetInterfaceConfig(names[i]);
           				if (conf.getAddress().equals(arg)
           					|| conf.getDescription().replace(" ",  "").startsWith(arg.replace(" ", "")))
           				{
           					itfName = names[i];
           				}
           			}
           		}
	           	if (itfName == null)
	           	{
	           		throw new ClifException(
	           			"Network probe error: " + arg + " is not a valid interface.\n"
	           			+ "\nValid interfaces: " + getConfiguredInterfacesAsString());
	           	}
           	}
           	catch (SigarException ex)
           	{
           		throw new ClifException("Network probe initialization error: " + ex.getMessage(), ex);
           	}
        }
        else
        {
        	throw new ClifException("Disk probe error: the target interface argument (name, IP, or trimmed description) is missing.");
        }
    }


	@Override
	public ProbeEvent doProbe()
	{	
		NetworkEvent event = null;
		try
		{
			long currentTime = System.currentTimeMillis();
			probe.gather(SigarUtil.getSigar(), itfName);
			if (prevTime != -1)
			{
				long elapsedTime = currentTime - prevTime;
				long[] values = new long[11];
				values[0] = (8000 * (probe.getRxBytes() - prevRxBytes)) / elapsedTime;
           		values[1] = probe.getRxPackets() - prevRxPackets;
           		values[2] = (8000 * (probe.getTxBytes() - prevTxBytes)) / elapsedTime;
           		values[3] = probe.getTxPackets() - prevTxPackets;
           		values[4] = probe.getRxErrors() - prevRxErrors;
           		values[5] = probe.getRxOverruns() - prevRxOverruns;
           		values[6] = probe.getRxDropped() - prevRxDrops;
           		values[7] = probe.getTxErrors() - prevTxErrors;
           		values[8] = probe.getTxOverruns() - prevTxOverruns;
           		values[9] = probe.getTxDropped() - prevTxDrops;
           		values[10] = probe.getTxCollisions() - prevTxCollisions;
				event =  new NetworkEvent(currentTime, values);
			}
			prevTime = currentTime;
			prevTxPackets = probe.getTxPackets();
			prevTxBytes = probe.getTxBytes();
			prevTxErrors = probe.getTxErrors();
			prevTxDrops = probe.getTxDropped();
			prevTxOverruns = probe.getTxOverruns();
			prevTxCollisions = probe.getTxCollisions();
			prevRxPackets = probe.getRxPackets();
			prevRxBytes = probe.getRxBytes();
			prevRxErrors = probe.getRxErrors();
			prevRxDrops = probe.getRxDropped();
			prevRxOverruns = probe.getRxOverruns();
		}
		catch (Exception ex)
		{	
			ex.printStackTrace(System.err);
		}
		return event;
	}


	static public void main(String args[])
	throws Exception
	{
		Insert ins = new Insert();
		ins.setExtraArguments(Arrays.asList(args));
		while (true)
		{
			ProbeEvent measurement = ins.doProbe();
			System.out.println(measurement);
			Thread.sleep(1000);
		}
	}
}
