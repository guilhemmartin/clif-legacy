/*
* CLIF is a Load Injection Framework
* Copyright (C) 2014 Orange
*
* This library is free software; you can redistribute it and/or
* modify it under the terms of the GNU Lesser General Public
* License as published by the Free Software Foundation; either
* version 2 of the License, or (at your option) any later version.
*
* This library is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
* Lesser General Public License for more details.
*
* You should have received a copy of the GNU Lesser General Public
* License along with this library; if not, write to the Free Software
* Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*
* Contact: clif@ow2.org
*/

package org.ow2.clif.probe.cpu;

import org.hyperic.sigar.Cpu;
import org.ow2.clif.probe.util.AbstractDumbInsert;
import org.ow2.clif.probe.util.SigarUtil;
import org.ow2.clif.storage.api.ProbeEvent;
import org.ow2.clif.supervisor.api.ClifException;


/**
 * Insert for CPU probe based on SIGAR
 * @author Bruno Dillenseger
 */
public class Insert extends AbstractDumbInsert
{
	private long previousSys = -1;
	private long previousUser = -1;
	private long previousTotal = -1;
	private Cpu probe;


	public Insert()
	throws ClifException
	{
		super();
		probe = new Cpu();
		eventStorageStatesMap.put("store-lifecycle-events", storeLifeCycleEvents);
		eventStorageStatesMap.put("store-alarm-events", storeAlarmEvents);
		eventStorageStatesMap.put("store-" + CPUEvent.EVENT_TYPE_LABEL + "-events", storeProbeEvents);
	}


	@Override
	public ProbeEvent doProbe()
	{
		CPUEvent event = null;
		try
		{
			probe.gather(SigarUtil.getSigar());
			long currentTotal = probe.getTotal();
			long currentUser = probe.getUser();
			long currentSys = probe.getSys();
			long[] values = new long[3];
			if (previousTotal != -1 && previousTotal != currentTotal)
			{
				long delta = currentTotal - previousTotal;
				values[1] = 100 * (currentUser - previousUser);
				values[2] = 100 * (currentSys - previousSys);
				values[0] = (values[1] + values[2]) / delta;
				values[1] /= delta;
				values[2] /= delta;
			}
			previousSys = currentSys;
			previousUser = currentUser;
			previousTotal = currentTotal;
			event =  new CPUEvent(System.currentTimeMillis(), values);
		}
		catch (Exception ex)
		{
			ex.printStackTrace(System.err);
		}
		return event;
	}


	static public void main(String args[])
	throws Exception
	{
		Insert cpuProbe = new Insert();
		while (true)
		{
			ProbeEvent measurement = cpuProbe.doProbe();
			System.out.println(measurement);
			Thread.sleep(1000);
		}
	}
}
