/*
 * CLIF is a Load Injection Framework
 * Copyright (C) 2009 France Telecom
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 *
 * CLIF
 *
 * Contact: clif@ow2.org
 */

package org.ow2.clif.probe.rtp;

import java.io.IOException;
import java.lang.Thread;

import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.InetSocketAddress;

import java.nio.ByteBuffer;
import java.nio.channels.DatagramChannel;
import java.nio.channels.SelectionKey;
import java.nio.channels.Selector;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.HashMap;
import java.util.Iterator;

import org.ow2.clif.scenario.isac.exception.IsacRuntimeException;

/**
 * Thread to open and listen UDP packets.
 * 
 * @author Rémi Druilhe
 */
public class RTPListener extends Thread
{
	// Singleton
	private static RTPListener instance;
	
	private boolean endThread = false;
	
	private Selector selector;
	private LinkedList<RTPInformation> packetTable = new LinkedList<RTPInformation>();
	private HashMap<Integer, DatagramChannel> channels = new HashMap<Integer, DatagramChannel>();
	
	/**
	 * Contructor
	 */
	private RTPListener()
	{
		try
		{
			selector = Selector.open();
		}
		catch(IOException e)
		{
			throw new IsacRuntimeException("Unable to open selector : " + e);
		}
	}

	/**
	 * Create or get the single instance of the thread
	 * 
	 * @return the single instance of the thread
	 */
	public static synchronized RTPListener getInstance()
	{
		if(instance == null)
			instance = new RTPListener();

		return instance;
	}
	
	/**
	 * Open sockets using java.nio
	 * 
	 * @param addr : the address to open socket (can be null)
	 * @param ports : the ports to open 
	 */
	public void openSocket(InetAddress addr, ArrayList<Integer> ports)
	{	
		try
		{
			for(int i=0; i<ports.size(); i++)
			{			
				if(!channels.containsKey(ports.get(i)))
				{
					DatagramChannel dtgChannel = DatagramChannel.open();
					dtgChannel.configureBlocking(false);
	
					DatagramSocket dtgSocket = dtgChannel.socket();
					InetSocketAddress address;
					
					if(addr != null)
						address = new InetSocketAddress(addr, ports.get(i));
					else
						address = new InetSocketAddress(ports.get(i));
					
					dtgSocket.bind(address);

					dtgChannel.register(selector, SelectionKey.OP_READ | SelectionKey.OP_WRITE);
					
					channels.put(ports.get(i), dtgChannel);
				}
			}
		}
		catch(IOException e)
		{
			throw new IsacRuntimeException("Unable to open port : " + e);
		}
		
		endThread = false;
	}
	
	/**
	 * Start the thread of the singleton if it hasn't been started yet.
	 */
	public synchronized void startListener()
	{
		if(!instance.isAlive())
			instance.start();
	}
	
	/**
	 * @see java.lang.Thread#run()
	 * 
	 * This method listen to the ports opened previously with openSocket.
	 * 
	 * The size of RTP/RTCP packets must be less than 512 bytes.
	 */
	@Override
	public void run()
	{		
		ByteBuffer buffer = ByteBuffer.allocate(512);

		while(!endThread)
		{
			try
			{
				selector.select();
			}
			catch(IOException e)
			{
				throw new IsacRuntimeException("Unable to select the selector : " + e);
			}
				
			Iterator<SelectionKey> it = selector.selectedKeys().iterator();
			
			while(it.hasNext())
			{
				SelectionKey key = it.next();
				
				it.remove();

				if(key.isReadable()) 
				{
					buffer.clear();						
					
					try
					{
						((DatagramChannel) key.channel()).receive(buffer);
					}
					catch(IOException e)
					{
						throw new IsacRuntimeException("Buffer writing failed : " + e);
					}

					byte[] tempBuffer = new byte[buffer.array().length];
					
					for(int i=0; i<buffer.array().length; i++)
						tempBuffer[i] = buffer.array()[i];
					
					RTPInformation rtpInformation = new RTPInformation(tempBuffer, System.currentTimeMillis(), ((DatagramChannel) key.channel()).socket().getLocalPort());
					
					synchronized(packetTable)
					{
						packetTable.add(rtpInformation);
						packetTable.notify();
					}
				}
			}
		}
		
		try
		{
			selector.select();
		}
		catch(IOException e)
		{
			throw new IsacRuntimeException("Unabled to select the selector : " + e);
		}
		
		Iterator<SelectionKey> it = selector.selectedKeys().iterator();
		
		while(it.hasNext())
		{
			SelectionKey key = it.next();
			
			it.remove();
			
			((DatagramChannel) key.channel()).socket().close();
			
			try
			{
				((DatagramChannel) key.channel()).disconnect();
			}
			catch(IOException e)
			{
				throw new IsacRuntimeException("Unabled to disconnect the channel : " + e);
			}
		}
		
		try
		{
			selector.close();
		}
		catch(IOException e)
		{
			throw new IsacRuntimeException("Unabled to close the selector : " + e);
		}
		
		synchronized(packetTable)
		{
			packetTable.clear();
			packetTable.notify();
		}	
		
		channels.clear();
		instance = null;
	}
	
	/**
	 * This method return list of packets before cleaning it.
	 * 
	 * @return the list of packets.
	 */
	public LinkedList<RTPInformation> getPackets(Integer timeout)
	{
		LinkedList<RTPInformation> tempTable = new LinkedList<RTPInformation>();

		if(!endThread)
		{
			synchronized(packetTable) 
			{
				if(packetTable.isEmpty() && !instance.isInterrupted())
				{
					try
					{
						packetTable.wait(timeout);
					}
					catch(InterruptedException e)
					{
						//throw new IsacRuntimeException("Getting packets failed : " + e);
					}
				}
	
				if(!packetTable.isEmpty())
				{				
					while(!packetTable.isEmpty())
						tempTable.addLast(packetTable.removeFirst());
	
					packetTable.notify();
				}
			}
		}
		
		return tempTable;
	}
	
	/**
	 * Send a RTP packet on the given local port to the remote client.
	 * 
	 * @param data : the packet.
	 * @param remoteAddress : InetAddress of the remote client.
	 * @param remotePort : the port of the remote client.
	 * @param localPort : the port on which the packet is sent.
	 */
	public void sendPacket(byte[] data, InetAddress remoteAddress, int remotePort, int localPort)
	{
		ByteBuffer buffer = ByteBuffer.wrap(data);
		
		InetSocketAddress rmtAddr = new InetSocketAddress(remoteAddress, remotePort);

		try
		{	
			DatagramChannel dtgChannel = channels.get(localPort);
			
			dtgChannel.send(buffer, rmtAddr);
			
			buffer.clear();
		}
		catch(IOException e)
		{
			throw new IsacRuntimeException("Unable to send packet : " + e);
		}
	}
	
	/**
	 * Close the listener and the sockets by stopping thread.
	 */
	public void close()
	{
		endThread = true;
	}
}