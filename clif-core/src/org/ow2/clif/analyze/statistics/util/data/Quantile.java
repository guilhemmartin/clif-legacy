/*
 * CLIF is a Load Injection Framework
 * Copyright (C) 2005 France Telecom R&D
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 *
 * CLIF $Name:  $
 *
 * Contact: clif@ow2.org
 */
package org.ow2.clif.analyze.statistics.util.data;

/**
 * Bucket class stores the number of values (number) in a range defined by
 * min (included) and  max (included). This object is used by
 * LongStatistics to create an ArrayList of Bucket objects.
 * @author Guy Vachet
 */
public class Quantile {
    private int minIndex;
    private int maxIndex;
    private long min = Long.MIN_VALUE;
    private long max = Long.MIN_VALUE;
    private long median = Long.MIN_VALUE;

    public Quantile(int minIndex, int maxIndex) {
        this.minIndex = minIndex;
        this.maxIndex = maxIndex;
    }

    public Quantile(int minIndex, int maxIndex, long min, long max, long median) {
        this(minIndex, maxIndex);
        setMin(min);
        setMax(max);
        setMedian(median);
    }

    public int getMinIndex() {
        return minIndex;
    }

    public int getMaxIndex() {
        return maxIndex;
    }

    public int getNumber() {
        return 1 + maxIndex - minIndex;
    }

    public void setMin(long min) {
        this.min = min;
    }

    public long getMin() {
        return min;
    }

    public void setMax(long max) {
        this.max = max;
    }

    public long getMax() {
        return max;
    }

    public void setMedian(long median) {
        this.median = median;
    }

    public long getMedian() {
        return median;
    }

    public double getDensity() {
        if ((min == Long.MIN_VALUE) || (max == Long.MIN_VALUE))
            return -1;
        if (max == min)
            return Double.MAX_VALUE;
        else
            return (double) getNumber() / (max - min);
    }

    @Override
	public String toString() {
        StringBuffer sb = new StringBuffer();
        sb.append(min).append("\t").append(max).append("\t");
        sb.append(median).append("\t").append(getNumber()).append("\t\t");
        return sb.append(minIndex).append("\t").append(maxIndex).toString();
    }

}
