/*
 * CLIF is a Load Injection Framework
 * Copyright (C) 2005 France Telecom R&D
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 *
 * CLIF $Name:  $
 *
 * Contact: clif@ow2.org
 */
package org.ow2.clif.analyze.statistics.util.data;

import java.util.*;
import java.util.logging.Level;

/**
 * Class for performances statistical calculation (data stored in ListOfLong) with
 * median, average value, standard deviation ... more specifically tuned to
 * time measures
 * 
 * @author Guy Vachet
 */
public class LongStatistics extends StatOnLongs {
	// verbose mode for verification
	private static final boolean VERBOSE = false;
	// to format the text of data summary
	private static final int MAX_LABEL_LENGTH = 15;
	private static final int MIN_LABEL_LENGTH = 7;
	private static final String CLASSIC_LABELS = "Mean\tStd\tstatNb\t| rawNb\tmean\tstd\tmin\tmax";

	/**
	 * longStat empty constructor
	 */
	public LongStatistics() {
		super();
	}

	/**
	 * Constructs a LongStatistics with an empty list of long contructed with
	 * the specified initial capacity.
	 * 
	 * @param initialCapacity
	 */
	public LongStatistics(int initialCapacity) {
		super(initialCapacity);
	}

	/**
	 * longStat constructor based on existing ListOfLong.
	 * 
	 * @param lol
	 *            the ListOfLong as initial value.
	 */
	public LongStatistics(ListOfLong lol) {
		super(lol);
	}

	/**
	 * print the statistical summary of data on the standard output stream.
	 * 
	 * @param label
	 *            of the statistic
	 * @param verbose
	 *            mode
	 */
	public void outputStatisticalSummary(String label, boolean verbose) {
		StringBuffer sb = new StringBuffer();
		if (verbose) {
			sb.append("        \t stat-sort (k=");
			sb.append(getStatisticalSortFactor()).append(" r=");
			sb.append(getStatisticalSortPercentage()).append(")\t|     raw data\n");
			sb.append("        \t").append(CLASSIC_LABELS).append("\n");
		}
		if (label.length() > MAX_LABEL_LENGTH)
			sb.append(label.substring(0, MAX_LABEL_LENGTH));
		else if (label.length() < MIN_LABEL_LENGTH)
			sb.append(label).append("\t");
		else
			sb.append(label);
		if (size() == 0) {
			sb.append("\t").append(size()).append(" measures !");
			sb.append("\tno values for statistical calculation.");
		} else {
			// results of data after statistical sort
			sb.append("\t").append(Math4Long.round(getStatSortMean(), size()));
			sb.append("\t").append(Math4Long.round(getStatSortStd(), size()));
			sb.append("\t").append(getStatSortDataNumber());
			// raw data results
			sb.append("\t").append(size());
			sb.append("\t").append(Math4Long.round(getMean(), size()));
			sb.append("\t").append(Math4Long.round(getStd(), size()));
			sb.append("\t").append(getMin()).append("\t").append(getMax());
		}
		System.out.println(sb.toString());
	}

	/**
	 * print the data analyze by enhanced statistics, including median result,
	 * on the standard output stream.
	 * 
	 * @param label
	 *            of the statistic
	 * @param verbose
	 *            mode
	 */
	public void outputStatistics(String label, boolean verbose) {
		StringBuffer sb = new StringBuffer();
		if (verbose) {
			sb.append("\t  stat-sort (k=").append(getStatisticalSortFactor());
			sb.append(" r=").append(getStatisticalSortPercentage());
			sb.append(")\t|     raw data\n\tMedian\t").append(CLASSIC_LABELS);
			sb.append("\n");
		}
		if (label.length() > MIN_LABEL_LENGTH)
			sb.append(label.substring(0, MIN_LABEL_LENGTH));
		else
			sb.append(label);
		if (size() == 0)
			sb.append("\tno measure then no statistics !!.....");
		else {
			// results of data after statistical sort
			sb.append("\t").append(getStatSortMedian());
			sb.append("\t").append(Math4Long.round(getStatSortMean(), size()));
			sb.append("\t").append(Math4Long.round(getStatSortStd(), size()));
			sb.append("\t").append(getStatSortDataNumber());
			// raw data results
			sb.append("\t").append(size());
			sb.append("\t").append(Math4Long.round(getMean(), size()));
			sb.append("\t").append(Math4Long.round(getStd(), size()));
			sb.append("\t").append(getMin()).append("\t").append(getMax());
		}
		System.out.println(sb.toString());
	}

	/**
	 * prints the first tenth reckoning on the standard output stream.
	 * 
	 * @param label
	 *            of the statistic
	 * @param verbose
	 *            mode
	 */
	public void outputFirstTenthReckoning(String label, boolean verbose) {
		StringBuffer sb = new StringBuffer();
		if (verbose) {
			sb.append("\t\t  first (of ten) quantile results\n");
			sb.append("\t\tsubMed\tsubAvg\tnb.\tsubMin\tsubMax\n");
		}
		if (label.length() > MAX_LABEL_LENGTH)
			sb.append(label.substring(0, MAX_LABEL_LENGTH));
		else if (label.length() < MIN_LABEL_LENGTH)
			sb.append(label).append("\t");
		else
			sb.append(label);
		if (size() == 0) {
			sb.append("\t").append(size()).append(" measures !");
			sb.append("\tno values for calculation.");
		} else {
			try {
				sb.append("\t").append(subMedian(1, 10)).append("\t");
				sb.append(Math4Long.round(subMean(1, 10), size() / 10));
				sb.append("\t").append(size() / 10).append("\t");
				sb.append(subMin(1, 10)).append("\t").append(subMax(1, 10));
			} catch (Exception e) {
				sb.append("\t").append(e).append("\t").append(size());
				sb.append(" measures.").append("\tmin = ").append(getMin());
				sb.append("\tmax = ").append(getMax());
			}
		}
		System.out.println(sb.toString());
	}

	/**
	 * 
	 * @param index
	 *            of sorted data to be analyze by frequency distribution
	 * @param jump
	 *            to speed the next index search
	 * @param maxThld
	 *            upper threshold of current bucket (excluded value)
	 * @param maxIndex
	 *            index of the last sorted value
	 * @return first index of the next bucket of data distribution
	 */
	private int getNextIndex(int index, int jump, long maxThld, int maxIndex) {
		if (jump == 1) {
			for (; index <= maxIndex; index++) {
				if (getSortedValue(index) >= maxThld)
					break;
			}
			return index;
		} else {
			if (index + jump > maxIndex)
				return getNextIndex(index, jump / 2, maxThld, maxIndex);
			else if (getSortedValue(index + jump) < maxThld)
				return getNextIndex((index + jump), jump - 1, maxThld, maxIndex);
			else
				return getNextIndex(index, jump / 2, maxThld, maxIndex);
		}
	}

	/**
	 * @param number
	 *            the number of groups in which data is distributed.
	 * @param minIndex
	 *            first index of the data, sorted value.
	 * @param maxIndex
	 *            last index of the data, sorted value.
	 * @return distribution
	 */
	private List<Bucket> getBuckets(int number, int minIndex, int maxIndex) {
		long min = getSortedValue(minIndex);
		long max = getSortedValue(maxIndex);
		int nextIndex, index = minIndex, jump = 2;
		long step, minThreshold = min, maxThreshold;
		List<Bucket> buckets = new ArrayList<Bucket>(number);
		Bucket b;
		step = Math.round(Math.ceil((double) (max - min) / number));
		// in order to always include the max value in the last bucket
		if ((min + step * number) == max)
			step++;
		if (VERBOSE) {
			StringBuffer sb = new StringBuffer("note: distribution step = ");
			logger.log(Level.FINEST, sb.append(step).toString());
		}
		for (maxThreshold = min + step; maxThreshold <= max;) {
			nextIndex = getNextIndex(index, jump, maxThreshold, maxIndex);
			if (nextIndex > index) {
				b = new Bucket(nextIndex - index, minThreshold, maxThreshold);
				b.setMedian(getMedian(index, nextIndex - 1));
				buckets.add(b);
			} else
				buckets.add(new Bucket(0, minThreshold, maxThreshold));
			if (VERBOSE) {
				StringBuffer sb = new StringBuffer("bucket: ").append(b);
				sb.append("\nmin, max indexex: ").append(index).append(", ");
				sb.append(nextIndex - 1).append(" (1st jump: ").append(jump);
				logger.log(Level.FINEST, sb.append(")\n").toString());
			}
			jump = 1 + 7 * (nextIndex - index);
			if (jump < 25)
				jump = 2;
			index = nextIndex;
			minThreshold = maxThreshold;
			maxThreshold += step;
			while (maxThreshold < getSortedValue(nextIndex)) {
				buckets.add(new Bucket(0, minThreshold, maxThreshold));
				minThreshold = maxThreshold;
				maxThreshold += step;
			}
		}
		if (maxIndex >= index) {
			b = new Bucket(1 + maxIndex - index, minThreshold, maxThreshold);
			b.setMedian(getMedian(index, maxIndex));
			buckets.add(b);
		} else
			buckets.add(new Bucket(0, minThreshold, maxThreshold));
		// in order to complete the list to get 'number' rows
		for (int i = buckets.size(); i < number; i++) {
			minThreshold = maxThreshold;
			maxThreshold += step;
			buckets.add(new Bucket(0, minThreshold, maxThreshold));
		}
		if (VERBOSE) {
			for (int i = 0; i < buckets.size(); i++) {
				StringBuffer sb = new StringBuffer("adds new Bucket\t");
				logger.log(Level.FINEST, sb.append(buckets.get(i)).toString());
			}
		}
		return buckets;
	}

	/**
	 * determines the distribution of raw data, as the number of values in
	 * 'number' buckets.
	 * 
	 * @param number
	 *            the number of groups in which data is distributed.
	 * @return the list of Bucket objects that represent the minimal threshold
	 *         (included) and the maximal threshold (excluded) and the number of
	 *         values in each bucket.
	 * @see Bucket
	 */
	public List<Bucket> rawDataFrequency(int number) {
		if (VERBOSE) {
			StringBuffer sb = new StringBuffer("Raw data frequency( ");
			sb.append(number).append(") from min = ").append(getMin());
			sb.append(" until max = ").append(getMax());
			sb.append("\nas (min, max thresholds, median, frequency) list:");
			logger.log(Level.FINEST, sb.toString());
		}
		if (getMin() == getMax()) {
			long min = getMin();
			List<Bucket> distrib = new ArrayList<Bucket>(number);
			distrib.add(new Bucket(size(), min, min + 1, min));
			// complete the list in order to get 'number' rows
			for (int index = 1; index < number; index++) {
				min++;
				distrib.add(new Bucket(0, min, min + 1));
			}
			return distrib;
		} else {
			return getBuckets(number, 0, (size() - 1));
		}
	}

	/**
	 * prints the list which contains the data distribution in 'number' buckets.
	 * The raw data is used. The format is :
	 * <p>
	 * <number of values> <br>
	 * <first low threshold (inclusive)> <first high threshold (exclusive)> ...
	 * 
	 * @param number
	 *            the number of groups in which data is distributed.
	 */
	public void outputRawDataFrequency(int number) {
		List<Bucket> distribution = rawDataFrequency(number);
		StringBuffer sb = new StringBuffer("Frequency of raw data [");
		sb.append(getMin()).append("..").append(getMax());
		sb.append("] as:\n[min\tmax[\tmedian\tfrequency");
		for (int index = 0; index < number; index++)
			sb.append("\n").append(distribution.get(index).toString());
		System.out.println(sb.toString());
	}

	/**
	 * determines the distribution of data after statistical rejection, as the
	 * number of values in 'number' buckets ; the data is used within the
	 * statistical range (rejection based on mean +/- factor x std).
	 * 
	 * @param number
	 *            the number of groups in which data is distributed.
	 * @return the list of Bucket objects that represent the minimal threshold
	 *         (included) and the maximal threshold (excluded) and the number of
	 *         values in each bucket.
	 * @see Bucket
	 */
	public List<Bucket> statisticalSortDataFrequency(int number) {
		if (VERBOSE) {
			StringBuffer sb = new StringBuffer(
					"\nStatistical-sort data frequency( ");
			sb.append(number).append(") from min = ").append(
					getMinStatSortValue());
			sb.append(" until max = ").append(getMaxStatSortValue());
			sb.append("\nas (min, max thresholds, median, frequency) list:");
			logger.log(Level.FINEST, sb.toString());
		}
		if (getMinStatSortValue() == getMaxStatSortValue()) {
			long min = getMinStatSortValue();
			List<Bucket> distrib = new ArrayList<Bucket>(number);
			distrib.add(new Bucket(size(), min, min + 1, min));
			// complete the list in order to get 'number' rows
			for (int index = 1; index < number; index++) {
				min++;
				distrib.add(new Bucket(0, min, min + 1));
			}
			return distrib;
		} else {
			return getBuckets(number, getMinStatSortIndex(),
					getMaxStatSortIndex());
		}
	}

	/**
	 * prints the list which contains the data distribution in 'number' buckets.
	 * The statistical consistant data is used. The format is :
	 * <p>
	 * <number of values> <br>
	 * <first low threshold (inclusive)> <first high threshold (exclusive)> ...
	 * 
	 * @param number
	 *            the number of groups in which data is distributed.
	 */
	public void outputStatisticalSortDataFrequency(int number) {
		List<Bucket> distribution = statisticalSortDataFrequency(number);
		StringBuffer sb = new StringBuffer(
				"Frequency of statistical-sort data [");
		sb.append(getMinStatSortValue()).append("..").append(
				getMaxStatSortValue());
		sb.append("] as:\n[min\t[max\tmedian\tfrequency");
		for (int index = 0; index < number; index++)
			sb.append("\n").append(distribution.get(index).toString());
		System.out.println(sb.toString());
	}

	/**
	 * get the median quantile with these indexes at equal distance of the
	 * median. Then median quantile has indexes based on the size of quantile
	 * and the first and the last indexes of the data.
	 * 
	 * @param quantileSize
	 *            size of quantile
	 * @param theMinIndex
	 *            min index of data
	 * @param theMaxIndex
	 *            min index of data
	 * @return median quantile
	 */
	private Quantile getMedianQuantile(int quantileSize, int theMinIndex,
			int theMaxIndex) {
		int minIndexOfMedianQ;
		int maxIndexOfMedianQ;
		minIndexOfMedianQ = 1 + (theMaxIndex + theMinIndex - quantileSize) / 2;
		maxIndexOfMedianQ = minIndexOfMedianQ + quantileSize - 1;
		return new Quantile(minIndexOfMedianQ, maxIndexOfMedianQ);
	}

	/**
	 * 
	 * @param quantiles
	 * @return set of completed quantiles
	 */
	private List<Quantile> enhanceQuantiles(Collection<Quantile> quantiles) {
		Quantile quantile;
		int index;
		for (Iterator<Quantile> iter = quantiles.iterator(); iter.hasNext();) {
			quantile = (Quantile) iter.next();
			quantile.setMin(getSortedValue(quantile.getMinIndex()));
			quantile.setMax(getSortedValue(quantile.getMaxIndex()));
			if ((quantile.getNumber() % 2) == 0) {
				// median = mean of the next lower and the next upper center
				index = (quantile.getMinIndex() + quantile.getMaxIndex() - 1) / 2;
				quantile.setMedian((getSortedValue(index) + getSortedValue(index + 1)) / 2);
			} else {
				index = (quantile.getMinIndex() + quantile.getMaxIndex()) / 2;
				quantile.setMedian(getSortedValue(index));
			}
			if (VERBOSE)
				System.out.println(quantile);
		}
		return new ArrayList<Quantile>(quantiles);
	}

	/**
	 * first set indexes (in order to define a subset of values) of quantiles
	 * and enhanced this collection by min, max, median of contained values
	 * 
	 * @param number
	 *            number of quantile in which dat is distributed
	 * @param quantileSize
	 *            size of quantile
	 * @param theMinIndex
	 *            the first index of the data.
	 * @param theMaxIndex
	 *            the last index of the data.
	 * @return set of 'number -roughly !-' quantiles that represent data from
	 *         the theMinIndex untill theMaxIndex
	 */
	private List<Quantile> getQuantiles(int number, int quantileSize,
			int theMinIndex, int theMaxIndex) {
		int minIndex, maxIndex;
		Quantile medianQuantile;
		// in order to center the median in the center quantile
		medianQuantile = getMedianQuantile(quantileSize, theMinIndex,theMaxIndex);
		minIndex = medianQuantile.getMinIndex();
		maxIndex = medianQuantile.getMaxIndex();
		Map<Integer, Quantile> quantileIndex = new TreeMap<Integer, Quantile>();
		for (int i = number / 2; i > 0; i--) {
			if (VERBOSE)
				System.out.println(i + " \t-> min index = " + minIndex
						+ " max index = " + maxIndex);
			quantileIndex.put(new Integer(i), new Quantile(minIndex, maxIndex));
			if (minIndex == theMinIndex) {
				break;
			} else {
				maxIndex = minIndex - 1;
				minIndex -= quantileSize;
				if (minIndex < theMinIndex)
					break;
			}
		}
		if (VERBOSE)
			System.out.println("0 \t-> min index = " + theMinIndex
					+ " max index = " + maxIndex);
		quantileIndex.put(new Integer(0), new Quantile(theMinIndex, maxIndex));
		if (medianQuantile.getMaxIndex() < theMaxIndex) {
			minIndex = medianQuantile.getMaxIndex() + 1;
			maxIndex = minIndex + quantileSize - 1;
			for (int i = 1 + (number / 2); i < number; i++) {
				if (maxIndex > theMaxIndex) {
					if (VERBOSE)
						System.out.println(i + " \t-> min index = " + minIndex
								+ " max index = " + theMaxIndex);
					quantileIndex.put(new Integer(i), new Quantile(minIndex,
							theMaxIndex));
					break;
				} else {
					if (VERBOSE)
						System.out.println(i + " \t-> min index = " + minIndex
								+ " max index = " + maxIndex);
					quantileIndex.put(new Integer(i), new Quantile(minIndex,
							maxIndex));
					if (maxIndex == theMaxIndex)
						break;
					minIndex = maxIndex + 1;
					maxIndex = minIndex + quantileSize - 1;
				}
			}
		}
		return enhanceQuantiles(quantileIndex.values());
	}

	/**
	 * determines the set of 'number' quantiles (specific buckets that contain
	 * the same number of values) that divide data after statistical rejection.
	 * 
	 * @param number
	 *            the number of groups in which data is distributed.
	 * @return the list of Quantiles that divide raw data as set of min
	 *         (included) and max (excluded) thresholds. All buckets have the
	 *         same size.
	 * @see com.francetelecom.util.data.statistics.Quantile
	 */
	
	/**
	 * get the max density of the quantile set, to decorate this kind of results
	 */
	private double getMaxDensity(Collection<Quantile> quantiles) {
		double density, maxDensity = Double.MIN_VALUE;
		for (Iterator<Quantile> iter = quantiles.iterator(); iter.hasNext();) {
			density = (iter.next()).getDensity();
			if (density > maxDensity)
				maxDensity = density;
		}
		return maxDensity;
	}

	public List<Quantile> rawDataQuantiles(int number) {
		if (getMax() == getMin()) {
			long min = getMin();
			List<Quantile> distribution = new ArrayList<Quantile>();
			distribution.add(new Quantile(0, (size() - 1), min, min, min));
			return distribution;
		} else {
			int quantileSize;
			quantileSize = (int) Math
					.round(Math.ceil((double) size() / number));
			if (VERBOSE)
				logger.log(Level.FINEST, "Quantile size = " + quantileSize);
			return getQuantiles(number, quantileSize, 0, (size() - 1));
		}
	}

	/**
	 * prints list which contains the raw data distribution in 'number'
	 * quantiles (buckets that contain the same number of values).
	 * 
	 * @param number
	 *            the number of groups in which raw data is distributed.
	 */
	public void outputRawDataQuantiles(int number) {
		List<Quantile> quantiles = rawDataQuantiles(number);
		Quantile quantile;
		StringBuffer sb = new StringBuffer("Quantiles of raw data [")
				.append(getMin());
		sb.append("..").append(getMax()).append(
				"] as:\nwidth\tnumber\tmedian\tdensity");
		if (quantiles.size() == 1) {
			quantile = quantiles.get(0);
			sb.append("\n0\t").append(quantile.getNumber()).append("\t");
			sb.append(quantile.getMedian()).append(
					"\tinfinity\t** Max of density");
			for (int i = 1; i < number; i++) {
				sb.append("\n0\t0\t0\t0");
			}
			System.out.println(sb.toString());
		} else {
			double maxDensity = getMaxDensity(quantiles);
			// then ready to output (enhanced results by density)
			for (int i = 0; i < quantiles.size(); i++) {
				quantile = quantiles.get(i);
				int nb = quantile.getNumber();
				long width = quantile.getMax() - quantile.getMin();
				if (width > 0) {
					sb.append("\n").append(width).append("\t").append(nb);
					sb.append("\t").append(quantile.getMedian()).append("\t");
					double density = (double) nb / width;
					sb.append(Math4Long.round(density, size() / number));
					if (density == maxDensity)
						sb.append("\t***** Max of density");
					else if (density > 0.9 * maxDensity)
						sb.append("\t**");
					else if (density > 0.5 * maxDensity)
						sb.append("\t*");
				} else {
					sb.append("\n0\t").append(nb).append("\t");
					sb.append(quantile.getMedian()).append(
							"\tinfinity\t** Max of density");
				}
			}
			System.out.println(sb.toString());
		}
	}

	/**
	 * determines the list of 'number' buckets (= quantile because contains the
	 * same number of values) that divide data after statistical rejection.
	 * 
	 * @param number
	 *            the number of groups in which data is distributed.
	 * @return the list of Quantiles that divide data as set of min (included)
	 *         and max (excluded) thresholds. All buckets have the same size.
	 * @see Quantile
	 */
	public List<Quantile> statisticalSortDataQuantiles(int number) {
		if (getMinStatSortValue() == getMaxStatSortValue()) {
			long min = getMinStatSortValue();
			List<Quantile> distribution = new ArrayList<Quantile>();
			distribution.add(new Quantile(getMinStatSortIndex(),
					getMaxStatSortIndex(), min, min, min));
			return distribution;
		} else {
			int quantileSize = (int) Math.round(Math
					.ceil((double) getStatSortDataNumber() / number));
			if (VERBOSE)
				logger.log(Level.FINEST, "Quantile size = " + quantileSize);
			return getQuantiles(number, quantileSize, getMinStatSortIndex(),
					getMaxStatSortIndex());
		}
	}

	/**
	 * prints the list which contains the data distribution in 'number'
	 * quantiles (buckets that contain the same number of values). The
	 * statistical sort data (rejection based on mean +/- factor x std).
	 * 
	 * @param number
	 *            the number of groups in which data is distributed.
	 */
	public void outputStatisticalSortDataQuantiles(int number) {
		List<Quantile> quantiles = statisticalSortDataQuantiles(number);
		Quantile quantile;
		StringBuffer sb = new StringBuffer(
				"Quantiles of statistical-sort data [");
		sb.append(getMinStatSortValue()).append("..").append(
				getMaxStatSortValue());
		sb.append("] as:\nwidth\tnumber\tmedian\tdensity");
		if (quantiles.size() == 1) {
			quantile = quantiles.get(0);
			sb.append("\n0\t").append(quantile.getNumber()).append("\t");
			sb.append(quantile.getMedian()).append(
					"\tinfinity\t** Max of density");
			for (int i = 1; i < number; i++) {
				sb.append("\n0\t0\t0\t0");
			}
			System.out.println(sb.toString());
		} else {
			double maxDensity = getMaxDensity(quantiles);
			// then ready to output (enhanced results by density)
			for (int i = 0; i < quantiles.size(); i++) {
				quantile = quantiles.get(i);
				int nb = quantile.getNumber();
				long width = quantile.getMax() - quantile.getMin();
				if (width > 0) {
					sb.append("\n").append(width).append("\t").append(nb);
					sb.append("\t").append(quantile.getMedian()).append("\t");
					double density = (double) nb / width;
					sb.append(Math4Long.round(density, size() / number));
					if (density == maxDensity)
						sb.append("\t***** Max of density");
					else if (density > 0.75 * maxDensity)
						sb.append("\t**");
					else if (density > 0.5 * maxDensity)
						sb.append("\t*");
				} else {
					sb.append("\n0\t").append(nb).append("\t").append(
							quantile.getMedian());
					sb.append("\tinfinity\t** Max of density");
				}
			}
			System.out.println(sb.toString());
		}
	}

	/**
	 * Prints the medians of 'number' slices of raw data (increased-value sort).
	 * 
	 * @param number
	 *            the number of groups in which data is sliced up.
	 */
	public void outputResultsOfRawDataSlicing(int number) {
		StringBuffer sb = new StringBuffer("Results of ").append(number);
		sb.append(" slices (on raw data, sorted by increasing value) as\nmin\tmax\tmedian:\n");
		try {
			for (int i = 1; i <= number; i++) {
				sb.append(subMin(i, number)).append("\t").append(subMax(i, number));
				sb.append("\t").append(subMedian(i, number)).append("\n");
			}
		} catch (Exception e) {
			sb.append(e.getMessage());
		}
		System.out.println(sb.toString());
	}

}
