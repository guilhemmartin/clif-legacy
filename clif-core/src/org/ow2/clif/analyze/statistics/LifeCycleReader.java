/*
 * CLIF is a Load Injection Framework
 * Copyright (C) 2005 France Telecom R&D
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 *
 * CLIF $Name:  $
 *
 * Contact: clif@ow2.org
 */
package org.ow2.clif.analyze.statistics;

import org.ow2.clif.storage.api.BladeEvent;
import org.ow2.clif.storage.api.LifeCycleEvent;
import org.ow2.clif.supervisor.api.ClifException;

import java.util.Iterator;
import java.util.List;

/**
 * @author Guy Vachet
 */
public class LifeCycleReader implements Constants {
	// verbose mode for verification
	private static final boolean VERBOSE = false;
	private List<BladeEvent> lifeCycleEvents = null;

	public LifeCycleReader(BladeStoreReader bladeReader) throws ClifException {
		lifeCycleEvents = bladeReader.getAllEvents(LIFECYCLE_EVENT_TYPE_LABEL);
	}

	private long getTime(int stateCode) {
		long time = Long.MIN_VALUE;
		LifeCycleEvent lifeCycleEvent;
		for (Iterator<BladeEvent> it = lifeCycleEvents.iterator(); it.hasNext();) {
			lifeCycleEvent = (LifeCycleEvent) it.next();
			if (lifeCycleEvent.getStateId() == stateCode) {
				time = lifeCycleEvent.getDate();
				break;
			}
		}
		if (VERBOSE)
			System.out.println("State " + stateCode + " at time = \t" + time);
		return time;
	}

	/**
	 * @return beginning of elapsed time
	 */
	public long getMinTime() {
		long time = getTime(BLADE_STATE_RUNNING_CODE);
		return time < 0 ? getTime(BLADE_STATE_INITIALIZED_CODE) : time;
	}

	/**
	 * @return end of elapsed time
	 */
	public long getMaxTime() {
		long time = getTime(BLADE_STATE_COMPLETED_CODE);
		return time < 0 ? getTime(BLADE_STATE_STOPPING_CODE) : time;
	}

}
