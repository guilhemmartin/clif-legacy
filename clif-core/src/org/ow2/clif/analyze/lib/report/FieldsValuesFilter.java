/*
 * CLIF is a Load Injection Framework
 * Copyright (C) 2017 Orange SA
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 *
 * Contact: clif@ow2.org
 */
package org.ow2.clif.analyze.lib.report;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import org.ow2.clif.analyze.lib.graph.FieldFilterAndOp;
import org.ow2.clif.storage.api.BladeEvent;


/**
 * Constants and utility methods for handling filters on field values.
 * 
 * @author Bruno Dillenseger
 */
public class FieldsValuesFilter
{

	// filter operators
	public static final String OPERATOR_EQUALS                 = "equals";
	public static final String OPERATOR_EQUALS_NOT             = "equals not";
	public static final String OPERATOR_EQUALS_REGEX           = "equals regex";
	public static final String OPERATOR_EQUALS_NOT_REGEX       = "equals not regex";
	public static final String OPERATOR_CONTAINS               = "contains";
	public static final String OPERATOR_CONTAINS_NOT           = "contains not";
	public static final String OPERATOR_BEGINS_WITH            = "begins with";
	public static final String OPERATOR_BEGINS_NOT_WITH        = "begins not with";
	public static final String OPERATOR_ENDS_WITH              = "ends with";
	public static final String OPERATOR_ENDS_NOT_WITH          = "ends not with";
	public static final String OPERATOR_GREATER_THAN           = "greater than";
	public static final String OPERATOR_LESS_THAN              = "less than";
	public static final String OPERATOR_GREATER_THAN_OR_EQUALS = "greater than or equals";
	public static final String OPERATOR_LESS_THAN_OR_EQUALS    = "less than or equals";

	static public final String[] OPERATORS = {
		FieldsValuesFilter.OPERATOR_EQUALS,
		FieldsValuesFilter.OPERATOR_EQUALS_NOT,
		FieldsValuesFilter.OPERATOR_EQUALS_REGEX,
		FieldsValuesFilter.OPERATOR_EQUALS_NOT_REGEX,
		FieldsValuesFilter.OPERATOR_CONTAINS,
		FieldsValuesFilter.OPERATOR_CONTAINS_NOT,
		FieldsValuesFilter.OPERATOR_BEGINS_WITH,
		FieldsValuesFilter.OPERATOR_BEGINS_NOT_WITH,
		FieldsValuesFilter.OPERATOR_ENDS_WITH,
		FieldsValuesFilter.OPERATOR_ENDS_NOT_WITH,
		FieldsValuesFilter.OPERATOR_GREATER_THAN,
		FieldsValuesFilter.OPERATOR_GREATER_THAN_OR_EQUALS,
		FieldsValuesFilter.OPERATOR_LESS_THAN,
		FieldsValuesFilter.OPERATOR_LESS_THAN_OR_EQUALS
	};

	public enum LogicalEnum {
		AND, OR
	}

	private LogicalEnum logicalOp;
	private List<FieldFilterAndOp> filterList;


	static public List<BladeEvent> fieldFilter(
		List<BladeEvent> bladeEventList,
		String field,
		String filterValue,
		String filterOperator)
	{
		List<BladeEvent> result = new ArrayList<BladeEvent>(bladeEventList.size());
		Double filterValueAsDouble = null;
		try
		{
			filterValueAsDouble = new Double(filterValue);
		}
		catch (NumberFormatException ex)
		{
			// nothing to do
		}
		Pattern regex = null;
		if (filterOperator.equals(FieldsValuesFilter.OPERATOR_EQUALS_REGEX)
			|| filterOperator.equals(FieldsValuesFilter.OPERATOR_EQUALS_NOT_REGEX))
		{
			regex = Pattern.compile(filterValue);
		}
		boolean selected;
		for (BladeEvent event : bladeEventList)
		{
			Object fieldValue = event.getFieldValue(field);
			selected = false;
			if (filterOperator.equals(FieldsValuesFilter.OPERATOR_EQUALS_REGEX)
				|| filterOperator.equals(FieldsValuesFilter.OPERATOR_EQUALS_NOT_REGEX))
			{		
				selected = isEventSelected(fieldValue.toString(), regex, filterOperator);
			}
			else if (! (fieldValue instanceof Number)
				|| filterOperator.equals(FieldsValuesFilter.OPERATOR_CONTAINS)
				|| filterOperator.equals(FieldsValuesFilter.OPERATOR_CONTAINS_NOT)
				|| filterOperator.equals(FieldsValuesFilter.OPERATOR_BEGINS_WITH)
				|| filterOperator.equals(FieldsValuesFilter.OPERATOR_BEGINS_NOT_WITH)
				|| filterOperator.equals(FieldsValuesFilter.OPERATOR_ENDS_WITH)
				|| filterOperator.equals(FieldsValuesFilter.OPERATOR_ENDS_NOT_WITH))
			{
				selected = isEventSelected(fieldValue.toString(), filterValue, filterOperator);
			} 
			else if (fieldValue instanceof Number && filterValueAsDouble != null)
			{
				selected = isEventSelected(
					((Number)fieldValue).doubleValue(),
					filterValueAsDouble,
					filterOperator);
			}
			if (selected)
			{
				result.add(event);
			}
		}
		return result;
	}


	static private boolean isEventSelected(double fieldValue, double filterValue, String filterOperator)
	{
		if (filterOperator.equals(FieldsValuesFilter.OPERATOR_EQUALS))
		{
			return fieldValue == filterValue;
		}
		else if(filterOperator.equals(FieldsValuesFilter.OPERATOR_EQUALS_NOT))
		{
			return fieldValue != filterValue;
		}
		else if (filterOperator.equals(FieldsValuesFilter.OPERATOR_GREATER_THAN))
		{
			return fieldValue > filterValue;
		}
		else if (filterOperator.equals(FieldsValuesFilter.OPERATOR_LESS_THAN))
		{
			return fieldValue < filterValue;
		}
		else if (filterOperator.equals(FieldsValuesFilter.OPERATOR_GREATER_THAN_OR_EQUALS))
		{
			return fieldValue >= filterValue;
		}
		else if (filterOperator.equals(FieldsValuesFilter.OPERATOR_LESS_THAN_OR_EQUALS))
		{
			return fieldValue <= filterValue;
		}
		else
		{
			return false;
		}
	}


	static private boolean isEventSelected(String fieldValue, Pattern pattern, String filterOperator)
	{
		Matcher matcher = pattern.matcher(fieldValue);
		if (matcher.matches())
		{
			if (filterOperator.equals(FieldsValuesFilter.OPERATOR_EQUALS_REGEX))
			{
				return true;
			}
		}
		else if (filterOperator.equals(FieldsValuesFilter.OPERATOR_EQUALS_NOT_REGEX))
		{
			return true;
		}
		return false;
	}


	static private boolean isEventSelected(String fieldValue, String filterValue, String filterOperator)
	{
		if (filterOperator.equals(FieldsValuesFilter.OPERATOR_EQUALS))
		{
			return fieldValue.equals(filterValue);
		}
		else if(filterOperator.equals(FieldsValuesFilter.OPERATOR_EQUALS_NOT))
		{
			return ! fieldValue.equals(filterValue);
		}
		else if (filterOperator.equals(FieldsValuesFilter.OPERATOR_CONTAINS))
		{
			return fieldValue.contains(filterValue);
		}
		else if (filterOperator.equals(FieldsValuesFilter.OPERATOR_CONTAINS_NOT))
		{
			return ! fieldValue.contains(filterValue);
		}
		else if (filterOperator.equals(FieldsValuesFilter.OPERATOR_BEGINS_WITH))
		{
			return fieldValue.startsWith(filterValue);
		}
		else if (filterOperator.equals(FieldsValuesFilter.OPERATOR_BEGINS_NOT_WITH))
		{
			return ! fieldValue.startsWith(filterValue);
		}
		else if (filterOperator.equals(FieldsValuesFilter.OPERATOR_ENDS_WITH))
		{
			return fieldValue.endsWith(filterValue);
		}
		else if (filterOperator.equals(FieldsValuesFilter.OPERATOR_ENDS_NOT_WITH))
		{
			return ! fieldValue.endsWith(filterValue);
		}
		else if (filterOperator.equals(FieldsValuesFilter.OPERATOR_GREATER_THAN))
		{
			return fieldValue.compareTo(filterValue) > 0;
		}
		else if (filterOperator.equals(FieldsValuesFilter.OPERATOR_LESS_THAN))
		{
			return fieldValue.compareTo(filterValue) < 0;
		}
		else if (filterOperator.equals(FieldsValuesFilter.OPERATOR_GREATER_THAN_OR_EQUALS))
		{
			return fieldValue.compareTo(filterValue) >= 0;
		}
		else if (filterOperator.equals(FieldsValuesFilter.OPERATOR_LESS_THAN_OR_EQUALS))
		{
			return fieldValue.compareTo(filterValue) <= 0;
		}
		else
		{
			// should never occur
			return false;
		}
	}


	public FieldsValuesFilter() {
		filterList = new ArrayList<FieldFilterAndOp>();
		logicalOp = LogicalEnum.AND;
	}


	// getters and setters
	public LogicalEnum getLogicalOp() {
		return logicalOp;
	}

	public void setLogicalOp(LogicalEnum _logicalOp) {
		logicalOp = _logicalOp;
	}

	public List<FieldFilterAndOp> getFilterList() {
		return filterList;
	}

	public void setFilterList(List<FieldFilterAndOp> _filterList) {
		filterList = _filterList;
	}

	public void addExpression(FieldFilterAndOp exp){
		filterList.add(exp);
	}

	public boolean isEmpty()
	{
		return filterList.isEmpty();
	}

	@Override
	public String toString()
	{
		StringBuilder result = new StringBuilder();
		if (null != filterList && !filterList.isEmpty())
		{
			Iterator<FieldFilterAndOp> iter = filterList.iterator();
			result.append(iter.next().toString());
			while (iter.hasNext())
			{
				result.append(" ").append(logicalOp).append(" ").append(iter.next());
			}
		}
		return result.toString();
	}


	public String dump() {
		return exportToTXT(3);
	}


	public String exportToTXT(int nbTabs) {
		String tabs = "";
		for (int i = 0 ; i<= nbTabs;i++){
			tabs += "\t";
		}
		String suff = "";
		StringBuffer text = new StringBuffer(tabs + "Fields Values Filter:\n");
		
		if (null == filterList){
			return new String(tabs + "  null\n");
		}
		if (filterList.size() ==0){
			return new String(tabs + "  []\n");
		}

		if (filterList.size() > 1){
			text.append(tabs + "  [" + logicalOp + " {");
			suff = "}";
		}else{
			text.append(tabs + "  [");
		}
		suff = "]\n";
		String pre = "";
		for (FieldFilterAndOp ffo : filterList){
			text.append(pre + ffo.toString());
			pre = ", ";
		}
		text.append(suff);
		return text.toString();
	}
}
