package org.ow2.clif.analyze.lib.report;

import java.util.Arrays;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.SortedSet;
import java.util.TreeMap;
import java.util.TreeSet;
import org.apache.commons.math3.stat.StatUtils;
import org.jdom.Attribute;
import org.jdom.Element;
import org.ow2.clif.analyze.statistics.util.data.ListOfLong;
import org.ow2.clif.analyze.statistics.util.data.LongStatistics;
import org.ow2.clif.analyze.statistics.util.data.StatOnLongs;
import org.ow2.clif.storage.api.BladeEvent;


/**
 * The Class Statistics
 */
public class Statistics {

	// given values
	private String field = null;
	private StatOnLongs stolDate;
	private StatOnLongs stol;
	private long startTime;
	private long endTime;

	private Section section;
	private Dataset dataset;
	private Datasource datasource;


	// computed values
	private int originalEventsNumber            = 0;
	private int timeFilteredEventsNumber        = 0;
	private int fieldsValueFilteredEventsNumber = 0;
	private int statsFilteredEventsNumber       = 0;
	private int finalEventsNumber               = 0;

	private double throughput        = Double.NaN;
	private double minimum           = Double.NaN;
	private double maximum           = Double.NaN;
	private double median            = Double.NaN;
	private double average           = Double.NaN;
	private double standardDeviation = Double.NaN;

	// Constructors

	public Statistics(List<BladeEvent> eventsList, String _field, int eventsNumber) {
		LogAndDebug.tracep("(List<BladeEvent>, \"" + _field + "\", " + eventsNumber + ")");
		field = _field;
		this.originalEventsNumber = eventsNumber;
		finalEventsNumber = (eventsList == null ? 0 : eventsList.size());
		if (finalEventsNumber > 0){
			startTime = eventsList.get(0).getDate();
			endTime   = eventsList.get(finalEventsNumber-1).getDate();
			setThroughput();
		}
		if (null != field){
			createStolAndStoldate(eventsList, field);
			setMinimum();
			setMaximum();
			setMedian();
			setAverage();
			setStdDvt();
		}
		dump(eventsList);
		LogAndDebug.tracem("(List<BladeEvent>, \"" + _field + "\", " + eventsNumber + ")");
	}


	// dump statistics
	String dump() {
		String tabs = "\t\t\t\t";
		StringBuffer text = new StringBuffer("");

		text.append(tabs + "  field: \"" + field + "\"\n");
		text.append(tabs + "  startTime: " + startTime + "\n");
		text.append(tabs + "  endTime: " + endTime + "\n");
		text.append(tabs + "  minimum: " + minimum + "\n");
		text.append(tabs + "  maximum: " + maximum + "\n");
		text.append(tabs + "  average: " + average + "\n");
		text.append(tabs + "  median: " + median + "\n");
		text.append(tabs + "  stdDev: " + standardDeviation + "\n");
		text.append(tabs + "  troughput: " + throughput + "\n");

		if (null == section){
			text.append(tabs + "  section: null\n");
		}else{
			text.append(tabs + "  section: \"" + section + "\"\n");
		}
		if (null == dataset){
			text.append(tabs + "  dataset: null\n");
		}else{
			text.append(tabs + "  dataset: \"" + dataset + "\"\n");
		}
		text.append(tabs + "  datasource: \"" + LogAndDebug.toText(datasource) + "\"\n");

		text.append(tabs + "  stol:     " + toString(stol) + "\n");
		text.append(tabs + "  stolDate: " + toString(stolDate) + "\n");

		// computed values
		text.append(tabs + "  originalEventsNumber: " + originalEventsNumber + "\n");
		text.append(tabs + "  timeFilteredEventsNumber: " + timeFilteredEventsNumber + "\n");
		text.append(tabs + "  fieldsValueFilteredEventsNumber: " + fieldsValueFilteredEventsNumber + "\n");
		text.append(tabs + "  statsFilteredEventsNumber: " + statsFilteredEventsNumber + "\n");
		text.append(tabs + "  finalEventsNumber: " + finalEventsNumber + "\n");

		text.append(tabs + "  -----\n");
		return text.toString();
	}


	// dump statistics
	private void dump(List<BladeEvent> eventsList) {
		LogAndDebug.log("   dump statistics");
		LogAndDebug.log("      field: " + field);
		LogAndDebug.log("     values: " + eventsList);
		if (eventsList.size() >0){
			LogAndDebug.log("      start: " + eventsList.get(0).getDate());
			LogAndDebug.log("        end: " + eventsList.get(finalEventsNumber-1).getDate());
		}
		LogAndDebug.log("  troughput: " + throughput);
		if (null != field){
			LogAndDebug.log("       stol: " + toString(stol));
			LogAndDebug.log("   stolDate: " + toString(stolDate));
			LogAndDebug.log("    minimum: " + minimum);
			LogAndDebug.log("    maximum: " + maximum);
			LogAndDebug.log("    average: " + average);
			LogAndDebug.log("     median: " + median);
			LogAndDebug.log("     stdDev: " + standardDeviation);
		}
		LogAndDebug.log("  -----");
	}


	private String toString(StatOnLongs stol) {
		String txt = "[";
		String prev = "";
		if (null == stol){
			return "[]";
		}
		ListOfLong lol = stol.getData();
		for(Long l : lol){
			txt += prev + Long.toString(l);
			prev = ", ";
		}
		txt += "]";

		return txt;
	}


	// data access methods

	private void createStolAndStoldate(List<BladeEvent> eventsList, String field)
	{
		stolDate = new StatOnLongs();
		stol = new StatOnLongs();
		for (BladeEvent bladeEvent : eventsList)
		{
			Long date = bladeEvent.getDate();
			stolDate.addLong(date);
			if (field != null)
			{
				Object value = bladeEvent.getFieldValue(field);
				if (value instanceof Number)
				{
					stol.addLong(((Number)value).longValue());
				}
				else if (value instanceof Boolean)
				{
					stol.addLong((Boolean)value ?  1 : 0);
				}
				else
				{
					stol.addLong(1);
				}
			}
		}
	}

	// computing methods

	/**
	 * Set minimum 
	 */
	public void setMinimum()
	{
		if (stol != null && stol.getMin() != null)
		{
			minimum = stol.getMin();
		}
		else
		{
			minimum = Double.NaN;
		}
	}


	/**
	 * Set maximum
	 */
	public void setMaximum()
	{
		if (stol != null && stol.getMax() != null)
		{
			maximum = stol.getMax();
		}
		else
		{
			maximum = Double.NaN;
		}
	}

	/**
	 * Set median
	 */
	public void setMedian()
	{
		Long medianTmp = stol.getMedian();
		if (medianTmp == null)
		{
			median = Double.NaN;
		}
		else
		{
			median = medianTmp;
		}
	}


	/**
	 * Set average
	 */
	public void setAverage(){
		this.average = this.stol.getMean();
	}


	/**
	 * Set standard deviation
	 */
	public void setStdDvt(){
		this.standardDeviation = this.stol.getStd();
	}


	private void setThroughput() {
		throughput = computeThroughput();
	}


	// getters and setters

	public StatOnLongs getStolDate()
	{
		return stolDate;
	}

	public void setStolDate(StatOnLongs stolDate)
	{
		this.stolDate = stolDate;
	}


	public StatOnLongs getStol()
	{
		return stol;
	}


	public void setStol(StatOnLongs stol)
	{
		this.stol = stol;
	}


	public double getMinimum()
	{
		return minimum;
	}


	public void setMinimum(double d)
	{
		this.minimum = d;
	}


	public double getMaximum()
	{
		return maximum;
	}


	public void setMaximum(double d)
	{
		this.maximum = d;
	}


	public double getMedian()
	{
		return median;
	}


	public void setMedian(double d)
	{
		this.median = d;
	}


	public double getAverage()
	{
		return average;
	}


	public void setAverage(double d)
	{
		this.average = d;
	}


	public double getStandardDeviation()
	{
		return standardDeviation;
	}


	public void setStandardDeviation(double d)
	{
		this.standardDeviation = d;
	}


	/**
	 * Number of values
	 * 
	 * @return number of values for this 
	 */
	public double getNumberOfValues(){
		return stolDate.size();
	}


	/**
	 * Get throughput
	 * 
	 * @return throughput
	 */
	public double getThroughput(){
		return throughput;
	}


	public double computeThroughput(){
		//calculate throughput per second
		double throughput = finalEventsNumber* 1000/((double)(endTime - startTime));
		return throughput;
	}



	////////////////////////////////////
	// MOVING STAT                 //
	////////////////////////////////////

	/**
	 * Get moving minimum according to time window and the step given
	 * @param timeWindow
	 * @param step
	 * @return moving minimum
	 */
	public Object[][] getMovingMinimum(int timeWindow, int step) {
		Object[][] minValues = null;
		double[] vals = toDoubleArray(this.stol.getData());
		Long[] splitTime = split(toDoubleArray(stolDate.getData()), timeWindow, step);
		minValues = new Object[2][splitTime.length];

		for (int i = 0; i < splitTime.length; i++) {
			minValues[0][i] = timeWindow + i * step;
			if (i == splitTime.length-1)
			{
				minValues[1][i] = StatUtils.min(vals, splitTime[i].intValue(), vals.length-splitTime[i].intValue());
			}
			else
			{
				minValues[1][i] = StatUtils.min(vals, splitTime[i].intValue(), (splitTime[i+1].intValue()-splitTime[i].intValue()));
			}
		}
		return minValues;
	}


	/**
	 * Get moving maximum according to time window and the step given
	 * @param timeWindow
	 * @param step
	 * @return moving maximum
	 */
	public Object[][] getMovingMaximum(int timeWindow, int step) 
	{
		Object[][] maxValues = null;
		double[] vals = toDoubleArray(this.stol.getData());
		Long[] splitTime = split(toDoubleArray(stolDate.getData()), timeWindow, step);
		maxValues = new Object[2][splitTime.length];

		for (int i = 0; i < splitTime.length; i++) {
			maxValues[0][i] = timeWindow + i * step;
			if (i == splitTime.length-1)
			{
				maxValues[1][i] = StatUtils.max(vals, splitTime[i].intValue(), vals.length-splitTime[i].intValue());
			}
			else
			{
				maxValues[1][i] = StatUtils.max(vals, splitTime[i].intValue(), (splitTime[i+1].intValue()-splitTime[i].intValue()));
			}
		}

		return maxValues;
	}


	/**
	 * Get moving average according to time window and the step given
	 * @param timeWindow
	 * @param step
	 * @return moving average
	 */
	public Object[][] getMovingAverages(int timeWindow, int step) 
	{
		Object[][] averageValues = null;
		double[] vals = toDoubleArray(this.stol.getData());
		Long[] splitTime = split(toDoubleArray(stolDate.getData()), timeWindow, step);
		averageValues = new Object[2][splitTime.length];

		for (int i = 0; i < splitTime.length; i++) {
			averageValues[0][i] = timeWindow + i * step;
			if (i == splitTime.length-1)
			{
				averageValues[1][i] = StatUtils.mean(vals, splitTime[i].intValue(), vals.length-splitTime[i].intValue());
			}
			else
			{
				averageValues[1][i] = StatUtils.mean(vals, splitTime[i].intValue(), (splitTime[i+1].intValue()-splitTime[i].intValue()));
			}
		}
		return averageValues;
	}


	/**
	 * Get moving standard deviation according to time window and the step given
	 * @return moving standard deviation
	 */
	public Object[][] getMovingStandardDeviations(int timeWindow, int step) 
	{
		Object[][] stdDevValues = null;
		double[] vals = toDoubleArray(this.stol.getData());
		Long[] splitTime = split(toDoubleArray(stolDate.getData()), timeWindow, step);
		stdDevValues = new Object[2][splitTime.length];

		for (int i = 0; i < splitTime.length; i++) {
			stdDevValues[0][i] = timeWindow + i * step;
			if (i == splitTime.length-1)
			{
				stdDevValues[1][i] = Math.sqrt(StatUtils.variance(vals, splitTime[i].intValue(), (vals.length-splitTime[i].intValue())));
			}
			else
			{
				stdDevValues[1][i] = Math.sqrt(StatUtils.variance(vals, splitTime[i].intValue(), (splitTime[i+1].intValue()-splitTime[i].intValue())));
			}
		}

		for (int i = 0; i < splitTime.length-1; i++) {
			stdDevValues[0][i] = timeWindow + i * step;
			stdDevValues[1][i] = Math.sqrt(StatUtils.variance(vals, splitTime[i].intValue(), (splitTime[i+1].intValue()-splitTime[i].intValue())));
		}
		return stdDevValues;
	}

	/**
	 * Get moving throughput according to time window and the step given
	 * @param timeWindow
	 * @param step
	 * @return moving throughput
	 */
	public Object[][] getMovingThroughput(int timeWindow, int step) 
	{
		// moving throughput per second
		Object[][] throughputValues = null;
		Long[] splitTime = split(toDoubleArray(stolDate.getData()), timeWindow, step);
		throughputValues = new Object[2][splitTime.length];
		double numberOfValues;
		int startTime;
		int endTime;

		for (int i = 0; i < splitTime.length; i++) {
			throughputValues[0][i] = timeWindow + i * step;
			if (i < splitTime.length-1)
			{
				startTime = timeWindow + i * step;
				endTime = timeWindow + 2 * i * step;
				numberOfValues = getNumberOfValues(stolDate.getData(), startTime, endTime);
				throughputValues[1][i] = numberOfValues*1000/(endTime - startTime);
			}
		}
		return throughputValues;
	}


	/**
	 * Get the number of values in interval
	 * @param data
	 * @param startTime
	 * @param endTime
	 * @return number of values in interval
	 */
	private double getNumberOfValues(ListOfLong data, int startTime, int endTime) {
		double numberOfValues = 0;
		for (Long longTmp : data){
			if((longTmp >= startTime) && (longTmp < endTime)){
				numberOfValues ++;
			}
		}
		return numberOfValues;
	}


	public String getField() {
		return field;
	}


	public void setField(String field) {
		this.field = field;
	}


	public Section getSection() {
		return section;
	}


	public void setSection(Section section) {
		this.section = section;
	}

	public Dataset getDataset() {
		return dataset;
	}


	public void setDataset(Dataset dataset) {
		this.dataset = dataset;
	}


	public Datasource getDatasource() {
		return datasource;
	}


	public void setDatasource(Datasource datasource) {
		this.datasource = datasource;
	}


	public long getStartTime() {
		return startTime;
	}


	public long getEndTime() {
		return endTime;
	}



	/**
	 * Get frequencies in a map that contains value for key and frequency for number of occurencies
	 * @param values
	 * @return frequencies for this values
	 */
	public Map<Integer, Integer> frequency(double[] values){
		Map<Integer, Integer> mapResult = new TreeMap<Integer, Integer>();
		int key;
		int frequency;
		for (int i = 0; i < values.length; i++) {
			//give floor value
			key = (int) values[i];
			if(mapResult.get(key) == null){
				mapResult.put(key, 1);
			}
			else{
				frequency = mapResult.get(key);
				mapResult.put(key, frequency + 1);
			}
		}
		return mapResult;
	}


	/**
	 * @param ls LongStatistics
	 * @param n the number of slices
	 * @return value for each slice
	 */
	public double[] rawDataFrequency(LongStatistics ls, int n){
		ls = (LongStatistics) this.getStol();
		return null;
	}


	/**
	 * Get quantiles 
	 * @param n
	 * @param values
	 * @return array that contains the quantiles
	 */
	public double[] quantiles(int n, double[] values) {
		values = (double[])values.clone();
		Arrays.sort(values);
		double[] qtls = new double[n+1];
		for ( int i=0; i<=n; ++i ) {
			qtls[i] = values[((values.length-1)*i)/n];
		}
		return qtls;
	}


	////////////////////////////////////
	// PRIVATE //
	////////////////////////////////////

	/**
	 * Convert long list to double array
	 * 
	 * @param longList
	 * @return double array
	 */
	private double[] toDoubleArray(List<Long> longList){
		int size = longList.size();
		double[] longArray = new double[size];
		Iterator<Long> iterator = longList.iterator();
		int count = 0;
		while (iterator.hasNext()) {
			double currentValue = (double) iterator.next();
			longArray[count] = currentValue;
			count++;
		}
		return longArray;
	}


	/**
	 * Split.
	 * 
	 * @param values the values
	 * @param timeWindow the time window
	 * @param step the step
	 * 
	 * @return the long[] 
	 */
	private Long[] split(double[] values, int timeWindow, int step){
		long prev=0;
		int k=0;
		int i = 0;
		SortedSet<Long> res = new TreeSet<Long>();

		while(i<values.length){
			while( (i<values.length) && ((((Number)values[i]).longValue())<(k*step)+timeWindow) ){
				prev = i;
				i++;
			}
			res.add(prev);
			k++;
			prev = i;
			i++;
		}
		return res.toArray(new Long[0]);
	}


	public void setOriginalEventsNumber(int i) {
		originalEventsNumber = i;
	}


	public int getOriginalEventsNumber(){
		return originalEventsNumber;
	}


	public int getEventNumber(){
		return originalEventsNumber;
	}


	public void setThroughput(double d) {
		this.throughput = d;
	}


	public int getTimeFilteredEventsNumber() {
		return timeFilteredEventsNumber;
	}


	public void setTimeFilteredEventsNumber(int i) {
		this.timeFilteredEventsNumber = i;
	}


	public int getFiledsValueFilteredEventsNumber() {
		return fieldsValueFilteredEventsNumber;
	}


	public void setFieldsValueFilteredEventsNumber(int i) {
		this.fieldsValueFilteredEventsNumber = i;
	}


	public int getStatsFilteredEventsNumber() {
		return statsFilteredEventsNumber;
	}


	public void setStatsFilteredEventsNumber(int i) {
		this.statsFilteredEventsNumber = i;
	}


	public int getFinalEventsNumber() {
		return finalEventsNumber;
	}


	public void setFinalEventsNumber(int finalEventsNumber) {
		this.finalEventsNumber = finalEventsNumber;
	}


	public int getTimeDiscardedEventsNumber()
	{
		return originalEventsNumber - timeFilteredEventsNumber;
	}


	public int getFieldsValuesDiscardedEventsNumber()
	{
		return timeFilteredEventsNumber - fieldsValueFilteredEventsNumber;
	}


	public void setStartTime(long startTime) {
		this.startTime = startTime;
	}


	public void setEndTime(long endTime) {
		this.endTime = endTime;
	}


	// export methods
	public Element exportXML(){

		Element statis =new Element("statistics");

		Element statsminimum = new Element("min");
			Attribute minStatsAttribute= new Attribute("min",String.valueOf(this.getMinimum()));
			statsminimum.setAttribute(minStatsAttribute);
		statis.addContent(statsminimum);

		Element statsmaximum = new Element("max");
			Attribute maxStatsAttribute= new Attribute("max",String.valueOf(this.getMaximum()));
			statsmaximum.setAttribute(maxStatsAttribute);
		statis.addContent(statsmaximum);

		Element statsmean = new Element("mean");
			Attribute meanStatsAttribute= new Attribute("mean",String.valueOf(this.getAverage()));;
			statsmean.setAttribute(meanStatsAttribute);
		statis.addContent(statsmean);

		Element statsmedian = new Element("median");
			Attribute medianStatsAttribute= new Attribute("median",String.valueOf(this.getMedian()));
			statsmedian.setAttribute(medianStatsAttribute);
		statis.addContent(statsmedian);

		Element statsstandardeviation = new Element("std");
			Attribute stdStatsAttribute= new Attribute("std",String.valueOf(this.getStandardDeviation()));
			statsstandardeviation.setAttribute(stdStatsAttribute);
		statis.addContent(statsstandardeviation);

		Element statsnbvalues = new Element("nbValues");
		Attribute OriginalEventsNumberAttribute= new Attribute("nbValues",String.valueOf(this.getOriginalEventsNumber()	));
		Attribute FinalEventsNumberAttribute= new Attribute("nbValues",String.valueOf(this.getFinalEventsNumber()	));				
		Attribute nbValuesUnitAttribute= new Attribute("nbValues","(final/raw)");
		statsnbvalues.setAttribute(OriginalEventsNumberAttribute);
		statsnbvalues.setAttribute(FinalEventsNumberAttribute);
		statsnbvalues.setAttribute(nbValuesUnitAttribute);
		statis.addContent(statsnbvalues);

		return statis;
	}


	public String exportToHtml(int nbTabs) {
		StringBuffer htmlText = new StringBuffer("");
		int timeFilterDiscarded = originalEventsNumber - timeFilteredEventsNumber;
		int valueFilterDiscarded = timeFilteredEventsNumber - fieldsValueFilteredEventsNumber;
// TODO		int statsFilterDiscarded = fieldsValueFilteredEventsNumber - statsFilteredEventsNumber;

		htmlText = new StringBuffer("");
		LogAndDebug.htmlWriteNP(htmlText, "<table class=\"stats\">");
		LogAndDebug.htmlTabsp();
		LogAndDebug.htmlWriteNP(htmlText, "<caption>Statistics</caption>");
		LogAndDebug.htmlWriteNP(htmlText, "<tr>");
		LogAndDebug.htmlTabsp();
		LogAndDebug.htmlWriteNP(htmlText, "<th colspan=\"2\">Samples</th>");
		LogAndDebug.htmlWriteNP(htmlText, "<td>" + finalEventsNumber + " (out of " + originalEventsNumber + ")</td>");
		LogAndDebug.htmlTabsm();
		LogAndDebug.htmlWriteNP(htmlText, "</tr>");
		LogAndDebug.htmlWriteNP(htmlText, "<tr>");
		LogAndDebug.htmlTabsp();
		LogAndDebug.htmlWriteNP(htmlText, "<th rowspan=\"3\">Discarded samples</th>");
		LogAndDebug.htmlWriteNP(htmlText, "<th>time filter</th>");
		LogAndDebug.htmlWriteNP(htmlText, "<td>" + timeFilterDiscarded + "</td>");
		LogAndDebug.htmlTabsm();
		LogAndDebug.htmlWriteNP(htmlText, "</tr>");
		LogAndDebug.htmlWriteNP(htmlText, "<tr>");
		LogAndDebug.htmlTabsp();
		LogAndDebug.htmlWriteNP(htmlText, "<th>fields values filter</th>");
		LogAndDebug.htmlWriteNP(htmlText, "<td>" + valueFilterDiscarded + "</td>");
		LogAndDebug.htmlTabsm();
		LogAndDebug.htmlWriteNP(htmlText, "</tr>");
		LogAndDebug.htmlWriteNP(htmlText, "<tr>");
		LogAndDebug.htmlTabsp();
		LogAndDebug.htmlWriteNP(htmlText, "<th>total discarded</th>");
		LogAndDebug.htmlWriteNP(htmlText, "<td>" + (originalEventsNumber - finalEventsNumber) + "</td>");
		LogAndDebug.htmlTabsm();
		LogAndDebug.htmlWriteNP(htmlText, "</tr>");
		if (finalEventsNumber > 0)
		{
			LogAndDebug.htmlWriteNP(htmlText, "<tr>");
			LogAndDebug.htmlTabsp();
			LogAndDebug.htmlWriteNP(htmlText, "<th colspan=\"2\">Throughput (samples/second)</th>");
			LogAndDebug.htmlWriteNP(htmlText, "<td>" + Math.rint(throughput * 100) / 100f + "</td>");
			LogAndDebug.htmlTabsm();
			LogAndDebug.htmlWriteNP(htmlText, "</tr>");
			if (dataset.isPerformStatAnalysis())
			{
				LogAndDebug.htmlWriteNP(htmlText, "<tr><th colspan=\"2\">Minimum</th><td>" + Math.round(minimum) + "</td></tr>");
				LogAndDebug.htmlWriteNP(htmlText, "<tr><th colspan=\"2\">Maximum</th><td>" + Math.round(maximum) + "</td></tr>");
				LogAndDebug.htmlWriteNP(htmlText, "<tr><th colspan=\"2\">Average</th><td>" + Math.rint(average * 100) / 100f + "</td></tr>");
				LogAndDebug.htmlWriteNP(htmlText, "<tr><th colspan=\"2\">Median</th><td>" + Math.round(median) + "</td></tr>");
				LogAndDebug.htmlWriteNP(htmlText, "<tr><th colspan=\"2\">Standard Deviation</th><td>" + Math.rint(standardDeviation * 100) / 100f + "</td></tr>");
			}
			else
			{
				LogAndDebug.htmlWriteNP(htmlText, "<tr><td colspan=\"2\"><span class=\"notice\">No statistical analysis performed</span></td></tr>");
			}
		}
		LogAndDebug.htmlTabsm();
		LogAndDebug.htmlWriteNP(htmlText, "</table>");
		return htmlText.toString();
	}

	
	public Element exportToXML(int i) {
		return null;
	}
	
	
	Element statisticXml( ) {		
		
		Element statis =new Element("statistics");		
		
		Element statsminimum = new Element("minimum");
		statsminimum.setText(String.valueOf(getMinimum()));
		statis.addContent(statsminimum);

		Element statsmaximum = new Element("maximum");
		statsmaximum.setText(String.valueOf(getMaximum()));
		statis.addContent(statsmaximum);

		Element statsmean = new Element("mean");;
		statsmean.setText(String.valueOf(getAverage()));
		statis.addContent(statsmean);

		Element statsmedian = new Element("median");
		statsmedian.setText(String.valueOf(getMedian()));
		statis.addContent(statsmedian);

		Element statsstandardeviation = new Element("standarddeviation");
		statsstandardeviation.setText(String.valueOf(getStandardDeviation()));
		statis.addContent(statsstandardeviation);

		Element statspopulation = new Element("population");
		statspopulation.setText(String.valueOf(getOriginalEventsNumber()));
		statis.addContent(statspopulation);
		
//		Element statsrejected = new Element("rejected");
//		int rejectedNumber=getOriginalEventsNumber()-getFinalEventsNumber();
//		statsrejected.setText(String.valueOf(rejectedNumber));
//		statis.addContent(statsrejected);

		Element statsthroughput = new Element("throughput");					
		statsthroughput.setText(String.valueOf(getThroughput())+" (events per second)");
		statis.addContent(statsthroughput);		
		return statis;
	}

	public String exportToTxt(int nbTabs) {
		String tabs = "";
		for (int i = 0 ; i< nbTabs;i++){
			tabs += "\t";
		}
		StringBuffer text = new StringBuffer();
		StringBuffer name = new StringBuffer();
		if (null == dataset){
			if (null == datasource){
				name.append("<no dataset or datasource for these statistics>");
			}else{
				name.append(datasource.getName());
			}
		}else{
			switch (dataset.getDatasetType()){
			case SIMPLE_DATASET:
				name.append(dataset.getName());
				break;
			case MULTIPLE_DATASET:
				name.append(datasource.getName());
				break;
			case AGGREGATE_DATASET:
				name.append(dataset.getName());
				break;
			default:
				name.append("<unknown name>");
			}
		}
		name.append("\n");

		if (finalEventsNumber == 0){
			text.append(tabs + "No statistics.\n");
		}else{
			text.append(tabs + "Statistics for " + name);
		}
		if (0 != finalEventsNumber){
			text.append(tabs + "  number of values: " + 	 Integer.toString(finalEventsNumber) + "/" + Integer.toString(originalEventsNumber) + "\n");
		}
		if (! Double.isNaN(throughput)){
			text.append(tabs + "  throughput: " + (Math.rint(throughput * 100) / 100d) + "\n");
		}
		if (! Double.isNaN(minimum)){
			text.append(tabs + "  minimum: " + Math.round(minimum) + "\n");
		}
		if (! Double.isNaN(maximum)){
			text.append(tabs + "  maximum: " + Math.round(maximum) + "\n");
		}
		if (! Double.isNaN(median)){
			text.append(tabs + "  median: " + Math.round(median) + "\n");
		}
		if (! Double.isNaN(average)){
			text.append(tabs + "  average: " + (Math.rint(average * 100) / 100d) + "\n");
		}
		if (! Double.isNaN(standardDeviation)){
			text.append(tabs + "  standard deviation: " + (Math.rint(standardDeviation * 100) / 100d) + "\n");
		}
		text.append("\n");
		return text.toString();
	}


	public String getname() {
		return this.datasource.getName();

	}

}
