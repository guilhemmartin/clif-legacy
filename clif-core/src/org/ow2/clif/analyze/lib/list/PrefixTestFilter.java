/*
* CLIF is a Load Injection Framework
* Copyright (C) 2006 France Telecom
*
* This library is free software; you can redistribute it and/or
* modify it under the terms of the GNU Lesser General Public
* License as published by the Free Software Foundation; either
* version 2 of the License, or (at your option) any later version.
*
* This library is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
* Lesser General Public License for more details.
*
* You should have received a copy of the GNU Lesser General Public
* License along with this library; if not, write to the Free Software
* Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*
* CLIF $Name$
*
* Contact: clif@ow2.org
*/
package org.ow2.clif.analyze.lib.list;

import org.ow2.clif.storage.api.TestDescriptor;
import org.ow2.clif.storage.api.TestFilter;

/**
 * Instances of this class filter tests according to a given name prefix.  
 * 
 * @author Bruno Dillenseger
 */
public class PrefixTestFilter implements TestFilter
{
	private String prefix = null;

	/**
	 * Creates a new prefix name filter for test descriptors with a null prefix,
	 * which means all test names will match this new filter.
	 */
	public PrefixTestFilter()
	{
		this(null);
	}

	/**
	 * Creates a new prefix name filter for test descriptors with the given prefix string.
	 * @param prefix the prefix that test name must match.
	 * If null, all test names will match this new filter.
	 */
	public PrefixTestFilter(String prefix)
	{
		this.prefix = prefix;
	}

	/**
	 * Accepts all test names matching this filter's prefix attribute.
	 * @param desc the test descriptor to accept or reject.
	 * @return true if the given test's name matches the filter's prefix,
	 * false otherwise.
	 */
	@Override
	public boolean accept(TestDescriptor desc)
	{
		return prefix == null | desc.getName().startsWith(prefix);
	}
}
