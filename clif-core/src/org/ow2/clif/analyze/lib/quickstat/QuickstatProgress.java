/*
* CLIF is a Load Injection Framework
* Copyright (C) 2012 France Telecom R&D
*
* This library is free software; you can redistribute it and/or
* modify it under the terms of the GNU Lesser General Public
* License as published by the Free Software Foundation; either
* version 2 of the License, or (at your option) any later version.
*
* This library is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
* Lesser General Public License for more details.
*
* You should have received a copy of the GNU Lesser General Public
* License along with this library; if not, write to the Free Software
* Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*
* Contact: clif@ow2.org
*/

package org.ow2.clif.analyze.lib.quickstat;

/**
 * Interface for monitoring, and possibly stop,
 * the progress of the simple statistical report computation.
 * @author Bruno Dillenseger
 */
public interface QuickstatProgress
{
	/**
	 * Called to give the target test name, and an estimation
	 * of the total amount of computation
	 * @param testId the target test name
	 * @param fullSize the estimated amount of work
	 */
	public void quickstatStarted(String testId, int fullSize);

	/**
	 * Called to indicate that the statistical report computation
	 * is complete. 
	 */
	public void quickstatComplete();

	/**
	 * Called to check whether the computation should be canceled.
	 */
	public boolean quickstatIsCanceled();

	/**
	 * Called to give the amount of work achieved up to now,
	 * as well as a description of current subtask.
	 */
	public void quickstatProgress(String subtask, int done);
}
