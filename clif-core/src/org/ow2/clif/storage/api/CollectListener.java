/*
* CLIF is a Load Injection Framework
* Copyright (C) 2005 France Telecom R&D
*
* This library is free software; you can redistribute it and/or
* modify it under the terms of the GNU Lesser General Public
* License as published by the Free Software Foundation; either
* version 2 of the License, or (at your option) any later version.
*
* This library is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
* Lesser General Public License for more details.
*
* You should have received a copy of the GNU Lesser General Public
* License along with this library; if not, write to the Free Software
* Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*
* CLIF $Name: not supported by cvs2svn $
*
* Contact: clif@ow2.org
*/
package org.ow2.clif.storage.api;

/**
 * This interface is dedicated to monitoring the progress of test results/data collection.
 * The client contract is the following:
 * <ul>
 * <li> first call collectStart method to provide the listener with the test identifier
 * concerned by this collect and the total number of bytes to be collected
 * <li> then, blades must be collected in sequence. For each blade:
 *   <ul>
 *   <li> first call bladeCollectStart method to provide the listener with the blade
 * identifier and the number of bytes to be collected for this blade
 *   <li> then repeatedly invoke the listener's progress method to tell the total number
 * of bytes collected for this blade
 *   </ul>
 * <li> the client should invoke as much as reasonable and relevant the listener's
 * isCanceled methods to make sure that neither current blade collect nor the full
 * collect has not been canceled.
 * <li> at the end of the collect, the done() method must be called to tell the listener
 * the collect is complete.
 * </ul>
 * @author Bruno Dillenseger
 */
public interface CollectListener
{
	/**
	 * Called at the collect beginning.  
	 * @param testId the identifier of the test to be collected
	 * @param size the total number of bytes to be collected
	 */
	public void collectStart(String testId, long size);

	/**
	 * Called before collecting test results from a blade.
	 * @param bladeId the identifier of the blade about to be collected 
	 * @param size the total number of bytes to collect from the given blade
	 */
	public void bladeCollectStart(String bladeId, long size);

	/**
	 * Called an arbitrary number of times when collecting test results from a blade
	 * to give current progress
	 * @param bladeId the identifier of the blade test results are being collected from
	 * @param done number of bytes already collected (cannot decrease, nor be
	 * greater then the size given in the preliminary bladeCollectStart call)
	 */
	public void progress(String bladeId, long done);

	/**
	 * Called when the full collect is complete.
	 */
	public void done();

	/**
	 * Called an arbitrarily number of times at arbitrary moments during a collect
	 * to ensure the collect must go on or be canceled.
	 * @return true if the full collect must be canceled, false to go on.
	 */
	public boolean isCanceled();

	/**
	 * Called an arbitrarily number of times at arbitrary moments during a collect
	 * to ensure the collect must go on or be canceled for the given blade.
	 * @param bladeId the identifier of the considered blade
	 * @return true if the full collect must be canceled for the given blade,
	 * false to go on.
	 */
	public boolean isCanceled(String bladeId);
}
