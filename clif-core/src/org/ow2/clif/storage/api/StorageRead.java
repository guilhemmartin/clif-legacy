/*
* CLIF is a Load Injection Framework
* Copyright (C) 2005 France Telecom R&D
*
* This library is free software; you can redistribute it and/or
* modify it under the terms of the GNU Lesser General Public
* License as published by the Free Software Foundation; either
* version 2 of the License, or (at your option) any later version.
*
* This library is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
* Lesser General Public License for more details.
*
* You should have received a copy of the GNU Lesser General Public
* License along with this library; if not, write to the Free Software
* Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*
* CLIF $Name: not supported by cvs2svn $
*
* Contact: clif@ow2.org
*/

package org.ow2.clif.storage.api;

import org.ow2.clif.supervisor.api.ClifException;

import java.io.Serializable;
import java.util.Properties;


/**
 * This interface specifies the methods to browse test runs and access to tests measurements/results
 * @author Bruno Dillenseger
 */
public interface StorageRead
{
	// Fractal interface name
	static public final String STORAGE_READ = "Storage read";

	/**
	 * Gets a list of test runs matching a given filter.
	 * @param filter the test run filter. If null, all available test runs are returned.
	 * @return an array of test run descriptors
	 */
	public TestDescriptor[] getTests(TestFilter filter)
	throws ClifException;

	/**
	 * Gets the test plan definition for a given test run name.
	 * @param filter retains only blades (injectors, probes) accepted by this filter. If null,
	 * all blades are retained.
	 * @param testName the test run name
	 * @return an array of descriptors for blades composing the test run and accepted by the
	 * optional filter.
	 */
	public BladeDescriptor[] getTestPlan(String testName, BladeFilter filter)
	throws ClifException;

	/**
	 * Gets the java system properties for the given blade from the given test run
	 * @param testName the test run name
	 * @param bladeId the blade identifier
	 * @return the Java system properties for the given blade
	 * @see TestDescriptor#getName()
	 * @see BladeDescriptor#getId()
	 */
	public Properties getBladeProperties(String testName, String bladeId)
	throws ClifException;

	/**
	 * Gets the labels of fields hold by a given event type.
	 * @param testName the test run name
	 * @param bladeId the blade identifier from the given test run
	 * @param eventTypeLabel the label of the event type
	 * @return an array of event field labels provided by the given event type
	 */
	public String[] getEventFieldLabels(String testName, String bladeId, String eventTypeLabel);

	/**
	 * Creates an event iterator.
	 * @param testName the test name to retrieve events from
	 * @param bladeId the blade identifier in this test to retrieve events from
	 * @param eventTypeLabel the type label of the retrieved events
	 * @param filter the filter object to be used for event selection. If null, the iterator
	 * will return all events. The filter object may throw a NoMoreEvent exception, in which
	 * case current iterator step stops and currently selected events are returned.
	 * @return the iterator key to be used to iterate on getting events, and then to discard
	 * the iterator when done
	 * @throws ClifException the given test, blade, or event type could not be found
	 * @see #getNextEvents(Serializable, int)
	 * @see #closeEventIterator(Serializable)
	 */
	public Serializable getEventIterator(
		String testName,
		String bladeId,
		String eventTypeLabel,
		EventFilter filter)
	throws ClifException;

	/**
	 * Gets next events from the given event iterator.
	 * @param iteratorKey the key for the target event iterator
	 * @param count the number of event to get (at most)
	 * @return selected events
	 * @throws ClifException
	 * @see #getEventIterator(String, String, String, EventFilter)
	 * @see #closeEventIterator(Serializable)
	 */
	public BladeEvent[] getNextEvents(Serializable iteratorKey, int count)
		throws ClifException;

	/**
	 * Discards the iterator associated to the given key, possibly releasing resources. 
	 * @param iteratorKey the key for the target event iterator
	 */
	public void closeEventIterator(Serializable iteratorKey);

	/**
	 * Retrieves a given number of events matching a given filter, from a given index.
	 * @param testName the test name to retrieve events from
	 * @param bladeId the blade identifier in this test to retrieve events from
	 * @param eventTypeLabel the type label of the retrieved events
	 * @param filter the filter object to be used for event selection.
	 * If null, all events are selected. If the filter throws a NoMoreEvent exception,
	 * the selection process stops, and currently selected events are returned.
	 * @param fromIndex the index of the first event to be considered (in other words, events
	 * in range 0..fromIndex-1 are ignored). If negative and count parameter is greater than zero,
	 * then the latest events will be retrieved.
	 * @param count the maximum number of events to retrieve. If negative, the number of retrieved
	 * events is not limited.
	 * @return selected blade events
	 */
	public BladeEvent[] getEvents(
		String testName,
		String bladeId,
		String eventTypeLabel,
		EventFilter filter,
		long fromIndex,
		int count)
	throws ClifException;

	/**
	 * Counts the number of available events matching a given filter.
 	 * @param testName the test name to retrieve events from
	 * @param bladeId the blade identifier in this test to retrieve events from
	 * @param eventTypeLabel the type label of the retrieved events
	 * @param filter the filter object to be used for event selection. If null, all events
	 * are counted. Counting stops before completion if the filter object throws a NoMoreEvent
	 * exception, in which case count value before exception is returned.
	 * @return the number of available blade events matching the provided filter.
	 */
	public long countEvents(
		String testName,
		String bladeId,
		String eventTypeLabel,
		EventFilter filter)
	throws ClifException;
}
