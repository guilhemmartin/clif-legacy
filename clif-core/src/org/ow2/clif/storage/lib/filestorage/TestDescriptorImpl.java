/*
* CLIF is a Load Injection Framework
* Copyright (C) 2005, 2011 France Telecom R&D
*
* This library is free software; you can redistribute it and/or
* modify it under the terms of the GNU Lesser General Public
* License as published by the Free Software Foundation; either
* version 2 of the License, or (at your option) any later version.
*
* This library is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
* Lesser General Public License for more details.
*
* You should have received a copy of the GNU Lesser General Public
* License along with this library; if not, write to the Free Software
* Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*
* Contact: clif@ow2.org
*/

package org.ow2.clif.storage.lib.filestorage;

import org.ow2.clif.storage.api.TestDescriptor;
import java.io.File;
import java.io.IOException;
import java.util.Date;
import java.util.Calendar;
import java.text.DateFormat;


/**
 * Gathers description elements of a test:
 * test name (aka test run id), execution date 
 * @author Bruno Dillenseger
 */
public class TestDescriptorImpl implements TestDescriptor
{
	private static final long serialVersionUID = -965512042386095330L;
	protected String name;
	protected Date date;


	public TestDescriptorImpl(File testDir)
		throws IOException
	{
		name = testDir.getName();
		Calendar calendar = Calendar.getInstance();
		calendar.setTimeInMillis(testDir.lastModified());
		date = calendar.getTime();
	}


	@Override
	public String toString()
	{
		return getName() + ", " + DateFormat.getDateTimeInstance().format(date);
	}


	//////////////////////////////
	// TestDescriptor interface //
	//////////////////////////////


	@Override
	public String getName()
	{
		return name;
	}


	@Override
	public Date getDate()
	{
		return date;
	}
}

