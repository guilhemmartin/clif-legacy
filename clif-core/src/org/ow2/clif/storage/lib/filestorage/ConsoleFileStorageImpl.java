/*
* CLIF is a Load Injection Framework
* Copyright (C) 2003-2005, 2008, 2010-2013 France Telecom R&D
* Copyright (C) 2003 INRIA
*
* This library is free software; you can redistribute it and/or
* modify it under the terms of the GNU Lesser General Public
* License as published by the Free Software Foundation; either
* version 2 of the License, or (at your option) any later version.
*
* This library is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
* Lesser General Public License for more details.
*
* You should have received a copy of the GNU Lesser General Public
* License along with this library; if not, write to the Free Software
* Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*
* Contact: clif@ow2.org
*/

package org.ow2.clif.storage.lib.filestorage;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.Serializable;
import java.net.Socket;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import org.objectweb.fractal.api.control.BindingController;
import org.ow2.clif.console.lib.ClifDeployDefinition;
import org.ow2.clif.console.lib.TestPlanReader;
import org.ow2.clif.console.lib.TestPlanWriter;
import org.ow2.clif.storage.api.BladeDescriptor;
import org.ow2.clif.storage.api.BladeEvent;
import org.ow2.clif.storage.api.BladeFilter;
import org.ow2.clif.storage.api.CollectListener;
import org.ow2.clif.storage.api.EventFilter;
import org.ow2.clif.storage.api.StorageAdmin;
import org.ow2.clif.storage.api.StorageProxyAdmin;
import org.ow2.clif.storage.api.StorageRead;
import org.ow2.clif.storage.api.TestDescriptor;
import org.ow2.clif.storage.api.TestFilter;
import org.ow2.clif.supervisor.api.ClifException;
import org.ow2.clif.util.UniqueKey;

/**
 * Part of the storage.
 * This part is deployed in the console
 *
 * @author Julien Buret
 * @author Nicolas Droze
 * @author Bruno Dillenseger
 */
public class ConsoleFileStorageImpl
	implements StorageAdmin, BindingController, StorageRead
{
	protected Map<String,StorageProxyAdmin> distributedStorage = new Hashtable<String,StorageProxyAdmin>();
	protected BufferedWriter test = null;
	protected String testDirname = null;
	protected Serializable testId = null;
	// the StorageRead interface is delegated to this object
	protected StorageRead readerImpl;


	/////////////////////////////////
	// interface BindingController //
	/////////////////////////////////


	public Object lookupFc(String clientItfName)
	{
		if (clientItfName.startsWith(StorageProxyAdmin.STORAGEPROXY_ADMIN))
		{
			return distributedStorage.get(clientItfName);
		}
		return null;
	}


	public void bindFc(String clientItfName, Object serverItf)
	{
		if (clientItfName.startsWith(StorageProxyAdmin.STORAGEPROXY_ADMIN))
		{
			distributedStorage.put(clientItfName, (StorageProxyAdmin)serverItf);
		}
	}


	public void unbindFc(String clientItfName)
	{
		if (clientItfName.startsWith(StorageProxyAdmin.STORAGEPROXY_ADMIN))
		{
			distributedStorage.remove(clientItfName);
		}
	}


	public String[] listFc()
	{
		return distributedStorage.keySet().toArray(new String[distributedStorage.size()]);
	}


	////////////////////////////
	// interface StorageAdmin //
	////////////////////////////


	/**
	 * Inform the storage system the beginning of a new test
	 * @param testPlan Map of scenario deployment definitions, indexed by a unique identifier
	 * @see org.ow2.clif.deploy.DeployDefinition
	 */
	public void newTest(
		Serializable testId,
		Map<String,ClifDeployDefinition> testPlan)
	throws ClifException
	{
		this.testId = testId;
		this.testDirname = FileStorageCommons.newTestDir(testId, null).getPath();
		try
		{
			if (testPlan != null)
			{
				TestPlanWriter.write2prop(
					new FileOutputStream(testDirname + TestPlanReader.FILE_EXT),
					testPlan);
			}
		}
		catch (Exception e)
		{
			throw new ClifException("Can't create new test run " + testId, e);
		}
	}


	public void collect(Serializable selBladesId, CollectListener listener)
	{
	    Iterator<StorageProxyAdmin> list = distributedStorage.values().iterator();
	    List<String> selBladesIdList = null;
	    if (selBladesId != null)
	    {
	    	selBladesIdList = Arrays.asList((String[])selBladesId);
	    }
	    Map<String,StorageProxyAdmin> storageProxiesByBladeId = new HashMap<String,StorageProxyAdmin>();
	    Map<String,UniqueKey> collectKeysByBladeId = new HashMap<String,UniqueKey>();
	    long fullCollectSize = 0;
	    StorageProxyAdmin store;
	    String bladeId;
	    UniqueKey key;
		while (list.hasNext())
		{
			store = list.next();
			bladeId = store.getBladeId();
			if (selBladesId == null || selBladesIdList.contains(bladeId))
			{
				key = store.initCollect(testId);
				storageProxiesByBladeId.put(bladeId, store);
				collectKeysByBladeId.put(bladeId, key);
		    	fullCollectSize += store.getCollectSize(key);
		    }
		}
	    if (listener != null)
	    {
	    	listener.collectStart(testId.toString(), fullCollectSize);
	    }
	    Iterator<Map.Entry<String,StorageProxyAdmin>> entryList = storageProxiesByBladeId.entrySet().iterator();
	    byte[] buffer = new byte[FileStorageCollect.BLOCK_SIZE];
	    try
	    {
	    	new File(testDirname).mkdirs();
	        while (entryList.hasNext() && (listener == null || !listener.isCanceled()))
	        {
	        	Map.Entry<String,StorageProxyAdmin> entry = entryList.next();
	            store = entry.getValue();
	            bladeId = entry.getKey();
	            key = collectKeysByBladeId.get(bladeId);
	            long size = store.getCollectSize(key);
	            if (listener != null)
	            {
	            	listener.bladeCollectStart(bladeId, size);
	            }
	            long progress = 0;
	            File directory = new File(testDirname, store.getBladeId());
				if (directory.canWrite() || directory.mkdir())
				{
					FileStorageCollectStep step;
					Socket sock;
					while (
						(listener == null || ! listener.isCanceled() || ! listener.isCanceled(bladeId))
						&& (step = (FileStorageCollectStep) store.collect(key)) != null)
					{
					    File outputFile = new File(directory, step.getFilename());
					    if (!outputFile.exists())
					    {
							sock = new Socket();
							sock.connect(step.getSocketAddress(), 0);
							InputStream in = sock.getInputStream();
							OutputStream out = new FileOutputStream(outputFile);
							int n = in.read(buffer);
							while (
								(listener == null || ! listener.isCanceled() || ! listener.isCanceled(bladeId))
								&& n != -1)
							{
								out.write(buffer, 0, n);
								progress += n;
								if (listener != null)
								{
									listener.progress(bladeId, progress);
								}
								n = in.read(buffer);
							}
							in.close();
							out.close();
							sock.close();
					    }
	                }
	            }
				else if (listener != null)
				{
					listener.progress(bladeId, size);
				}
	        }
	        if (listener != null)
	        {
	        	listener.done();
	        }
	    }
	    catch (Exception ex)
	    {
	        throw new RuntimeException("Could not collect data: " + ex.getLocalizedMessage(), ex);
	    }
	}


	/**
	 * Terminate the storage system
	 */
	public void terminate()
	{
	}


	///////////////////////////
	// interface StorageRead //
	///////////////////////////


	/**
	 * @param filter only tests matching this filter will be retained
	 * @return an array of test description records describing retained tests
	 */
	public TestDescriptor[] getTests(final TestFilter filter)
	throws ClifException
	{
		readerImpl = new FileStorageReader(null, true);
		return readerImpl.getTests(filter);
	}


	public BladeDescriptor[] getTestPlan(String testName, BladeFilter filter)
	throws ClifException
	{
		readerImpl = new FileStorageReader(null, true);
		return readerImpl.getTestPlan(testName, filter);
	}


	public Properties getBladeProperties(String testName, String bladeId)
	throws ClifException
	{
		return readerImpl.getBladeProperties(testName, bladeId);
	}


	public String[] getEventFieldLabels(String testName, String bladeId, String eventTypeLabel)
	{
		return readerImpl.getEventFieldLabels(testName, bladeId, eventTypeLabel);
	}


	public Serializable getEventIterator(
		String testName,
		String bladeId,
		String eventTypeLabel,
		EventFilter filter)
	throws ClifException
	{
		return readerImpl.getEventIterator(testName, bladeId, eventTypeLabel, filter);
	}


	public BladeEvent[] getNextEvents(Serializable iteratorKey, int count)
	throws ClifException
	{
		return readerImpl.getNextEvents(iteratorKey, count);
	}


	public void closeEventIterator(Serializable iteratorKey)
	{
		readerImpl.closeEventIterator(iteratorKey);
	}


	public BladeEvent[] getEvents(
		String testName,
		String bladeId,
		String eventTypeLabel,
		EventFilter filter,
		long fromIndex,
		int count)
	throws ClifException
	{
		return readerImpl.getEvents(testName, bladeId, eventTypeLabel, filter, fromIndex, count);
	}


	public long countEvents(
		String testName,
		String bladeId,
		String eventTypeLabel,
		EventFilter filter)
	throws ClifException
	{
		return readerImpl.countEvents(testName, bladeId, eventTypeLabel, filter);
	}
}
