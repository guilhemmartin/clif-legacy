/*
 * CLIF is a Load Injection Framework
 * Copyright (C) 2005 France Telecom R&D
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 *
 * CLIF
 *
 * Contact: clif@ow2.org
 */
package org.ow2.clif.datacollector.lib;

import java.util.ArrayList;
import java.util.Arrays;

import org.ow2.clif.datacollector.api.DataCollectorFilter;
import org.ow2.clif.scenario.isac.util.BooleanHolder;
import org.ow2.clif.storage.api.ActionEvent;
import org.ow2.clif.storage.api.AlarmEvent;
import org.ow2.clif.storage.api.LifeCycleEvent;
import org.ow2.clif.storage.api.ProbeEvent;
import org.ow2.clif.supervisor.api.ClifException;

/**
 * @author Emmanuel Varoquaux
 * @author Stéphanie Monticelli
 */
public class GenericFilter implements DataCollectorFilter {
	private static final long serialVersionUID = -8354193368431682968L;
	private static final ArrayList<String> ACCEPTED_FIELD_LABELS =
			new ArrayList<String>(Arrays.asList(
				"type",
				"success",
				"duration",
				"comment",
				"result"));
	
	private boolean	acceptActionEvents;
	private boolean filterActionEvents;
	private final boolean	acceptAlarmEvents;
	private final boolean	acceptLifeCycleEvents;
	private final boolean	acceptProbeEvents;
	private final ArrayList<Boolean> keepList ;
	private final ArrayList<String>	fieldList;
	private final ArrayList<String>	valueList;

	public GenericFilter(String acceptActionEvents, boolean acceptAlarmEvents,
			boolean acceptLifeCycleEvents, boolean acceptProbeEvents) {
		this.acceptAlarmEvents = acceptAlarmEvents;
		this.acceptLifeCycleEvents = acceptLifeCycleEvents;
		this.acceptProbeEvents = acceptProbeEvents;
		
		keepList = new ArrayList<Boolean>();
		fieldList = new ArrayList<String>();
		valueList = new ArrayList<String>();
		
		String filter;
		if (acceptActionEvents != null && !acceptActionEvents.isEmpty()) {
			BooleanHolder tmp = new BooleanHolder(true);
			try
			{
				tmp.setBooleanValue(acceptActionEvents);
				this.acceptActionEvents = tmp.getBooleanValue();
				filterActionEvents = false;
			}
			catch (ClifException ex)
			{
				this.acceptActionEvents = true;
				filterActionEvents = true;
				filter = acceptActionEvents;

				String [] filters = filter.split("\\|");
				for (int i=0; i < filters.length; i++) {
					if (filters[i].startsWith("+")) {
						keepList.add(i, true);
					} else  if (filters[i].startsWith("-")) {
						keepList.add(i, false);
					} else {
						System.err.println("Warning: ignored incorrect setting for clif.store.action property: each part must begin with '+' or '-'.");
						filterActionEvents = false;
						break;
					}
					
					int colonIndex = filters[i].indexOf(":");
					if (colonIndex == -1) {
						System.err.println("Warning: ignored incorrect setting for clif.store.action property: missing ':' separator.");
						filterActionEvents = false;
						break;
					}
					String field = filters[i].substring(1, colonIndex);
					if (ACCEPTED_FIELD_LABELS.contains(field)) {
						if (field.equals("type")) {
							fieldList.add(i, "action type");
						} else {
							fieldList.add(i, field);
						}
					} else {
						System.err.println("Warning: ignored incorrect setting for clif.store.action property: unknown parameter "+field+". Accepted list is: type, success, duration, comment and result");
						filterActionEvents = false;
						break;
					}
					
					String value = filters[i].substring(colonIndex+1);
					valueList.add(i,value);
				}
			}
		} else {
			System.err.println("Warning: ignored incorrect setting for clif.store.action property: empty value");
		}
	}

	public boolean accept(ActionEvent action) {
		boolean result = true;
		if (filterActionEvents) {
			for (int i=0; i<keepList.size(); i++) {
				Object valueToCheck = action.getFieldValue(fieldList.get(i)).toString();
				if (keepList.get(i)) {
					result =checkValue(valueToCheck, valueList.get(i));
				} else {
					result =!checkValue(valueToCheck, valueList.get(i));
				}
				if (!result) {
					return result;
				}
			}
			return result;
		} else {
			return acceptActionEvents;
		}
	}

	public boolean accept(AlarmEvent alarm) {
		return acceptAlarmEvents;
	}

	public boolean accept(LifeCycleEvent event) {
		return acceptLifeCycleEvents;
	}

	public boolean accept(ProbeEvent measure) {
		return acceptProbeEvents;
	}
	
	
	private boolean checkValue(Object valueToCheck, String filterValue) {
		if (valueToCheck instanceof String) {
			return ((String)valueToCheck).matches(filterValue);
		} else {
			return valueToCheck.toString().equals(filterValue);
		}
	}
}
