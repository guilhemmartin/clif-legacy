package org.ow2.clif.datacollector.lib;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;
import java.util.Date;

import org.ow2.clif.storage.api.ActionEvent;
/**
 * Test GenericFilter instantiation and call to accept method with different values for parameter clif.store.action.
 * 
 * @author Stéphanie Monticelli
 *
 */
public class GenericFilterTest {

	private static GenericFilter filter;
	private static ActionEvent action1;
	private static ActionEvent action2;
	
	private final static ByteArrayOutputStream errContent = new ByteArrayOutputStream();


	/**
	 * Runs the test.
	 */
	public static void main(String[] args)
	{
		action1 = new ActionEvent(new Date().getTime(),1,"MQTT get",1,true,2632,"for test","keep on report");
		action2 = new ActionEvent(new Date().getTime(),1,"HTTP get",1,false,2632,"for test","keep on report only some times.");

		// keep all actions
		filter = new GenericFilter("true", false, false, false);
		assert filter.accept(action1) : "Action must be keep on report";
		
		filter = new GenericFilter("1", false, false, false);
		assert filter.accept(action1) : "Action must be keep on report";
		
		filter = new GenericFilter("yes", false, false, false);
		assert filter.accept(action1) : "Action must be keep on report";
		
		filter = new GenericFilter("on", false, false, false);
		assert filter.accept(action1) : "Action must be keep on report";
		
		// skip all actions
		filter = new GenericFilter("false", false, false, false);
		assert !filter.accept(action1) : "Action must not be keep on report";
		
		filter = new GenericFilter("0", false, false, false);
		assert !filter.accept(action1) : "Action must not be keep on report";
		
		filter = new GenericFilter("no", false, false, false);
		assert !filter.accept(action1) : "Action must be keep on report";
				
		filter = new GenericFilter("off", false, false, false);
		assert !filter.accept(action1) : "Action must not be keep on report";

		// keep action1 but not action2
		filter = new GenericFilter("+type:MQTT .*", false, false, false);
		assert filter.accept(action1) : "Action must be keep on report";
		assert !filter.accept(action2) : "Action must not be keep on report";
		
		filter = new GenericFilter("-success:false", false, false, false);
		assert filter.accept(action1) : "Action must be keep on report";
		assert !filter.accept(action2) : "Action must not be keep on report";
		
		filter = new GenericFilter("+result:keep on report", false, false, false);
		assert filter.accept(action1) : "Action must be keep on report";
		assert !filter.accept(action2) : "Action must not be keep on report";
		
		filter = new GenericFilter("+type:.*get|-success:false", false, false, false);
		assert filter.accept(action1) : "Action must be keep on report";
		assert !filter.accept(action2) : "Action must not be keep on report";
		
		// keep both actions
		filter = new GenericFilter("+comment:for test", false, false, false);
		assert filter.accept(action1) : "Action must be keep on report";
		assert filter.accept(action2) : "Action must be keep on report";
		
		filter = new GenericFilter("+comment:.*test", false, false, false);
		assert filter.accept(action1) : "Action must be keep on report";
		assert filter.accept(action2) : "Action must be keep on report";
		
		// skip both actions
		filter = new GenericFilter("-type:.*get", false, false, false);
		assert !filter.accept(action1) : "Action must not be keep on report";
		assert !filter.accept(action2) : "Action must not be keep on report";
		
		// malformed filter: need redirect error output to verify message thrown and add carriage return at the end of messages.
		PrintStream originalErr = System.err;
	    System.setErr(new PrintStream(errContent));

		filter = new GenericFilter("type:get", false, false, false);
		boolean test1 = errContent.toString().equals("Warning: ignored incorrect setting for clif.store.action property: each part must begin with '+' or '-'.\n");
		
		errContent.reset();
		filter = new GenericFilter("+success", false, false, false);
		boolean test2 = errContent.toString().equals(
				"Warning: ignored incorrect setting for clif.store.action property: missing ':' separator.\n");

		errContent.reset();
		filter = new GenericFilter("+types:get", false, false, false);
		boolean test3 = errContent.toString().equals(
				"Warning: ignored incorrect setting for clif.store.action property: unknown parameter types. Accepted list is: type, success, duration, comment and result\n");

		errContent.reset();
		filter = new GenericFilter("", false, false, false);
		boolean test4 = errContent.toString().equals(
				"Warning: ignored incorrect setting for clif.store.action property: empty value\n");
		
		errContent.reset();
		filter = new GenericFilter(null, false, false, false);
		boolean test5 = errContent.toString().equals(
				"Warning: ignored incorrect setting for clif.store.action property: empty value\n");

		System.setErr(originalErr);
		
		assert test1 : "Wrong warning message 1";
		assert test2 : "Wrong warning message 2";
		assert test3 : "Wrong warning message 3";
		assert test4 : "Wrong warning message 4";
		assert test5 : "Wrong warning message 5";
	}
}
