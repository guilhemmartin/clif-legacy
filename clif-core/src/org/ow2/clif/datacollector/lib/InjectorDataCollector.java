/*
* CLIF is a Load Injection Framework
* Copyright (C) 2004,2010 France Telecom R&D
*
* This library is free software; you can redistribute it and/or
* modify it under the terms of the GNU Lesser General Public
* License as published by the Free Software Foundation; either
* version 2 of the License, or (at your option) any later version.
*
* This library is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
* Lesser General Public License for more details.
*
* You should have received a copy of the GNU Lesser General Public
* License along with this library; if not, write to the Free Software
* Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*
* Contact: clif@ow2.org
*/

package org.ow2.clif.datacollector.lib;


import java.io.Serializable;

import org.ow2.clif.storage.api.ActionEvent;


/**
 * Data collector for load-injectors generating ActionEvent events.
 * statistics:
 * - time frame, i.e. ellapsed time since previous getStat() call
 * - average reponse time during this time frame
 * - max response time during this time frame
 * - min response time during this time frame
 * - number of actions during this time frame
 * - throughput (number of actions/sec) during this time frame
 * - number of error occurrences during this time frame
 * - error rate (number of error/sec) during this time frame
 * - response time standard deviation
 *
 * @author Bruno Dillenseger
 */
public class InjectorDataCollector extends AbstractDataCollector
{
	static public final String[] LABELS = {
		"time frame (ms)",
		"average response time (ms)",
		"min response time (ms)",
		"max response time (ms)",
		"number of actions",
		"action throughput (actions/s)",
		"number of errors",
		"error throughput (errors/s)",
		"error rate (%)",
		"response time std deviation (ms)"};


	/** absolute date of previous call to getStat() */
	protected long statStartTime;
	/** number of successful actions performed since the previous call to getStat() */
	protected long successfullActions;
	/** number of error occurrences since the previous call to getStat() */
	protected long errors;
	/** duration of the quickest action since the previous call to getStat() */
	protected long actionMinDuration;
	/** duration of the longest action since the previous call to getStat() */
	protected long actionMaxDuration;
	/** total amount of time spent by actions since the previous call to getStat() */
	protected long actionTotalDuration;
	/** sum of square response times since the previous call to getStat(), used to compute moving standard deviation */
	protected double actionTotalSquareDuration;
	/** total number of actions performed since current test initialization */
	protected long cumulativeSuccessfullActions;
	/** total number of error occurrences since current test initialization */
	protected long cumulativeErrors;


	public InjectorDataCollector()
	{
		super();
	}


	/**
	 * resets all statistics but the cumulative ones
	 */
	private void resetStats()
	{
		statStartTime = System.currentTimeMillis();
		successfullActions = 0;
		errors = 0;
		actionMinDuration = Long.MAX_VALUE;
		actionMaxDuration = Long.MIN_VALUE;
		actionTotalDuration = 0;
		actionTotalSquareDuration = 0;
	}


	/**
	 * new test initialization => call super class' init() method and resets all statistics,
	 * including the cumulative ones
	 */
	public void init(Serializable testId, String scenarioId)
	{
		super.init(testId, scenarioId);
		resetStats();
		cumulativeSuccessfullActions = 0;
		cumulativeErrors = 0;
	}


	/**
	 * new measure available => call super class' add() method and compute statistics
	 */
	public void add(ActionEvent event)
	{
		super.add(event);
		if (event.isSuccessful())
		{
			++successfullActions;
			actionTotalDuration += event.duration;
			actionTotalSquareDuration += ((long) event.duration) * event.duration;
			if (event.duration > actionMaxDuration)
			{
				actionMaxDuration = event.duration;
			}
			if (event.duration < actionMinDuration)
			{
				actionMinDuration = event.duration;
			}
		}
		else
		{
			++errors;
		}
	}


	//////////////////////////////////
	// interface DataCollectorAdmin //
	//////////////////////////////////


	/**
	 * Get statistics computed since previous call (or initialization for 1st call).
	 * @return injector action statistics
	 */
	public long[] getStat()
	{
		long[] stats = new long[LABELS.length];
		// ellapsed time since previous call
		stats[0] = System.currentTimeMillis() - statStartTime;
		// min, max and average response times, error rate
		if (successfullActions == 0)
		{
			// when no action has been performed, these statistics are set to zero
			stats[1] = 0; // average response time
			stats[2] = 0; // min response time
			stats[3] = 0; // max response time
			stats[9] = 0; // response time standard deviation
			if (errors == 0) // error rate special case
			{
				stats[8] = 0;
			}
			else
			{
				stats[8] = 100;
			}
		}
		else
		{
			stats[1] = actionTotalDuration / successfullActions; // average response time
			stats[2] = actionMinDuration; // min response time
			stats[3] = actionMaxDuration; // max response time
			stats[8] = (errors * 100) / (successfullActions + errors); // error rate
			stats[9] = Math.round(Math.sqrt(
				actionTotalSquareDuration / successfullActions
				- stats[1] * stats[1])); // response time standard deviation
		}
		cumulativeSuccessfullActions += successfullActions;
		stats[4] = cumulativeSuccessfullActions;
		// action and error throughput /s
		cumulativeErrors += errors;
		stats[6] = cumulativeErrors;
		if (stats[0] == 0)
		{
			stats[5] = 0;
			stats[7] = 0;
		}
		else
		{
			stats[5] = (successfullActions * 1000) / stats[0];
			stats[7] = (errors * 1000) / stats[0];
		}
		resetStats();
		return stats;
	}


	public String[] getLabels()
	{
		return LABELS;
	}
}
