/*
 * CLIF is a Load Injection Framework
 * Copyright (C) 2003,2004 France Telecom R&D
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 *
 * CLIF $Name: not supported by cvs2svn $
 *
 * Contact: clif@ow2.org
 */

package org.ow2.clif.datacollector.api;

import java.io.Serializable;

import org.ow2.clif.storage.api.ActionEvent;
import org.ow2.clif.storage.api.AlarmEvent;
import org.ow2.clif.storage.api.LifeCycleEvent;
import org.ow2.clif.storage.api.ProbeEvent;

/**
 * Interface for collecting information generated by a blade.
 * 
 * @author Bruno Dillenseger
 */
public interface DataCollectorWrite
{
	public final String DATA_COLLECTOR_WRITE = "Data collector write";

	/**
	 * Initialize a test
	 * 
	 * @param testId
	 *            test identifier
	 * @param bladeId
	 *            scenario identifier
	 */
	public void init(Serializable testId, String bladeId);

	/**
	 * End of a test
	 */
	public void terminate();

	/**
	 * Add a new lifecycle event
	 * 
	 * @param event
	 *            new lifecycle event
	 */
	public void add(LifeCycleEvent event);

	/**
	 * Add a new action (injection) event
	 * 
	 * @param action
	 *            new measure
	 */
	public void add(ActionEvent action);

	/**
	 * Add a new alarm
	 * 
	 * @param alarm
	 *            new alarm event
	 */
	public void add(AlarmEvent alarm);

	/**
	 * Add a new probe (measure) event
	 * 
	 * @param measure
	 */
	public void add(ProbeEvent measure);

	/**
	 * Sets a filter selecting generated events forwarded to Storage
	 * 
	 * @param filter
	 *            event filter; a null filter forwards all events to Storage
	 */
	public void setFilter(DataCollectorFilter filter);
}
