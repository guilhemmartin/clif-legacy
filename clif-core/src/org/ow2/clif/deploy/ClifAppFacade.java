/*
* CLIF is a Load Injection Framework
* Copyright (C) 2005, 2009-2011 France Telecom R&D
*
* This library is free software; you can redistribute it and/or
* modify it under the terms of the GNU Lesser General Public
* License as published by the Free Software Foundation; either
* version 2 of the License, or (at your option) any later version.
*
* This library is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
* Lesser General Public License for more details.
*
* You should have received a copy of the GNU Lesser General Public
* License along with this library; if not, write to the Free Software
* Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*
* Contact: clif@ow2.org
*/

package org.ow2.clif.deploy;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.Serializable;
import java.nio.channels.FileChannel;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.ListIterator;
import java.util.Map;
import java.util.Observable;
import java.util.Observer;
import java.util.Timer;
import java.util.TimerTask;
import org.objectweb.fractal.adl.ADLException;
import org.objectweb.fractal.adl.FactoryFactory;
import org.objectweb.fractal.api.Component;
import org.objectweb.fractal.api.Interface;
import org.objectweb.fractal.api.NoSuchInterfaceException;
import org.objectweb.fractal.api.control.BindingController;
import org.objectweb.fractal.api.control.ContentController;
import org.objectweb.fractal.api.control.IllegalBindingException;
import org.objectweb.fractal.api.control.IllegalContentException;
import org.objectweb.fractal.api.control.IllegalLifeCycleException;
import org.objectweb.fractal.util.Fractal;
import org.ow2.clif.console.lib.ClifDeployDefinition;
import org.ow2.clif.console.lib.batch.BatchUtil;
import org.ow2.clif.datacollector.api.DataCollectorAdmin;
import org.ow2.clif.server.api.BladeControl;
import org.ow2.clif.storage.api.AlarmEvent;
import org.ow2.clif.storage.api.CollectListener;
import org.ow2.clif.storage.api.StorageProxyAdmin;
import org.ow2.clif.storage.lib.filestorage.FileStorageCommons;
import org.ow2.clif.supervisor.api.BladeState;
import org.ow2.clif.supervisor.api.ClifException;
import org.ow2.clif.supervisor.api.SupervisorInfo;
import org.ow2.clif.supervisor.api.TestControl;
import org.ow2.clif.supervisor.lib.BladeObservation;
import org.ow2.clif.util.CodeServer;
import org.ow2.clif.util.ExecutionContext;
import org.ow2.clif.util.FractalAdl;
import org.ow2.clif.util.ItfName;


/**
 * This class is responsible for creating the initial Clif application. Then, it provides test
 * plan deployment.
 * @author Bruno Dillenseger
 * @author Joan Chaumont
 * @author Florian Francheteau
 */
public class ClifAppFacade extends Observable implements Observer
{
	private Object deployLock = new Object();
	private volatile boolean deployed = false;
	protected String clifAppDefinition;
	protected String clifAppName;
	protected Serializable currentTestId;
	protected Component clifApp;
	protected Component storage;
	protected Component supervisor;
	/** clif application components indexed by their name (storage, supervisor, analyzer) */
	protected Map<String,Component> components = new HashMap<String,Component>();
	protected BindingController storageBc;
	protected BindingController supervisorBc;
	protected ContentController clifAppCc;
	/** contains bound clif server components, indexed by their names */
	private TestControl testControl;
	private SupervisorInfo supInf;

	/**
	 * Create new ClifAppFacade with all components of a ClifApplication :
	 * supervisor, storage...
	 * @param testName the name in the Registry to be associated with the
	 * resulting Clif Application.
	 * @param appDefinition fully-qualified Fractal ADL definition file of the
	 * CLIF application to instantiate.
	 * @throws Error
	 */
	public ClifAppFacade(String testName, String appDefinition)
	{
		clifAppName = testName;
		clifAppDefinition = appDefinition;
		try
		{
			clifApp = (Component) FactoryFactory.getFactory(
				FractalAdl.CLIF_BACKEND).newComponent(clifAppDefinition, null);
			setClifAppComponents();
		} catch (Exception e) {
			throw new Error("Error creating Clif application " + appDefinition + " for " + testName, e);
		}
	}

	/**
	 * Constructor for ClifAppFacade. Get all sub-components
	 * from the clifApp
	 * @param clifApp the existing Clif application component
	 */
	public ClifAppFacade (Component clifApp, String name)
	{
		this.clifApp = clifApp;
		this.clifAppName = name;
		try {
			setClifAppComponents();
		} catch (Exception e) {
			throw new Error("Error creating Clif application " + name, e);
		}
	}

	/**
	 * Set clifApp sub-components
	 * @throws ADLException
	 * @throws NoSuchInterfaceException
	 */
	private void setClifAppComponents()
		throws ADLException, NoSuchInterfaceException {
		Component[] subComp = Fractal.getContentController(clifApp).getFcSubComponents();
		for (int i=0 ; i<subComp.length ; ++i)
		{
			components.put(Fractal.getNameController(subComp[i]).getFcName(), subComp[i]);
			if (Fractal.getNameController(subComp[i]).getFcName().equals("storage"))
			{
				storage = subComp[i];
			}
			else if (Fractal.getNameController(subComp[i]).getFcName().equals("supervisor"))
			{
				supervisor = subComp[i];
			}
		}
		storageBc = Fractal.getBindingController(storage);
		supervisorBc = Fractal.getBindingController(supervisor);
		clifAppCc = Fractal.getContentController(clifApp);
		testControl = (TestControl)supervisor.getFcInterface(TestControl.TEST_CONTROL);
		supInf = (SupervisorInfo)ClifAppFacade.this.supervisor.getFcInterface(SupervisorInfo.SUPERVISOR_INFO);
	}

	/**
	 * Get a component of the clifApplication by his Fractal name
	 * @param name the name of the component to find
	 * @return the reference of a component contained by the Clif Application whose name equals the
	 * String passed as parameter, or null if there is no such component.
	 */
	public Component getComponentByName(String name)
	{
		return components.get(name);
	}

	/**
	 * Get clifApp component
	 * @return Returns the clifApp component.
	 */
	public Component getClifApp() {
		return clifApp;
	}

	/**
	 * Asynchronously deploys blades among CLIF servers according to the given definitions.
	 * First, a new CLIF code server is launched.
	 * Then, current CLIF application is stopped, reconfigured and started again.
	 * In case the CLIF application is doing wrong (typically one of its distributed parts
	 * is no longer available), a new one is instantiated. So, be careful to always call
	 * {@link #getClifApp()} from one deployment to another whenever you need to get the
	 * reference to the CLIF application component.
	 * Old blades that may have been previously deployed by this supervisor are removed,
	 * unless a new CLIF application is instantiated or previous blades became unreachable.
	 * DeployObservation objects will be send to Observers to inform about deployment
	 * success or failure. BladeObservation objects are also sent to notify blades state changes.
	 * @param definitions a Map containing test plan definitions, indexed by blades identifiers
	 * @param registry the CLIF registry to be used for getting CLIF servers references
	 * @see org.ow2.clif.deploy.DeployDefinition
	 * @see org.ow2.clif.deploy.DeployObservation
	 * @see org.ow2.clif.supervisor.lib.BladeObservation
	 * @see #syncDeploy(Map, ClifRegistry)
	 * @throws ClifException when the CLIF code server could not be started,
	 * or when the test plan definition is null
	 */
	public void deploy(final Map<String,ClifDeployDefinition> definitions, final ClifRegistry registry)
		throws ClifException
	{
		if (definitions == null)
		{
			throw new ClifException("Could not deploy a null test plan.");
		}
	    try
	    {
	        CodeServer.launch(null, Integer.parseInt(System.getProperty("clif.codeserver.port", "1357")),
	        	ExecutionContext.getBaseDir() + "lib/ext/", System.getProperty("clif.codeserver.path", "."));
	    }
        catch (Exception ex)
        {
        	if (! ExecutionContext.codeServerIsShared())
        	{
        		throw new ClifException("Could not start the CLIF code server.", ex);
        	}
        	// otherwise, assume the code server is already running with an appropriate path
        }
        testControl.deleteObservers();
		testControl.addObserver(ClifAppFacade.this);
    	new Thread(
    		new Runnable() {
        		@Override
				public void run()
        		{
        			DeployThread deployerThr = new DeployThread(definitions, registry);
        			deployerThr.start();
        		}},
        	"Test plan deployment"
    	).start();
	}

	/**
	 * Synchronous version of deploy method (returns when deployment is complete).
	 * @param definitions a Map containing test plan definitions, indexed by blades identifiers
	 * @param registry the CLIF registry to be used for getting CLIF servers references
	 * @see #deploy(Map, ClifRegistry)
	 * @see org.ow2.clif.deploy.DeployDefinition
	 * @see org.ow2.clif.deploy.DeployObservation
	 */
	public void syncDeploy(Map<String,ClifDeployDefinition> definitions, ClifRegistry registry)
		throws ClifException
	{
		deployed = false;
		deploy(definitions, registry);
		synchronized(deployLock)
		{
			while (!deployed)
			{
				try
				{
					deployLock.wait();
				}
				catch (InterruptedException ex)
				{
				}
			}
		}
	}

	/**
	 * Init selected blades if global state is deployed
	 * @param testId selected blades ids
	 */
	public void init(String testId)
		throws ClifException
	{
		try
		{
			supInf.waitStationaryState(null);
		}
		catch (InterruptedException ex)
		{
			throw new ClifException("Can't initialize: interrupted while waiting for a stationary state.", ex);
		}
		if (supInf.getGlobalState(null).equals(BladeState.DEPLOYED)
			|| supInf.getGlobalState(null).equals(BladeState.COMPLETED)
			|| supInf.getGlobalState(null).equals(BladeState.ABORTED)
			|| supInf.getGlobalState(null).equals(BladeState.STOPPED))
		{
			currentTestId = new TestNameAndDate(testId);
			if (! ExecutionContext.useGlobalTime())
			{
				currentTestId = currentTestId.toString();
			}
	        testControl.init(currentTestId);
		}
		else
		{
			throw new ClifException("Can't initialize: lifecycle error");
		}
	}

    /**
	 * Start selected blades if global state is initialized
	 * @param selBladesId selected blades ids
	 * @return the int code return for exit info
	 */
	public int start(String[] selBladesId)
	{
		try
		{
			supInf.waitStationaryState(selBladesId);
			if (supInf.getGlobalState(selBladesId).equals(BladeState.INITIALIZED))
			{
				testControl.start(selBladesId);
				return BatchUtil.SUCCESS;
			}
			else
			{
				return BatchUtil.ERR_LIFECYCLE;
			}
		}
		catch (InterruptedException ex)
		{
			ex.printStackTrace(System.err);
			return BatchUtil.ERR_EXEC;
		}
	}

	/**
	 * Stop selected blades if global state is not stopped/completed/aborted
	 * @param selBladesId selected blades ids
	 * @return the int code return for exit info
	 */
	public int stop(String[] selBladesId)
	{
		try
		{
			supInf.waitStationaryState(selBladesId);
			if (!supInf.getGlobalState(selBladesId).equals(BladeState.STOPPED)
				&& !supInf.getGlobalState(selBladesId).equals(BladeState.COMPLETED)
				&& !supInf.getGlobalState(selBladesId).equals(BladeState.ABORTED))
			{
				testControl.stop(selBladesId);
				return BatchUtil.SUCCESS;
			}
			else
			{
				return BatchUtil.ERR_LIFECYCLE;
			}
		}
		catch (InterruptedException ex)
		{
			ex.printStackTrace(System.err);
			return BatchUtil.ERR_EXEC;
		}
	}

	/**
	 * Suspend selected blades if global state is running
	 * @param selBladesId selected blades ids
	 * @return the int code return for exit info
	 */
	public int suspend(String[] selBladesId)
	{
		try
		{
			supInf.waitStationaryState(selBladesId);
			if (supInf.getGlobalState(selBladesId).equals(BladeState.RUNNING))
			{
			    testControl.suspend(selBladesId);
			    return BatchUtil.SUCCESS;
			}
			else
			{
				return BatchUtil.ERR_LIFECYCLE;
			}
		}
		catch (InterruptedException ex)
		{
			ex.printStackTrace(System.err);
			return BatchUtil.ERR_EXEC;
		}
	}

	/**
	 * Resume selected blades if global state is suspended
	 * @param selBladesId selected blades ids
	 * @return the int code return for exit info
	 */
	public int resume(String[] selBladesId)
	{
		try
		{
			supInf.waitStationaryState(selBladesId);
			if (supInf.getGlobalState(selBladesId).equals(BladeState.SUSPENDED))
			{
			    testControl.resume(selBladesId);
			    return BatchUtil.SUCCESS;
			}
			else
			{
				return BatchUtil.ERR_LIFECYCLE;
			}
		}
		catch (InterruptedException ex)
		{
			ex.printStackTrace(System.err);
			return BatchUtil.ERR_EXEC;
		}
	}

	/**
	 * Join selected blades if global state is running
	 * @param selBladesId selected blades ids
	 * @return the int code return for exit info
	 */
	public int join(String[] selBladesId)
	{
		try
		{
			supInf.waitStationaryState(selBladesId);
			if (supInf.getGlobalState(selBladesId).equals(BladeState.RUNNING))
			{
				testControl.join(selBladesId);
				return BatchUtil.SUCCESS;
			}
			else
			{
				return BatchUtil.ERR_LIFECYCLE;
			}
		}
		catch (InterruptedException ex)
		{
			ex.printStackTrace(System.err);
			return BatchUtil.ERR_EXEC;
		}
	}

	/**
	 * Collect selected blades if global state is completed or stopped
	 * @param selBladesId selected blades ids
	 * @return the int return code for exit information
	 */
	public int collect(String[] selBladesId, CollectListener listener)
	{
		try
		{
			supInf.waitStationaryState(selBladesId);
			supInf.waitEndOfRun(selBladesId);
			if (supInf.getGlobalState(selBladesId).equals(BladeState.COMPLETED)
				|| supInf.getGlobalState(selBladesId).equals(BladeState.STOPPED)
				|| supInf.getGlobalState(selBladesId).equals(BladeState.ABORTED))
			{
				testControl.collect(selBladesId, listener);
				return BatchUtil.SUCCESS;
			}
			else
			{
				return BatchUtil.ERR_LIFECYCLE;
			}
		}
		catch (Throwable ex)
		{
			ex.printStackTrace(System.err);
			return BatchUtil.ERR_EXEC;
		}
	}

	/**
	 * Get global state for these blades.
	 * @param selBladesId the blades ids
	 * @return BladeState the global state
	 */
	public BladeState getGlobalState(String[] selBladesId)
	{
		BladeState globalState = null;
		globalState = supInf.getGlobalState(selBladesId);
		return globalState;
	}

    /**
     * Get the state of a blade
     * @param id the bladeId tested
     * @return the blade state
     */
	public BladeState getState(String id) {
        String [] testId = {id};
        return getGlobalState(testId);
    }

    /**
     * Waits until this state is reached
     * @param selBladesId
     * @param state
     * @return int return code
     */
    public int waitForState(String[] selBladesId, BladeState state)
    {
    	try
    	{
	        if (supInf.waitForState(selBladesId, state))
	        {
	            return BatchUtil.SUCCESS;
	        }
	        else
	        {
	        	return BatchUtil.ERR_LIFECYCLE;
	        }
	    }
    	catch (InterruptedException ex)
    	{
    		ex.printStackTrace(System.err);
    		return BatchUtil.ERR_EXEC;
    	}
    }

    /**
	 * Retrieve the ActionStat of a specific host.
	 * @param bladeId The blade identifier to get the statistical data from
	 * @return An array containing some informations about the test for this host
	 */
	public long[] getStats(String bladeId) {
        return testControl.getStats(bladeId);
	}

	/**
	 * Retrieve the ActionStat of a specific host.
	 * @param bladeId The blade identifier to get the statistics labels from
	 * @return An array containing some informations about the test for this host
	 */
	public String[] getStatLabels(String bladeId) {
		return testControl.getStatLabels(bladeId);
	}
	
	/**
	 * Start the monitoring of ActionStat of a specific host.
	 * @param bladesId The blade identifier to get the statistical data from (if null, get data for all blade)
	 * @param period The monitoring period
	 */
	public void startMonitoring(final String[] bladesId, long period) {
		TimerTask task = new MonitorThread(bladesId);
		Timer stats = new Timer();
		stats.scheduleAtFixedRate(task, period, period);
	}

	public Map<String,Serializable> getCurrentParameters(String id) {
	    return testControl.getCurrentParameters(id);
    }

	/**
	 * Retrieves current test identifier.
	 * @return A string composed of the test name,
	 * and the date and time of its initialization
	 */
	public String getCurrentTestId() {
	    return currentTestId.toString();
    }

    public void changeParameter(String bladeId, String name, String text)
    	throws ClifException
    {
        testControl.changeParameter(bladeId, name, text);
    }

    @Override
	public void update(Observable supervisor, Object observation) {
	    if(countObservers() == 0 && observation instanceof AlarmEvent)
	    {
	        AlarmEvent alarm = (AlarmEvent)observation;
			if (alarm.argument != null)
			{
				if (alarm.argument instanceof Throwable)
				{
				    ((Throwable)alarm.argument).printStackTrace();
				    System.exit(1);
				}
			}
	    }
		setChanged();
		notifyObservers(observation);
	}

	class DeployThread extends Thread
	{
		Map<String,ClifDeployDefinition> definitions;
		Map<String,Collection<Map.Entry<String,ClifDeployDefinition>>> definitionsByServer =
			new HashMap<String,Collection<Map.Entry<String,ClifDeployDefinition>>>();
		ClifRegistry registry;


		private void bindBlade(Component blade)
		throws
			IllegalLifeCycleException,
			IllegalContentException,
			IllegalBindingException,
			NoSuchInterfaceException
		{
			ClifAppFacade.this.clifAppCc.addFcSubComponent(blade);
			Fractal.getBindingController(blade).bindFc(
				SupervisorInfo.SUPERVISOR_INFO,
				ClifAppFacade.this.supInf);
			ClifAppFacade.this.storageBc.bindFc(
				ItfName.gen(StorageProxyAdmin.STORAGEPROXY_ADMIN),
				blade.getFcInterface(StorageProxyAdmin.STORAGEPROXY_ADMIN));
			ClifAppFacade.this.supervisorBc.bindFc(
				ItfName.gen(DataCollectorAdmin.DATA_COLLECTOR_ADMIN),
				blade.getFcInterface(DataCollectorAdmin.DATA_COLLECTOR_ADMIN));
			ClifAppFacade.this.supervisorBc.bindFc(
				ItfName.gen(BladeControl.BLADE_CONTROL),
				blade.getFcInterface(BladeControl.BLADE_CONTROL));
		}


		DeployThread(Map<String,ClifDeployDefinition> testPlan, ClifRegistry registry)
		{
			super("ClifAppFacade deployment thread");
			this.definitions = testPlan;
			this.registry = registry;
			if (testPlan != null)
			{
				for (Map.Entry<String,ClifDeployDefinition> entry : testPlan.entrySet())
				{
					Collection<Map.Entry<String,ClifDeployDefinition>> blades =
						definitionsByServer.get(entry.getValue().getServerName());
					if (blades == null)
					{
						blades = new LinkedList<Map.Entry<String,ClifDeployDefinition>>();
						definitionsByServer.put(entry.getValue().getServerName(), blades);
					}
					blades.add(entry);
				}
			}
		}


		@Override
		public void run()
		{
			boolean bestEffort = ExecutionContext.deploymentIsBestEffort();
			List<String> undeployedBlades = new ArrayList<String>(definitions.size());
			DeployObservation result = null;
			Map<String,Component> serversComp = null;
			try
			{
				// get all available CLIF server components from the registry
				serversComp = registry.getServerComponents();
				// remove the ones that are not involved in the test plan
				Iterator<Map.Entry<String,Component>> serverIter = serversComp.entrySet().iterator();
				while (serverIter.hasNext())
				{
					if (! definitionsByServer.containsKey(serverIter.next().getKey()))
					{
						serverIter.remove();
					}
				}
				// check that necessary CLIF servers are available
				for (String serverName : definitionsByServer.keySet().toArray(new String[definitionsByServer.size()]))
				{
					if (!serversComp.containsKey(serverName))
					{
						System.err.println("Warning: CLIF server " + serverName + " is missing.");
						if (bestEffort)
						{
							for (Map.Entry<String,ClifDeployDefinition> entry : definitionsByServer.get(serverName))
							{
								definitions.remove(entry.getKey());
								undeployedBlades.add(entry.getKey());
								System.out.println(
									"Best effort deployment: blade "
									+ entry.getKey()
									+ " discarded from the test plan.");
							}
							definitionsByServer.remove(serverName);
						}
						else
						{
							throw new ClifException("Deployment aborted because CLIF server " + serverName + " is missing.");
						}
					}
				}
				String[] itfNames;
				// remove Supervisor bindings with Blades
				itfNames = ClifAppFacade.this.supervisorBc.listFc();
				for (String itfName : itfNames) {
					if (itfName.startsWith(DataCollectorAdmin.DATA_COLLECTOR_ADMIN) || itfName.startsWith(BladeControl.BLADE_CONTROL)) {
						ClifAppFacade.this.supervisorBc.unbindFc(itfName);
					}
				}
				// remove Storage bindings with Blades, Blades bindings with Supervisor,
				// and remove Blades from the CLIF application
				itfNames = ClifAppFacade.this.storageBc.listFc();
				for (String itfName : itfNames)
				{
					if (itfName.startsWith(StorageProxyAdmin.STORAGEPROXY_ADMIN))
					{
						try
						{
							ClifAppFacade.this.storageBc.unbindFc(itfName);
							Interface bladeItf = ((Interface)ClifAppFacade.this.storageBc.lookupFc(itfName));
							if (bladeItf != null)
							{
								Component blade = bladeItf.getFcItfOwner();
								Fractal.getBindingController(blade).unbindFc(SupervisorInfo.SUPERVISOR_INFO);
								/* Removing blades from the CLIF application may hang if at least one blade is not responding.
								 * So, we prefer leaving blades, which may not have any consequence, expect a small heap
								 * memory leak through consecutive deployments. Anyway, since there is no distributed garbage
								 * collection for blades, they will always remain however.
								clifAppCc.removeFcSubComponent(blade);
								*/
							}
						}
						catch (Exception ex)
						{
							// ignored: typically due to a CLIF server restart
						}
					}
				}
			}
			catch (Exception ex)
			{
				result = new DeployObservation(false, ex);
			}
			if (result == null && definitions != null)
			{
				try
				{
					// deploy new blades
					LinkedList<BladeDeploy> pendingBlades = new LinkedList<BladeDeploy>();
					synchronized (pendingBlades)
					{
						for (Map.Entry<String,Collection<Map.Entry<String,ClifDeployDefinition>>> entry : definitionsByServer.entrySet())
						{
							BladeDeploy deployThr = new BladeDeploy(
								entry.getKey(),
								serversComp.get(entry.getKey()),
								entry.getValue(),
								pendingBlades);
							pendingBlades.addLast(deployThr);
							deployThr.start();
							for (Map.Entry<String,ClifDeployDefinition> def : entry.getValue())
							{
								ClifAppFacade.this.setChanged();
								ClifAppFacade.this.notifyObservers(new BladeObservation(def.getKey(), BladeState.DEPLOYING));
							}
						}
						// add the new blades to the CLIF Application, and set bindings between
						// supervisor and storage components with deployed blades.
						while (! pendingBlades.isEmpty() && (result == null || bestEffort))
						{
							boolean hasTimedOut = true;
							pendingBlades.wait(ExecutionContext.getDeploymentTimeOut());
							ListIterator<BladeDeploy> iter = pendingBlades.listIterator();
							while (iter.hasNext() && (result == null || bestEffort))
							{
								BladeDeploy bd = iter.next();
								if (bd.isComplete())
								{
									hasTimedOut = false;
									iter.remove();
									try
									{
										for (Map.Entry<String,Object> entry : bd.get().entrySet())
										{
											String bladeId = entry.getKey();
											if (entry.getValue() != null && entry.getValue() instanceof Component)
											{
												bindBlade((Component)entry.getValue());
												ClifAppFacade.this.setChanged();
												ClifAppFacade.this.notifyObservers(
													new BladeObservation(bladeId,BladeState.DEPLOYED));
												ClifAppFacade.this.supInf.setBladeState(bladeId, BladeState.DEPLOYED);
											}
											else
											{
												undeployedBlades.add(bladeId);
												if (entry.getValue() == null)
												{
													result = new DeployObservation(
														false,
														new ClifException(
															"Deployment of blade " + entry.getKey() + " failed for unexpected reason."));
												}
												else
												{
													result = new DeployObservation(
														false,
														new ClifException(
															"Deployment of blade " + entry.getKey() + " failed.",
															(Exception) entry.getValue()));
												}
												if (bestEffort)
												{
													definitions.remove(entry.getKey());
													result.getException().printStackTrace(System.err);
													System.out.println(
														"Best effort deployment: blade "
														+ entry.getKey()
														+ " discarded from the test plan.");
												}
												ClifAppFacade.this.setChanged();
												ClifAppFacade.this.notifyObservers(
													new BladeObservation(bladeId, BladeState.UNDEPLOYED));
												ClifAppFacade.this.supInf.setBladeState(bladeId, BladeState.UNDEPLOYED);
											}
										}
									}
									catch (ClifException ex)
									{
										for (Map.Entry<String,ClifDeployDefinition> entry : bd.getDefinitions())
										{
											undeployedBlades.add(entry.getKey());
											if (bestEffort)
											{
												definitions.remove(entry.getKey());
												ex.printStackTrace(System.err);
												System.out.println(
													"Best effort deployment: blade "
													+ entry.getKey()
													+ " discarded from the test plan.");
											}
											ClifAppFacade.this.setChanged();
											ClifAppFacade.this.notifyObservers(
												new BladeObservation(entry.getKey(), BladeState.UNDEPLOYED));
										}
										result = new DeployObservation(false, ex);
									}
								}
							}
							if (hasTimedOut)
							{
								for (BladeDeploy bd : pendingBlades)
								{
									for (Map.Entry<String,ClifDeployDefinition> entry : bd.getDefinitions())
									{
										undeployedBlades.add(entry.getKey());
										if (bestEffort)
										{
											definitions.remove(entry.getKey());
											System.out.println(
												"Best effort deployment: blade "
												+ entry.getKey()
												+ " discarded because CLIF server "
												+ entry.getValue().getServerName()
												+ " is not responding.");
										}
										ClifAppFacade.this.setChanged();
										ClifAppFacade.this.notifyObservers(
											new BladeObservation(entry.getKey(), BladeState.UNDEPLOYED));
									}
								}
								pendingBlades.clear();
								if (! bestEffort)
								{
									result = new DeployObservation(
										false,
										new ClifException(
											"These blades could not be deployed because of mute CLIF server(s):\n"
											+ undeployedBlades));
								}
							}
						}
					}
				}
				catch (NoSuchInterfaceException ex)
				{
					result = new DeployObservation(
						false,
						new Error(
							"Fatal error in blade deployment: Clif server type mismatch",
							ex));
				}
				catch (Exception ex)
				{
					result = new DeployObservation(false, new ClifException("Deployment failure: " + ex.getMessage(), ex));
				}
			}
			ClifAppFacade.this.supInf.setDefinitions(definitions);
			if (result == null && undeployedBlades.isEmpty())
			{
				result = new DeployObservation();
			}
			else if (bestEffort)
			{
				if (definitions.size() == 0)
				{
					result = new DeployObservation(
						false,
						new ClifException("Deployment failed: no blade could be deployed."));
				}
				else
				{
					result = new DeployObservation(
						true,
						new ClifException(
							"Deployment partially completed with "
							+ undeployedBlades.size()
							+ " undeployed blade(s):\n"
							+ undeployedBlades));
				}
			}
			ClifAppFacade.this.setChanged();
			ClifAppFacade.this.notifyObservers(result);
			synchronized(ClifAppFacade.this.deployLock)
			{
				ClifAppFacade.this.deployed = true;
				ClifAppFacade.this.deployLock.notify();
			}
		}
	}
	
	/**
	 * Task getting blades stats and log them to files.
	 * 
	 */
	class MonitorThread extends TimerTask{
		
		String[] bladesId;
		String pathToStoreMonitoring;
		long beginTime;
		
		
		public MonitorThread(String[] bladesId) {
			super();
			if (bladesId == null) {
				this.bladesId = ClifAppFacade.this.testControl.getBladesIds();
			} else {
				this.bladesId = bladesId;
			}
			File csvFile = new File(
				System.getProperty(
					FileStorageCommons.REPORT_DIR_PROP,
					FileStorageCommons.REPORT_DIR_DEFAULT),
				ClifAppFacade.this.testControl.getCurrentTestId());
			csvFile.mkdirs();
			pathToStoreMonitoring = csvFile.getPath();
			beginTime = System.currentTimeMillis();
		}

		/**
	     * Store monitoring data in a CSV File
	     * @param bladeId 	identifier of the blade
	     * @param label 	blade labels
	     * @param time		time of measure to collect
	     * @param stat		blade statistics (corresponding to the label)
	     */
	    private void storeMonitoring(String bladeId, String[] label, int time, long[] stat){
	    	File f = new File(pathToStoreMonitoring + "/" + (String)bladeId+".csv");
	    	FileOutputStream fileOutput;
	    	FileChannel channel;
	    	String writeOutput;
	    	time = time/1000;
	    	try {
	    		if (!f.exists()){
					f.createNewFile();
					fileOutput = new FileOutputStream(f,false);
		    		channel = fileOutput.getChannel();
		    		channel.tryLock();
		    		writeOutput = "#Time(sec),";
		    		fileOutput.write(writeOutput.getBytes());
					for (int i=0; i<label.length-1;i++){
						writeOutput = label[i]+",";
						fileOutput.write(writeOutput.getBytes());
					}
					writeOutput = label[label.length-1]+"\n";
					fileOutput.write(writeOutput.getBytes());
				}else{
					fileOutput = new FileOutputStream(f,true);
					channel = fileOutput.getChannel();
		    		channel.tryLock();
				}
	    		writeOutput = time+",";
	    		fileOutput.write(writeOutput.getBytes());
				for (int i=0; i<stat.length-1;i++){
					writeOutput = stat[i]+",";
					fileOutput.write(writeOutput.getBytes());
				}
				writeOutput = stat[stat.length-1]+"\n";
				fileOutput.write(writeOutput.getBytes());
				fileOutput.close();
			} catch (IOException ex) {
				ex.printStackTrace(System.err);
			}
	    }

		@Override
		public void run() {
			for (String bladeId: bladesId) {
				int totalTime = (int) (((System.currentTimeMillis() - beginTime)/1000)*1000);
				long[] stats = ClifAppFacade.this.getStats(bladeId);
				if (stats != null)
				{
					storeMonitoring(bladeId, ClifAppFacade.this.getStatLabels(bladeId), totalTime, stats);
				}
			}			
		}
	}
}
