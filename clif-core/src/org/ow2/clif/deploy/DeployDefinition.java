/*
* CLIF is a Load Injection Framework
* Copyright (C) 2004,2011 France Telecom R&D
*
* This library is free software; you can redistribute it and/or
* modify it under the terms of the GNU Lesser General Public
* License as published by the Free Software Foundation; either
* version 2 of the License, or (at your option) any later version.
*
* This library is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
* Lesser General Public License for more details.
*
* You should have received a copy of the GNU Lesser General Public
* License along with this library; if not, write to the Free Software
* Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*
* Contact: clif@ow2.org
*/

package org.ow2.clif.deploy;

import java.io.Serializable;
import java.util.Map;


/**
 * For scenario deployment definition.
 * @author Bruno Dillenseger
 */
public class DeployDefinition implements Serializable
{
	private static final long serialVersionUID = 577488283074674901L;
	private String servername;
	private String adlDefinition;
	private Map<String, String> context;
	private String argument;
	private String comment;


	/**
	 * Creates a new scenario deployment definition.
	 * @param servername the name of the CLIF server where this scenario must be deployed
	 * @param adlDefinition the fully qualified name of the file containing the Fractal definition
	 * for the component to be deployed
	 * @param context the context that will be used when instantiating the component
	 * @param argument the blade argument to be set
	 * @param comment an arbitrary String to be associated with this blade (e.g. a role for
	 * the blade or any other useful information or comment for monitoring or results reading...)
	 */
	public DeployDefinition(
		String servername,
		String adlDefinition,
		Map<String, String> context,
		String argument,
		String comment)
	{
		this.servername = servername;
		this.adlDefinition = adlDefinition;
		this.context = context;
		this.argument = argument;
		this.comment = comment;
	}


	/**
	 * @return the CLIF server name involved by this deployment definition.
	 */
	public String getServerName()
	{
		return servername;
	}


	/**
	 * @return the fully qualified name of the resource file holding the ADL definition
	 * for the deployed component.
	 */
	public String getAdlDefinition()
	{
		return adlDefinition;
	}


	/**
	 * @return the blade creation context
	 */
	public Map<String, String> getContext()
	{
		return context;
	}


	/**
	 * @return the scenario argument string
	 */
	public String getArgument()
	{
		return argument;
	}


	/**
	 * @return the comment played by the scenario (an arbitrary string)
	 */
	public String getComment()
	{
		return comment;
	}


	@Override
	public String toString()
	{
		return
			"server=" + servername
			+ ", ADL definition=" + adlDefinition
			+ ", context=" + context
			+ ", argument=" + argument
			+ ", comment=" + comment;
	}
}
