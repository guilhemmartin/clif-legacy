/*
* CLIF is a Load Injection Framework
* Copyright (C) 2005 France Telecom R&D
*
* This library is free software; you can redistribute it and/or
* modify it under the terms of the GNU Lesser General Public
* License as published by the Free Software Foundation; either
* version 2 of the License, or (at your option) any later version.
*
* This library is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
* Lesser General Public License for more details.
*
* You should have received a copy of the GNU Lesser General Public
* License along with this library; if not, write to the Free Software
* Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*
* Contact: clif@ow2.org
*/

package org.ow2.clif.util;

import java.util.ArrayList;

/**
 * String utility for getting fields separated by the given delimiter characters. It differs
 * from the JDK's StringTokenizer basically in that it returns empty strings for each double
 * delimiter occurrence. In other words, a double delimiter is considered as if it embraces
 * a null-sized field. A trailing delimiter, as well as a starting delimiter, is considered
 * to be followed (resp. preceeded) by a null-sized field.
 * @author Bruno Dillenseger
 */
public abstract class StringSplitter
{
	/**
	 * @param source the delimiter-separated fields
	 * @param delimiters the delimiter characters (each character is a delimiter)
	 * @return fields values as an array of strings
	 */
	static public String[] split(String source, String delimiters)
	{
		return split(source, delimiters, null);
	}


	/**
	 * @param source the delimiter-separated fields
	 * @param delimiters the delimiter characters (each character is a delimiter)
	 * @param result the array which will be filled with extracted field values and returned.
	 * If its size is greater than the actual number of fields, trailing array elements won't
	 * be set. If its size is less than the actual number of fields, extra fields will be
	 * ignored.
	 * @return result argument, filled with fields values
	 */
	static public String[] split(String source, String delimiters, String[] result)
	{
		if (source == null)
		{
			return null;
		}
		ArrayList<String> resultTmp = null;
		int index = 0;
		int start = 0;
		int end = 0;

		if (result == null)
		{
			resultTmp = new ArrayList<String>();
		}
		while (start < source.length() && (result == null || index < result.length))
		{
			if (end == source.length())
			{
				if (result == null)
				{
					resultTmp.add(source.substring(start));
				}
				else
				{
					result[index++] = source.substring(start);
				}
				start = end + 1;
			}
			else if (delimiters.indexOf(source.charAt(end)) != -1)
			{
				if (result == null)
				{
					resultTmp.add(source.substring(start, end));
				}
				else
				{
					result[index++] = source.substring(start, end);
				}
				start = end + 1;
				end = start;
			}
			else
			{
				++end;
			}
		}
		if (start == source.length() && (result == null || index < result.length))
		{
			if (result == null)
			{
				resultTmp.add("");
			}
			else
			{
				result[index] = "";
			}
		}
		if (result == null)
		{
			return resultTmp.toArray(new String[resultTmp.size()]);
		}
		else
		{
			return result;
		}
	}
}
