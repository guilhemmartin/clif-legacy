/*
* CLIF is a Load Injection Framework
* Copyright (C) 2011 France Telecom R&D
*
* This library is free software; you can redistribute it and/or
* modify it under the terms of the GNU Lesser General Public
* License as published by the Free Software Foundation; either
* version 2 of the License, or (at your option) any later version.
*
* This library is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
* Lesser General Public License for more details.
*
* You should have received a copy of the GNU Lesser General Public
* License along with this library; if not, write to the Free Software
* Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*
* Contact: clif@ow2.org
*/

package org.ow2.clif.util;

/**
 * Abstract class for running asynchronous tasks. Usage:
 * <ol>
 *   <li>derive this class and define the futureExecution() method<li>
 *   <li>create an instance of the derived class</li>
 *   <li>call start() to run the task implemented by the futureExecution() method</li>
 *   <li>then:
 *     <ul>
 *       <li>poll with isComplete() method to know if the task is complete</li>
 *       <li>directly get the task result or exception with method futureGet(),
 *       waiting for its completion when not completed yet</li>
 *       <li>synchronize with the task completion through the optional lock object,
 *       to be notified on task completion</li>
 *     </ul>
 *   </li>
 * </ol>
 * 
 * @author Bruno Dillenseger
 */
public abstract class Future extends Thread
{
	private Exception _exception = null;
	private volatile boolean _terminated = false;
	private Object _result = null;
	private Object _lock = null;


	/**
	 * Creates an asynchronous task, but does not run it.
	 * Method start() must be called then, to actually run the task.
	 * @param threadName an arbitrary convenient name to identify this thread
	 * (for debugging purpose)
	 * @param lock an optional lock object to wait on for synchronization with
	 * this task completion. When null, just 
	 * @see java.lang.Thread#start()
	 */
	public Future(String threadName, Object lock)
	{
		super(threadName);
		if (lock == null)
		{
			_lock = this;
		}
		else
		{
			_lock = lock;
		}
	}


	/**
	 * Calls abstract method futureExecution() which is supposed to implement
	 * (through inheritance) the task, and manages synchronization: on return
	 * from futureExecution():
	 * <ul>
	 *   <li>all calls blocked on method futureGet() are resumed</li>
	 *   <li>method isComplete() definitely returns true instead of false</li>
	 *   <li>when a lock is provided, all threads waiting on this lock are
	 *   notified</li>
	 * </ul>
	 */
	public final void run()
	{
		try
		{
			_result = futureExecution();
		}
		catch (Exception ex)
		{
			_exception = ex;
		}
		synchronized (_lock)
		{
			_terminated = true;
			_lock.notifyAll();
		}
	}


	/**
	 * Blocks caller until the task is completed, and get its result
	 * or exception. A recommended usage is to define an extra
	 * method which calls this method and casts the result to the
	 * specific result type of the task.
	 * @return the task result, i.e. the object returned by method
	 * futureExecution()
	 * @throws Exception if the task defined by method futureExecution()
	 * threw an exception
	 */
	public final Object futureGet()
	throws Exception
	{
		synchronized(_lock)
		{
			while (! _terminated)
			{
				try
				{
					_lock.wait();
				}
				catch (InterruptedException ex)
				{
					ex.printStackTrace(System.err);
				}
			}
			if (_exception == null)
			{
				return _result;
			}
			throw _exception;
		}
	}


	/**
	 * Checks if the task is complete.
	 * @return true if the task is complete, whatever it is successful or not
	 * (i.e. result available or exception thrown), false otherwise.
	 */
	public final boolean isComplete()
	{
		synchronized (_lock)
		{
			return _terminated;
		}
	}


	/**
	 * Define this method to implement the task.
	 * @return the task result, of arbitrary type.
	 * @throws Exception the task could not complete.
	 */
	abstract protected Object futureExecution()
	throws Exception;
}
