/*
 * CLIF is a Load Injection Framework
 * Copyright (C) 2002-2004, 2011 France Telecom R&D
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: clif@ow2.org
 */

package org.ow2.clif.util;

import java.util.HashMap;
import java.util.Map;
import org.objectweb.fractal.adl.implementations.ImplementationBuilder;
import org.objectweb.fractal.api.Component;
import org.objectweb.fractal.api.NoSuchInterfaceException;
import org.objectweb.fractal.api.type.ComponentType;
import org.objectweb.fractal.util.Fractal;

/**
 * A Fractal API-based implementation of the ImplementationBuilder interface
 * for the Fractal ADL parser. This implementation forces the use of
 * clifPrimitive or clifComposite controller specification (see julia.cfg).
 * The goal is to get rid of LifeCycleController for all CLIF components.
 * Based on original source code by Eric Bruneton from OW2 Fractal project:
 * org.objectweb.fractal.adl.implementations.FractalImplementationBuilder.java
 * 
 * @author Eric Bruneton
 * @author Bruno Dillenseger
 */
public class FractalAdl implements ImplementationBuilder
{
	static public final String CLIF_BACKEND = "org.ow2.clif.util.FractalAdlBackend";

	// //////////////////////////////////
	// ImplementationBuilder interface //
	// //////////////////////////////////

	public Object createComponent(
		final Object type,
		final String name,
		final String definition,
		Object controllerDesc,
		final Object contentDesc,
		final Object context)
	throws Exception
	{
		ClassLoader loader = null;
		if (context instanceof Map)
		{
			loader = (ClassLoader) ((Map) context).get("classloader");
		}
		Component bootstrap = null;
		if (context != null)
		{
			bootstrap = (Component) ((Map) context).get("bootstrap");
		}
		if (bootstrap == null)
		{
			if (loader != null)
			{
				Map ctxt = new HashMap();
				ctxt.put("classloader", loader);
				bootstrap = Fractal.getBootstrapComponent(ctxt);
			}
			else
			{
				bootstrap = Fractal.getBootstrapComponent();
			}
		}
		// enforce use of Clif components controller descriptors (wrt julia.cfg)
		controllerDesc = (contentDesc == null ? "clifComposite" : "clifPrimitive");
		Component result = Fractal.getGenericFactory(bootstrap).newFcInstance(
			(ComponentType) type,
			loader == null
				? controllerDesc
				: new Object[] { loader, controllerDesc },
			contentDesc);
		try
		{
			Fractal.getNameController(result).setFcName(name);
		}
		catch (NoSuchInterfaceException ignored)
		{
		}
		return result;
	}
}
