package org.ow2.clif.util;

import java.io.File;
import java.io.IOException;
import java.lang.reflect.Method;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;
import java.util.Enumeration;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;
import java.util.jar.JarEntry;
import java.util.jar.JarFile;


public class ClifPrefetcher {
	private Set<Class<?>> references = new HashSet<Class<?>>();
	
	public int prefetch() {
		prefetchPackage("org.ow2.clif", false);
		prefetchPackage("org.objectweb.fractal", false);
		prefetchPackage("com.wutka", false);
		prefetchPackage("org.objectweb.jonathan", false);
		prefetchPackage("org.jdom", false);
		prefetchPackage("org.jaxen", false);
		prefetchPackage("org.apache.xerces", false);
		prefetchPackage("org.apache.xpath", false);
		prefetchPackage("org.ow2.isac.plugin.chrono", false);
		prefetchPackage("org.ow2.isac.plugin.common", false);
		prefetchPackage("org.ow2.isac.plugin.constanttimer", false);
		prefetchPackage("org.ow2.isac.plugin.context", false);
		prefetchPackage("org.ow2.isac.plugin.counter", false);
		prefetchPackage("org.ow2.isac.plugin.random", false);
		prefetchPackage("org.ow2.isac.plugin.stringhandler", false);
		prefetchPackage("org.ow2.isac.plugin.commandline", false);
		prefetchPackage("org.ow2.isac.plugin.dke_trader", false);
		prefetchPackage("org.ow2.clif.probe.cpu", false);
		return references.size();
	}

	private void prefetch(Class<?> c) throws ClassNotFoundException {

		references.add(c);
		
		for (Class<?> lev2 : c.getDeclaredClasses()) {
			lev2.getDeclaredClasses();
			references.add(lev2);
		}

		for (Method m : c.getDeclaredMethods()) {
			for (Class<?> lev2 : m.getParameterTypes()) {
				lev2.getDeclaredClasses();
			}
			references.add(m.getReturnType());
		}
	}

	public int prefetchPackage(String packageName, boolean exactMatch) {
		
		int count = 0, errors = 0;
		try {
			List<String> list = findClassesInPackage(packageName, exactMatch);
			for (String className : list) {
				try {
					prefetch(Class.forName(className, true, ClassLoader.getSystemClassLoader()));
					count++;
				} catch (Throwable t) {
					errors++;
//					LOGGER.warn("Can't prefetch class " + className + " with System Class Loader");
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
//		LOGGER.info("Prefetched: " + count + " class(es) from " + packageName + (exactMatch?"":"*") + ", and got " + errors + " error(s), total " + references.size() + " classes");
		return count;
	}

	static public List<String> findClassesInPackage(String packageName,
			boolean exactMatch) throws IOException, URISyntaxException {
		packageName = packageName.replace(".", "/");
		URL u = ClifPrefetcher.class.getClassLoader().getResource(packageName);
		LinkedList<String> result = new LinkedList<String>();

		// Only works if all the classes from a package are in a jar
		if (u != null) {
			String file = u.getFile();
			
			if (file.contains("!")) {
				file = file.substring(0, file.indexOf("!"));
			}
			URI uri = new URI(file);
			JarFile jar = new JarFile(new File(uri));
	
			if (!packageName.endsWith("/")) {
				packageName += "/";
			}
	
			for (Enumeration<JarEntry> iter = jar.entries(); iter.hasMoreElements();) {
				JarEntry je = iter.nextElement();
				String name = je.getName();
	
				if (name.startsWith(packageName) && name.endsWith(".class")) {
					String lastportion = name.substring(packageName.length() + 1);
					if (lastportion.contains("/") == false || exactMatch == false) {
						name = name.substring(0, name.length() - ".class".length());
						name = name.replace("/", ".");
						if (name.startsWith("/")) {
							name = name.substring(1);
						}
						result.add(name);
					}
				}
			}
		} else {
//			LOGGER.warn("Found no class to prefetch for package " + packageName);
		}
		return result;
	}

	public static void main(String[] args) throws IOException,
			URISyntaxException {

		ClifPrefetcher c = new ClifPrefetcher();
		c.prefetch();
	}

}
