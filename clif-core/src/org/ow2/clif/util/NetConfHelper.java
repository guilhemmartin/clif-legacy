/*
* CLIF is a Load Injection Framework
* Copyright (C) 2015-2016 Orange SA
*
* This library is free software; you can redistribute it and/or
* modify it under the terms of the GNU Lesser General Public
* License as published by the Free Software Foundation; either
* version 2 of the License, or (at your option) any later version.
*
* This library is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
* Lesser General Public License for more details.
*
* You should have received a copy of the GNU Lesser General Public
* License along with this library; if not, write to the Free Software
* Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*
* Contact: clif@ow2.org
*/
package org.ow2.clif.util;

import java.io.ObjectInputStream;
import java.io.ObjectOutput;
import java.io.ObjectOutputStream;
import java.net.InetAddress;
import java.net.ServerSocket;
import java.net.InetSocketAddress;
import java.net.Socket;
import java.util.Arrays;
import org.objectweb.fractal.rmi.registry.Registry;
import org.ow2.clif.supervisor.api.ClifException;

/**
 * Utility class for automatic IP address selection.
 * Contains both a server part and a client part, to implement a bidirectional
 * TCP-based communication for selecting an IP address that is reachable from
 * the server part:
 * <ol>
 *   <li>the server must be running and listening to an address that is reachable
 *   from the client;</li>
 *   <li>the client opens a TCP connection to the server, and successively sends
 *   the IP addresses of all its locally configured network interfaces (but localhost);</li>
 *   <li>for each received address, the server tries to open a new socket towards the client
 *   towards the received address, and sends the actual client address advertised by the
 *   socket previously opened by the client;</li>
 *   <li>if the client receives the address sent by the server (within a given time-out),
 *   and if this address equals the candidate address that was first sent by the client,
 *   then this address is validated. Otherwise, the candidate address is rejected.
 *   Most probably, the client went through a proxy or Network Address Translation (NAT).</li>
 * </ol>
 * The server part logs all submitted candidate addresses and tells whether they seem to be
 * suitable or not (comparing the sent address with the actual client address advertised by
 * the incoming socket). If no trace is displayed while a configuration client completes,
 * then, either the configuration server is not bound to a reachable IP address, or the
 * client's network connectivity is not sufficient to reach the server. You should try another
 * server IP address.
 * @author Bruno Dillenseger
 */
public class NetConfHelper implements Runnable
{
	/**
	 * Runs either the server part or the client part.
	 * @param args two forms:
	 * <ul>
	 *   <li>server server_IP_address [server_port] - runs the server part,
	 *   opening a TCP server socket on the given IP address and port number.
	 *   When the port number is not given, it defaults to the CLIF registry's
	 *   default port number + 1.</li>
	 *   <li>client server_IP_address server_port timeout_ms - runs the client
	 *   part, trying all available local IP addresses.</li>
	 * </ul>
	 * @throws Exception either the provided arguments are not correct, or the
	 * server part could not be started because of a network-related problem
	 * (e.g. it can't bind to the provided IP address/port). 
	 */
	static public void main(String[] args)
	throws Exception
	{
		if (args[0].equalsIgnoreCase("server"))
		{
			new NetConfHelper().startServer(
				new InetSocketAddress(
					args[1],
					args.length > 1 ? Integer.parseInt(args[2]) : Registry.DEFAULT_PORT + 1));
		}
		else if (args[0].equalsIgnoreCase("client"))
		{
			tryAddresses(args[1], Integer.parseInt(args[2]), Integer.parseInt(args[3]));
		}
		else
		{
			throw new ClifException("Network configuration helper does not understand arguments " + Arrays.toString(args));
		}
	}

	/**
	 * Submit all IP addresses of locally configured network interfaces to
	 * the server part.
	 * @param ip the IP address of the configuration helper's server part
	 * @param port the port number of the configuration helper's server part
	 * @param timeout_ms the time-out value waiting for a reply from the server
	 * after having submitted an IP address.  
	 * @return a valid IP address, or null if none passed the selection.
	 */
	static public InetAddress tryAddresses(String ip, int port, int timeout_ms)
	{
		InetAddress result = null;
		InetSocketAddress remote = new InetSocketAddress(ip, port);
		for (InetAddress local : Network.getInetAddresses())
		{
			if (tryAddress(local, remote, timeout_ms))
			{
				result = local;
			}
		}
		return result;
	}

	/**
	 * Submit an IP address to the network configuration server for checking. 
	 * @param local an IP address among the locally configured network interfaces
	 * @param remote the socket address of the network configuration server
	 * @param timeout_ms timeout for deciding that the address is not correctly routed
	 * @return true if the candidate address was validated by the network configuration
	 * protocol, false otherwise
	 */
	static private boolean tryAddress(InetAddress local, InetSocketAddress remote, int timeout_ms)
	{
		boolean result = false;
		try
		{
			ServerSocket serv = new ServerSocket();
			serv.bind(new InetSocketAddress(local, 0));
			serv.setSoTimeout(timeout_ms);
			Socket outSock = new Socket();
			outSock.bind(new InetSocketAddress(local, 0));
			outSock.connect(remote, timeout_ms);
			ObjectOutputStream oos = new ObjectOutputStream(outSock.getOutputStream());
			oos.writeObject(new InetSocketAddress(serv.getInetAddress(), serv.getLocalPort()));
			oos.writeInt(timeout_ms);
			oos.flush();
			Socket inSock = serv.accept();
			ObjectInputStream ois = new ObjectInputStream(inSock.getInputStream());
			InetAddress returnedLocal = (InetAddress)ois.readObject();
			outSock.close();
			serv.close();
			result = returnedLocal.equals(local);
		}
		catch (Exception ex)
		{
			// never mind: just discard this address
		}
		return result;
	}

	private ServerSocket srvSock;

	/**
	 * Start the server part.
	 * @param localAddr the local IP address and port to bind to.
	 * @throws ClifException the server part of the configuration helper could not be started.
	 */
	public void startServer(InetSocketAddress localAddr)
	throws ClifException
	{
		try
		{
			this.srvSock = new ServerSocket();
			this.srvSock.bind(localAddr);
			this.srvSock.setSoTimeout(0);
			new Thread(this, "Network configuration helper").start();
		}
		catch (Exception ex)
		{
			throw new ClifException("Could not start network configuration server on " + localAddr , ex);
		}
	}

	/**
	 * Stop the server part
	 * @throws ClifException problem while trying to close the server socket
	 */
	public void stopServer()
	throws ClifException
	{
		try
		{
			srvSock.close();
		}
		catch (Exception ex)
		{
			throw new ClifException("Problem while closing the network configuration server on " + srvSock.getInetAddress(), ex);
		}
	}

	////////////////////////
	// Runnable interface //
	////////////////////////

	/**
	 * Server part activity
	 */
	@Override
	public void run()
	{
		while (! srvSock.isClosed())
		{
			try
			{
				// get a candidate IP address from some client
				Socket inSock = srvSock.accept();
				InetAddress remoteAddr = inSock.getInetAddress();
				ObjectInputStream objIn = new ObjectInputStream(inSock.getInputStream());
				InetSocketAddress sentAddr = (InetSocketAddress)objIn.readObject();
				int timeout_ms = objIn.readInt();
				// compare the sent address with the client address advertised by the socket
				// in any case, send the client socket address back to the client, using the sent address
				System.out.println(
					"Received candidate address " + sentAddr.getAddress().getHostAddress()
					+ " from " + remoteAddr.getHostAddress()
					+ (remoteAddr.equals(sentAddr.getAddress()) ? ": OK" : ": NAT or proxy detected (not supported)"));
				Socket outSock = new Socket();
				outSock.bind(null);
				outSock.connect(sentAddr, timeout_ms);
				ObjectOutput objOut = new ObjectOutputStream(outSock.getOutputStream());
				objOut.writeObject(remoteAddr);
				outSock.close();
				inSock.close();
				inSock = null;
			}
			catch (Exception ex)
			{
				// never mind: the candidate address is not suitable
			}
		}
	}
}
