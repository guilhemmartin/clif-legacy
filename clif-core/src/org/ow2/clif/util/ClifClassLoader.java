/*
* CLIF is a Load Injection Framework
* Copyright (C) 2004, 2008, 2011 France Telecom R&D
*
* This library is free software; you can redistribute it and/or
* modify it under the terms of the GNU Lesser General Public
* License as published by the Free Software Foundation; either
* version 2 of the License, or (at your option) any later version.
*
* This library is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
* Lesser General Public License for more details.
*
* You should have received a copy of the GNU Lesser General Public
* License along with this library; if not, write to the Free Software
* Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*
* Contact: clif@ow2.org
*/

package org.ow2.clif.util;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.net.Socket;
import java.net.URL;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.Vector;
import org.ow2.clif.util.bytearray.ByteArrayURLStreamHandler;

/**
 * Special classloader bound to the CodeServer utility, enabling network download
 * of classes and resources. Regardless this download facility, it also provides
 * classes prefetching utilities.
 * @see CodeServer
 * @author Bruno Dillenseger
 */
public class ClifClassLoader extends ClassLoader
{
	static private ClifClassLoader cl = new ClifClassLoader();


	/**
	 * Get the CLIF class loader singleton.
	 * Caution: the singleton changes when calling static method clear().
	 * @return the CLIF class loader singleton
	 * @see #clear()
	 */
	static public ClifClassLoader getClassLoader()
	{
		return cl;
	}


	/**
	 * flushes the class cache and renews the CLIF class loader singleton
	 */
	static public void clear()
	{
		cl.classCache.clear();
		cl.dataCache.clear();
		cl.loadedClasses = null;
		cl = new ClifClassLoader();
	}


	/**
	 * Loads all dependencies of a class, i.e. classes involved in its superclass,
	 * attributes, methods and constructors, in a recursive manner.
	 * Important note: does not use the CLIF class loader but the default context
	 * class loader (i.e. the class loader that loaded the ClifClassLoader class).
	 * @param clazz the class to look for dependencies
	 * @return the list of preloaded classes clazz depends on. The caller may keep
	 * a reference to this list to avoid garbage collection of preloaded classes.
	 */
	static public synchronized List<Class<?>> fetchClassDependencies(Class<?> clazz)
	{
		List<Class<?>> classes = new ArrayList<Class<?>>();
		classes.add(clazz);
		fetchClassDependencies(classes);
		return classes;
	}

	/**
	 * Loads all dependencies of a provided list of classes, i.e. classes involved in
	 * their superclasses, attributes, methods and constructors, in a recursive manner.
	 * Important note: does not use the CLIF class loader but the default context
	 * class loader (i.e. the class loader that loaded the ClifClassLoader class).
	 * @param classes the initial list of classes, that will be extended with all
	 * dependent, preloaded classes
	 */
	static public synchronized void fetchClassDependencies(List<Class<?>> classes)
	{
		if (classes != null)
		{
			int i=0;
			while (i < classes.size())
			{
				addAllUnique(classes, classes.get(i).getClasses());
				addAllUnique(classes, classes.get(i).getDeclaredClasses());
				for (Method meth : classes.get(i).getDeclaredMethods())
				{
					addAllUnique(classes, meth.getParameterTypes());
					addUnique(classes, meth.getReturnType());
				}
				for (Field fld : classes.get(i).getDeclaredFields())
				{
					addUnique(classes, fld.getType());
				}
				for (Constructor<?> cnstr : classes.get(i).getDeclaredConstructors())
				{
					addAllUnique(classes, cnstr.getParameterTypes());
				}
				addUnique(classes, classes.get(i).getSuperclass());
				++i;
			}
		}
	}


	/**
	 * Adds a class to a list of classes if it meets 3 conditions: it is not null,
	 * it is not a primitive type, and it does not belong to the list yet.
	 * @param classes the initial list of classes, that will be extended with the
	 * provided class if it meets the 3 conditions
	 * @param newClass the class to add to the list
	 */
	static private void addUnique(List<Class<?>> classes, Class<?> newClass)
	{
		if (newClass != null && ! newClass.isPrimitive() && ! classes.contains(newClass))
		{
			classes.add(newClass);
		}
	}


	/**
	 * Adds classes to a list of classes if they meet 3 conditions: they are not null,
	 * they are not of a primitive type, and they do not belong to the list yet.
	 * @param classes the initial list of classes, that will be extended with the
	 * provided classes that meet the 3 conditions
	 * @param newClasses the array of classes to add to the list
	 */
	static private void addAllUnique(List<Class<?>> classes, Class<?>[] newClasses)
	{
		for (Class<?> clazz : newClasses)
		{
			addUnique(classes, clazz);
		}
	}


	Map<String,Class<?>> classCache = new HashMap<String,Class<?>>();
	Map<String,byte[]> dataCache = new HashMap<String,byte[]>();
	Set<Class<?>> loadedClasses = new HashSet<Class<?>>();
	Socket sock = null;
	DataInputStream dins;
	DataOutputStream douts;


	private ClifClassLoader()
	{
	    super(ClifClassLoader.class.getClassLoader());
	}


	public synchronized byte[] getBytes(String name)
		throws IOException
	{
		if (sock == null)
		{
			sock = new Socket(
				System.getProperty("clif.codeserver.host", "localhost"),
				Integer.parseInt(
					System.getProperty(
						"clif.codeserver.port",
						String.valueOf(CodeServer.DEFAULT_PORT))));
			dins = new DataInputStream(sock.getInputStream());
			douts = new DataOutputStream(sock.getOutputStream());
		}
		byte[] result = null;
		// try to get the bytes from the cache (in case it has already been downloaded)
		result = dataCache.get(name);
		if (result == null && sock != null)
		{
			douts.writeUTF(name);
			douts.flush();
			int length = dins.readInt();
			if (length >= 0)
			{
				result = new byte[length];
				int offset = 0;
				while (length > 0)
				{
					int n = dins.read(result, offset, length);
					length -= n;
					offset += n;
				}
				dataCache.put(name, result);
			}
			else if (length == CodeServer.NO_SUCH_RESOURCE)
			{
				throw new FileNotFoundException("Code server could not find " + name);
			}
			else if (length == CodeServer.RESOURCE_TOO_BIG)
			{
				throw new IOException("Resource or class " + name + " is too big");
			}
			else
			{
				throw new IOException("Code server cannot deliver " + name + " for unknown reason");
			}
		}
		return result;
	}


	protected synchronized Class<?> findClass(String name)
		throws ClassNotFoundException
	{
		if (name.startsWith("org.objectweb.fractal"))
		{
			return getParent().loadClass(name);
		}
		Class<?> result = null;
		String pathname = name.replace('.', '/') + ".class";
		// first, see if the class can be found using the classpath
		try
		{
			ClassLoader parent = getParent();
			if (parent == null)
			{
				parent = getSystemClassLoader();
			}
			result = parent.loadClass(name);
		}
		catch (ClassNotFoundException e)
		{
			// second, see if the class has already been defined
			result = classCache.get(name);
			if (result == null)
			{
				byte[] code = null;
				// then, try to get the bytes and define the class
				try
				{
					code = getBytes(pathname);
				}
				catch (IOException ex)
				{
					throw new ClassNotFoundException(name, ex);
				}
				if (code != null)
				{
					result = defineClass(name, code, 0, code.length);
					classCache.put(name, result);
				}
				else
				{
					throw new ClassNotFoundException(name);
				}
			}
		}
		return result;
	}


	public URL findResource(String name)
	{
		URL result = super.findResource(name);
		if (result == null)
		{
			try
			{
				result = new URL(null, "bytearray:" + name, new ByteArrayURLStreamHandler(this));
			}
			catch (Exception ex)
			{
				ex.printStackTrace();
			}
		}
		return result;
	}


	/**
	 * @param name the resource's name
	 * @return an empty or single-value URL enumeration for the named resource
	 * @see #findResource(String)
	 */
	public Enumeration<URL> findResources(String name)
	{
		Vector<URL> result = new Vector<URL>(1);
		URL url = findResource(name);
		if (url != null)
		{
			result.add(url);
		}
		return result.elements();
	}
}
