/*
* CLIF is a Load Injection Framework
* Copyright (C) 2003, 2007 France Telecom R&D
* Copyright (C) 2003 INRIA
*
* This library is free software; you can redistribute it and/or
* modify it under the terms of the GNU Lesser General Public
* License as published by the Free Software Foundation; either
* version 2 of the License, or (at your option) any later version.
*
* This library is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
* Lesser General Public License for more details.
*
* You should have received a copy of the GNU Lesser General Public
* License along with this library; if not, write to the Free Software
* Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*
* CLIF $Name$
*
* Contact: clif@ow2.org
*/

package org.ow2.clif.util;


import java.io.Serializable;
import java.math.BigInteger;

/**
 *
 * @author Bruno Dillenseger
 */
public class UniqueKey implements Serializable
{
	private static final long serialVersionUID = -7891940242919741736L;

	static protected BigInteger nextKey = BigInteger.ZERO;

	protected BigInteger value;


	public UniqueKey()
	{
		synchronized (UniqueKey.class)
		{
			value = nextKey;
			nextKey = nextKey.add(BigInteger.ONE);
		}
	}


	@Override
	public boolean equals(Object obj)
	{
		if (obj instanceof UniqueKey)
		{
			return ((UniqueKey)obj).value.equals(value);
		}
		else
		{
			return false;
		}
	}


	@Override
	public int hashCode()
	{
		return value.hashCode();
	}
}
