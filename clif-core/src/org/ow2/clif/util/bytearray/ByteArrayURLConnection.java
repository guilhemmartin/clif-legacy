/*
* CLIF is a Load Injection Framework
* Copyright (C) 2004, 2005 France Telecom R&D
*
* This library is free software; you can redistribute it and/or
* modify it under the terms of the GNU Lesser General Public
* License as published by the Free Software Foundation; either
* version 2 of the License, or (at your option) any later version.
*
* This library is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
* Lesser General Public License for more details.
*
* You should have received a copy of the GNU Lesser General Public
* License along with this library; if not, write to the Free Software
* Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*
* CLIF $Name: not supported by cvs2svn $
*
* Contact: clif@ow2.org
*/

package org.ow2.clif.util.bytearray;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.net.URLConnection;

import org.ow2.clif.util.ClifClassLoader;


/**
 *
 * @author Bruno Dillenseger
 */
public class ByteArrayURLConnection	extends URLConnection
{
	String name;
	ClifClassLoader loader;
	byte[] bytes;


	public ByteArrayURLConnection(URL url, ClifClassLoader classloader)
	{
		super(url);
		name = url.getFile();
		loader = classloader;
	}


	@Override
	public void connect()
		throws IOException
	{
		if (! connected)
		{
			connected = true;
			bytes = loader.getBytes(name);
		}
	}


	@Override
	public int getContentLength()
	{
		try
		{
			connect();
		}
		catch (IOException ex)
		{
		}
		return bytes == null ? -1 : bytes.length;
	}


	@Override
	public String getContentType()
	{
		String type = guessContentTypeFromName(name);
		if (type == null)
		{
			try
			{
				connect();
				if (bytes != null)
				{
					type = guessContentTypeFromStream(new ByteArrayInputStream(bytes));
				}
			}
			catch (IOException ex)
			{
			}
		}
		return type;
	}


	@Override
	public Object getContent()
		throws IOException
	{
		connect();
		if (bytes != null)
		{
			return bytes;
		}
		else
		{
			throw new IOException("Resource " + name + " is not available");
		}
	}


	@Override
	public InputStream getInputStream()
		throws IOException
	{
		connect();
		if (bytes != null)
		{
			return new ByteArrayInputStream(bytes);
		}
		else
		{
			throw new IOException("Resource " + name + " is not available");
		}
	}


	@Override
	public String toString()
	{
		return "ByteArrayURLConnection for " + loader + ", URL " + getURL();
	}
}
