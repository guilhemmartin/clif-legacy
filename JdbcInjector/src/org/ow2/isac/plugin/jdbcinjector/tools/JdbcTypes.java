/*
 * CLIF is a Load Injection Framework
 * Copyright (C) 2004 France Telecom R&D
 *
 * Contact: clif@ow2.org
 *
 * @author P Crepieux 18/04/2007
 */

package org.ow2.isac.plugin.jdbcinjector.tools;
import java.util.HashMap;

public final class JdbcTypes {
	public static HashMap toJdbcMap=new HashMap();;
	
	static {
		toJdbcMap.put("CHAR"    ,new Integer(java.sql.Types.CHAR));
		toJdbcMap.put("VARCHAR" ,new Integer(java.sql.Types.VARCHAR));
		toJdbcMap.put("TINYINT" ,new Integer(java.sql.Types.TINYINT));
		toJdbcMap.put("SMALLINT",new Integer(java.sql.Types.SMALLINT));
		toJdbcMap.put("INTEGER" ,new Integer(java.sql.Types.INTEGER));
		toJdbcMap.put("BIGINT"  ,new Integer(java.sql.Types.BIGINT));
		toJdbcMap.put("FLOAT"   ,new Integer(java.sql.Types.FLOAT));
		toJdbcMap.put("DATE"    ,new Integer(java.sql.Types.DATE));
		toJdbcMap.put("TIME"    ,new Integer(java.sql.Types.TIME));
		toJdbcMap.put("TIMESTAMP",new Integer(java.sql.Types.TIMESTAMP));
	}
}
 