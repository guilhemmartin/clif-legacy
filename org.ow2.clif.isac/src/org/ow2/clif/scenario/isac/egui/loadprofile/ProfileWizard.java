/*
* CLIF is a Load Injection Framework
* Copyright (C) 2005 France Telecom R&D
*
* This library is free software; you can redistribute it and/or
* modify it under the terms of the GNU Lesser General Public
* License as published by the Free Software Foundation; either
* version 2 of the License, or (at your option) any later version.
*
* This library is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
* Lesser General Public License for more details.
*
* You should have received a copy of the GNU Lesser General Public
* License along with this library; if not, write to the Free Software
* Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*
* Contact: clif@ow2.org
*/

package org.ow2.clif.scenario.isac.egui.loadprofile;

import java.util.ArrayList;
import java.util.Iterator;
import org.eclipse.jface.viewers.ILabelProviderListener;
import org.eclipse.jface.viewers.ISelectionChangedListener;
import org.eclipse.jface.viewers.IStructuredContentProvider;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.jface.viewers.ITableLabelProvider;
import org.eclipse.jface.viewers.SelectionChangedEvent;
import org.eclipse.jface.viewers.TableViewer;
import org.eclipse.jface.viewers.Viewer;
import org.eclipse.jface.wizard.Wizard;
import org.eclipse.jface.wizard.WizardPage;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.ModifyEvent;
import org.eclipse.swt.events.ModifyListener;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.SelectionListener;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Group;
import org.eclipse.swt.widgets.Table;
import org.eclipse.swt.widgets.TableColumn;
import org.eclipse.swt.widgets.Text;
import org.eclipse.ui.INewWizard;
import org.eclipse.ui.IWorkbench;
import org.ow2.clif.scenario.isac.egui.FileName;
import org.ow2.clif.scenario.isac.egui.Icons;
import org.ow2.clif.scenario.isac.egui.model.ModelWriterXIS;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;

/**
 * This wizard is used for editing or creating a profile
 * for a behavior
 * @author Joan Chaumont
 *
 */
public class ProfileWizard extends Wizard implements INewWizard {
   
    private String behaviorId;
    private ProfileWizardPage page;
    private Document doc;
    
    private LoadProfile profile;
    private boolean force;
    
    /**
     * Constructor
     * @param doc
     * @param behaviorId
     * @param profile
     */
    public ProfileWizard(Document doc, String behaviorId, LoadProfile profile) {
        super();
        
        this.behaviorId = behaviorId;
        this.doc = doc;
        this.profile = profile;
        this.force = (profile == null)?false:profile.getForceStop();
        
        setWindowTitle("Load profile");
    }

    class ProfileContentProvider 
    implements IStructuredContentProvider, ITableLabelProvider {

        public Object[] getElements(Object inputElement) {
            LoadProfile p = (LoadProfile)inputElement;
            return p.getProfilesPoints().toArray();
        }

        public void dispose() {}

        public void inputChanged(Viewer viewer, Object oldInput, Object newInput) {}

        public Image getColumnImage(Object element, int columnIndex) {
            if(columnIndex == 2) {
                String style = FileName.LINE_ICON;
                if(((ProfilePoint)element).rampStyle.equals("crenel_vh")) {
                    style = FileName.VH_ICON;
                }
                else if(((ProfilePoint)element).rampStyle.equals("crenel_hv")) {
                    style = FileName.HV_ICON;
                }
                return Icons.getImageRegistry().get(style);
            }
            return null;
        }

        public String getColumnText(Object element, int columnIndex) {
            switch (columnIndex) {
            case 0:
                return String.valueOf(((ProfilePoint)element).time);
            case 1:
                return String.valueOf(((ProfilePoint)element).population);
            default:
                break;
            }
            return null;
        }

        public void addListener(ILabelProviderListener listener) {}
        public void removeListener(ILabelProviderListener listener) {}
        public boolean isLabelProperty(Object element, String property) {
            return false;
        }
    }
    
    
    class ProfileWizardPage extends WizardPage 
    implements ModifyListener, ISelectionChangedListener, SelectionListener {
        
        private Button bForce;
        private Button bVH;
        private Button bLine;
        private Button bHV;
        
        private Text tTime;
        private Text tPopulation;
        
        private Table table;
        private TableViewer tableV;

        private Button addPoint;
        private Button modifyPoint;
        private Button removePoint;
        
        protected ProfileWizardPage() {
            super("profilWizard");
            setTitle("Add points for this profile " + behaviorId);
            
            if(profile == null) {
                profile = new LoadProfile(behaviorId, new ArrayList<ProfilePoint>(), false);            
            }
        }
        
        public void createControl(Composite parent) {
            Composite main = new Composite(parent, SWT.NONE);
            main.setLayout(new GridLayout());
            main.setLayoutData(new GridData(GridData.FILL_BOTH));
            
            Composite edit = new Composite(main, SWT.NONE);
            GridLayout layout = new GridLayout(3, true);
            edit.setLayout(layout);
            edit.setLayoutData(new GridData(GridData.FILL_HORIZONTAL));

            /* Params of a point : time, population, rampStyle */
            Group time = new Group(edit, SWT.NONE);
            time.setLayout(new GridLayout(2, false));
            time.setLayoutData(new GridData(GridData.FILL_HORIZONTAL));
            time.setText("Time (s)");
            tTime = new Text(time, SWT.BORDER);
            tTime.setLayoutData(new GridData(GridData.FILL_HORIZONTAL));
            tTime.addModifyListener(this);
            
            Group population = new Group(edit, SWT.NONE);
            population.setLayout(new GridLayout(2, false));
            population.setLayoutData(new GridData(GridData.FILL_HORIZONTAL));
            population.setText("Population");
            tPopulation = new Text(population, SWT.BORDER);
            tPopulation.setLayoutData(new GridData(GridData.FILL_HORIZONTAL));
            tPopulation.addModifyListener(this);
            
            /* Radio buttons for rampStyle */
            Group rampStyle = new Group(edit, SWT.NONE);
            rampStyle.setLayout(new GridLayout(3, false));
            rampStyle.setLayoutData(new GridData(GridData.FILL_HORIZONTAL));
            rampStyle.setText("Ramp style");
            bLine = new Button(rampStyle, SWT.RADIO);
            bLine.setImage(Icons.getImageRegistry().get(FileName.LINE_ICON));
            bLine.setSelection(true);
            
            bVH = new Button(rampStyle, SWT.RADIO);
            bVH.setImage(Icons.getImageRegistry().get(FileName.VH_ICON));
            bHV = new Button(rampStyle, SWT.RADIO);
            bHV.setImage(Icons.getImageRegistry().get(FileName.HV_ICON));
                      
            bForce = new Button(main, SWT.CHECK);
            bForce.setText("Force stop");
            bForce.setSelection(force);
            bForce.addSelectionListener(this);
            
            /* Points edition */
            Group tablePoints = new Group(main, SWT.NONE);
            tablePoints.setText("Points");
            tablePoints.setLayout(new GridLayout(2, false));
            tablePoints.setLayoutData(new GridData(GridData.FILL_BOTH));
            
            table = new Table(tablePoints, SWT.SINGLE);
            table.setLayout(new GridLayout());
            table.setLayoutData(new GridData(GridData.FILL_BOTH));
            table.setHeaderVisible(true);
            
            TableColumn col = new TableColumn(table, SWT.LEFT);
            col.setText("Time");
            col.setWidth(100);
            col = new TableColumn(table, SWT.LEFT);
            col.setText("Population");
            col.setWidth(100);
            col = new TableColumn(table, SWT.LEFT);
            col.setText("Ramp style");
            col.setWidth(100);
            
            /* This table displays the points of this profile */
            tableV = new TableViewer(table);
            ProfileContentProvider provider = new ProfileContentProvider();
            tableV.setContentProvider(provider);
            tableV.setLabelProvider(provider);
            tableV.setInput(profile);
            tableV.addSelectionChangedListener(this);
            
            /* Buttons for point edition */
            Composite tableButtons = new Composite(tablePoints, SWT.NULL);
            tableButtons.setLayout(new GridLayout());
            
            addPoint = new Button(tableButtons, SWT.PUSH);
            addPoint.setLayoutData(new GridData(GridData.FILL_HORIZONTAL));
            addPoint.setText("Add point");
            addPoint.addSelectionListener(new SelectionAdapter() {
                public void widgetSelected(SelectionEvent e) {
                    addPoint();
                }
            });
            modifyPoint = new Button(tableButtons, SWT.PUSH);
            modifyPoint.setLayoutData(new GridData(GridData.FILL_HORIZONTAL));
            modifyPoint.setText("Modify point");
            modifyPoint.setEnabled(false);
            modifyPoint.addSelectionListener(new SelectionAdapter() {
                public void widgetSelected(SelectionEvent e) {
                    modifyPoint();
                }
            });
            removePoint = new Button(tableButtons, SWT.PUSH);
            removePoint.setLayoutData(new GridData(GridData.FILL_HORIZONTAL));
            removePoint.setText("Remove");
            removePoint.setEnabled(false);
            removePoint.addSelectionListener(new SelectionAdapter() {
                public void widgetSelected(SelectionEvent e) {
                    removePoint();
                }
            });
            setControl(parent);
            setPageComplete(profile.isCorrect() && profile.enoughPoints());
        }
        
        private void addPoint() {
            int time = Integer.parseInt(tTime.getText());
            int population = Integer.parseInt(tPopulation.getText());
            String rampStyle = getRampStyle();
            
            profile.addPoint(new ProfilePoint(time, population, rampStyle));
            tableV.setInput(profile);
            setPageComplete(profile.isCorrect() && profile.enoughPoints());
        }

        private void modifyPoint() {
            ProfilePoint pt = getSelection();
            if(pt != null) {
                int time = Integer.parseInt(tTime.getText());
                int population = Integer.parseInt(tPopulation.getText());
                profile.removePoint(pt);
                profile.addPoint(new ProfilePoint(time, population, getRampStyle()));
                tableV.setInput(profile);
            }
            setPageComplete(profile.isCorrect() && profile.enoughPoints());
        }
        
        private void removePoint() {
            profile.removePoint(getSelection());
            tableV.setInput(profile);
            setPageComplete(profile.isCorrect() && profile.enoughPoints());
        }

        /* on modify text we test if 
         * the new values are correct */
        public void modifyText(ModifyEvent e) {
            setErrorMessage(null);
            try {
                int population = Integer.parseInt(tPopulation.getText());
                int time = Integer.parseInt(tTime.getText());
                if(time < 0 || population < 0) {
                    addPoint.setEnabled(false);
                    modifyPoint.setEnabled(false);
                    setErrorMessage("Invalid values");
                }
                else {
                    addPoint.setEnabled(true);
                    modifyPoint.setEnabled(getSelection() != null);
                }
            } catch (NumberFormatException ex) {
                addPoint.setEnabled(false);
                modifyPoint.setEnabled(false);
                setErrorMessage("Invalid values");
            }
            setPageComplete(isPageComplete());
        }

        public void selectionChanged(SelectionChangedEvent event) {
            ProfilePoint pt = getSelection();
            if(pt != null) {
                tTime.setText(String.valueOf(pt.time));
                tPopulation.setText(String.valueOf(pt.population));
                setRampStyle(pt);
                modifyPoint.setEnabled(false);
                removePoint.setEnabled(false);
            }
            
            addPoint.setEnabled(true);
            modifyPoint.setEnabled(true);
            removePoint.setEnabled(true);
            modifyText(null);
        }

        private ProfilePoint getSelection() {
            IStructuredSelection sel = (IStructuredSelection)tableV.getSelection();
            if(sel != null) {
                return (ProfilePoint)sel.getFirstElement();
            }
            return null;
        }

        private String getRampStyle() {
            String rampStyle = "line";
            if(bVH.getSelection()) {
                rampStyle = "crenel_vh";
            }
            else if(bHV.getSelection()) {
                rampStyle = "crenel_hv";
            }
            return rampStyle;
        }

        private void setRampStyle(ProfilePoint pt) {
            bLine.setSelection(false);
            bVH.setSelection(false);
            bHV.setSelection(false);
            if(pt.rampStyle.equals("crenel_vh")) {
                bVH.setSelection(true);
            }
            else if(pt.rampStyle.equals("crenel_hv")) {
                bHV.setSelection(true);
            }
            else {
                bLine.setSelection(true);
            }
        }

        public void setPageComplete(boolean complete) {
            String message1 = "More points needed";
            String message2 = "Two points have the same time value";
            String message;
            if(!profile.enoughPoints()) {
                message = message1;
            }
            else {
                message = message2;
            }
            if(!complete) {
                setErrorMessage(message); 
            }
            else {
                String errorMsg = getErrorMessage();
                if(!message1.equals(errorMsg) && !message2.equals(errorMsg)) {
                    setErrorMessage(errorMsg);
                }
                else {
                    setErrorMessage(null);
                }
            }
            super.setPageComplete(complete);
        }

        public void widgetSelected(SelectionEvent e) {
            if(e.getSource().equals(bForce)) {
                force = bForce.getSelection();
            }
        }

        public void widgetDefaultSelected(SelectionEvent e) {}
    }
    
    public void addPages() {
        page = new ProfileWizardPage();
        addPage(page);
    }
    
    /* Write profile to file */
    public boolean performFinish() {
        NodeList groups = doc.getElementsByTagName("group");
        int nbGroups = groups.getLength();
        Element grp = null;
        
        /* test if loadprofile node exists */
        Element loadprofile = 
            (Element)doc.getElementsByTagName("loadprofile").item(0);
        if(loadprofile == null) {
            loadprofile = ModelWriterXIS.correctLoadProfile(doc);
        }
        
        /* Remove all child in group if this profile already exists */
        for (int i = 0; i < nbGroups; i++) {
            Element tmp = (Element)groups.item(i);
            if(tmp.getAttribute("behavior").equals(behaviorId)) {
                grp = tmp;
                while(grp.hasChildNodes()) {
                    grp.removeChild(grp.getFirstChild());
                }
                break;
            }
        }
        
        /* create new group */
        if(grp == null) {
            grp = doc.createElement("group");
            grp.setAttribute("behavior", behaviorId);
        }
        
        grp.setAttribute("forceStop", String.valueOf(force));
        profile.setForceStop(force);
        
        /* add point at (0,0) if there is no point at time = 0 */
        if(((ProfilePoint)profile.getProfilesPoints().get(0)).time != 0) {
            profile.addPoint(new ProfilePoint(0,0,"line"));
        }
        
        Iterator iter = profile.getProfilesPoints().iterator();
        ProfilePoint start = (ProfilePoint)iter.next();
        while(iter.hasNext()) {
            ProfilePoint end = (ProfilePoint)iter.next();
            Element ramp = doc.createElement("ramp");
            ramp.setAttribute("style", end.rampStyle);
            
            /* new 2 points for a ramp : see scenario.dtd */
            Element points = doc.createElement("points");
            Element eltS = doc.createElement("point");
            Element eltE = doc.createElement("point");
            
            eltS.setAttribute("x", String.valueOf(start.time));
            eltS.setAttribute("y", String.valueOf(start.population));
            eltE.setAttribute("x", String.valueOf(end.time));
            eltE.setAttribute("y", String.valueOf(end.population));
            
            points.appendChild(eltS);
            points.appendChild(eltE);
            
            ramp.appendChild(points);
            grp.appendChild(ramp);
            
            /* the end point is the start point of next ramp */
            start = end;
        }
        
        loadprofile.appendChild(grp);
        return true;
    }

    public void init(IWorkbench workbench, IStructuredSelection selection) {}
}
