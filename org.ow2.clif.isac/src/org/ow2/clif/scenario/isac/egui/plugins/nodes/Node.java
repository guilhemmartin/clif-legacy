/*
* CLIF is a Load Injection Framework
* Copyright (C) 2004 France Telecom R&D
*
* This library is free software; you can redistribute it and/or
* modify it under the terms of the GNU Lesser General Public
* License as published by the Free Software Foundation; either
* version 2 of the License, or (at your option) any later version.
*
* This library is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
* Lesser General Public License for more details.
*
* You should have received a copy of the GNU Lesser General Public
* License along with this library; if not, write to the Free Software
* Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*
* CLIF 
*
* Contact: clif@ow2.org
*/
package org.ow2.clif.scenario.isac.egui.plugins.nodes;

/**
 * This interface defined the differents types allowed for tree nodes
 * 
 * @author JC Meillaud
 * @author A Peyrard
 */
public class Node {
    /** This type is used to be an unknow type */
    public static final String UNKNOW = "unknow";
    /** Type for a control scenario */
    public static final String SCENARIO = "scenario";
    /** Type for a control node */
    public static final String IF = "if";
    /** Type for a control node */
    public static final String THEN = "then";
    /** Type for a control node */
    public static final String ELSE = "else";
    /** Type for a control node */
    public static final String WHILE = "while";
    /** Type for a control node */
    public static final String PREEMPTIVE = "preemptive";
    /** Type for a control node */
    public static final String NCHOICE = "nchoice";
    /** Type for separate the different choice for a nchoice controler */
    public static final String CHOICE = "choice" ;
    
    /** Xml name for behaviorS tag */
    public static final String BEHAVIORS = "behaviors";
    /** Xml name for behavior tag */
    public static final String BEHAVIOR = "behavior";
    /** Xml name for plugins tag */
    public static final String PLUGINS = "plugins";
    /** Xml name for test tag */
    public static final String TEST = "test";
    /** Xml name for timer tag */
    public static final String TIMER = "timer";
    /** Xml name for control tag */
    public static final String CONTROL = "control";
    /** Xml name for sample tag */
    public static final String SAMPLE = "sample";
    /** Xml name for action tag (replacement of sample?) */
    public static final String ACTION = "action";
    /** Xml name for use tag */
    public static final String USE = "use";
    /** Xml name for loadprofile tag */
    public static final String LOAD_PROFILE = "loadprofile";
    /** Xml name for condition tag */
    public static final String CONDITION = "condition" ;
    /** Xml name for paramS tag */
    public static final String PARAMS = "params" ;
    /** Type for a param node */
    public static final String PARAM = "param" ;
    
    /** type for a definition of a group */
    public static final String GROUP = "group";
    /** type for the definition of a ramp */
    public static final String RAMP = "ramp";
    /** type for the definition of the points of a ramp */
    public static final String POINTS = "points";
    /** type for a point node */
    public static final String POINT = "point";
       
    /**
     * Method which evaluate if the given string is a type of a plugin node
     * @param type The type which will be evaluate
     * @return True if it's a plugin node
     */
    public static boolean isPluginNode(String type) {
        if (type == null) {
            return false ;
        }
        return (type.equals(Node.SAMPLE) || type.equals(Node.TIMER) 
                || type.equals(Node.TEST) || type.equals(Node.USE))
                || type.equals(Node.ACTION) || type.equals(Node.CONTROL);
    }
    
    /**
     * ethod which evaluate if the given string is a type of a use node
     * @param type he type which will be evaluate
     * @return True if it's a use node
     */
    public static boolean isUseNode(String type) {
        if (type == null) {
            return false ;
        }
        return type.equals(Node.USE);
    }
    
    /**
     * Method which evaluate if the given string is a type of a controler node
     * @param type The type which will be evaluate
     * @return True if it's a controler type
     */
    public static boolean isControllerNode(String type) {
        if (type == null) {
            return false; 
        }
        return (type.equals(Node.IF) || type.equals(Node.WHILE)
                || type.equals(Node.PREEMPTIVE)); 
    }
    
    /**
     * This method evaluate if the given type is a behaviors tree node
     * @param type The type wich will be evaluate
     * @return True if the type is a behaviors tree node, False in the others cases
     */
    public static boolean isBehaviorsNode(String type) {
        if (type == null) {
            return false ;
        }
        return (type.equals(Node.SAMPLE) || type.equals(Node.TIMER)
                || type.equals(Node.TEST) || type.equals(Node.USE)
                || type.equals(Node.IF) || type.equals(Node.THEN)
                || type.equals(Node.ELSE) || type.equals(Node.WHILE)
                || type.equals(Node.PREEMPTIVE) || type.equals(Node.NCHOICE)
                || type.equals(Node.CHOICE) || type.equals(Node.BEHAVIORS)
                || type.equals(Node.BEHAVIOR) || type.equals(Node.PLUGINS)
                || type.equals(Node.CONTROL));
    }
    
    /**
     * Method which return if the type is a structure node type
     * @param type The node type
     * @return True id the type is structure node type
     */
    public static boolean isStructureNode(String type) {
        if (type == null) {
            return false ;
        }
        return (type.equals(BEHAVIOR) || type.equals(PLUGINS) 
                || type.equals(BEHAVIORS)); 
    }
    
    /**
     * Method which return if the type is a choice node type
     * @param type The node type
     * @return True id the type is choice node type
     */
    public static boolean isChoiceNode(String type) {
        if (type == null) {
            return false ;
        }
        return type.equals(CHOICE); 
    }
}
