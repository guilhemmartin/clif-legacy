/*
* CLIF is a Load Injection Framework
* Copyright (C) 2005 France Telecom R&D
*
* This library is free software; you can redistribute it and/or
* modify it under the terms of the GNU Lesser General Public
* License as published by the Free Software Foundation; either
* version 2 of the License, or (at your option) any later version.
*
* This library is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
* Lesser General Public License for more details.
*
* You should have received a copy of the GNU Lesser General Public
* License along with this library; if not, write to the Free Software
* Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*
* CLIF $Name: not supported by cvs2svn $
*
* Contact: clif@ow2.org
*/

package org.ow2.clif.scenario.isac.egui.pages.pageImport;

import java.util.Iterator;
import java.util.Vector;

import org.eclipse.jface.action.Action;
import org.eclipse.jface.dialogs.IInputValidator;
import org.eclipse.jface.dialogs.InputDialog;
import org.eclipse.jface.text.IDocument;
import org.eclipse.jface.viewers.ISelectionChangedListener;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.jface.viewers.SelectionChangedEvent;
import org.eclipse.jface.wizard.WizardDialog;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.SelectionListener;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Link;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Text;
import org.eclipse.swt.widgets.Tree;
import org.eclipse.swt.widgets.TreeItem;
import org.eclipse.ui.forms.DetailsPart;
import org.eclipse.ui.forms.IManagedForm;
import org.eclipse.ui.forms.MasterDetailsBlock;
import org.eclipse.ui.forms.SectionPart;
import org.eclipse.ui.forms.widgets.FormToolkit;
import org.eclipse.ui.forms.widgets.ScrolledForm;
import org.eclipse.ui.forms.widgets.Section;
import org.eclipse.wst.xml.core.internal.document.DocumentImpl;
import org.eclipse.wst.xml.core.internal.document.ElementImpl;
import org.ow2.clif.scenario.isac.egui.IsacEditor;
import org.ow2.clif.scenario.isac.egui.IsacScenarioPlugin;
import org.ow2.clif.scenario.isac.egui.ScenarioManager;
import org.ow2.clif.scenario.isac.egui.model.ModelReaderXIS;
import org.ow2.clif.scenario.isac.egui.model.ModelWriterXIS;
import org.w3c.dom.Element;
import org.w3c.dom.Node;

/**
 * Master part of the pattern Master/Details.
 * It display a table of all imported plug-ins and
 * define a set of actions for managing these plug-ins.
 *  
 * @author Joan Chaumont
 */
public class ImportMasterPage extends MasterDetailsBlock implements ISelectionChangedListener {

    private ScenarioManager scenario;
    private IsacEditor editor;
    
    private ImportTableViewer importTable;
    
    private SelectionListener actionAddPlugin;
    private SelectionListener actionRemovePlugin;
    private SelectionListener actionRemoveAllPlugins;
    private SelectionListener actionHelpPlugin;
    private SelectionListener actionAddBehavior;
    private SelectionListener actionUp;
    private SelectionListener actionDown;
    
    private ImportDetailsPage detailsPage;
    private IManagedForm managedForm;
    private SectionPart spart;
    
    /**
     * Constructor
     * @param scenario
     * @param editor
     */
    public ImportMasterPage(ScenarioManager scenario, IsacEditor editor) {
        super();
        this.scenario = scenario;
        this.editor = editor;
        
        detailsPage = new ImportDetailsPage(this.scenario, this.editor);
    }

    protected void createMasterPart(IManagedForm managedForm, Composite parent) {
        this.managedForm = managedForm;
        FormToolkit toolkit = managedForm.getToolkit();
        ScrolledForm form = managedForm.getForm();
        form.setText("Import Page :");
        
        /* Create "Injectors and probes" section */
        Section section = toolkit.createSection(parent,
                Section.DESCRIPTION|Section.EXPANDED);
        section.setText("Plug-ins :");
        section.setDescription("List of plug-ins used in this scenario");
        toolkit.createCompositeSeparator(section);
        section.marginWidth = 10;
        section.marginHeight = 5;
        
        /* Composite client for plug-ins table */
        Composite sectionClient = toolkit.createComposite(section, SWT.FILL);
        GridLayout layout = new GridLayout();
        layout.numColumns = 2;
        layout.marginWidth = 1;
        layout.marginHeight = 1;
        sectionClient.setLayout(layout);
        sectionClient.setLayoutData(new GridData(GridData.FILL_BOTH));
        
        section.setClient(sectionClient);
        this.spart = new SectionPart(section);
        final SectionPart spart = this.spart;
        managedForm.addPart(spart);
        
        importTable = new ImportTableViewer(sectionClient, 
                SWT.MULTI | SWT.H_SCROLL | SWT.V_SCROLL | SWT.BORDER, scenario);
        importTable.getControl().setLayoutData(new GridData(GridData.FILL_BOTH));
        
        importTable.addSelectionChangedListener(this);
        
        /* Actions are needed for adding buttons */
        createActions();
        
        /* Edit buttons composite */
        Composite sectionButtons = toolkit.createComposite(sectionClient, SWT.WRAP);
        sectionButtons.setLayout(new GridLayout());
        sectionButtons.setLayoutData(new GridData(GridData.VERTICAL_ALIGN_BEGINNING));
        
        Button bAdd = toolkit.createButton(sectionButtons, "Add", SWT.PUSH);
        bAdd.setToolTipText("Add a new plug-in");
        GridData gd = new GridData(GridData.FILL_HORIZONTAL);
        bAdd.setLayoutData(gd);
        bAdd.addSelectionListener(actionAddPlugin);
        
        Button bRemove = toolkit.createButton(sectionButtons, "Remove", SWT.PUSH);
        bRemove.setToolTipText("Remove the selected plug-in");
        gd = new GridData(GridData.FILL_HORIZONTAL);
        bRemove.setLayoutData(gd);
        bRemove.addSelectionListener(actionRemovePlugin);
        
        Button bRemoveAll = toolkit.createButton(sectionButtons, "Remove All", SWT.PUSH);
        bRemoveAll.setToolTipText("Remove all plug-ins");
        gd = new GridData(GridData.FILL_HORIZONTAL);
        bRemoveAll.setLayoutData(gd);
        bRemoveAll.addSelectionListener(actionRemoveAllPlugins);
        
        /* Show help for plugin */
        Button bHelp = toolkit.createButton(sectionButtons, "Help", SWT.PUSH);
        bHelp.setToolTipText("Plug-in help");
        bHelp.setLayoutData(new GridData(GridData.FILL_HORIZONTAL));
        bHelp.addSelectionListener(actionHelpPlugin);
        
        /* up and down buttons (new composite 
         * because we put 2 buttons on a line) */
        Composite upDown = toolkit.createComposite(sectionButtons);
        upDown.setLayout(new GridLayout(2, true));
        Button bUp = toolkit.createButton(upDown, "Up", SWT.PUSH);
        bUp.addSelectionListener(actionUp);
        bUp.setLayoutData(new GridData(GridData.FILL_HORIZONTAL));
        Button bDown = toolkit.createButton(upDown, "Down", SWT.PUSH);
        bDown.setLayoutData(new GridData(GridData.FILL_HORIZONTAL));
        bDown.addSelectionListener(actionDown);
        
        /* Link for adding behavior */
        Link addBehavior = new Link(sectionButtons, SWT.NONE);
        addBehavior.setText("<A>Add behavior</A>");
        addBehavior.addSelectionListener(actionAddBehavior);
        
        section.setClient(sectionClient);
        toolkit.paintBordersFor(sectionClient);
    }
    
    private void createActions() {
        /* Manage plug-ins */
        createAddAction();
        createRemoveAction();
        createRemoveAllAction();
        createHelpAction();
        
        /* Move plug-ins*/
        createActionUp();
        createActionDown();
        
        /* Add behavior */
        createAddBehavior();
    }


    /* Create the add action
     * @see PluginWizard */
    private void createAddAction() {
        actionAddPlugin = new SelectionAdapter() {

            public void widgetSelected(SelectionEvent e) {
                Shell s = ((Button)e.getSource()).getShell();
                DocumentImpl doc = (DocumentImpl)importTable.getInput();
                WizardDialog dialog = 
                    new WizardDialog(s, new PluginWizard(doc, scenario.getPluginManager()));
                dialog.setPageSize(300,400);
                dialog.open();
                importTable.refresh();
                refresh();
            }
        };
    }
    
    /* Remove all selected plug-ins */
    private void createRemoveAction() {
        actionRemovePlugin = new SelectionAdapter() {

            public void widgetSelected(SelectionEvent e) {
                DocumentImpl doc = (DocumentImpl)importTable.getInput();
                IStructuredSelection sel = 
                    (IStructuredSelection)importTable.getSelection();
                
                Iterator iter = sel.iterator();
                while(iter.hasNext()) {
                    Element plugin = (Element)iter.next();
                    Element plugins = (Element)plugin.getParentNode();
                    /* Test if node plug-ins is correct */
                    if(!plugins.getNodeName().equals("plugins")) {
                        plugins = ModelWriterXIS.correctPlugins(doc);
                    }
                    
                    plugins.removeChild(plugin);
                }
                importTable.refresh();
                refresh();
            }
        };
    }
    /* Remove all plug-ins */
    private void createRemoveAllAction() {
        actionRemoveAllPlugins = new SelectionAdapter() {

            public void widgetSelected(SelectionEvent e) {
                DocumentImpl doc = (DocumentImpl)importTable.getInput();
                
                Element plugins = 
                    (Element)doc.getElementsByTagName("plugins").item(0);
                if(plugins == null) {
                    plugins = ModelWriterXIS.correctPlugins(doc);
                }
                
                while(plugins.hasChildNodes()) {
                    Node plugin = plugins.getFirstChild();
                    plugins.removeChild(plugin);
                }
                importTable.refresh();
                refresh();
            }
        };
    }
    
    private void createHelpAction() {
        actionHelpPlugin = new SelectionAdapter() {

            public void widgetSelected(SelectionEvent e) {
                
            	Tree tree = importTable.getTree();
            	
            	if (tree != null && tree.getSelectionCount() > 0) {
            		
            		TreeItem item = tree.getSelection()[0];
            		
            		Element element = (Element)item.getData();
                    Vector<String> help = ModelReaderXIS.getPluginHelp(element, scenario.getPluginManager());
                    String shelp;
                    if(help == null) {
                    	shelp = "No plug-in help.";
                    }
                    else {
                    	shelp = "";
                    	 String[] lines = help.toArray(new String[0]);
                         for (int i = 0; i < lines.length; i++) {
                         	shelp = shelp + lines[i]; 
                         }
                    }
            		
            		Shell shell = ((Button)e.getSource()).getShell();
                    Shell s = new Shell(shell, SWT.SHELL_TRIM);
                    s.setLayout(new GridLayout(1, true));
                    s.setText(item.getText());
                    
                    Text tHelp = new Text(s, SWT.MULTI | SWT.BORDER | SWT.V_SCROLL | SWT.WRAP);
                    GridData gdata  = new GridData(GridData.FILL_BOTH);
            		tHelp.setLayoutData(gdata);
            		tHelp.setText(shelp);
            		tHelp.setEditable(false);
            		tHelp.setSize(400, 250);
            		
            		s.setSize(400, 250);
            		s.open();
            	}
            }
        };
    }
    
    private void createAddBehavior() {
        actionAddBehavior = new SelectionAdapter() {

            public void widgetSelected(SelectionEvent e) {
                DocumentImpl doc = (DocumentImpl)importTable.getInput();
                final String[] ids = ModelReaderXIS.getBehaviorsId(doc);
                
                /* Ask for new behavior id */
                Shell s = ((Link)e.getSource()).getShell();
                InputDialog dia = new InputDialog(s, "New Behavior", 
                        "Name of the new behavior", null, new IInputValidator() {
                    public String isValid(String newText) {
                        
                        if(newText.equals("")) {
                            return "";
                        }
                        
                        for (int i = 0; i < ids.length; i++) {
                            if(ids[i].equals(newText)) {
                                return "This behavior already exists!";
                            }
                        }
                        return null;
                    }
                });
                if(dia.open() == InputDialog.CANCEL) {
                    return;
                }
                String id = dia.getValue();
                
                /*Create a new behavior */
                Node behaviors = doc.getElementsByTagName("behaviors").item(0);
                if(behaviors == null) {
                    behaviors = ModelWriterXIS.correctBehaviorsNode(doc);
                }
                Element behavior = doc.createElement("behavior");
                behavior.setAttribute("id", id);
                behaviors.appendChild(behavior);
                editor.modelChanged();
            }
        };
        
    }
    /* Move a plug-in up in table (and file) */
    private void createActionUp() {
        actionUp = new SelectionAdapter() {
            
            public void widgetSelected(SelectionEvent e) {
                DocumentImpl doc = (DocumentImpl)importTable.getInput();
                
                IStructuredSelection sel = 
                    (IStructuredSelection)importTable.getSelection();
                Element plugin = (Element)sel.getFirstElement();
                if(plugin != null) {
                    Node n = plugin.getPreviousSibling();
                    
                    /* find previous Element*/
                    while(n != null && n.getNodeType() != Node.ELEMENT_NODE) {
                        n = n.getPreviousSibling();
                    }
                    if(n != null) {
                        Element plugins = (Element)plugin.getParentNode();
                        if(!plugins.getNodeName().equals("plugins")) {
                            plugins = ModelWriterXIS.correctPlugins(doc);
                        }
                        /* insert selection before the previous element */
                        plugins.insertBefore(plugin, n);
                    }
                    importTable.refresh();
                }
            }
        };
    }
    /* Move a plug-in down in table (and file) */
    private void createActionDown() {
        actionDown = new SelectionAdapter() {
            
            public void widgetSelected(SelectionEvent e) {
                DocumentImpl doc = (DocumentImpl)importTable.getInput();
                
                IStructuredSelection sel = 
                    (IStructuredSelection)importTable.getSelection();
                Element plugin = (Element)sel.getFirstElement();
                if(plugin != null) {
                    Node n = plugin.getNextSibling();
                    
                    /* find next Element*/
                    while(n != null && n.getNodeType() != Node.ELEMENT_NODE) {
                        n = n.getNextSibling();
                    }
                    if(n != null) {
                        Element plugins = (Element)plugin.getParentNode();
                        if(!plugins.getNodeName().equals("plugins")) {
                            plugins = ModelWriterXIS.correctPlugins(doc);
                        }
                        /* insert selection after the next element */
                        plugins.insertBefore(n, plugin);
                    }
                    importTable.refresh();
                }
            }
        };
    }
    
    protected void registerPages(DetailsPart detailsPart) {
        detailsPart.registerPage(ElementImpl.class, detailsPage);
    }

    protected void createToolBarActions(IManagedForm managedForm) {
        final ScrolledForm form = managedForm.getForm();
        
        /* Horizontal orientation action */
        Action haction = new Action("hor", Action.AS_RADIO_BUTTON) {
            public void run() {
                sashForm.setOrientation(SWT.HORIZONTAL);
                form.reflow(true);
            }
        };
        haction.setChecked(true);
        haction.setToolTipText("Horizontal orientation");
        haction.setImageDescriptor(IsacScenarioPlugin.imageDescriptorFromPlugin(
                IsacScenarioPlugin.PLUGIN_ID, "icons/hor.ico"));
        
        /* Vertical orientation action */
        Action vaction = new Action("ver", Action.AS_RADIO_BUTTON) {
            public void run() {
                sashForm.setOrientation(SWT.VERTICAL);
                form.reflow(true);
            }
        };
        vaction.setChecked(false);
        vaction.setToolTipText("Vertical orientation");
        vaction.setImageDescriptor(IsacScenarioPlugin.imageDescriptorFromPlugin(
                IsacScenarioPlugin.PLUGIN_ID, "icons/vor.ico"));
        
        /* Add actions to toolbar */
        form.getToolBarManager().add(haction);
        form.getToolBarManager().add(vaction);        
    }
    
    /**
     * Set document
     * @param doc
     */
    public void setDocument(IDocument doc) {
        importTable.setDocument(doc);
    }

    /**
     * Refresh details part and table part
     */
    public void refresh() {
        detailsPage.refresh();
        importTable.refresh();
        selectionChanged(new SelectionChangedEvent(importTable, importTable.getSelection()));
    }
    
    public void selectionChanged(SelectionChangedEvent event) {
        IStructuredSelection sel = (IStructuredSelection)event.getSelection();
        final IManagedForm mForm = managedForm;
        final SectionPart spart = this.spart;
        mForm.fireSelectionChanged(spart, sel);
    }
}
