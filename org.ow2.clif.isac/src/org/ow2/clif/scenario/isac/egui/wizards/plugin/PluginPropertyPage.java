/*
 * CLIF is a Load Injection Framework Copyright (C) 2006 France Telecom R&D
 * 
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2 of the License, or (at your option) any
 * later version.
 * 
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with this library; if not, write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 * 
 * CLIF $Name$
 * 
 * Contact: clif@ow2.org
 */
package org.ow2.clif.scenario.isac.egui.wizards.plugin;

import org.eclipse.core.resources.IProject;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.ui.dialogs.PropertyPage;

/**
 * This class creates a delegate class (InteractionManager) in order to manage the plugin
 * property page in 'eclipse project properties'.
 * @author Fabrice Rivart
 */
public class PluginPropertyPage extends PropertyPage {

	
	private InteractionManager interaction;
	
	/* (non-Javadoc)
	 * @see org.eclipse.jface.preference.PreferencePage#createContents(org.eclipse.swt.widgets.Composite)
	 */
	@Override
	protected Control createContents(Composite arg0) {
		
		noDefaultAndApplyButton();
		interaction = new InteractionManager(this, (IProject) getElement(), "plugin");
		interaction.createPluginContents(arg0);
		interaction.initContents();
		
		return null;
	}

	/* (non-Javadoc)
	 * @see org.eclipse.jface.preference.PreferencePage#performOk()
	 */
	@Override
	public boolean performOk() {
		try {
			interaction.apply();
		}
		catch (Exception e) {
			interaction.catchException(e);
		}
		return super.performOk();
	}

	/* (non-Javadoc)
	 * @see org.eclipse.jface.preference.PreferencePage#performCancel()
	 */
	@Override
	public boolean performCancel() {
		interaction.cancel();
		return super.performCancel();
	}
}
