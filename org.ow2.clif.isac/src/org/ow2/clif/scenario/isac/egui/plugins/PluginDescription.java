/*
 * CLIF is a Load Injection Framework
 * Copyright (C) 2004 France Telecom R&D
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 *
 * CLIF 
 *
 * Contact: clif@ow2.org
 */
package org.ow2.clif.scenario.isac.egui.plugins;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.Vector;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;
import org.ow2.clif.scenario.isac.egui.FileName;
import org.ow2.clif.scenario.isac.egui.plugins.gui.PluginGUIManager;
import org.ow2.clif.scenario.isac.egui.plugins.nodes.Node;
import org.ow2.clif.scenario.isac.egui.plugins.nodes.NodeDescription;
import org.ow2.clif.scenario.isac.egui.plugins.parser.AnalyseSaxPlugin;
import org.ow2.clif.util.XMLEntityResolver;
import org.ow2.clif.supervisor.api.ClifException;
import org.xml.sax.InputSource;
import org.xml.sax.XMLReader;

/**
 * Implementation of an object which stre the plugin description
 * 
 * @author JC Meillaud
 * @author A Peyrard
 * @author Thomas Escalle
 * 
 */
public class PluginDescription {
	private String name;
	private Map<String, ActionDescription> samples;
	private Map<String, ActionDescription> tests;
	private Map<String, ActionDescription> timers;
	private Map<String, ActionDescription> controls;
	private ObjectDescription object;

	/**
	 * constructor, use the 'loadPluginDescription' method to build an instance
	 * of the object which is contains in a XML file
	 * 
	 * @param name
	 *            The jar file name
	 * @param samples
	 * @param tests
	 * @param timers
	 * @param controls
	 * @param o
	 * @param h
	 */
	public PluginDescription(String name,
			Map<String, ActionDescription> samples,
			Map<String, ActionDescription> tests,
			Map<String, ActionDescription> timers,
			Map<String, ActionDescription> controls, ObjectDescription o,
			Vector h) {
		this.name = name;
		this.samples = samples;
		this.tests = tests;
		this.timers = timers;
		this.controls = controls;
		this.object = o;
	}

	/**
	 * Create the nodes descriptions of each actions defined in this plugin of
	 * the given type
	 * 
	 * @param type
	 * @return The nodes descriptions
	 */
	public Vector<NodeDescription> createNodesDescriptions(String type) {
		Vector<NodeDescription> result = new Vector<NodeDescription>();
		if (type.equals(Node.SAMPLE)) {
			Iterator<ActionDescription> iter = samples.values().iterator();
			while (iter.hasNext()) {
				NodeDescription temp = new NodeDescription(Node.SAMPLE);
				temp.setPlugin(this.name);
				ActionDescription tempSample = iter.next();
				tempSample.createNodeDescription(temp);
				result.add(temp);
			}
			return result;
		}
		if (type.equals(Node.TIMER)) {
			Iterator<ActionDescription> iter = timers.values().iterator();
			while (iter.hasNext()) {
				NodeDescription temp = new NodeDescription(Node.TIMER);
				temp.setPlugin(this.name);
				iter.next().createNodeDescription(temp);
				result.add(temp);
			}
			return result;
		}
		if (type.equals(Node.TEST)) {
			Iterator<ActionDescription> iter = tests.values().iterator();
			while (iter.hasNext()) {
				NodeDescription temp = new NodeDescription(Node.TEST);
				temp.setPlugin(this.name);
				iter.next().createNodeDescription(temp);
				result.add(temp);
			}
			return result;
		}
		if (type.equals(Node.CONTROL)) {
			Iterator<ActionDescription> iter = controls.values().iterator();
			while (iter.hasNext()) {
				NodeDescription temp = new NodeDescription(Node.CONTROL);
				temp.setPlugin(this.name);
				iter.next().createNodeDescription(temp);
				result.add(temp);
			}
			return result;
		}
		if (type.equals(Node.USE)) {
			if (object != null) {
				NodeDescription temp = new NodeDescription(Node.USE);
				temp.setPlugin(this.name);
				object.createNodeDescription(temp);
				result.add(temp);
			}
			return result;
		}
		return null;
	}

	/**
	 * Create a node description of a selected action descripted by it type and
	 * name
	 * 
	 * @param type
	 *            The type of the action
	 * @param name
	 *            The name of the action
	 * @return The node desciption for this action
	 */
	public NodeDescription createNodeDescription(String type, String name) {
		/* enumerate each type which could be */
		if (type.equals(Node.USE)) {
			NodeDescription node = new NodeDescription(Node.USE);
			node.setPlugin(this.name);
			this.object.createNodeDescription(node);
			return node;
		}
		if (type.equals(Node.TIMER)) {
			NodeDescription node = new NodeDescription(Node.TIMER);
			node.setPlugin(this.name);
			if (timers.containsKey(name)) {
				ActionDescription timer = timers.get(name);
				timer.createNodeDescription(node);
				return node;
			} else {
				return null;
			}
		}
		if (type.equals(Node.SAMPLE)) {
			NodeDescription node = new NodeDescription(Node.SAMPLE);
			node.setPlugin(this.name);
			if (samples.containsKey(name)) {
				ActionDescription sample = samples.get(name);
				sample.createNodeDescription(node);
				return node;
			} else {
				return null;
			}
		}
		if (type.equals(Node.CONTROL)) {
			NodeDescription node = new NodeDescription(Node.CONTROL);
			node.setPlugin(this.name);
			if (controls.containsKey(name)) {
				ActionDescription control = controls.get(name);
				control.createNodeDescription(node);
				return node;
			} else {
				return null;
			}
		}
		/*
		 * for controler type, we create a node description with test
		 * description but the type of the node is the type of the controler
		 */
		if (Node.isControllerNode(type)) {
			NodeDescription node = new NodeDescription(type);
			node.setPlugin(this.name);
			if (tests.containsKey(name)) {
				ActionDescription test = tests.get(name);
				test.createNodeDescription(node);
				return node;
			} else {
				return null;
			}
		}
		return null;
	}

	/**
	 * Load a plugin description from a properties file which has it path
	 * specified in parameter
	 * 
	 * @param pluginGUIManager
	 * @param file the directory of the plugin
	 * @return A PluginDescription object
	 */
	public static List<PluginDescription> loadPluginDescription(
		PluginGUIManager pluginGUIManager, File file)
		throws ClifException
	{
		List<PluginDescription> result = new ArrayList<PluginDescription> ();
		// get each properties files contained into jar
		List<InputStream> lsIs = SearchPluginFile.searchInputStreams(file,
				FileName.PLUGIN_PROPERTIES_FILE);		
		
		/* load the plugin properties */
		InputStream is;
		for (InputStream isOfPropertiesfile :lsIs) {
			Properties properties = new Properties();
			String pluginXMLName="not defined";
			StringBuffer pluginXMLFile=new StringBuffer("not defined");
			StringBuffer guiXMLFile=new StringBuffer("not defined");
			try {
				properties.load(isOfPropertiesfile);
				isOfPropertiesfile.close();
				pluginXMLName = properties.getProperty("plugin.name");
				
				pluginXMLFile = new StringBuffer(pluginXMLName);
				pluginXMLFile.append("/");
				pluginXMLFile.append(properties.getProperty("plugin.xmlFile"));		    	
				
				guiXMLFile = new StringBuffer(pluginXMLName);
				guiXMLFile.append("/");
				guiXMLFile.append(properties.getProperty("plugin.guiFile"));
				
				is = SearchPluginFile.searchInputStream(file, pluginXMLFile.toString());
				if (is==null){
					throw new RuntimeException("unable to find :"+pluginXMLFile.toString());
				}
					
				/* open the XML definition file */				
				AnalyseSaxPlugin handler = new AnalyseSaxPlugin();
				SAXParserFactory factory = SAXParserFactory.newInstance();
				factory.setValidating(true);
				SAXParser saxParser = factory.newSAXParser();
				XMLReader reader = saxParser.getXMLReader();
				reader.setContentHandler(handler);
				reader.setErrorHandler(handler);
				reader.setEntityResolver(new XMLEntityResolver());
				reader.parse(new InputSource(is));
				is.close();
			
				PluginDescription tempDesc = handler.getPluginDescription();
				is = SearchPluginFile.searchInputStream(file, guiXMLFile.toString());
				if (is==null){
					throw new RuntimeException("unable to find :"+guiXMLFile.toString());
				}
				pluginGUIManager.createPanels(tempDesc, is);
				is.close();
				
				result.add(tempDesc);				
			} catch (Exception e) {
				throw new ClifException("could not load ISAC plug-in " + file.getName(), e);
			}
		}
		return result;
	}

	/**
	 * Attribute name getter
	 * 
	 * @return The name of the plugin
	 */
	public String getName() {
		return name;
	}

	/**
	 * This method return the action help of the action whose name is given in
	 * parameter
	 * 
	 * @param type
	 *            The type of the action
	 * @param action
	 *            The name of the action
	 * @return The help lines of the action
	 */
	public Vector<String> getActionHelp(String type, String action) {
		if (type.equals(Node.USE)) {
			return object.getHelp();
		} else {
			ActionDescription desc = null;
			if (type.equals(Node.SAMPLE) && samples.containsKey(action)) {
				desc = samples.get(action);
			} else if (type.equals(Node.TIMER) && timers.containsKey(action)) {
				desc = timers.get(action);
			} else if (type.equals(Node.TEST) && tests.containsKey(action)) {
				desc = tests.get(action);
			} else if (type.equals(Node.CONTROL)
					&& controls.containsKey(action)) {
				desc = controls.get(action);
			}
			if (desc != null) {
				return desc.getHelp();
			} else {
				return null;
			}
		}
	}


	/**
	 * Get the gui key of a specified action
	 * 
	 * @param type
	 *            The type of the action
	 * @param actionName
	 *            The action name
	 * @return The gui key
	 */
	public String getActionGUIKey(String type, String actionName) {
		if (type.equals(Node.USE)) {
			return object.getGUIKey();
		}
		if (type.equals(Node.SAMPLE)) {
			if (samples.containsKey(actionName)) {
				ActionDescription sample = samples.get(actionName);
				return sample.getGUIKey();
			}
			return null;
		}
		if (type.equals(Node.TIMER)) {
			if (timers.containsKey(actionName)) {
				ActionDescription timer = timers.get(actionName);
				return timer.getGUIKey();
			}
			return null;
		}
		if (type.equals(Node.TEST)) {
			if (tests.containsKey(actionName)) {
				ActionDescription test = tests.get(actionName);
				return test.getGUIKey();
			}
			return null;
		}
		if (type.equals(Node.CONTROL)) {
			if (controls.containsKey(actionName)) {
				ActionDescription control = controls.get(actionName);
				return control.getGUIKey();
			}
			return null;
		}
		return null;
	}

	/**
	 * @return Returns the object.
	 */
	public ObjectDescription getObject() {
		return object;
	}

	/**
	 * @return Returns the samples.
	 */
	public Map<String, ActionDescription> getSamples() {
		return samples;
	}

	/**
	 * @return Returns the tests.
	 */
	public Map<String, ActionDescription> getTests() {
		return tests;
	}

	/**
	 * @return Returns the timers.
	 */
	public Map<String, ActionDescription> getTimers() {
		return timers;
	}

	/**
	 * @return Returns the controls.
	 */
	public Map<String, ActionDescription> getControls() {
		return controls;
	}
}