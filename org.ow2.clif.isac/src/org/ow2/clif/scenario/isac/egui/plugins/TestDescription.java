/*
* CLIF is a Load Injection Framework
* Copyright (C) 2004 France Telecom R&D
*
* This library is free software; you can redistribute it and/or
* modify it under the terms of the GNU Lesser General Public
* License as published by the Free Software Foundation; either
* version 2 of the License, or (at your option) any later version.
*
* This library is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
* Lesser General Public License for more details.
*
* You should have received a copy of the GNU Lesser General Public
* License along with this library; if not, write to the Free Software
* Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*
* CLIF
*
* Contact: clif@ow2.org
*/
package org.ow2.clif.scenario.isac.egui.plugins;

import java.util.HashMap;
import java.util.Map;
import java.util.Vector;
import org.ow2.clif.scenario.isac.egui.plugins.nodes.NodeDescription;

/**
 * This class store a test description
 *
 * @author JC Meillaud
 * @author A Peyrard
 */
public class TestDescription implements ActionDescription {
    private String name;
    private String number;
    private Vector<ParameterDescription> params;
    private Vector<String> help;
    private String guiKey;

    /**
     * Build a new test description object, used to store information about a test action
     * @param n The name of the action
     * @param nb The number of the test in the object test table
     * @param p The parameters definitions
     * @param h The help for this action
     */
    public TestDescription(String n, String nb, Vector<ParameterDescription> p, Vector<String> h) {
        this.name = n;
        this.number = nb;
        this.params = p;
        this.help = h;
        this.guiKey = null;
    }

    public void createNodeDescription(NodeDescription desc) {
        desc.setActionName(this.name);
        Map<String,String> paramsValues = new HashMap<String,String>();
        for (int i = 0; i < this.params.size(); i++) {
            paramsValues.put(params.elementAt(i).getName(), "");
        }
        desc.setParams(paramsValues);
    }

    public String getGUIKey() {
        return guiKey;
    }

    public void setGUIKey(String key) {
        this.guiKey = key ;
    }
    /**
     * Attribute help getter
     * @return The help of the action
     */
    public Vector<String> getHelp() {
        return help;
    }

    /**
     * Attribute name getter
     * @return The name of the action
     */
    public String getName() {
        return name;
    }

    /**
     * Attribute number getter
     * @return The number of the test, in the object tests table
     */
    public String getNumber() {
        return number;
    }

    /**
     * Attribute params getter
     * @return The parameters descriptions
     */
    public Vector<ParameterDescription> getParams() {
        return params;
    }
}
