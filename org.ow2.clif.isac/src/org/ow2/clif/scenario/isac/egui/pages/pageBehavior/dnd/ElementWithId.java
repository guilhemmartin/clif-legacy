/*
* CLIF is a Load Injection Framework
* Copyright (C) 2005 France Telecom R&D
*
* This library is free software; you can redistribute it and/or
* modify it under the terms of the GNU Lesser General Public
* License as published by the Free Software Foundation; either
* version 2 of the License, or (at your option) any later version.
*
* This library is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
* Lesser General Public License for more details.
*
* You should have received a copy of the GNU Lesser General Public
* License along with this library; if not, write to the Free Software
* Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*
* CLIF $Name: not supported by cvs2svn $
*
* Contact: clif@ow2.org
*/

package org.ow2.clif.scenario.isac.egui.pages.pageBehavior.dnd;

import org.w3c.dom.Element;

/**
 * A org.w3c.dom Element with an id for testing
 * during transfer
 * @author Joan Chaumont
 *
 */
public class ElementWithId {
    
    private String id;
    private Element elt;
    
    /** Constructor 
     * @param id 
     * @param elt 
     */
    public ElementWithId(String id, Element elt) {
        this.id = id;
        this.elt = elt;
    }

    /**
     * Element getter
     * @return Element
     */
    public Element getElt() {
        return elt;
    }
    /**
     * Id getter
     * @return String id
     */
    public String getId() {
        return id;
    }
}
