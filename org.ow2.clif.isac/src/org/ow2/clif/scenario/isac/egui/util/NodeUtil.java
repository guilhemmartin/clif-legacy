/*
* CLIF is a Load Injection Framework
* Copyright (C) 2005, 2009 France Telecom R&D
*
* This library is free software; you can redistribute it and/or
* modify it under the terms of the GNU Lesser General Public
* License as published by the Free Software Foundation; either
* version 2 of the License, or (at your option) any later version.
*
* This library is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
* Lesser General Public License for more details.
*
* You should have received a copy of the GNU Lesser General Public
* License along with this library; if not, write to the Free Software
* Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*
* Contact: clif@ow2.org
*/
package org.ow2.clif.scenario.isac.egui.util;

import org.w3c.dom.Element;

/**
 * Utilities for node manipulation
 * @author Anthonin Bonnefoy
 */
public class NodeUtil {

	/**
	 * Add a node after the given Node.
	 * @param newNode	  New Node to insert
	 * @param selectedNode Referenced Node.
	 */
	static void insertAfterNode(Element newNode, Element selectedNode) {
		if (selectedNode.getNextSibling() != null) {
			selectedNode.getParentNode().insertBefore(newNode, selectedNode.getNextSibling());
		} else {
			selectedNode.getParentNode().appendChild(newNode);
		}
	}

	/**
	 * Check if it is possible to insert node as a sibling and insert it.
	 * If it is not possible to insert a sibling, insert the new node after the parent of the selectedNode
	 * @param newNode	  Node to insert
	 * @param selectedNode Referenced node
	 */
	static void checkAndInsertAfter(Element newNode, Element selectedNode) {
		if (newNode.getNodeName().equals("choice")) {
			if (selectedNode.getParentNode().getNodeName().equals("nchoice")) {
				insertAfterNode(newNode, selectedNode);
			}
			return;
		}
		if (BehaviorUtil.siblingAllowed(selectedNode, newNode)) {
			insertAfterNode(newNode, selectedNode);
		} else {
			insertAfterParentNode(newNode, selectedNode);
		}
	}

	/**
	 * Insert the node as a sibling of the parent of the selectedNode
	 * @param newNode	  Node to insert
	 * @param selectedNode Reference node
	 */
	static void insertAfterParentNode(Element newNode, Element selectedNode) {
		if (selectedNode.getParentNode().getNextSibling() != null) {
			selectedNode.getParentNode().getParentNode().insertBefore(newNode, selectedNode.getParentNode().getNextSibling());
		} else {
			selectedNode.getParentNode().getParentNode().appendChild(newNode);
		}
	}

	/**
	 * Check if it is possible to insert a sibling to the selectedNode and insert it. If it is not possible, insert it
	 * before the parent.
	 * @param newNode	  Node to insert
	 * @param selectedNode Referenced node
	 */
	static void checkAndInsertBefore(Element newNode, Element selectedNode) {
		if (newNode.getNodeName().equals("choice")) {
			if (selectedNode.getParentNode().getNodeName().equals("nchoice")) {
				selectedNode.getParentNode().insertBefore(newNode, selectedNode);
			}else{
				return;
			}
		}
		if (BehaviorUtil.siblingAllowed(selectedNode, newNode)) {
			/* if it is an "insert action",
						* add new item before the parent of selected item
						*/
			selectedNode.getParentNode().insertBefore(newNode, selectedNode);
		} else {
			/* if it is an "insert action" and
						* selected item is "then", "else" or "choice",
						* add new item before the grandparent of selected item
						*/
			selectedNode.getParentNode().getParentNode().insertBefore(newNode, selectedNode.getParentNode());
		}
	}

	/**
	 * Insert the node at the begining of the document
	 * @param newNode		   Node to insert
	 * @param rootBehaviourNode Root node of the document
	 */
	static void insertAtBegin(Element newNode, Element rootBehaviourNode) {
		if (rootBehaviourNode.getFirstChild() != null) {
			rootBehaviourNode.insertBefore(newNode, rootBehaviourNode.getFirstChild());
		} else {
			rootBehaviourNode.appendChild(newNode);
		}
	}
}
