/*
 * CLIF is a Load Injection Framework
 * Copyright (C) 2004, 2011 France Telecom R&D
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 *
 * CLIF
 *
 * Contact: clif@ow2.org
 */
package org.ow2.clif.scenario.isac.egui.plugins.gui;

import org.eclipse.swt.SWT;
import org.eclipse.swt.custom.ScrolledComposite;
import org.eclipse.swt.events.ModifyEvent;
import org.eclipse.swt.events.ModifyListener;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.SelectionListener;
import org.eclipse.swt.graphics.Color;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Combo;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Label;
import org.ow2.clif.scenario.isac.egui.IsacEditor;
import org.ow2.clif.scenario.isac.egui.ScenarioManager;
import org.ow2.clif.scenario.isac.egui.model.ModelReaderXIS;
import org.ow2.clif.scenario.isac.egui.plugins.PluginDescription;
import org.ow2.clif.scenario.isac.egui.plugins.nodes.Node;
import org.ow2.clif.scenario.isac.egui.plugins.nodes.NodeDescription;
import org.ow2.clif.scenario.isac.egui.plugins.nodes.NonePluginNode;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;
import java.io.InputStream;
import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Implementation of the object which will store the parameters definition
 * panels
 * @author JC Meillaud
 * @author A Peyrard
 * @author Bruno Dillenseger
 */
public class PluginGUIManager implements SelectionListener, ModifyListener {

    /* The table which will store all 
     * the editable panels for each gui key */
    private Map<String, ParametersWidgetsNode> panels;

    private Composite parent;
    private ScrolledComposite top;
    private ScrolledComposite contents;
    private Composite contentsComposite;
    private Composite topComposite;
    private Composite mainComposite;
    private Composite main;
    private Label topLabel;
    private GridData gridDataTop;
    private ParameterPanel parametersPanel;

    private Element currentNode;
    private String currentType;

    private Combo comboTestTop;
    private Combo comboSampleTop;
    private Combo comboTimerTop;
    private Combo comboControlTop;

    private ScenarioManager manager;

    private Map<String, String> usedPlugins;
    private Document doc;
    private IsacEditor editor;

    private boolean commit = false;

    /**
     * Constructor
     * @param window
     */
    public PluginGUIManager(ScenarioManager window)
    {
        panels = new HashMap<String, ParametersWidgetsNode>();
        this.manager = window;
    }

    /**
     * Method which will set the parent composite, we will create the parameters
     * panel into it
     * @param parent The parent composite
     */
    public void setParentComposite(Composite parent)
    {
        this.parent = new Composite(parent, SWT.FLAT);

        /* set the parent layout */
        GridLayout gridLayout = new GridLayout();
        gridLayout.verticalSpacing = 4;
        gridLayout.numColumns = 1;
        gridLayout.marginHeight = 0;
        gridLayout.marginWidth = 0;
        this.parent.setLayout(gridLayout);
        this.parent.setLayoutData(new GridData(GridData.FILL_BOTH));

        /* create the top composite */
        top = new ScrolledComposite(this.parent, SWT.BORDER | SWT.H_SCROLL
                | SWT.V_SCROLL);
        top.setExpandHorizontal(true);
        top.setExpandVertical(true);
        top.setAlwaysShowScrollBars(false);

        /* set the grid data of the top composite */
        gridDataTop = new GridData(GridData.FILL_HORIZONTAL);
        gridDataTop.grabExcessHorizontalSpace = true;
        top.setLayoutData(gridDataTop);
        /* create the main composite */
        main = new Composite(this.parent, SWT.BORDER);
        main.setLayout(new GridLayout());
        main.setBackground(this.parent.getDisplay().getSystemColor(
                SWT.COLOR_WHITE));
        GridData gridDataMain = new GridData(GridData.FILL_BOTH);
        gridDataMain.grabExcessHorizontalSpace = true;
        gridDataMain.grabExcessVerticalSpace = true;
        main.setLayoutData(gridDataMain);
        mainComposite = new Composite(main, SWT.BORDER);
        mainComposite.setBackground(this.parent.getDisplay()
                .getSystemColor(SWT.COLOR_WHITE));

        createLabelTopComposite("Panel for parameters nodes edition");
    }

    /**
     * Update the scrolled composite which contains the top panel
     */
    private void updateTopPanel()
    {
        top.setContent(topComposite);
        top.setMinSize(
                topComposite.computeSize(SWT.DEFAULT, SWT.DEFAULT));
        top.layout();
        topComposite.layout();
        gridDataTop.heightHint =
                topComposite.computeSize(SWT.DEFAULT, SWT.DEFAULT).y;
        parent.layout();
    }

    /**
     * Create a new Panel for the top of the form in this panel we will print
     * the name of the node
     * @param text The text to be printed in the label
     */
    private void createLabelTopComposite(String text)
    {
        /* create the top composite */
        topComposite = new Composite(top, SWT.FLAT);
        topComposite.setBackground(top.getDisplay().getSystemColor(
                SWT.COLOR_LIST_BACKGROUND));

        /* define the layout of the composite */
        GridLayout gridLayout = new GridLayout();
        gridLayout.numColumns = 1;
        topComposite.setLayout(gridLayout);
        topLabel = new Label(topComposite, SWT.CENTER | SWT.BORDER);

        /* Set the alignement of the label that will be added */
        topLabel.setLayoutData(new GridData(GridData.FILL_HORIZONTAL));
        topLabel.setText(text);
        updateTopPanel();
    }

    /**
     * This method create a new switcher, to select the test for the controler
     * node
     * @param type The type of the controler node
     */
    private void createTestSwitcherTopComposite(String type)
    {
        topComposite = new Composite(top, SWT.FLAT);

        /* define the layout of the composite */
        topComposite.setLayout(new GridLayout());
        topComposite.setLayoutData(new GridData(GridData.FILL_BOTH));
        topComposite.setBackground(top.getDisplay().getSystemColor(
                SWT.COLOR_LIST_BACKGROUND));

        topLabel = new Label(topComposite, SWT.CENTER | SWT.BORDER);

        /* Set the alignement of the label that will be added */
        topLabel.setLayoutData(new GridData(GridData.FILL_HORIZONTAL));
        topLabel.setText("Select a test condition of the " + type
                + " controler node :");

        /* add the combo to show the diffrents choices */
        comboTestTop = new Combo(topComposite, SWT.FLAT | SWT.READ_ONLY);
        comboTestTop.setLayoutData(new GridData(GridData.FILL_HORIZONTAL));
        comboTestTop.addSelectionListener(this);

        /* get the used plugins... */
        Vector<String> names = new Vector<String>(usedPlugins.values());
        Vector<NodeDescription> nodesDescs = manager.getPluginManager()
                .createNodesDescriptionsByPlugins(names, Node.TEST);
        /* add to the combo the different tests */
        for (int i = 0; i < nodesDescs.size(); i++)
        {
            NodeDescription nodeDesc = nodesDescs.elementAt(i);
            comboTestTop.add(nodeDesc.getPlugin() + "." + nodeDesc.getActionName());
        }
        /* set the default text of the combo */
        comboTestTop.setText("");
        updateTopPanel();
    }

    /**
     * Change the parameters panels
     * @param node The node which the parameters must be show
     */
    public void switchPanel(Element node)
    {
        /* update the current node variable */
        currentNode = node;
        /* check if the two switchable panels are 
         * not null if they aren't dispose them */
        if (topComposite != null)
        {
            topComposite.dispose();
        }
        if (mainComposite != null)
        {
            mainComposite.dispose();
        }

        currentType = node.getNodeName();
        doc = node.getOwnerDocument();
        usedPlugins = ModelReaderXIS.getPlugins(doc);

        if (currentType == null)
        {
            createLabelTopComposite("Panel for parameters nodes edition");
            showParametersEditionPanel(null, null, null);
            return;
        }
        /* check if the node is a plugin node */
        if (Node.isUseNode(currentType))
        {
            switchUsePanel(node, currentType);
        } else if (Node.isPluginNode(currentType))
        {
            switchPluginPanel(node, currentType);
        }
        /* if controler node */
        else if (Node.isControllerNode(currentType))
        {
            switchControllerPanel(currentNode, currentType);
        } else if (Node.isChoiceNode(currentType))
        {
            switchChoicePanel(currentNode, currentType);
        }
        /* if others node */
        else if (!Node.isPluginNode(currentType))
        {
            createLabelTopComposite("The node is a " + currentType);
        }
    }

    private void switchChoicePanel(Element node, String type)
    {
        if (!panels.containsKey(type))
        {
            ParametersWidgetsNode pwn = NonePluginNode.createParametersWidgetsNode(type);
            if (pwn == null)
            {
                pwn = new ParametersWidgetsNode(null);
            }
            panels.put(type, pwn);
        }

        Map<String, String> params = new HashMap<String, String>();
        params.put("proba", ((Element) currentNode).getAttribute("proba"));

        createLabelTopComposite("The node is a " + currentType);
        showParametersEditionPanel(type, null, params);
    }

    private void switchControllerPanel(Element node, String type)
    {
        /* get correct condition node or create one */
        org.w3c.dom.NodeList cond = node.getElementsByTagName("condition");
        org.w3c.dom.Node first = null;
        for (int i = 0; i < cond.getLength(); i++)
        {
            if (cond.item(i).getParentNode().equals(currentNode))
            {
                first = cond.item(i);
                break;
            }
        }
        if (first == null)
        {
            Element condition = doc.createElement("condition");
            currentNode.insertBefore(condition, node.getFirstChild());
            currentNode = condition;
        } else
        {
            currentNode = (Element) first;
        }

        createTestSwitcherTopComposite(type);
        /* show the right panel, if the plugin and the test has been edited */
        String use = currentNode.getAttribute("use");
        String name = currentNode.getAttribute("name");
        if (use != null && name != null
                && !use.equals("") && (String) usedPlugins.get(use) != null)
        {
            switchTestParametersPanel((String) usedPlugins.get(use), name);
            comboTestTop.setText((String) usedPlugins.get(use) + "." + name);
        } else
        {
            showParametersEditionPanel(null, null, null);
        }
    }

    private void switchUsePanel(Element node, String type)
    {
        String id = usedPlugins.get(node.getAttribute("id"));
        String name = node.getAttribute("name");
        if (id == null)
        {
            createLabelTopComposite("Invalid node");
            showParametersEditionPanel(null, null, null);
            return;
        }
        createLabelTopComposite(type + " : " + id + "." + name);
        String guiKey = manager.getPluginManager()
                .getPluginActionGUIKey(id, type, name);
        Map<String, String> params = ModelReaderXIS.getParams(node);
        params.put("id", node.getAttribute("id"));
        showParametersEditionPanel(guiKey, name, params);
    }

    private void switchPluginPanel(Element node, String type)
    {
        String plugName = usedPlugins.get(node.getAttribute("use"));
        String actName = node.getAttribute("name");
        String guiKey = null;
        if (plugName != null)
        {
            guiKey = manager.getPluginManager().getPluginActionGUIKey(plugName, type, actName);
        }

        if (guiKey != null)
        {
            createLabelTopComposite(type + " : " + plugName + "." + actName);
            Map<String, String> params = ModelReaderXIS.getParams(node);
            params.put("id", node.getAttribute("use"));
            showParametersEditionPanel(guiKey, plugName, params);
        } else
        {
            Vector<String> names = null;
            if (plugName == null)
            {
                names = new Vector<String>(usedPlugins.values());
            } else
            {
                names = new Vector<String>(1);
                names.add(plugName);
            }
            if (type.equals(Node.SAMPLE) || type.equals(Node.ACTION))
            {
                createSampleSwitcherTopComposite(names);
            } else if (type.equals(Node.TIMER))
            {
                createTimerSwitcherTopComposite(names);
            } else if (type.equals(Node.CONTROL))
            {
                createControlSwitcherTopComposite(names);
            }
        }
    }

    /**
     * This method create a new switcher, to select
     * the test for the controller node
     * @param names
     */
    private void createSampleSwitcherTopComposite(Vector<String> names)
    {
        topComposite = new Composite(top, SWT.FLAT);

        /* define the layout of the composite */
        topComposite.setLayout(new GridLayout());
        topComposite.setLayoutData(new GridData(GridData.FILL_BOTH));
        topComposite.setBackground(top.getDisplay().getSystemColor(
                SWT.COLOR_LIST_BACKGROUND));

        topLabel = new Label(topComposite, SWT.CENTER | SWT.BORDER);

        /* Set the alignement of the label that will be added */
        topLabel.setLayoutData(new GridData(GridData.FILL_HORIZONTAL));
        topLabel.setText("Select a sample :");

        /* add the combo to show the differents choices */
        comboSampleTop = new Combo(topComposite, SWT.FLAT | SWT.READ_ONLY);
        comboSampleTop.setLayoutData(new GridData(GridData.FILL_HORIZONTAL));
        comboSampleTop.addSelectionListener(this);

        /* get the used plugins... */
        Vector<NodeDescription> nodesDescs = manager.getPluginManager()
                .createNodesDescriptionsByPlugins(names, Node.SAMPLE);
        /* add to the combo the different tests */
        for (int i = 0; i < nodesDescs.size(); i++)
        {
            NodeDescription nodeDesc = nodesDescs.elementAt(i);
            comboSampleTop.add(nodeDesc.getPlugin() + "." + nodeDesc.getActionName());
        }
        /* set the default text of the combo */
        comboSampleTop.setText("");
        updateTopPanel();
    }

    private void createTimerSwitcherTopComposite(Vector<String> names)
    {
        topComposite = new Composite(top, SWT.FLAT);

        /* define the layout of the composite */
        topComposite.setLayout(new GridLayout());
        topComposite.setLayoutData(new GridData(GridData.FILL_BOTH));
        topComposite.setBackground(top.getDisplay().getSystemColor(
                SWT.COLOR_LIST_BACKGROUND));

        topLabel = new Label(topComposite, SWT.CENTER | SWT.BORDER);

        /* Set the alignement of the label that will be added */
        topLabel.setLayoutData(new GridData(GridData.FILL_HORIZONTAL));
        topLabel.setText("Select a timer :");

        /* add the combo to show the differents choices */
        comboTimerTop = new Combo(topComposite, SWT.FLAT | SWT.READ_ONLY);
        comboTimerTop.setLayoutData(new GridData(GridData.FILL_HORIZONTAL));
        comboTimerTop.addSelectionListener(this);

        /* get the used plugins... */
        Vector<NodeDescription> nodesDescs = manager.getPluginManager()
                .createNodesDescriptionsByPlugins(names, Node.TIMER);
        /* add to the combo the different tests */
        for (int i = 0; i < nodesDescs.size(); i++)
        {
            NodeDescription nodeDesc = nodesDescs.elementAt(i);
            comboTimerTop.add(nodeDesc.getPlugin() + "." + nodeDesc.getActionName());
        }
        /* set the default text of the combo */
        comboTimerTop.setText("");
        updateTopPanel();
    }

    private void createControlSwitcherTopComposite(Vector<String> names)
    {
        topComposite = new Composite(top, SWT.FLAT);

        /* define the layout of the composite */
        topComposite.setLayout(new GridLayout());
        topComposite.setLayoutData(new GridData(GridData.FILL_BOTH));
        topComposite.setBackground(top.getDisplay().getSystemColor(
                SWT.COLOR_LIST_BACKGROUND));

        topLabel = new Label(topComposite, SWT.CENTER | SWT.BORDER);

        /* Set the alignement of the label that will be added */
        topLabel.setLayoutData(new GridData(GridData.FILL_HORIZONTAL));
        topLabel.setText("Select a control :");

        /* add the combo to show the differents choices */
        comboControlTop = new Combo(topComposite, SWT.FLAT | SWT.READ_ONLY);
        comboControlTop.setLayoutData(new GridData(GridData.FILL_HORIZONTAL));
        comboControlTop.addSelectionListener(this);

        /* get the used plugins... */
        Vector<NodeDescription> nodesDescs = manager.getPluginManager()
                .createNodesDescriptionsByPlugins(names, Node.CONTROL);
        /* add to the combo the different tests */
        for (int i = 0; i < nodesDescs.size(); i++)
        {
            NodeDescription nodeDesc = nodesDescs.elementAt(i);
            comboControlTop.add(nodeDesc.getPlugin() + "." + nodeDesc.getActionName());
        }
        /* set the default text of the combo */
        comboControlTop.setText("");
        updateTopPanel();
    }

    /**
     * Show the parameters edition panels
     * @param guiKey     The gui key of the panel to shown
     * @param pluginName
     * @param params     The parameters values to init the widgets
     */
    private void showParametersEditionPanel(String guiKey, String pluginName, Map<String, String> params)
    {
        Color white = parent.getDisplay().getSystemColor(SWT.COLOR_WHITE);

        /* dispose it if it already exist */
        if (mainComposite != null)
        {
            mainComposite.dispose();
        }

        mainComposite = new Composite(main, SWT.FLAT);
        mainComposite.setBackground(white);
        /* exit if they are no guikey */
        if (guiKey == null)
        {
            main.layout();
            return;
        }

        /* create the main composite */
        mainComposite.setLayout(new GridLayout());
        mainComposite.setLayoutData(new GridData(GridData.FILL_BOTH));

        /* create the content composite */
        contents = new ScrolledComposite(mainComposite, SWT.H_SCROLL | SWT.V_SCROLL);
        contents.setBackground(white);
        contents.setExpandHorizontal(true);
        contents.setExpandVertical(true);
        contents.setAlwaysShowScrollBars(false);

        /* set the contents grid data */
        contents.setLayoutData(new GridData(GridData.FILL_BOTH));

        /* create the new panel which will contains all the parameters widgets */
        contentsComposite = new Composite(contents, SWT.FLAT);
        contentsComposite.setBackground(white);
        contents.setContent(contentsComposite);
        contentsComposite.setLayout(new GridLayout());

        /* get the tree description */
        ParametersWidgetsNode node = panels.get(guiKey);
        parametersPanel =
                ParameterPanel.createParameterPanel(node, contentsComposite, this);

        if (!currentType.equals(Node.USE))
        {
            if (Node.isPluginNode(currentType) || Node.isControllerNode(currentType))
            {
                Vector<String> values = new Vector<String>();
                for (String pluginKey : usedPlugins.keySet())
                {
                    String name = usedPlugins.get(pluginKey);
                    if (name.equals(pluginName))
                    {
                        values.add(pluginKey);
                    }
                }
                parametersPanel.setComboValues(values);
            }
        }
        parametersPanel.setParametersValues(params);
        /* update the main composite */
        main.layout();
    }

    /**
     * Switch the edition panel to the test edition parameters panel selected
     * @param pluginName The plugin name of the test
     * @param testName   The test name
     */
    private void switchTestParametersPanel(String pluginName, String testName)
    {
        String guiKey = manager.getPluginManager()
                .getPluginActionGUIKey(pluginName, Node.TEST, testName);

        Map<String, String> params = ModelReaderXIS.getParams(currentNode);
        params.put("id", currentNode.getAttributes().getNamedItem("use").getNodeValue());

        showParametersEditionPanel(guiKey, pluginName, params);
    }


    /**
     * Create panels from a gui XML file, and store it
     * @param plugin  The plugin description
     * @param guiFile The gui file with all panels descriptions
     */
    public void createPanels(PluginDescription plugin, InputStream guiFile)
    {
        ParameterPanel.createNewPanelsFromXML(new Vector(),
                plugin, guiFile, panels);
    }

    /**
     * @see org.eclipse.swt.events.SelectionListener#widgetDefaultSelected(org.eclipse.swt.events.SelectionEvent)
     */
    public void widgetDefaultSelected(SelectionEvent event)
    {
    }

    /**
     * @see org.eclipse.swt.events.SelectionListener#widgetSelected(org.eclipse.swt.events.SelectionEvent)
     */
    public void widgetSelected(SelectionEvent event)
    {
        if (event.getSource() == comboControlTop
                || event.getSource() == comboSampleTop
                || event.getSource() == comboTimerTop
                || event.getSource() == comboTestTop)
        {
            StringTokenizer st = new StringTokenizer(((Combo) event.getSource()).getText(), ".");
            if (st.countTokens() < 2)
            {
                return;
            }
            String plugin = "";
            /* the token before the last is the name of the plugin */
            while (st.countTokens() > 2)
            {
                String token = st.nextToken();
                plugin = plugin.concat(token + ".");
            }
            plugin = plugin.concat(st.nextToken());
            String test = st.nextToken();

            for (String pluginKey : usedPlugins.keySet())
            {
                if (usedPlugins.get(pluginKey).equals(plugin))
                {
                    plugin = pluginKey;
                    break;
                }
            }

            currentNode.setAttribute("use", plugin);
            currentNode.setAttribute("name", test);

            /* remove oldParams */
            NodeList params = currentNode.getElementsByTagName("params");
            for (int i = 0; i < params.getLength(); i++)
            {
                org.w3c.dom.Node param = params.item(i);
                if (param.getParentNode().equals(currentNode))
                {
                    currentNode.removeChild(param);
                }
            }
            editor.refresh();
        }
        else if (parametersPanel.addButtonSelected(event.getSource()))
        {
            /* refresh the scrolled pane set
     * the minSize of the contents composite */
            contents.setMinSize(
                    contentsComposite.computeSize(SWT.DEFAULT, SWT.DEFAULT));
            /* update the main composite */
            main.layout();
        }
        else if (parametersPanel.removeButtonSelected(event.getSource()))
        {
            commit = true;
	        editor.setDirty(true);
            /* refresh the scrolled pane set 
             * the minSize of the contents composite */
            contents.setMinSize(
                    contentsComposite.computeSize(SWT.DEFAULT, SWT.DEFAULT));
            /* update the main composite */
            main.layout();
        }
        else
        {
	        /* else it a radio group param or a check box what has been modify */
	        commit = true;
	        editor.setDirty(true);
        }
    }

    /**
     * Commit changes done into file.
     */
    public void commit()
    {
        if (!commit)
        {
            return;
        }

        Map<String, String> values = parametersPanel.getParametersValues();

        /* if only one value need to be commited */
        if (values.size() <= 1)
        {
            /* if this value is the id */
            if (values.containsKey("id"))
            {
                if (Node.isUseNode(currentType))
                {
                    updatePluginId(currentNode.getAttribute("id"), values.get("id"));
                    currentNode.setAttribute("id", values.get("id"));
                    commit = false;
                    editor.setDirty(false);
                }
                else if (Node.isPluginNode(currentType) || Node.isControllerNode(currentType))
                {
                    currentNode.setAttribute("use", values.get("id"));
                    commit = false;
                    editor.setDirty(false);
                }
                editor.refresh();
                return;
            }
            /* if the value is a proba (choice case)*/
            else if (values.containsKey("proba"))
            {
                if (Node.isChoiceNode(currentType))
                {
                    currentNode.setAttribute("proba", values.get("proba"));
                    commit = false;
                    editor.setDirty(false);
                }
                return;
            }
        }

        /* test if params node exists and if it not add it */
        org.w3c.dom.Node paramsNode;
        if (currentNode.getElementsByTagName("params").getLength() == 0)
        {
            paramsNode = doc.createElement("params");
            currentNode.appendChild(paramsNode);
        } else
        {
            paramsNode = currentNode.getElementsByTagName("params").item(0);
        }

        /* Commit all params */
        for (String name : values.keySet())
        {
            /* the case of id is different because you have to modify it
             * in the element node attribute */
            if (name.equals("id"))
            {
                if (Node.isUseNode(currentType))
                {
                    updatePluginId(currentNode.getAttribute("id"), values.get("id"));
                    currentNode.setAttribute("id", values.get(name));
                    continue;
                } else
                {
                    currentNode.setAttribute("use", values.get(name));
                    continue;
                }
            }

            int i = 0;
            NodeList paramList = currentNode.getElementsByTagName("param");
            int nbParams = paramList.getLength();
            /* search the param which need to be modify */
            while (i < nbParams)
            {
                org.w3c.dom.Node param = paramList.item(i);
                org.w3c.dom.Node attrName =
                        param.getAttributes().getNamedItem("name");
                org.w3c.dom.Node attrValue =
                        param.getAttributes().getNamedItem("value");

                if (attrName == null || attrValue == null)
                {
                    paramsNode.removeChild(param);
                } else if (attrName.getNodeValue().equals(name))
                {
                    attrValue.setNodeValue(values.get(name));
                    break;
                }

                i++;
            }

            /* the param don't exists, add it */
            if (i == nbParams)
            {
                Element param = doc.createElement("param");
                param.setAttribute("name", name);
                param.setAttribute("value", values.get(name));
                paramsNode.appendChild(param);
            }
        }
        commit = false;
        editor.setDirty(false);
        editor.refresh();
    }

    /**
     * Set editor
     * @param editor
     */
    public void setEditor(IsacEditor editor)
    {
        this.editor = editor;
    }

    /* ModifyListener */
    public void modifyText(ModifyEvent e)
    {
        parametersPanel.modifySomething(e.getSource());
        commit = true;
        editor.setDirty(true);
    }


    /**
     * Propagate a change in the plugin id
     * @param oldId old plugin id
     * @param newId new plugin id
     */
    private void updatePluginId(String oldId, String newId)
    {
        List<String> tagToUpdate = Arrays.asList(new String[]{"control", "sample", "timer", "condition"});
        for (String tag : tagToUpdate) {
            NodeList list = doc.getElementsByTagName(tag);
            changeUseIdInNodeList(oldId, newId, list);
        }
        NodeList paramNodeList = doc.getElementsByTagName("param");
        for (int i = 0; i < paramNodeList.getLength(); i++){
             Element element = (Element) paramNodeList.item(i);
            if(element.hasAttribute("value")){
                String attributeValue = element.getAttribute("value");
                element.setAttribute("value",getResultAfterPluginReplace(oldId,newId, attributeValue));
            }
        }
    }


    /**
     * Replace the plugin id inside the use attribute of the given list of tag
     * @param oldId old plugin id to search and replace
     * @param newId new plugin id value
     * @param list list of tags
     */
    private void changeUseIdInNodeList(String oldId, String newId, NodeList list) {
        for (int i = 0; i < list.getLength(); i++)
        {
            Element element = (Element) list.item(i);
            if (element.getAttribute("use").equals(oldId))
            {
                element.setAttribute("use", newId);
            }
        }
    }

    /**
     * Search and replace plugin id string inside the ${id:value} pattern
     * @param oldId old plugin id to search and replace
     * @param newId new plugin id value
     * @param string String to search
     * @return new string sequence with the new plugin id 
     */
    private String getResultAfterPluginReplace(String oldId, String newId, String string) {
        Pattern pattern = Pattern.compile(oldId);
        Matcher matcher = pattern.matcher(string);

        StringBuffer s = new StringBuffer();
        while (matcher.find()) {
            // Exit if nothing after match (no ':' or '}'
            if (matcher.hitEnd()) {
                continue;
            }
            // No possibilities of ${ before match if start is before 2
            if (matcher.start() < 2) {
                continue;
            }
            if (string.charAt(matcher.start() - 1) != '{') {
                continue;
            }
            if (string.charAt(matcher.start() - 2) != '$') {
                continue;
            }
            if (matcher.start() > 2) {
                if (string.charAt(matcher.start() - 3) == '$') {
                    continue;
                }
            }

            // Check after match
            // Replace the match if there is a ':' and a '}' after the match
            if (string.substring(matcher.end()).matches(":[^${]*}.*")) {
                matcher.appendReplacement(s, newId);
            }
        }
        matcher.appendTail(s);
        return s.toString();
    }

}