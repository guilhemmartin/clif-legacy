/*
* CLIF is a Load Injection Framework
* Copyright (C) 2005 France Telecom R&D
*
* This library is free software; you can redistribute it and/or
* modify it under the terms of the GNU Lesser General Public
* License as published by the Free Software Foundation; either
* version 2 of the License, or (at your option) any later version.
*
* This library is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
* Lesser General Public License for more details.
*
* You should have received a copy of the GNU Lesser General Public
* License along with this library; if not, write to the Free Software
* Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*
* CLIF $Name: not supported by cvs2svn $
*
* Contact: clif@ow2.org
*/

package org.ow2.clif.scenario.isac.egui.pages.pageBehavior;

import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.jface.text.IDocument;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.ui.IEditorInput;
import org.eclipse.ui.IEditorSite;
import org.eclipse.ui.PartInitException;
import org.eclipse.ui.forms.ManagedForm;
import org.eclipse.ui.part.EditorPart;
import org.ow2.clif.scenario.isac.egui.IsacEditor;
import org.ow2.clif.scenario.isac.egui.ScenarioManager;
import org.ow2.clif.scenario.isac.egui.loadprofile.LoadProfile;
/**
 * Each behavior in the scenario has his own
 * editor. For this reason, each editor has a behaviorId
 * and behaviorPos. BehaviorPos is used for ordering pages
 * of the Isac Editor.
 * 
 * @author Joan Chaumont
 */
public class BehaviorEditor extends EditorPart {

    private ScenarioManager scenario;
    private IsacEditor editor;
    
    private BehaviorMasterPage behaviorPage;
    private String behaviorId;
    private int behaviorPos;
    private LoadProfile behaviorProfile;
    
    /**
     * Constructor
     * @param scenario
     * @param editor
     * @param behaviorId
     * @param behaviorPos
     * @param profile
     */
    public BehaviorEditor (
            ScenarioManager scenario, IsacEditor editor, 
            String behaviorId, int behaviorPos, LoadProfile profile) {
        this.scenario = scenario;
        this.editor = editor;
        
        this.behaviorId = behaviorId;
        this.behaviorPos = behaviorPos;
        this.behaviorProfile = profile;
    }
    
    public void doSave(IProgressMonitor monitor) {}
    public void doSaveAs() {}

    public void init(IEditorSite site, IEditorInput input)
            throws PartInitException {
        setInput(input);
        setSite(site);
    }

    public boolean isDirty() {
        return false;
    }

    public boolean isSaveAsAllowed() {
        return false;
    }

    public void createPartControl(Composite parent) {
        behaviorPage = 
            new BehaviorMasterPage(
                    scenario, editor, behaviorId, behaviorPos, behaviorProfile);
        behaviorPage.createContent(new ManagedForm(parent));
    }

    public void setFocus() {}

    /**
     * Set the edited document 
     * @param doc
     */
    public void setDocument(IDocument doc) {
        behaviorPage.setDocument(doc);
    }
    
    /**
     * Set the behavior Id
     * @param behaviorId
     */
    public void setBehaviorId(String behaviorId) {
        this.behaviorId = behaviorId;
        behaviorPage.setBehaviorId(behaviorId);
    }

    /**
     * Set the position of this behavior in file
     * @param behaviorPos
     */
    public void setBehaviorPos(int behaviorPos) {
        this.behaviorPos = behaviorPos;
        behaviorPage.setBehaviorPos(behaviorPos);
    }

    /**
     * Set a profile for this behavior
     * @param profile
     */
    public void setProfile(LoadProfile profile) {
        behaviorProfile = profile;
        behaviorPage.setBehaviorProfile(behaviorProfile);
    }

    /**
     * @return ScenarioManager
     */
    public ScenarioManager getScenario() {
        return scenario;
    }
    
    /**
     * Behavior id getter
     * @return String behavior id
     */
    public String getBehaviorId() {
        return behaviorId;
    }

    /**
     * Behavior position getter
     * @return int behavior position
     */
    public int getBehaviorPos() {
        return behaviorPos;
    }
    
    /**
     * Refresh components in this editor
     */
    public void refresh() {
        behaviorPage.refresh();
    }
}
