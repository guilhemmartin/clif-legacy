<?xml version="1.0" encoding="UTF-8"?>

<xsl:transform
  version="1.0"
  xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

<xsl:output
   method="xml"
   indent="yes"
   encoding="utf-8"
/>

<xsl:variable name="targets">
	<xsl:comment> ========================= </xsl:comment>
	<xsl:comment> run saturation controller </xsl:comment>
	<xsl:comment> ========================= </xsl:comment>
	<target name="saturation" description="Run saturation controller">
		<java
			classname="org.ow2.clif.console.lib.batch.SaturationControlCmd"
			classpathref="baseclasspath" fork="yes" failonerror="yes">
			<jvmarg line="${{clif.parameters}}"/>
			<arg value="${{testplan.name}}"/>
		</java>
	</target>

	<xsl:comment> ========================================= </xsl:comment>
	<xsl:comment> run benchmark (load injection) controller </xsl:comment>
	<xsl:comment> ========================================= </xsl:comment>

	<target name="benchmark" description="Run load injection controller">
		<java
			classname="org.ow2.clif.console.lib.batch.InjectionControlCmd"
			classpathref="baseclasspath" fork="yes" failonerror="yes">
			<jvmarg line="${{clif.parameters}}"/>
			<arg value="${{testplan.name}}"/>
		</java>
	</target>

	
	<xsl:comment> ============= </xsl:comment>
	<xsl:comment> run selfbench </xsl:comment>
	<xsl:comment> ============= </xsl:comment>

	<target name="selfbench" description="Run a self-regulated load injection campaign">
		<java
			classname="org.ow2.clif.console.lib.batch.SelfbenchCmd"
			classpathref="baseclasspath" fork="yes" failonerror="yes">
			<jvmarg line="${{clif.parameters}}"/>
		</java>
	</target>


	<xsl:comment> ====================================================== </xsl:comment>
	<xsl:comment> Runs Joram JMS server configured with necessary topics </xsl:comment>
	<xsl:comment> ====================================================== </xsl:comment>

	<target name="joram">
		<java
			classname="org.ow2.clif.console.lib.batch.JoramServer"
			failonerror="yes"
			classpathref="baseclasspath"
			fork="yes">
			<jvmarg line="-Dcom.sun.management.jmxremote -DMXServer=com.scalagent.jmx.JMXServer"/>
			<arg line="stats saturation benchmark configuration" />
	    </java>
	</target>


	<xsl:comment> ============================================= </xsl:comment>
	<xsl:comment> Runs blade Statistics poller-publisher on JMS </xsl:comment>
	<xsl:comment> ============================================= </xsl:comment>
	<target name="stats">
		<java
			classname="org.ow2.clif.console.lib.batch.StatPublisher"
			failonerror="yes"
			classpathref="baseclasspath"
			fork="yes">
			<jvmarg line="${{clif.parameters}}"/>
			<arg line="${{testplan.name}} stats 5" />
	    </java>
	</target>
</xsl:variable>

<xsl:template match="@*|*|text()|comment()">
	<xsl:copy>
		<xsl:apply-templates select="@*|*|text()|comment()" />
	</xsl:copy>
</xsl:template>

<xsl:template match="/project/target[last()]">
    <xsl:copy-of select="."/>
   	<xsl:copy-of select="$targets" />
</xsl:template>

</xsl:transform>