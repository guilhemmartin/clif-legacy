/*
 * CLIF is a Load Injection Framework
 * Copyright (C) 2012 France Telecom
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 *
 * Contact: clif@ow2.org
 */
package org.ow2.clif.control.lib.srli.util;

import org.ow2.clif.control.lib.srli.InjectionDriverException;

/**
 * Matrix representation and associated computations.
 * @author Colin PUY
 * @author Bruno Dillenseger
 */
public class Matrix
{
    /**
     * Creates a new matrix resulting from the addition of two matrices
     * @param a a matrix
     * @param b another matrix
     * @return the addition of both matrices (a+b)
     * @throws InjectionDriverException the provided matrices don't have the same dimensions
     */
    static public Matrix add(Matrix a, Matrix b) throws InjectionDriverException 
    {
        if (a.getLines() != b.getLines() || a.getColumns() != b.getColumns()) 
        {
        	throw new InjectionDriverException(InjectionDriverException.Type.ILLEGAL_MATRIX_DIMENSION);
        }
        Matrix result = new Matrix(a.getLines(), a.getColumns());
        for (int i = 0; i < result.getLines(); i++)
        {
            for (int j = 0; j < result.getColumns(); j++)
            {
                result.setData(i, j, a.getData(i, j) + b.getData(i, j));
            }
        }
        return result;
    }

    /**
     * Creates a new matrix resulting from the subtraction between two matrices
     * @param a the first matrix
     * @param b the matrix to subtract to the first matrix
     * @return the subtraction result (a-b)
     * @throws InjectionDriverException the provided matrices don't have the same dimensions
     */
    static public Matrix sub(Matrix a, Matrix b) throws InjectionDriverException 
    {
        if (a.m_Lines != b.m_Lines || a.m_Columns != b.m_Columns)
        {
        	throw new InjectionDriverException(InjectionDriverException.Type.ILLEGAL_MATRIX_DIMENSION);
        }
        Matrix result = new Matrix(a.m_Lines, a.m_Columns);
        for (int i = 0; i < a.m_Lines; i++)
        {
            for (int j = 0; j < a.m_Columns; j++)
            {
                result.m_Data[i][j] = a.m_Data[i][j] - b.m_Data[i][j];
            }
        }
        return result;
    }

    /**
     * Creates a new matrix resulting from the product of two matrices
     * @param a the first matrix
     * @param b the second matrix
     * @return the product result (a*b)
     * @throws InjectionDriverException the number of columns of matrix a is different
     * from the number of lines of matrix b don't have the same dimensions
     */
    static public Matrix product(Matrix a, Matrix b) throws InjectionDriverException 
    {
        if (a.m_Columns != b.m_Lines)
        {
        	throw new InjectionDriverException(InjectionDriverException.Type.ILLEGAL_MATRIX_DIMENSION);
        }
        Matrix result = new Matrix(a.m_Lines, b.m_Columns);
        for (int i = 0; i < result.m_Lines; i++)
        {
            for (int j = 0; j < result.m_Columns; j++)
            {
                for (int k = 0; k < a.m_Columns; k++)
                {
                    result.m_Data[i][j] += (a.m_Data[i][k] * b.m_Data[k][j]);
                }
            }
        }
        return result;
    }

	private int m_Lines;
	private int m_Columns;
	private double m_Data[][];

	/**
	 * Creates a new matrix with the given dimension
	 * @param lines number of lines
	 * @param columns number of columns
	 */
	public Matrix(int lines, int columns)
	{
		m_Lines = lines;
		m_Columns = columns;
		m_Data = new double[lines][columns];
	}

	/**
	 * Copy constructor
	 */
	public Matrix(Matrix matrix)
	{
		m_Lines = matrix.m_Lines;
		m_Columns = matrix.m_Columns;
		m_Data = new double[matrix.m_Lines][ matrix.m_Columns];
		
		for (int i = 0; i < matrix.m_Lines; i++)
		{
			for (int j = 0; j < matrix.m_Columns; j++)
			{
				m_Data[i][j] = matrix.m_Data[i][j];
			}
		}
	}
	
	/**
	 * @return the number of lines of this matrix
	 */
	public int getLines() 
	{
		return m_Lines;
	}

	/**
	 * @return the number of columns of this matrix
	 */
	public int getColumns() 
	{
		return m_Columns;
	}

	/**
	 * @return the matrix coefficients
	 */
	public double[][] getData()
	{
		return m_Data;
	}

	/**
	 * Get the coefficient at a given position in this matrix
	 * @param lineIndex the line index (starting with 0)
	 * @param columnIndex the column index (starting at 0)
	 * @return the coefficient
	 * @throws InjectionDriverException the specified position was out of the matrix bounds
	 */
	public double getData(int lineIndex, int columnIndex)
	throws InjectionDriverException
	{
		if (this.m_Lines <= lineIndex || this.m_Columns <= columnIndex)
		{
			throw new InjectionDriverException(InjectionDriverException.Type.ILLEGAL_MATRIX_DIMENSION);
		}
		return m_Data[lineIndex][columnIndex];
	} 

	/**
	 * Set this matrix's coefficients
	 * @param data the array of coefficients values
	 * @throws InjectionDriverException the provided array's dimension
	 * was not compatible with this matrix's dimension
	 */
	public void setData(double data[][])
	throws InjectionDriverException
	{
		if(data.length != m_Lines)
		{
			throw new InjectionDriverException(InjectionDriverException.Type.ILLEGAL_MATRIX_DIMENSION);
		}
		for(int a = 0; a < data.length; a++)
		{
			if(data[a].length != m_Columns) 
			{
				throw new InjectionDriverException(InjectionDriverException.Type.ILLEGAL_MATRIX_DIMENSION);
			}
		}
		for (int i = 0; i < m_Lines; i++)
		{
			for (int j = 0; j < m_Columns; j++)
			{
				m_Data[i][j] = data[i][j];
			}
		}
	}

	/**
	 * Set one coefficient of the matrix at a specified position
	 * @param lineIndex the line index (starting with 0)
	 * @param columnIndex the column index (starting at 0)
	 * @param value the coefficient value to set
	 * @throws InjectionDriverException the specified position was out of the matrix bounds
	 */
	public void setData(int lineIndex, int columnIndex, double value)
	throws InjectionDriverException
	{
		if (this.m_Lines <= lineIndex || this.m_Columns <= columnIndex)
		{
			throw new InjectionDriverException(InjectionDriverException.Type.ILLEGAL_MATRIX_DIMENSION);
		}
		m_Data[lineIndex][columnIndex] = value;
	} 
	
	/**
	 * Set all coefficients of this matrix with a single value
	 * @param value	the coefficient value to set
	 */
	public void fill(double value)
	{
		for (int i = 0; i < this.m_Lines; i++)
		{
			for (int j = 0; j < this.m_Columns; j++)
			{
				m_Data[i][j] = value;
			}
		}
	}

	/**
	 * Creates the transposed matrix of this matrix
	 * @return the transposed matrix
	 */
    public Matrix transpose()
    {
    	Matrix transposed = new Matrix(this.m_Columns, this.m_Lines);

        for (int i = 0; i < this.m_Columns; i++)
        {
           for (int j = 0; j < this.m_Lines; j++)
           {
        	   try
        	   {
        		   transposed.setData(j, i, this.getData(i, j));
        	   }
        	   catch (InjectionDriverException ex)
        	   {
        		   // should never occur
        		   throw new Error("Algorithm error in matrix transposition", ex);
        	   }
           }
        }
        return transposed;
    }

	/**
	 * Pretty representation of this matrix
	 */
	public String toString()
	{
		StringBuilder result = new StringBuilder();
		for(int a = 0; a < m_Lines; a++)
		{
			result.append("\n( ");
			for(int b = 0; b < m_Columns; b++)
			{
				result.append(m_Data[a][b] + " ");
			}
			result.append(")");
		}
		result.append(System.getProperty("line.separator"));
		return result.toString();
	} 
}
