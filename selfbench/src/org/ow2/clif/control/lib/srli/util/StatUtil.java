/*
* CLIF is a Load Injection Framework
* Copyright (C) 2012 France Telecom R&D
*
* This library is free software; you can redistribute it and/or
* modify it under the terms of the GNU Lesser General Public
* License as published by the Free Software Foundation; either
* version 2 of the License, or (at your option) any later version.
*
* This library is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
* Lesser General Public License for more details.
*
* You should have received a copy of the GNU Lesser General Public
* License along with this library; if not, write to the Free Software
* Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*
* Contact: clif@ow2.org
*/

package org.ow2.clif.control.lib.srli.util;


/**
 * Miscellaneous statistical utilities.
 * @author Colin Puy
 * @author Nabila Salmi
 * @author Bruno Dillenseger
 */
public abstract class StatUtil
{
	/**
	 * Compute a weighted mean.
	 *
	 * @param values the values
	 * @param weights the values' weights
	 * @return the weighted mean
	 */
	static public double weightedMean(double[] values, long[] weights)
	{
		double mean = 0;
		long n = 0;
		double sum = 0;
		for (int i = 0; i < values.length; i++)
		{
			sum += values[i] * (double) weights[i];
			n += weights[i];
		}
		if (n != 0)
		{
			mean = sum / (double) n;
		}
		return mean;
	}


	/**
	 * Compute a global, weighted variance from a set of variance and mean values
	 *
	 * @param variances the variance values
	 * @param weights the values' weights
	 * @param means the mean values
	 * @return the global, weighted variance
	 */
	static public double weightedVariance(double[] variances, double[] means, long[] weights)
	{
		double varR = 0;
		long n = 0;
		double leftPart = 0;
		double rightPart = 0;
		double meanR = weightedMean(means, weights);
		
		for (int i = 0; i < variances.length; i++)
		{
			n += weights[i];
			leftPart += (double) weights[i] * variances[i];
			rightPart += (double) weights[i] * Math.pow((double) means[i] - meanR, 2);
		}
		if (n != 0)
		{
			varR = ((double) 1 / (double) n) * leftPart + ((double) 1 / (double) n) * rightPart;
		}
		return varR;
	}
}
