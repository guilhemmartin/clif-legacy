/*
* CLIF is a Load Injection Framework
* Copyright (C) 2012 France Telecom R&D
*
* This library is free software; you can redistribute it and/or
* modify it under the terms of the GNU Lesser General Public
* License as published by the Free Software Foundation; either
* version 2 of the License, or (at your option) any later version.
*
* This library is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
* Lesser General Public License for more details.
*
* You should have received a copy of the GNU Lesser General Public
* License along with this library; if not, write to the Free Software
* Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*
* Contact: clif@ow2.org
*/

package org.ow2.clif.control.lib.saturation;

import java.io.Serializable;
import org.ow2.clif.control.lib.saturation.SaturationChecker.ThresholdChecker;

/**
 * Exception thrown when a blade monitoring statistical value
 * exceeds a given threshold, according to a saturation policy
 * identified by a given key.
 * 
 * @author Bruno Dillenseger
 */
public class SaturationException extends Exception
{
	private static final long serialVersionUID = -4241705168502509374L;

	private String bladeId;
	private Serializable key;

	/**
	 * Creates a new exception notifying that a statistical monitoring value
	 * of a blade has exceeded a saturation threshold.
	 * @param bladeId the blade violating the saturation policy
	 * @param checker the threshold checker object which has detected
	 * the threshold exceed
	 * @param stat the value that exceeded the threshold
	 * @param key the key identifying the reference policy
	 */
	SaturationException(String bladeId, ThresholdChecker checker, long stat, Serializable key)
	{
		super("Blade " + bladeId + " violates condition " + checker + ": " + stat);
		this.bladeId = bladeId;
		this.key = key;
	}

	/**
	 * @return the identifier of the blade where the threshold has been exceeded
	 */
	public String getBladeId()
	{
		return bladeId;
	}

	/**
	 * @return the key identifying the policy that has been violated
	 */
	public Serializable getKey()
	{
		return key;
	}
}
