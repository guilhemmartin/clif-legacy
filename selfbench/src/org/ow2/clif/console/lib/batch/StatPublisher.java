/*
* CLIF is a Load Injection Framework
* Copyright (C) 2012 France Telecom R&D
*
* This library is free software; you can redistribute it and/or
* modify it under the terms of the GNU Lesser General Public
* License as published by the Free Software Foundation; either
* version 2 of the License, or (at your option) any later version.
*
* This library is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
* Lesser General Public License for more details.
*
* You should have received a copy of the GNU Lesser General Public
* License along with this library; if not, write to the Free Software
* Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*
* Contact: clif@ow2.org
*/

package org.ow2.clif.console.lib.batch;

import java.util.HashMap;
import javax.jms.JMSException;
import javax.jms.ObjectMessage;
import javax.jms.Session;
import javax.jms.Topic;
import javax.jms.TopicConnection;
import javax.jms.TopicConnectionFactory;
import javax.jms.TopicPublisher;
import javax.jms.TopicSession;
import javax.naming.Context;
import javax.naming.NamingException;
import org.objectweb.fractal.api.NoSuchInterfaceException;
import org.ow2.clif.control.lib.srli.SelfbenchConstants;
import org.ow2.clif.deploy.ClifAppFacade;
import org.ow2.clif.supervisor.api.TestControl;
import org.ow2.clif.util.ExecutionContext;

/**
 * A StatPubliher instance periodically gets the moving statistical values from all blades
 * (via the getStats(bladeId) operation from the TestControl interface of the CLIF supervisor
 * component) and publishes them on a given JMS topic.
 * 
 * @author Bruno Dillenseger
 */
public class StatPublisher implements Runnable
{
    private TopicSession pubSession;
    private TopicPublisher publisher;
    private TopicConnection connection;
    private int period_ms;
    private TestControl tcItf;


    public StatPublisher(TestControl tc, String topicName, int period_s)
    throws JMSException, NamingException
    {
    	this.tcItf = tc;
        this.period_ms = 1000 * period_s;
        Context jndi = JNDI.init();
        TopicConnectionFactory tcf = (TopicConnectionFactory)jndi.lookup("tcf");
        this.connection = tcf.createTopicConnection(SelfbenchConstants.USERNAME, SelfbenchConstants.PASSWORD);
        this.pubSession = connection.createTopicSession(false, Session.AUTO_ACKNOWLEDGE);
        Topic statsTopic = (Topic)jndi.lookup(topicName);
        this.publisher = pubSession.createPublisher(statsTopic);
        connection.start( );
    }


    /**
     * Runs a stats publisher instance bound to the given CLIF application
     * @param args args[0] gives the name of the deployed test plan,
     * args[1] gives the target topic name, args[2] gives the blades polling
     * period (aka the statistics moving window) as well as the publishing period
     * (expressed in seconds).
     */
    public static void main(String [] args)
    {
    	if (System.getSecurityManager() == null)
    	{
       		System.setSecurityManager(new SecurityManager());
       	}
       	if (args.length != 3)
       	{
       		BatchUtil.usage("expected arguments: <name_of_deployed_test_plan> <topic> <period_s>");
       	}
       	ExecutionContext.init("./");
       	try
       	{
	   		ClifAppFacade clifApp = BatchUtil.getClifAppFacade(args[0]);
	   		if (clifApp != null)
	   		{
		   		TestControl tcItf = (TestControl) clifApp.getComponentByName("supervisor").getFcInterface(TestControl.TEST_CONTROL);
		   		StatPublisher sp = new StatPublisher(tcItf, args[1], Integer.parseInt(args[2]));
		   		new Thread(sp).start();
	   		}
	   		else
	   		{
	   			System.err.println("Fatal: can't find CLIF application \"" + args[0]+ "\"." );
	   			System.exit(-1);
	   		}
       	}
       	catch (NoSuchInterfaceException ex)
       	{
       		System.err.println("Fatal: can't get the CLIF application's Test Control interface: " + ex);
       		ex.printStackTrace(System.err);
   			System.exit(-2);
       	}
       	catch (NamingException ex)
       	{
       		System.err.println("Fatal: can't get JMS services because of a JNDI problem: " + ex);
       		ex.printStackTrace(System.err);
   			System.exit(-3);
       	}
       	catch (JMSException ex)
       	{
       		System.err.println("Fatal: can't set-up the access to the JMS services: " + ex);
       		ex.printStackTrace(System.err);
   			System.exit(-4);
       	}
    }


	////////////////////////
	// interface Runnable //
	////////////////////////


	public void run()
	{
		String[] bladesIds = tcItf.getBladesIds();
		while (! Thread.interrupted())
		{
			try
			{
				Thread.sleep(period_ms);
				ObjectMessage message = pubSession.createObjectMessage();
				HashMap<String,long[]> stats = new HashMap<String, long[]>();
				for (String id : bladesIds)
				{
					stats.put(id,  tcItf.getStats(id));
				}
				message.setObject(stats);
				publisher.publish(message);
			}
			catch (InterruptedException ex)
			{
				// ignore: just exit from the while loop
			}
			catch (JMSException ex)
			{
				System.err.println("Warning: Stat Publisher is ignoring JMS exception " + ex);
				ex.printStackTrace(System.err);
			}
		}
		try
		{
			connection.close( );
		}
		catch (Throwable th)
		{
			throw new RuntimeException("Stat Publisher could not close the JMS connection on exit.", th);
		}
	}
}
