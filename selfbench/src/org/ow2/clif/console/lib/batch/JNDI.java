/*
 * CLIF is a Load Injection Framework
 * Copyright (C) 2012 France Telecom
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 *
 * Contact: clif@ow2.org
 */
package org.ow2.clif.console.lib.batch;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;
import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;

/**
 * Utility class to get the initial naming context.
 * It relies on contents of file etc/jndi.properties.
 * @author Bruno Dillenseger
 */
public abstract class JNDI
{
	static private final String JNDI_PROPS_PATH = "etc/jndi.properties";

	static public Context init()
	{
		Context jndi = null;
		Properties env = new Properties();
		try
		{
			InputStream inS = new FileInputStream(JNDI_PROPS_PATH);
			env.load(inS);
			jndi = new InitialContext(env);
		}
		catch (FileNotFoundException ex)
		{
			throw new Error("Can't find JNDI property file " + JNDI_PROPS_PATH, ex);
		}
		catch (IOException ex)
		{
			throw new Error("Can't read JNDI property file " + JNDI_PROPS_PATH, ex);
		}
		catch (NamingException ex)
		{
			throw new Error("Can't get JNDI initial context.", ex);
		}
		return jndi;
	}
}
