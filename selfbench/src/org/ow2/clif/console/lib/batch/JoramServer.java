/*
 * CLIF is a Load Injection Framework
 * Copyright (C) 2012 France Telecom
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 *
 * Contact: clif@ow2.org
 */

package org.ow2.clif.console.lib.batch;

import org.objectweb.joram.client.jms.Topic;
import org.objectweb.joram.client.jms.admin.AdminModule;
import org.objectweb.joram.client.jms.admin.User;
import org.objectweb.joram.client.jms.tcp.TcpConnectionFactory;
import org.ow2.clif.control.lib.srli.SelfbenchConstants;
import fr.dyade.aaa.agent.AgentServer;

/**
 * Runs a JORAM server initialized with user anonymous/anonymous
 * and provided topics.
 * Binds the topic connection factory ("tcf") and topics in JNDI.
 * 
 * @author Bruno Dillenseger, based on original materials from the JORAM project
 * by Frederic Maistre and ScalAgent Distributed Technologies
 */
public class JoramServer
{
	/**
	 * Runs the JORAM JMS server, creates anonymous/anonymous user
	 * and creates some topics.
	 * @param args names of JMS topics to create
	 * @throws Exception the JORAM server could not be launched
	 */
	public static void main(String[] args) throws Exception
	{
		AgentServer.init((short)0, "./s0", null);
		AgentServer.start();
		javax.jms.TopicConnectionFactory tcf =
			TcpConnectionFactory.create(AgentServer.getHostname((short)0), 16010);
		AdminModule.connect(tcf, "root", "root");
		User.create(SelfbenchConstants.USERNAME, SelfbenchConstants.PASSWORD);
		System.out.println("Created user anonymous");
		javax.naming.Context jndiCtx = new javax.naming.InitialContext();
		jndiCtx.bind("tcf", tcf);
		for (String topicName : args)
		{
			Topic topic = Topic.create(topicName);
			topic.setFreeReading();
			topic.setFreeWriting();
			jndiCtx.bind(topicName, topic);
			System.out.println("Created topic " + topicName);
		}
		jndiCtx.close();
		AdminModule.disconnect();
		System.out.println("The JORAM JMS server is ready.");
	}
}
