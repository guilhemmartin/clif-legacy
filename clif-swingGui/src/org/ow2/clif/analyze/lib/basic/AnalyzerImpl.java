/*
* CLIF is a Load Injection Framework
* Copyright (C) 2005 France Telecom R&D
*
* This library is free software; you can redistribute it and/or
* modify it under the terms of the GNU Lesser General Public
* License as published by the Free Software Foundation; either
* version 2 of the License, or (at your option) any later version.
*
* This library is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
* Lesser General Public License for more details.
*
* You should have received a copy of the GNU Lesser General Public
* License along with this library; if not, write to the Free Software
* Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*
* Contact: clif@ow2.org
*/

package org.ow2.clif.analyze.lib.basic;

import java.awt.Dimension;
import java.io.Serializable;
import java.util.Properties;
import javax.swing.JDesktopPane;
import javax.swing.JFrame;
import javax.swing.JInternalFrame;
import org.ow2.clif.analyze.api.AnalyzerLink;
import org.ow2.clif.storage.api.BladeDescriptor;
import org.ow2.clif.storage.api.BladeEvent;
import org.ow2.clif.storage.api.BladeFilter;
import org.ow2.clif.storage.api.EventFilter;
import org.ow2.clif.storage.api.StorageRead;
import org.ow2.clif.storage.api.TestDescriptor;
import org.ow2.clif.storage.api.TestFilter;
import org.ow2.clif.supervisor.api.ClifException;
import org.ow2.clif.util.ExecutionContext;
import org.objectweb.fractal.api.control.BindingController;


/**
 *
 * @author Bruno Dillenseger
 */
public class AnalyzerImpl implements BindingController, AnalyzerLink, StorageRead
{
	static private final String LABEL = "Basic analyzer";

	// list of client interfaces names
	static private String[] itfList = new String[] { StorageRead.STORAGE_READ };


	static public void main(String[] args)
	{
		try
		{
			ExecutionContext.init("./");
			// get access to the storage component
			AnalyzerLink analyzer = new AnalyzerImpl(
				(StorageRead)Class.forName(args[0]).newInstance());
			
			JFrame frame = new JFrame(LABEL);
			frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
			Dimension screenDim = frame.getToolkit().getScreenSize();
			frame.setSize((screenDim.width*3)/4, (screenDim.height*3)/4);
			JDesktopPane desktop = new JDesktopPane();
			frame.getContentPane().add(desktop);
			JInternalFrame browser = new JInternalFrame("Test browser", true, false, true, true); 
			desktop.add(browser);
			browser.setSize((screenDim.width*9)/16, (screenDim.height*9)/16);
			browser.setVisible(true);
			frame.setVisible(true);
			analyzer.setUIContext(browser);
		}
		catch (Throwable t)
		{
			t.printStackTrace(System.err);
			System.exit(-1);
		}
	}


	// field for client binding (access to stored data)
	private StorageRead srItf;
	SwingGUI gui = null;


	private AnalyzerImpl(StorageRead store)
	{
		this();
		srItf = store;
	}


	public AnalyzerImpl()
	{
	}


	public TestDescriptor[] getTests(TestFilter filter)
		throws ClifException
	{
		return srItf.getTests(filter);
	}


	public BladeDescriptor[] getTestPlan(String testName, BladeFilter filter)
		throws ClifException
	{
		return srItf.getTestPlan(testName, filter);
	}


	public Properties getBladeProperties(String testName, String bladeId)
		throws ClifException
	{
		return srItf.getBladeProperties(testName, bladeId);
	}


	public String[] getEventFieldLabels(String test, String blade, String eventTypeLabel)
	{
		return srItf.getEventFieldLabels(test, blade, eventTypeLabel);
	}


	public long countEvents(String test, String blade, String eventTypeLabel, EventFilter filter)
		throws ClifException
	{
		return srItf.countEvents(test, blade, eventTypeLabel, filter);
	}


	public Serializable getEventIterator(String test, String blade, String eventTypeLabel, EventFilter filter)
		throws ClifException
	{
		return srItf.getEventIterator(test, blade, eventTypeLabel, filter);
	}


	public BladeEvent[] getNextEvents(
		Serializable iteratorKey,
		int count)
	throws ClifException
	{
		return srItf.getNextEvents(iteratorKey, count);
	}


	public void closeEventIterator(Serializable iteratorKey)
	{
		srItf.closeEventIterator(iteratorKey);
	}


	public BladeEvent[] getEvents(
		String testName,
		String bladeId,
		String eventTypeLabel,
		EventFilter filter,
		long fromIndex,
		int count)
	throws ClifException
	{
		return srItf.getEvents(testName, bladeId, eventTypeLabel, filter, fromIndex, count);
	}


	/////////////////////////////////
	// interface BindingController //
	/////////////////////////////////


	public Object lookupFc(String clientItfName)
	{
		if (clientItfName.equals(StorageRead.STORAGE_READ))
		{
			return srItf;
		}
		else
		{
			return null;
		}
	}


	public synchronized void bindFc(String clientItfName, Object serverItf)
	{
		if (clientItfName.equals(StorageRead.STORAGE_READ))
		{
			srItf = (StorageRead) serverItf;
		}
	}


	public synchronized void unbindFc(String clientItfName)
	{
		if (clientItfName.equals(StorageRead.STORAGE_READ))
		{
			srItf = null;
		}
	}


	public String[] listFc()
	{
		return itfList;
	}


	////////////////////////////
	// interface AnalyzerLink //
	////////////////////////////


	public String getLabel()
	{
		return LABEL;
	}


	public void setUIContext(Object internalFrame)
	{
		try
		{
			if (gui != null)
			{
				((JInternalFrame)internalFrame).getContentPane().removeAll();
			}
			gui = new SwingGUI((JInternalFrame)internalFrame, this);
		}
		catch (Exception ex)
		{
			throw new Error(
				"Can't find a compatible Analyzer user interface for " + internalFrame,
				ex);
		}
	}
}
