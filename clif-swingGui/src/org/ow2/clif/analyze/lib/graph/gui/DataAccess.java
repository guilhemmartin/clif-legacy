/*
* CLIF is a Load Injection Framework
* Copyright (C) 2011 France Telecom R&D
* Copyright (C) 2016-2017 Orange SA
*
* This library is free software; you can redistribute it and/or
* modify it under the terms of the GNU Lesser General Public
* License as published by the Free Software Foundation; either
* version 2 of the License, or (at your option) any later version.
*
* This library is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
* Lesser General Public License for more details.
*
* You should have received a copy of the GNU Lesser General Public
* License along with this library; if not, write to the Free Software
* Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*
* Contact: clif@ow2.org
*/

package org.ow2.clif.analyze.lib.graph.gui;

import java.io.File;
import java.net.URL;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.LinkedList;
import java.util.List;
import java.util.SortedSet;
import java.util.TreeSet;
import javax.swing.BorderFactory;
import javax.swing.ImageIcon;
import javax.swing.JComponent;
import javax.swing.JInternalFrame;
import javax.swing.border.TitledBorder;
import javax.swing.tree.DefaultMutableTreeNode;
import org.jfree.chart.JFreeChart;
import org.ow2.clif.analyze.lib.graph.FieldFilterAndOp;
import org.ow2.clif.analyze.lib.graph.util.StorageReadHelper;
import org.ow2.clif.analyze.lib.report.Dataset;
import org.ow2.clif.analyze.lib.report.Dataset.DatasetType;
import org.ow2.clif.analyze.lib.report.Datasource;
import org.ow2.clif.analyze.lib.report.FieldsValuesFilter;
import org.ow2.clif.analyze.lib.report.LogAndDebug;
import org.ow2.clif.analyze.lib.report.Report;
import org.ow2.clif.analyze.lib.report.Section;
import org.ow2.clif.analyze.lib.report.Statistics;
import org.ow2.clif.storage.api.BladeDescriptor;
import org.ow2.clif.storage.api.BladeEvent;
import org.ow2.clif.storage.api.StorageRead;
import org.ow2.clif.storage.lib.util.NameBladeFilter;
import org.ow2.clif.supervisor.api.ClifException;


/**
 * @author Tomas Perez-Segovia
 * @author Bruno Dillenseger 
 *
 * Access to stored data
 */
public class DataAccess {

	JInternalFrame intFrame;

	private StorageReadHelper srHlp;

	public class W1SelectedDataset extends LinkedList<Datasource>{
		private static final long serialVersionUID = -2698999770235377172L;
		private long firstEventTime = -2;
		private long lastEventTime = -1;
		private DatasetType datasetType;

		public void setDatasetType(DatasetType datasetType) {
			this.datasetType = datasetType;
		}

		public DatasetType getDatasetType() {
			return datasetType;
		}


		public long getFirstEventTime() {
			return firstEventTime;
		}

		public void setFirstEventTime(long firstEventTime) {
			this.firstEventTime = firstEventTime;
		}

		public long getLastEventTime() {
			return lastEventTime;
		}

		public void setLastEventTime(long lastEventTime) {
			this.lastEventTime = lastEventTime;
		}

	}

	Dataset wizardDataset = null; 	// dataset (simple, multiple or aggregate) under Wizard1 processing
	Report report = new Report();


	public DataAccess(JInternalFrame _intFrame, StorageRead store) {
		intFrame = _intFrame;
		srHlp = new StorageReadHelper(store);
	}

	// initializations tree and path

	public DefaultMutableTreeNode getTestsTreeNode() {
		DefaultMutableTreeNode rootNode = new DefaultMutableTreeNode("CLIF");
		DefaultMutableTreeNode childNode;
		DefaultMutableTreeNode childChildNode;
		// Build Execution Tree
		SortedSet<String> testsNames = srHlp.getTestsNames();
		for (String testName : testsNames)
		{
			childNode = new DefaultMutableTreeNode(testName);
			SortedSet<String> bladesIds = srHlp.getBladesIds(testName);
			if (bladesIds != null)
			{
				for (String blade : bladesIds)
				{
					childChildNode = new DefaultMutableTreeNode(blade);
					childNode.add(childChildNode);
				}
			}
			rootNode.add(childNode);
		}
		return rootNode;
	}


	//////
	//////		getters and setters
	//////


	public JInternalFrame getInternalFrame() {
		return intFrame;
	}


	public void setInternalFrame(JInternalFrame _intFrame) {
		intFrame = _intFrame;
	}


	public BladeDescriptor[] getBladeDescriptor(String testName, NameBladeFilter filter)
	throws ClifException {
		return srHlp.getTestPlan(testName, filter);
	}


	// fields


	/**
	 * @param dataset Dataset
	 * @return String Array of Fields in dataset 
	 * Get fields, datasource by datasource, and merge them in a set (no duplications)
	 *   and place them in a String[]
	 */
	public SortedSet<String> getFieldLabels(Dataset dataset)
	{
		SortedSet<String> result = new TreeSet<String>();
		for (Datasource datasource : dataset.getDatasources())
		{
			SortedSet<String> fields = getFieldLabels(datasource);
			result.addAll(fields);
		}
		return result;
	}


	/**
	 * @param dataset Dataset
	 * @return String Array of Fields in dataset without "date" field
	 */
	public SortedSet<String> getFieldLabelsWithoutDate(Dataset dataset)
	{
		SortedSet<String> result = getFieldLabels(dataset);
		result.remove("date");
		return result;
	}


	public SortedSet<String> getFieldLabels(Datasource datasource)
	{
		SortedSet<String> result = new TreeSet<String>();
		String[] fieldLabels = srHlp.getEventFieldLabels(
			datasource.getTestName(),
			datasource.getBladeId(),
			datasource.getEventType());
		if (fieldLabels != null)
		{
			result = new TreeSet<String>();
			result.addAll(Arrays.asList(fieldLabels));
		}
		return result;
	}


	// values
	public List<BladeEvent> getBladeOneEventValuesList(Datasource datasource) {
		List<BladeEvent> bladeEventList = new ArrayList<BladeEvent>();
		BladeEvent[] bladeArray;

		bladeArray = getBladeOneEventValuesArray(
				datasource.getTestName(),
				datasource.getBladeId(),
				datasource.getEventType());
		bladeEventList = Arrays.asList(bladeArray);
		return bladeEventList;
	}

	public BladeEvent[] getBladeOneEventValuesArray(String testName,
			String bladeId, String eventTypeLabel){
		if (testName.equals("") || bladeId.equals("")){
			return null;
		}
		return srHlp.getBladeEvents(testName, bladeId, eventTypeLabel, null);
	}

	public BladeEvent[] getBladeOneEventValuesArray(Datasource datasource) {
		String testName = datasource.getTestName();
		String bladeId = datasource.getBladeId();
		String eventType = datasource.getEventType();
		return srHlp.getBladeEvents(testName, bladeId, eventType, null);
	}


	// eventsNumber
	@SuppressWarnings("unused")
	private int getEventsNumber(Dataset dataset) {
		int ret = 0;
		switch (dataset.getDatasetType()){
		case SIMPLE_DATASET:
			ret = getEventsNumber(dataset.getDatasources().get(0));
			break;

		case MULTIPLE_DATASET:
			LogAndDebug.err("ERROR DataAccess.getEventsNumber: should not have been called for a MULTIPLE_DATASET");
			break;

		case AGGREGATE_DATASET:
			for(Datasource datasource : dataset.getDatasources()){
				ret += getEventsNumber(datasource);
			}
		}
		return ret;
	}


	private int getEventsNumber(Datasource datasource) {
		Statistics statistics = datasource.getStatistics();
		return statistics.getFinalEventsNumber();
	}




	SortedSet<String> getEventTypes(String testName, String bladeId)
	{
		return srHlp.getEventsTypeLabels(testName, bladeId);
	}

	// Times

	public long getFirstEventTime(Datasource datasource) {
		return getMinTime(datasource.getTestName(),datasource.getBladeId());
	}


	public long getFirstEventTime(Dataset dataset){
		long first = -1;
		for(Datasource dd: dataset.getDatasources()){
			String testName = dd.getTestName();
			String bladeName = dd.getBladeId();
			String eventType = dd.getEventType();
			if (eventType.length() == 0){
				first = getMinTime(testName, bladeName);
			}else if (-1 == first){
				first = srHlp.getMinTimeFor(testName, bladeName);
			}else{
				first = Math.min(first, srHlp.getMinTimeFor(testName, bladeName));
			}
		}
		return first;
	}

	public long getLastEventTime(Datasource datasource) {
		return getMaxTime(datasource.getTestName(),datasource.getBladeId());
	}


	public long getLastEventTime(Dataset dataset){
		long last = 0;
		for(Datasource dd: dataset.getDatasources()){
			String testName = dd.getTestName();
			String bladeName = dd.getBladeId();
			last = Math.max(last, getMaxTime(testName, bladeName));
		}
		return last;
	}


	private long getMinTime(String tst, String bld) {
		return srHlp.getMinTimeFor(tst, bld);
	}


	private long getMaxTime(String tst, String bld) {
		return srHlp.getMaxTimeFor(tst, bld);
	}


	/**
	 * updates currentD's attributes when finishing Wizard's step 1:
	 * 	firstEventTime and lastEventTime
	 */
	void updateWizardDataset() {
		wizardDataset.setFirstEventTime(getFirstEventTime(wizardDataset));
		wizardDataset.setLastEventTime(getLastEventTime(wizardDataset));
		if (wizardDataset.getName().equals(wizardDataset.getInitialName())){		// we can change the name
			String newAlias = "";
			switch (wizardDataset.getDatasetType()){
			case SIMPLE_DATASET:
				newAlias = "Simple dataset " + wizardDataset.getId();
				break;
			case MULTIPLE_DATASET:
				newAlias = "Multiple dataset " + wizardDataset.getId() + " (size "+ wizardDataset.getDatasources().size() + ")";
				break;
			case AGGREGATE_DATASET:
				newAlias = "Aggregate dataset " + wizardDataset.getId() + " (size "+ wizardDataset.getDatasources().size() + ")";
				break;
			}
			wizardDataset.setInitialName(newAlias);
			wizardDataset.setName(newAlias);
		} else {
			;	// the alias name has been changed by the user -> don't touch it
		}
	}


	// Icons
	ImageIcon DELETE_ICON   = null;
	ImageIcon INJECTOR_ICON = null;
	ImageIcon PROBE_ICON    = null;


	private Object kCleaning;

	private Object percentCleaning;


	void loadIcons(){
		DELETE_ICON   = loadIcon("delete.gif");
		INJECTOR_ICON = loadIcon("injection20-20x20.png");
		PROBE_ICON    = loadIcon("Speed-meter-20x20.png");
	}

	
	// TODO make work icons loading
	private ImageIcon loadIcon(String path){
		ImageIcon icon = null;
		String fs = File.separator;
		fs = "\\\\";
		fs = "/";
		String[] paths = new String[]{
				path,
				"icons" + fs + path,		// for CLIF RCP Console
				"common" + fs + path,
				"icons" + fs + "common" + fs + path,
				"dist" + fs + "icons" + fs + "common" + fs + path,
				".." + fs + "dist" + fs + "icons" + fs + "common" + fs + path,
				"icons" + fs + path,
				"dist" + fs + "icons" + fs + path,
				".." + fs + "dist" + fs + "icons" + fs + path,
				"dist" + fs + "lib" + fs + "icons" + fs + "common" + fs + path,
				path
		};

		ClassLoader cl = getClass().getClassLoader();
		for (String s : paths){
			try{
				URL loc = ClassLoader.getSystemResource(s);
				if (null == loc){
//					LogAndDebug.trace("*** - Could not getSystemResource \"" + s + "\"");
				}else{
					icon = new ImageIcon(loc);
					LogAndDebug.trace("icon loaded \"" + s + "\"");
					break;
				}
			}catch (NullPointerException e) {
//				LogAndDebug.err("*** - NullPointerException at getSystemResource \"" + s + "\"");
			}

			try{
				URL loc = cl.getResource(s);
				if (null == loc){
//					LogAndDebug.trace"*** - Could not getResource \"" + s + "\"");
				}else{
					icon = new ImageIcon(loc);
					LogAndDebug.trace("icon loaded \"" + s + "\"");
					break;
				}
			}catch (NullPointerException e) {
//				LogAndDebug.err("*** - NullPointerException at getResource \"" + s + "\"");
			}
//			LogAndDebug.trace("*** - Could not load icon \"" + s + "\"");
		}
		return icon;
	}


	public void setBorder(JComponent component, String title) {
		TitledBorder titleBorder;
		titleBorder = BorderFactory.createTitledBorder(title);
		component.setBorder(titleBorder);
	}


	public void verifyWizardDataset() {
		if (null == wizardDataset){
			LogAndDebug.trace(" wizardDataset is null. Nothig to verify.");
			return;
		}
		wizardDataset.getId();

		computeDatasetStatistics(wizardDataset);
	}


	public DefaultMutableTreeNode getReportRoot() {
		DefaultMutableTreeNode rootNode = new DefaultMutableTreeNode("CLIF REPORT");
		DefaultMutableTreeNode sectionNode;
		DefaultMutableTreeNode datasetNode;
		// Build Tree
		for(Section sec : report.getSections()){
			sectionNode = new DefaultMutableTreeNode(sec);
			rootNode.add(sectionNode);
			for(Dataset dataset : sec.getDatasets()){
				datasetNode = new DefaultMutableTreeNode(dataset);
				sectionNode.add(datasetNode);
			}
		}
		return rootNode;
	}


	public List<BladeEvent> getBladeOneEventValues(String testName,
			String bladeId, String eventTypeLabel){
		return srHlp.getBladeOneEventValues(testName, bladeId, eventTypeLabel);
	}


	// Compute statistics

	/**
	 * based on "performStatAnalysis"'s value, computes
	 * all statistics for the dataset.
	 */
	public void computeSectionStatistics(Section section) {
		LogAndDebug.tracep("(<Section>" + section.getTitle() +"))");
		for(Dataset dataset : section.getDatasets()){
			computeDatasetStatistics(dataset);
		}
		LogAndDebug.tracem();
	}


//	public void computeDatasetStatistics(Dataset dataset) {
//		LogAndDebug.tracep("(" + LogAndDebug.toText(dataset) + ", " + dataset.getName() +"))");
//		List<BladeEvent> allEventsList = null;
//		List<BladeEvent> timeFilteredEventsList = null;
//		List<BladeEvent> eventFilteredEventsList = null;
//		List<BladeEvent> statsFilteredEventsList = null;
//
//		long start;
//		long end;
//
//		LogAndDebug.trace("() *** dataset= "+LogAndDebug.toText(dataset));
//		LogAndDebug.trace("() *** dataset.isPerformStatAnalysis()= "+dataset.isPerformStatAnalysis());
//		if (dataset.getDatasources().size() > 0){
//			LogAndDebug.trace("() *** dataset.getDatasources().get(0).getField()= "+dataset.getDatasources().get(0).getField());
//		}else{
//			LogAndDebug.trace("()  no datasources");
//			return;
//		}
//
//		String field = dataset.isPerformStatAnalysis() ? dataset.getDatasources().get(0).getField() : null;
//
//		FieldsValuesFilter filter = dataset.getFieldsValuesFilter();
//
//		Statistics stats;
//		Datasource datasource = null;
//		switch (dataset.getDatasetType()){
//		case SIMPLE_DATASET:
//			datasource = dataset.getDatasources().get(0);
//			long  st= dataset.getFilterStartTime();
//			System.out.println("long st  = " +st) ; 
//	//		st = datasource.;
//			 
//			start = (int) dataset.getFilterStartTime();
//			end   = (int) dataset.getFilterEndTime();
//			
//			String testName = datasource.getTestName();
//			String bladeId= datasource.getBladeId();
//			
//			start=srHlp.getMinTimeFor(testName, bladeId);
//			end =srHlp.getMaxTimeFor(testName, bladeId);
//			
//			allEventsList = getBladeOneEventValuesList(datasource);						// get all events // OK
//			int originalEventsNumber = allEventsList.size();
//			timeFilteredEventsList   = filterEventsByTime(allEventsList, start, end);
//			eventFilteredEventsList  = filterEventsByFieldsValues(datasource, timeFilteredEventsList, filter);
//			statsFilteredEventsList  = filterEventsByStatistics(eventFilteredEventsList, kCleaning, percentCleaning);
//
//
//			stats = new Statistics(statsFilteredEventsList, field, originalEventsNumber);
//			stats.setDatasource(datasource);
//			stats.setDataset(dataset);
//			Section sec = getSection(dataset);
//			sec = dataset.getSection();
//			stats.setSection(sec);
//			dataset.setStatistics(stats);
//			// update statistics
//			stats.setTimeFilteredEventsNumber(timeFilteredEventsList.size());
//			stats.setFieldsValueFilteredEventsNumber(eventFilteredEventsList.size());
//			stats.setStatsFilteredEventsNumber(statsFilteredEventsList.size());
//			stats.setFinalEventsNumber(stats.getStatsFilteredEventsNumber());
//			break;
//
//		case AGGREGATE_DATASET:
//			originalEventsNumber = 0;
//			start = Integer.MIN_VALUE;
//			end = Integer.MAX_VALUE;
//			List<BladeEvent> mergedstatsFiltered = new ArrayList<BladeEvent>();
//
//			for(Datasource datasourceA : dataset.getDatasources()){
//		//		start = Math.max(start, (int) dataset.getFilterStartTime());
//		//		end   = Math.min(end, (int) dataset.getFilterEndTime());
//
//				String testNameAggregate = datasource.getTestName();
//				String bladeIdAggregate= datasource.getBladeId();
//				int startNew=(int) srHlp.getMinTimeFor(testNameAggregate, bladeIdAggregate);
//				int endNew =(int) srHlp.getMaxTimeFor(testNameAggregate, bladeIdAggregate);
//				
//				if (startNew<start){
//					start=startNew;
//				}
//				
//				if (endNew>end){
//					end=endNew;
//				}	
//				
//				
//				mergedstatsFiltered.addAll(getBladeOneEventValuesList(datasourceA));						// get all events
//			}
//
//			timeFilteredEventsList  = filterEventsByTime(mergedstatsFiltered, start, end);
//			eventFilteredEventsList = filterEventsByFieldsValues(timeFilteredEventsList, filter);
//			statsFilteredEventsList = filterEventsByStatistics(eventFilteredEventsList, kCleaning, percentCleaning);
//
//			originalEventsNumber = mergedstatsFiltered.size();
//			stats = new Statistics(statsFilteredEventsList, field, originalEventsNumber);
//			stats.setDatasource(null);
//			stats.setDataset(dataset);
//			stats.setSection(getSection(dataset));
//			dataset.setStatistics(stats);
//
//			// update statistics
//			stats.setOriginalEventsNumber(mergedstatsFiltered.size());
//			stats.setTimeFilteredEventsNumber(timeFilteredEventsList.size());
//			stats.setFieldsValueFilteredEventsNumber(eventFilteredEventsList.size());
//			stats.setStatsFilteredEventsNumber(statsFilteredEventsList.size());
//			stats.setFinalEventsNumber(stats.getStatsFilteredEventsNumber());
//
//			break;
//
//		case MULTIPLE_DATASET:
//			
//			for(Datasource datasourceM : dataset.getDatasources()){
//				datasourceM.setDataset(dataset);
////				start = (int) dataset.getFilterStartTime();
////				end   = (int) dataset.getFilterEndTime();
//				
//				start = Integer.MIN_VALUE;
//				end = Integer.MAX_VALUE;
//				
//				String testNameAggregate = datasourceM.getTestName();
//				String bladeIdAggregate= datasourceM.getBladeId();
//				int startNew=(int) srHlp.getMinTimeFor(testNameAggregate, bladeIdAggregate);
//				int endNew =(int) srHlp.getMaxTimeFor(testNameAggregate, bladeIdAggregate);
//				System.out.println("startNew"+ startNew);
//				
//				System.out.println("endNew"+ endNew);
//				
//				if (startNew<start){
//					start=startNew;
//				}
//				
//				if (endNew>end){
//					end=endNew;
//				}	
//				
//				computeDatasourceStatistics(datasourceM, start, end);
//			}
//			break;
//		default:
//			break;
//		}
//		LogAndDebug.tracem("(" + LogAndDebug.toText(dataset) + ", " + dataset.getName() +"))");
//	}
	
	

	public void computeDatasetStatistics(Dataset dataset) {
		LogAndDebug.tracep("(" + LogAndDebug.toText(dataset) + ", " + dataset.getName() +"))");
		List<BladeEvent> allEventsList = null;
		List<BladeEvent> timeFilteredEventsList = null;
		List<BladeEvent> eventFilteredEventsList = null;
		List<BladeEvent> statsFilteredEventsList = null;

		long start;
		long end;

		LogAndDebug.trace("() *** dataset= "+LogAndDebug.toText(dataset));
		LogAndDebug.trace("() *** dataset.isPerformStatAnalysis()= "+dataset.isPerformStatAnalysis());
		if (dataset.getDatasources().size() > 0){
			LogAndDebug.trace("() *** dataset.getDatasources().get(0).getField()= "+dataset.getDatasources().get(0).getField());
		}else{
			LogAndDebug.trace("()  no datasources");
			return;
		}

		String field = dataset.isPerformStatAnalysis() ? dataset.getDatasources().get(0).getField() : null;

		FieldsValuesFilter filter = dataset.getFieldsValuesFilter();

		Statistics stats;
		Datasource datasource = null;
		switch (dataset.getDatasetType()){
		case SIMPLE_DATASET:
			datasource = dataset.getDatasources().get(0);
			start = (int) dataset.getFilterStartTime();
			end   = (int) dataset.getFilterEndTime();
			allEventsList = getBladeOneEventValuesList(datasource);
			int originalEventsNumber = allEventsList.size();
			timeFilteredEventsList   = sortAndFilterEventsByTime(allEventsList, start, end);
			eventFilteredEventsList  = filterEventsByFieldsValues(timeFilteredEventsList, filter);
			statsFilteredEventsList  = filterEventsByStatistics(eventFilteredEventsList, kCleaning, percentCleaning);
			stats = new Statistics(statsFilteredEventsList, field, originalEventsNumber);
			stats.setDatasource(datasource);
			stats.setDataset(dataset);
			Section sec = getSection(dataset);
			sec = dataset.getSection();
			stats.setSection(sec);
			dataset.setStatistics(stats);
			// update statistics
			stats.setTimeFilteredEventsNumber(timeFilteredEventsList.size());
			stats.setFieldsValueFilteredEventsNumber(eventFilteredEventsList.size());
			stats.setStatsFilteredEventsNumber(statsFilteredEventsList.size());
			stats.setFinalEventsNumber(stats.getStatsFilteredEventsNumber());
			break;
		case AGGREGATE_DATASET:
			originalEventsNumber = 0;
			start = Integer.MIN_VALUE;
			end = Integer.MAX_VALUE;
			List<BladeEvent> mergedstatsFiltered = new ArrayList<BladeEvent>();
			for(Datasource datasourceA : dataset.getDatasources()){
				start = Math.min(start, dataset.getFilterStartTime());
				end   = Math.max(end, dataset.getFilterEndTime());
				mergedstatsFiltered.addAll(getBladeOneEventValuesList(datasourceA));
			}
			timeFilteredEventsList  = sortAndFilterEventsByTime(mergedstatsFiltered, start, end);
			eventFilteredEventsList = filterEventsByFieldsValues(timeFilteredEventsList, filter);
			statsFilteredEventsList = filterEventsByStatistics(eventFilteredEventsList, kCleaning, percentCleaning);
			originalEventsNumber = mergedstatsFiltered.size();
			stats = new Statistics(statsFilteredEventsList, field, originalEventsNumber);
			stats.setDatasource(null);
			stats.setDataset(dataset);
			stats.setSection(getSection(dataset));
			dataset.setStatistics(stats);

			// update statistics
			stats.setOriginalEventsNumber(mergedstatsFiltered.size());
			stats.setTimeFilteredEventsNumber(timeFilteredEventsList.size());
			stats.setFieldsValueFilteredEventsNumber(eventFilteredEventsList.size());
			stats.setStatsFilteredEventsNumber(statsFilteredEventsList.size());
			stats.setFinalEventsNumber(stats.getStatsFilteredEventsNumber());
			break;
		case MULTIPLE_DATASET:
			for(Datasource datasourceM : dataset.getDatasources()){
				datasourceM.setDataset(dataset);
				start = (int) dataset.getFilterStartTime();
				end   = (int) dataset.getFilterEndTime();
				computeDatasourceStatistics(datasourceM, start, end);
			}
			break;
		default:
			break;
		}
		LogAndDebug.tracem("(" + LogAndDebug.toText(dataset) + ", " + dataset.getName() +"))");
	}


	// compute statistics when field has been set
	public void computeDatasourceStatistics(Datasource datasource, long start, long end) {
		String field = datasource.getField();
		String name = datasource.getName();
		LogAndDebug.trace("(<Datasource>" + name +", "+ start +", "+end+")");

		Dataset dataset = datasource.getDataset();
		if (null == dataset){
			LOOP1:
				for(Section sec : report.getSections()){
					for (Dataset ds : sec.getDatasets()){
						for(Datasource source : ds.getDatasources()){
							if (datasource == source){
								datasource.setDataset(ds);
								dataset = datasource.getDataset();
								System.out.println("dataAcess.report.dataset.datasource "+ source.toString());
								break LOOP1;
							}
						}
					}
				}
		}
		FieldsValuesFilter filter = dataset.getFieldsValuesFilter();

		List<BladeEvent> allEventsList = getBladeOneEventValuesList(datasource);
		int totalEventsNumber = allEventsList.size();
		List<BladeEvent> timeFilteredEventsList = sortAndFilterEventsByTime(allEventsList, start, end);
		List<BladeEvent> eventFilteredEventsList = filterEventsByFieldsValues(timeFilteredEventsList, filter);
		List<BladeEvent> statsFilteredEventsList = filterEventsByStatistics(eventFilteredEventsList, kCleaning, percentCleaning);
		Statistics stats = new Statistics(statsFilteredEventsList, field, totalEventsNumber);
		stats.setDatasource(datasource);
		stats.setDataset(dataset);
		stats.setSection(getSection(dataset));
		datasource.setStatistics(stats);

		// update statistics
		stats.setOriginalEventsNumber(allEventsList.size());
		stats.setTimeFilteredEventsNumber(timeFilteredEventsList.size());
		stats.setFieldsValueFilteredEventsNumber(eventFilteredEventsList.size());
		stats.setStatsFilteredEventsNumber(statsFilteredEventsList.size());
		stats.setFinalEventsNumber(stats.getStatsFilteredEventsNumber());
	}


	// filters

	public List<BladeEvent> sortAndFilterEventsByTime(
		List<BladeEvent> eventsList,
		long start,
		long end)
	{
		Collections.sort(eventsList);
		int startIndex = 0;
		while (startIndex < eventsList.size() && eventsList.get(startIndex).getDate() < start)
		{
			startIndex++;
		}
		int endIndex = eventsList.size() - 1;
		while (endIndex >= 0 && eventsList.get(endIndex).getDate() > end)
		{
			endIndex--;
		}
		if (endIndex > startIndex)
		{
			return eventsList.subList(startIndex, endIndex + 1);
		}
		else
		{
			return new ArrayList<BladeEvent>(0);
		}
	}


	public List<BladeEvent> filterEventsByFieldsValues(
		List<BladeEvent> eventsList,
		FieldsValuesFilter filter)
	{
		List<BladeEvent> result = eventsList;
		if (filter.getFilterList().size() > 0)
		{
			switch(filter.getLogicalOp())
			{
				case AND:
					for (FieldFilterAndOp expression : filter.getFilterList())
					{
						result = FieldsValuesFilter.fieldFilter(
							result,
							expression.getField(),
							expression.getValue(),
							expression.getOperator());
					}
					break;
				case OR:
					result = new ArrayList<BladeEvent>(eventsList.size());
					for (FieldFilterAndOp expression : filter.getFilterList())
					{
						for (BladeEvent event :
							FieldsValuesFilter.fieldFilter(
								eventsList,
								expression.getField(),
								expression.getValue(),
								expression.getOperator()))
						{
							if (! result.contains(event))
							{
								result.add(event);
							}
						}
					}
					Collections.sort(result);
					break;
				default:
					throw new Error("Unknown logical operator " + filter.getLogicalOp());
			}
		}
		return result;
	}


	public List<BladeEvent> filterEventsByStatistics(
			List<BladeEvent> eventsList, Object kCleaning, Object percentCleaning2) {
//		LogAndDebug.unimplemented();
		return eventsList;		// TODO : implement this filter
	}


	// List<BladeEvent> methods

	public List<BladeEvent> aggregateBladesOneSameEvent(
			List<List<BladeEvent>> bladeListList)
			{
		Comparator<BladeEvent> SORT_BY_TIME = new DateComp();
		TreeSet<BladeEvent> bladeTreeSet = new TreeSet<BladeEvent>(SORT_BY_TIME);

		for(List<BladeEvent> bladeEventList : bladeListList){
			for(BladeEvent bladeEvent : bladeEventList){
				bladeTreeSet.add(bladeEvent);
			}

		}
		List<BladeEvent> bladeEventListResult = new ArrayList<BladeEvent>();
		for(BladeEvent bladeEvent : bladeTreeSet){
			bladeEventListResult.add(bladeEvent);
		}
		return bladeEventListResult;
			}


//	public int[] getTimeWindow(Section sec){
//		return sec.getChart().getTimeWindow();
//	}

//	public int[] getStep(Section sec){
//		return sec.getChart().getStep();
//	}

//	public int[] getMaxPointsToDisplay(Section sec){
//		return sec.getChart().getMaxPointsToDisplay();
//	}

	public long getTimeStart(Section sec){
		return sec.getChart().getStartTime();
	}

	public long getTimeEnd(Section sec)
	{
		return sec.getChart().getEndTime();
	}


	/////////////////////////////////
	// Class DateComp //
	/////////////////////////////////
	public class DateComp implements Comparator<BladeEvent> {
		//sort by time
		public int compare(BladeEvent bladeEvent1, BladeEvent bladeEvent2){
			if(bladeEvent1.getDate()<bladeEvent2.getDate()){
				return -1;
			} else if (bladeEvent1.getDate()>bladeEvent2.getDate()){
				return 1;
			} else{
				return 0;
			}
		}
	}


	/////////////////////////////////
	// Charts //
	/////////////////////////////////
	public JFreeChart createJFreeChart(Section sec) {
		JFreeChart jfChart = sec.getChart().getJfChart();
		LogAndDebug.trace("(" + sec.getTitle() + ") -> " + jfChart);
		return jfChart;
	}


	//  special getters
	public Section getSection(Dataset current) {
		Section ret = null;
		SEC_LOOP:
			for(Section sec : report.getSections()){
				for(Dataset dataset : sec.getDatasets()){
					if (current == dataset){
						ret = sec;
						current.setSection(sec);
						break SEC_LOOP;
					}
				}
			}
		return ret;
	}


	@SuppressWarnings("unused")
	private Dataset getDatasetFromDatasource(Datasource currentDatasource) {
		Dataset ret = null;
		sectionLoop:
			for(Section sec : report.getSections()){
				for(Dataset dataset : sec.getDatasets()){
					for (Datasource datasource :  dataset.getDatasources()){
						if (currentDatasource == datasource){
							ret = dataset;
							break sectionLoop;
						}
					}
				}
			}
		return ret;
	}


	// Integer parsing
	static int integerParse(String text, int oldValue){
		int ret = oldValue;
		try{
			ret = Integer.parseInt(text);
		}catch (NumberFormatException e) {
			LogAndDebug.trace("("+text+") *** String \"" + text +"\" not convertible to int.");
		}
		return ret;
	}


	// Long parsing
	static long longParse(String text, long oldValue){
		long ret = oldValue;
		try{
			ret = Long.parseLong(text);
		}catch (NumberFormatException e) {
			LogAndDebug.trace("("+text+")  *** String \"" + text +"\" not convertible to long.");
		}
		return ret;
	}

	// Double parsing
	static double doubleParse(String text, double oldValue){
		double ret = oldValue;
		try{
			ret = Long.parseLong(text);
		}catch (NumberFormatException e) {
			LogAndDebug.trace("("+text+")  *** String \"" + text +"\" not convertible tou double.");
		}
		return ret;
	}
}
