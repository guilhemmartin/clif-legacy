/*
* CLIF is a Load Injection Framework
* Copyright (C) 2011 France Telecom R&D
*
* This library is free software; you can redistribute it and/or
* modify it under the terms of the GNU Lesser General Public
* License as published by the Free Software Foundation; either
* version 2 of the License, or (at your option) any later version.
*
* This library is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
* Lesser General Public License for more details.
*
* You should have received a copy of the GNU Lesser General Public
* License along with this library; if not, write to the Free Software
* Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*
* Contact: clif@ow2.org
*/

package org.ow2.clif.analyze.lib.graph.gui;

import java.awt.Component;
import java.util.ArrayList;
import java.util.List;
import java.util.Vector;
import javax.swing.JFrame;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.SwingConstants;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.DefaultTableModel;
import org.ow2.clif.analyze.lib.graph.gui.DataAccess.W1SelectedDataset;
import org.ow2.clif.analyze.lib.report.Datasource;
import org.ow2.clif.analyze.lib.report.LogAndDebug;
import org.ow2.clif.storage.api.BladeDescriptor;
import org.ow2.clif.storage.lib.util.NameBladeFilter;
import org.ow2.clif.supervisor.api.ClifException;


@SuppressWarnings("serial")
public class Wizard1TablePanel extends JScrollPane {
	Wizard1MainView mainView;
	DataAccess data; 
	JFrame frame;
//	Dataset currentDs ;
	W1SelectedDataset availableDs ;
	@SuppressWarnings("unused")
	private List<String> bladeIdList;
	private JTable jTable1;

	
	private DefaultTableModel model = new javax.swing.table.DefaultTableModel(
			new Object[][] {},
			new String[] {
					"Blade", "Type", "Class", "Server Name", "Arguments", "Comment" 
				}
			) {
		boolean[] canEdit = new boolean[] { false, false, false, false, false, false };

		@Override
		public boolean isCellEditable(int rowIndex, int columnIndex) {
			return canEdit[columnIndex];
		}
	};
	

	// Constructor

	public Wizard1TablePanel(Wizard1MainView _mainView) {
		super();
		mainView = _mainView;
		data = mainView.getDataAccess();
//		data.tracep("(mainView)");
		frame = mainView.frame;
//		currentDs = data.w1Dataset;
		jTable1 = new JTable(model);
		jTable1.setRowHeight(25);
		setViewportView(jTable1);
		jTable1.setRowSelectionAllowed(false);
		
//		data.tracem("(mainView)");
	}


	void tableAdd(String testName, String bladeName) {
		Vector<String> addSel = new Vector<String>();
		bladeIdList = new ArrayList<String>();
		NameBladeFilter filter = new NameBladeFilter(bladeName);

		BladeDescriptor[] bladeDescriptorArray = null;
		try {
			bladeDescriptorArray = data.getBladeDescriptor(testName, filter);
		} catch (ClifException e) {
			LogAndDebug.log("*** Exception on Step1/tableUpdate");
			LogAndDebug.log("   Cannot get test plan for " + testName + ", " + filter);
			LogAndDebug.log("   "+ e);
		}

		String bladeClass = bladeDescriptorArray[0].getClassname();
		String bladeServerName = bladeDescriptorArray[0].getServerName();
		String bladeArgument = bladeDescriptorArray[0].getArgument();
		String bladeComment = bladeDescriptorArray[0].getComment();
		addSel.add(bladeName);
		// TO ADD THE TYPE
		addSel.add("");
		addSel.add(bladeClass);
		addSel.add(bladeServerName);
		addSel.add(bladeArgument);
		addSel.add(bladeComment);

		model.insertRow(model.getRowCount(), addSel);
		jTable1.getColumnModel().getColumn(1).setCellRenderer(new MyTableRenderer());

		mainView.jbNext.setEnabled(true);
		return;
	}


	void tableClean() {
		int rowMax = model.getRowCount();
		for (int i = 0; i < rowMax; i++){
			model.removeRow(0);
		}
	}
	
	void tableUpdate(){
		tableClean();
		if (null == data.wizardDataset){
			return;
		}
		for(Datasource dd: data.wizardDataset.getDatasources()){
			tableAdd(dd.getTestName(), dd.getBladeId());
		}
	}
	
	
	public class MyTableRenderer extends DefaultTableCellRenderer
	{

		@Override
		public Component getTableCellRendererComponent(JTable table,
				Object value, boolean isSelected, boolean hasFocus, int row,
				int column){
			super.getTableCellRendererComponent(table, value, isSelected,
					hasFocus, row, column);
			// condition to change the leaf
			if (column == 1){
				if (table.getValueAt(row, 2).toString().endsWith("IsacRunner")){
					setHorizontalAlignment(SwingConstants.CENTER);
					setIcon(data.INJECTOR_ICON);
				} else {
					setHorizontalAlignment(SwingConstants.CENTER);
					setIcon(data.PROBE_ICON);
				}
			}
			return this;
		}
	}
}