/*
* CLIF is a Load Injection Framework
* Copyright (C) 2017 OrangeSA
*
* This library is free software; you can redistribute it and/or
* modify it under the terms of the GNU Lesser General Public
* License as published by the Free Software Foundation; either
* version 2 of the License, or (at your option) any later version.
*
* This library is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
* Lesser General Public License for more details.
*
* You should have received a copy of the GNU Lesser General Public
* License along with this library; if not, write to the Free Software
* Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*
* Contact: clif@ow2.org
*/

package org.ow2.clif.util.gui;

import java.awt.Component;
import java.awt.Container;

/**
 * Utility methods for Java swing GUI components
 */
public abstract class SwingHelper
{
	/**
	 * Enables or disables the given GUI component, as well as
	 * its sub-components recursively when it is a container.
	 * @param comp the target component to recursively enable
	 * or disable
	 * @param enable true to enable the component(s), false to
	 * disable the component(s)
	 */
	static public void setEnabled(Component comp, boolean enable)
	{
		if (comp instanceof Container)
		{
			for (Component subComp : ((Container)comp).getComponents())
			{
				if (comp instanceof Container)
				{
					setEnabled(subComp, enable);
				}
			}
		}
		comp.setEnabled(enable);
	}
}
