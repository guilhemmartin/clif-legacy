/*
* CLIF is a Load Injection Framework
* Copyright (C) 2003, 2004 France Telecom R&D
* Copyright (C) 2003 INRIA
* Copyright (C) 2016 Orange SA 
*
* This library is free software; you can redistribute it and/or
* modify it under the terms of the GNU Lesser General Public
* License as published by the Free Software Foundation; either
* version 2 of the License, or (at your option) any later version.
*
* This library is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
* Lesser General Public License for more details.
*
* You should have received a copy of the GNU Lesser General Public
* License along with this library; if not, write to the Free Software
* Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*
* Contact: clif@ow2.org
*/

package org.ow2.clif.console.lib.gui;

import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.FontMetrics;
import java.awt.geom.Line2D;
import java.awt.geom.Point2D;
import java.util.Vector;
import javax.swing.JPanel;

/**
 *
 * @author Julien Buret
 * @author Nicolas Droze
 * @author Bruno Dillenseger
 */
public class Graph extends JPanel
{
	private static final long serialVersionUID = -1744496681943967974L;
	// The area in which the curves are going to be displayed
	public GraphArea graphArea;
	// The X Axis
	public XAxis xAxis;
	// The Y Axis
	public YAxis yAxis;
	// Contains the list and points of all the hosts to display
	public Vector<InjectorsGraph[]> hostsToDisplay = new Vector<InjectorsGraph[]>();
	// Contains the list and points of all the hosts
	public InjectorsGraph[] allHosts;
	// Contains the list and points of one host
	private InjectorsGraph[] oneHost;
	// The number of data for each host
	public int nbElements;
	// The current view on the graph area
	public int VIEW = 0;
	// A limited graph means that we do not store all points for a host
	// and that we display only a given number of points on the graph.
	public boolean isLimited;
	// The object that will determine colors for the graphs
	private GraphColorChooser colorChooser = new GraphColorChooser();
	private Point2D.Double newPoint = null;

	/**
	 * The Graph constructor.
	 * @param injectors All the hosts to represent on this graph.
	 * @param nbElements The number of data for each host.
	 * @param isLimited True means that only a certain number of values will be stored in memory.
	 */
	public Graph(String[] injectors, int nbElements, boolean isLimited) {

		this.nbElements = nbElements;
		this.isLimited = isLimited;

		/* Initialize the content of the table allInjectors:
		 * For each injector, set its name,
		 * set its color (next color available)
		 * and a starting point (0,0) for each graph.
		 */
		allHosts = new InjectorsGraph[injectors.length];
		for (int i = 0; i < injectors.length; i++) {
			allHosts[i] = new InjectorsGraph(nbElements, isLimited);
			allHosts[i].name = injectors[i];
			allHosts[i].color = colorChooser.getNextColor();
//			for (int j = 0; j < nbElements; j++) {
//				allHosts[i].addPoint(new Point2D.Double(0, 0), j);
//			}
		}

		setLayout(null);

		graphArea = new GraphArea(isLimited);
		xAxis = new XAxis(graphArea);
		yAxis = new YAxis(graphArea);

		// Set the position and location of the graph panels.
		yAxis.setSize(YAxis.width, 300);
		xAxis.setSize(100, 20);
		yAxis.setLocation(0, 0);
		xAxis.setLocation(YAxis.width, 280);
		graphArea.setLocation(YAxis.width, 0);

		add(yAxis);
		add(xAxis);
		add(graphArea);
		doLayout();
	}

	/**
	 * Set the current view of the graph area. This method is called each time
	 * the user choose another item in the JComboBox.
	 * @param view The number of the view (The index of the value from the JComboBox).
	 */
	public void setView(int view) {
		this.VIEW = view;
		graphArea.resetScale();
	}


	/**
	 * Add a specific point to a specific host
	 * @param injector The name of the host the point belongs to.
	 * @param type The type of the data (cf possible values in the graph inherited)
	 * @param time The time value (x axis)
	 * @param value The value (y axis)
	 */
	public void addPoint(String injector, int type, int time, long value) {

		newPoint = new Point2D.Double(time, value);

		// Add the new point to the specific host
		for (int i = 0; i < allHosts.length; i++) {
			if (allHosts[i].name.equals(injector)) {
				allHosts[i].addPoint(newPoint, type);
				break;
			}
		}
	}

	/**
	 * Sets the maximum value on the X Axis.
	 * @param totalTime
	 */
	public void updateXAxis(int totalTime) {
		graphArea.xMax = totalTime;
	}

	/**
	 * This method re-calculate a correct scale for the graph
	 * and repaint all the graph components
	 */
	public void updateGraph()
	{
		graphArea.calculateScale(hostsToDisplay, allHosts);
		yAxis.repaint();
		xAxis.repaint();
		graphArea.repaint();
	}

	/**
	 * Add the points of a specific host into the table of the hosts to display
	 * @param injector The name of the host to display in the graph area.
	 */
	public void addPointsOnDisplay(String injector) {

		// Find the points data of the host
		for (int i = 0; i < allHosts.length; i++) {
			if (allHosts[i].name.equals(injector)) {
				// Get the points to visualize from this host
				oneHost = new InjectorsGraph[1];
				oneHost[0] = new InjectorsGraph(1, isLimited);
				oneHost[0].name = injector;
				oneHost[0].color = allHosts[i].color;
				oneHost[0].points.set(i, allHosts[i].points.get(VIEW));
				if (isLimited)
					oneHost[0].maxElements = allHosts[i].getMaxElements();
				hostsToDisplay.addElement(oneHost);
				break;
			}
		}
	}

	/**
	 * Add the points of all the hosts into the table of the hosts to display
	 * @param injectors The list of the hosts to display in the graph area.
	 */
	public void addAllPointsOnDisplay(Object[] injectors) {

		for (int i = 0; i < injectors.length; i++) {
			addPointsOnDisplay((String) injectors[i]);
		}
	}

	/**
	 * Remove the points of a specific host from the table of the hosts to display
	 * @param injector The name of the host to remove from the graph area.
	 */
	public void removePointsFromDisplay(String injector) {

		// Find the injector name in the display list and simply remove it.
		for (int i = 0; i < hostsToDisplay.size(); i++) {
			oneHost = hostsToDisplay.elementAt(i);
			if (oneHost[0].name.equals(injector)) {
				hostsToDisplay.removeElementAt(i);
				break;
			}
		}
	}

	/**
	 * Remove the points of all the hosts from the table of the hosts to display
	 * @param injectors The list of the hosts to remove from the graph area.
	 */
	public void removeAllPointsFromDisplay(Object[] injectors) {

		for (int i = 0; i < injectors.length; i++) {
			removePointsFromDisplay((String) injectors[i]);
		}
	}

	@Override
	public void paintComponent(Graphics g) {
		yAxis.setSize(YAxis.width, getHeight());
		xAxis.setSize(getWidth(), 20);
		xAxis.setLocation(YAxis.width, getHeight() - 20);
		graphArea.setSize(getWidth() - YAxis.width, getHeight() - 20);
		updateGraph();
	}

	/**
	 * This method adds a new host to the list of all the existing host of this graph.
	 * @param name The name of the host.
	 */
	public void addInjector(String name) {
		InjectorsGraph[] _allInjectors = allHosts;
		int _size = allHosts.length;

		// Transfer the old values into the new variables
		allHosts = new InjectorsGraph[_size + 1];
		for (int i = 0; i < _allInjectors.length; i++)
		{
			allHosts[i] = new InjectorsGraph(nbElements, isLimited);
			allHosts[i].name = _allInjectors[i].name;
			allHosts[i].color = _allInjectors[i].color;
		}
		// The last index contains the new injector
		allHosts[_size] = new InjectorsGraph(nbElements, isLimited);
		allHosts[_size].name = name;
		allHosts[_size].color = colorChooser.getNextColor();
	}

	/**
	 * Clear the graph area (Clear all points)
	 */
	public void clear() {
		for (int i = 0; i < allHosts.length; i++) {
			allHosts[i].clearAllPoints();
		}
		updateXAxis(0);
		updateGraph();
	}

	/**
	 * Sets the maximum number of points to store.
	 * (Useful only with limited graphs)
	 * @param nbPoints The maximum number of points to store.
	 */
	public void setNbPoints(int nbPoints) {
		for (int i = 0; i < allHosts.length; i++) {
			allHosts[i].setMaxElements(nbPoints);
		}
		updateGraph();
	}


	public int getGraphAreaWidth()
	{
		return graphArea.getWidth();
	}
}

/**
 * This is the JPanel which represents the X axis
 */
class XAxis extends JPanel
{
	private static final long serialVersionUID = 5001599521102469570L;
	private GraphArea graphArea;
	private FontMetrics fontMet;

	public XAxis(GraphArea graph)
	{
		this.graphArea = graph;
		fontMet = getFontMetrics(getFont());
	}

	@Override
	public void paint(Graphics g)
	{
		g.clearRect(0, 0, getWidth(), getHeight());
		g.drawLine(0, 1, getWidth(), 1);
		String time = String.valueOf(graphArea.xMax);
		g.drawString(
			time,
			getWidth() - fontMet.stringWidth(time) - YAxis.width,
			fontMet.getHeight());
	}
}

/**
 * This is the JPanel which represents the Y axis
 */
class YAxis extends JPanel
{
	private static final long serialVersionUID = -5608492010497693975L;
	static final int width = 90;
	private GraphArea graphArea;
	private FontMetrics fontMet;

	public YAxis(GraphArea graphArea)
	{
		this.graphArea = graphArea;
		fontMet = getFontMetrics(getFont());
	}

	@Override
	public void paint(Graphics g)
	{
		g.clearRect(0, 0, getWidth(), getHeight());
		g.drawLine(YAxis.width - 1, 0, YAxis.width - 1, getHeight() - 20);
		if (graphArea.maxValue > graphArea.minValue)
		{
			long middleValue = (graphArea.maxValue + graphArea.minValue) / 2;
			// Display the max value
			g.drawString(
				String.valueOf(graphArea.maxValue),
				1,
				fontMet.getAscent());
			// Display the middle value
			g.drawString(
				String.valueOf(middleValue),
				1,
				(graphArea.getHeight() - fontMet.getLeading() )/ 2);
			// Display the first quart value
			long quartDiff = (middleValue - graphArea.minValue) / 2;
			g.drawString(
				String.valueOf(graphArea.minValue + quartDiff),
				1,
				(3*graphArea.getHeight()) / 4);
			// Display the third quart value
			g.drawString(
				String.valueOf(graphArea.maxValue - quartDiff),
				1,
				graphArea.getHeight() / 4);
			// Display the minimum value
			g.drawString(
				String.valueOf(graphArea.minValue),
				1,
				graphArea.getHeight());
		}
	}
}

/**
 * This is the JPanel containing the graph
 */
class GraphArea extends JPanel {
	private static final long serialVersionUID = -2228672793416637975L;
	private BasicStroke defaultStroke = new BasicStroke();
	private BasicStroke dash;
	private Point2D.Double first = null;
	private Point2D.Double last = null;
	private Point2D.Double firstTransf = null;
	private Point2D.Double lastTransf = null;
	// The maximum value for each axis
	protected long yMax = 0;
	protected long yMin = 0;
	public int xMax = 0;
	// The scale of each axis, used for the drawing
	public double xScale;
	public double yScale;
	public boolean isLimited;
	private Vector<InjectorsGraph[]> injectorsToDisplay = new Vector<InjectorsGraph[]>();
	private InjectorsGraph[] oneInjector;
	protected long maxValue;
	protected long minValue;

	public GraphArea(boolean isLimited) {
		this.isLimited = isLimited;
		xScale = 0;
		yScale = 0;
		dash =
			new BasicStroke(
				defaultStroke.getLineWidth(),
				defaultStroke.getEndCap(),
				defaultStroke.getLineJoin(),
				defaultStroke.getMiterLimit(),
				new float[] { 8, 8 },
				0);
		oneInjector = new InjectorsGraph[1];
		oneInjector[0] = new InjectorsGraph(1, isLimited);
	}

	@Override
	public void paint(Graphics g)
	{
		Graphics2D g2 = (Graphics2D) g;
		g2.setStroke(defaultStroke);
		g2.setBackground(Color.WHITE);

		// Clear the graph area
		g2.clearRect(0, 0, getWidth(), getHeight());

		if (yMax > yMin)
		{
			// For each injector to display
			for (int i = 0; i < injectorsToDisplay.size(); i++) {
				oneInjector = injectorsToDisplay.elementAt(i);
				g2.setColor(oneInjector[0].color);

				/*
				 * We get all the points of this injector ...
				 * then we translate the coordinates into pixels format
				 * and finally draw a line between these 2 new points.
				 */
				for (int j = 0; j < oneInjector[0].points.get(0).size() - 1; j++) {
					first = oneInjector[0].points.get(0).elementAt(j);
					last =
						(Point2D.Double) oneInjector[0].points.get(0).elementAt(j + 1);

					firstTransf = (Point2D.Double) first.clone();
					lastTransf = (Point2D.Double) last.clone();

					// If the graph is limited, the calculation of the scale is a little
					// different than if the graph is not limited.
					if (!isLimited) {
						firstTransf.setLocation(
							first.getX() * xScale,
							getHeight() - ((first.getY() - yMin) * yScale));
						lastTransf.setLocation(
							last.getX() * xScale,
							getHeight() - ((last.getY() - yMin) * yScale));
					} else {
						firstTransf.setLocation(
							j * xScale,
							getHeight() - ((first.getY() - yMin) * yScale));
						lastTransf.setLocation(
							(j + 1) * xScale,
							getHeight() - ((last.getY() - yMin) * yScale));
					}
					g2.draw(new Line2D.Double(firstTransf, lastTransf));
				}
			}
			// display a grid.
			maxValue = yMax;
			minValue = yMin;
			g2.setStroke(dash);
			g2.setColor(Color.BLACK);
			g2.drawLine(0, getHeight() / 2, getWidth(), getHeight() / 2);
			g2.drawLine(0, getHeight() / 4, getWidth(), getHeight() / 4);
			g2.drawLine(0, 3 * getHeight() / 4, getWidth(), 3 * getHeight() / 4);
		}
	}


	/**
	 * Called when the view has changed and the scale on the Y Axis has to be adjusted
	 */
	public void resetScale()
	{
		yMax = Integer.MIN_VALUE;
		yMin = Integer.MAX_VALUE;
	}


	/**
	 * Calculate a correct scale for a proper drawing.
	 * @param injectorsToDisplay
	 * @param allInjectors
	 */
	public void calculateScale(
		Vector<InjectorsGraph[]> injectorsToDisplay,
		InjectorsGraph[] allInjectors) {

		this.injectorsToDisplay = injectorsToDisplay;

		// For each injector to display
		for (int i = 0; i < injectorsToDisplay.size(); i++) {
			oneInjector = (InjectorsGraph[]) injectorsToDisplay.elementAt(i);

			// We get each point of this injector
			for (int j = 0; j < oneInjector[0].points.get(0).size(); j++)
			{
				Point2D.Double currPoint = oneInjector[0].points.get(0).elementAt(j);
				if (currPoint.getY() > yMax)
				{
					yMax = new Double(currPoint.getY()).longValue();
				}
				if (currPoint.getY() < yMin)
				{
					yMin = new Double(currPoint.getY()).longValue();
				}
				if (yMax - yMin < 4)
				{
					yMax = yMin + 4;
				}
			}
		}

		// Find a good scale on the X axis and Y axis
		if (xMax != 0) {
			if (isLimited) {
				xScale =
					new Double(getWidth()).doubleValue()
						/ (oneInjector[0].getMaxElements() - 1);
			} else {
				xScale =
					new Double(getWidth()).doubleValue()
						/ (new Double(xMax).doubleValue());
			}
		} else
			xScale = 0;

		yScale =
			new Double(getHeight()).doubleValue()
				/ new Double(yMax - yMin).doubleValue();
	}
}
