/*
* CLIF is a Load Injection Framework
* Copyright (C) 2003 France Telecom R&D
* Copyright (C) 2003 INRIA
* Copyright (C) 2016 Orange SA
*
* This library is free software; you can redistribute it and/or
* modify it under the terms of the GNU Lesser General Public
* License as published by the Free Software Foundation; either
* version 2 of the License, or (at your option) any later version.
*
* This library is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
* Lesser General Public License for more details.
*
* You should have received a copy of the GNU Lesser General Public
* License along with this library; if not, write to the Free Software
* Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*
* Contact: clif@ow2.org
*/
package org.ow2.clif.console.lib.gui;

import java.awt.Color;
import java.awt.geom.Point2D;
import java.util.ArrayList;
import java.util.Vector;

/**
 * This structure represents a host with all its associated points, name and color
 * 
 * @author Julien Buret
 * @author Nicolas Droze
 * @author Bruno Dillenseger
 */
public class InjectorsGraph {
	// The name of the host
	public String name;
	// The graph color
	public Color color;
	// A vector containing all the points for a given type of data
	public ArrayList<Vector<Point2D.Double>> points;
	// True means that the graph has a limited number of points
	private boolean isLimited;
	// The number of maximum points to store
	public int maxElements;
	// The number of graph for each host
	private int size;

	/**
	 * The constructor, which create all the vectors that will contain the points to store.
	 * @param size The number of graphs for each host
	 * @param isLimited True means that the number of points to store is limited.
	 */
	public InjectorsGraph(int size, boolean isLimited) {
		this.isLimited = isLimited;
		this.size = size;
		// The default maximum number of points to store is 5
		maxElements = 5;

		// Initialize all the graphs for this host
		points = new ArrayList<Vector<Point2D.Double>>(size);
		for (int i = 0; i < size; i++) {
			points.add(new Vector<Point2D.Double>());
		}
	}


	/**
	 * This method adds a point to a specific graph of this host.
	 * @param point The point to add.
	 * @param type The type of graph this point belongs to.
	 */
	public void addPoint(Point2D.Double point, int type)
	{
		points.get(type).addElement(point);
		if (isLimited && points.get(type).size() > maxElements)
		{
			points.get(type).removeElementAt(0);
		}
	}


	/**
	 * Remove all the points for this host.
	 */
	public void clearAllPoints() {
		// For each graph of this host, we remove all the points
		for (int i = 0; i < points.size(); i++) {
			points.get(i).removeAllElements();
		}
	}


	/**
	 * Set the maximum number of points to store for the graph of this host.
	 * (Useful only if the graph is limited)
	 * @param nbPoints The number of maximum points to store.
	 */
	public void setMaxElements(int nbPoints)
	{
		if (nbPoints < maxElements)
		{
			for (int i = 0; i < size; i++)
			{
				while (points.get(i).size() > nbPoints)
				{
					points.get(i).removeElementAt(0);
				}
			}
		}
		maxElements = nbPoints;
	}

	public int getMaxElements() {
		return maxElements;
	}

}
