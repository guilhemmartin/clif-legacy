#! /bin/bash
# Auteur : Remi Druilhe
# Version 0.1
# Script permettant la génération de données utilisateurs
# qui seront stockées dans un fichier CSV

i=0
texte=""

while [ "$i" -lt "1000" ]
do
	if [ "$i" -lt "1000" ]
	then
                texte="$i"
        fi

	if [ "$i" -lt "100" ]
	then
                texte="0$i"
	fi

	if [ "$i" -lt "10" ]
	then
		texte="00$i"
	fi

	echo "lcl_sip$texte,1234,5$texte,40$texte,rmt_sip$texte,1234,5$texte,40$texte" >> clients_template.csv

	i=$((i + 1))
done
