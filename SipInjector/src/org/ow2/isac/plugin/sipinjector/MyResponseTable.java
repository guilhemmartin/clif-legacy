/*
 * CLIF is a Load Injection Framework
 * Copyright (C) 2009 France Telecom
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 *
 * CLIF
 *
 * Contact: clif@ow2.org
 */

package org.ow2.isac.plugin.sipinjector;

import javax.sip.message.*;

/**
 * @author Rémi Druilhe
 */
public class MyResponseTable 
{
	private Response response;
	private Long time;
	
	/**
	 * Constructor
	 */
	public MyResponseTable()
	{
		response = null;
		time = null;
	}
	
	/**
	 * Clear MyResponseTable
	 */
	public void clearMyResponseTable()
	{
		response = null;
		time = null;
	}

	/**
	 * @return the response
	 */
	public Response getMyResponse()
	{
		return(response);
	}
	
	/**
	 * @return the time
	 */
	public Long getMyTime()
	{
		return(time);
	}
	
	/**
	 * @param localResponse : the response to set
	 */
	public void setMyResponse(Response localResponse)
	{
		response = localResponse;
	}
	
	/**
	 * @param localTime : the time to set
	 */
	public void setMyTime(Long localTime)
	{
		time = localTime;
	}
}
