/*
 * CLIF is a Load Injection Framework
 * Copyright (C) 2009 France Telecom
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 *
 * CLIF
 *
 * Contact: clif@ow2.org
 */

package org.ow2.isac.plugin.sipinjector;

import javax.sip.message.*;

/**
 * @author Rémi Druilhe
 */
public class MyRequestTable 
{
	private Request request;
	private Long time;
	
	/**
	 * Constructor
	 */
	public MyRequestTable()
	{
		request = null;
		time = null;
	}
	
	/**
	 * Clear MyRequestTable
	 */
	public void clearMyRequestTable()
	{
		request = null;
		time = null;
	}
	
	/**
	 * @return the request
	 */
	public Request getMyRequest()
	{
		return(request);
	}
	
	/**
	 * @return the time
	 */
	public Long getMyTime()
	{
		return(time);
	}
	
	/**
	 * @param localRequest : the request to set
	 */
	public void setMyRequest(Request localRequest)
	{
		request = localRequest;
	}
	
	/**
	 * @param localTime : the time to set
	 */
	public void setMyTime(Long localTime)
	{
		time = localTime;
	}
}
