/*
 * CLIF is a Load Injection Framework
 * Copyright (C) 2004, 2008 France Telecom R&D
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 *
 * Contact: clif@ow2.org
 */
package org.ow2.clif.maven.plugin;

import org.apache.commons.lang.StringUtils;
import org.apache.maven.plugin.MojoExecutionException;

/**
 * Invokes Clif. Clif is invoked by spawning a separate process to make it
 * possible to control startup parameters. Can also be used by specifying a .ctp
 * file as testplan.
 *
 * @author Julien Coste
 * @goal launch
 * @requiresProject false
 * @phase integration-test
 */
public class LaunchMojo
    extends AbstractDeployClifMojo
{
    /**
     * The test run id to use
     *
     * @parameter expression="${clif.testrun.id}"
     */
    protected String testrunId;


    /**
     * If <code>testrunId</code> property is empty, set it to testplan name value.<p/>
     *
     * @throws MojoExecutionException not thrown
     */
    @Override
    protected void checkParameters()
        throws MojoExecutionException
    {
        super.checkParameters();
        if ( StringUtils.isBlank( this.testrunId ) )
        {
            this.testrunId = this.testplanName;
        }
    }

    @Override
    protected void printMojoInfo()
    {
        printInfo( "Running Clif test plan", this.testplan );
        printInfo( "Testplan name", this.testplanName );
        printInfo( "Test run id", this.testrunId );
        printInfo( "Report directory", this.reportDirectory.getAbsolutePath() );
        printInfo( "Clean Report directory", this.cleanReportDirBefore );
    }

    @Override
    protected void doExecute()
        throws MojoExecutionException
    {
        cleanReportDirIfNecessary();

        JavaCommand cmd = getCommand();
        cmd.systemProperty( "clif.filestorage.dir", reportDirectory.getAbsolutePath() )
           .systemProperty( "clif.codeserver.path", testplan.getParent() )
           .arg( this.testplanName )
           .arg( testplan.getAbsolutePath() )
           .arg( this.testrunId )
           .execute();
    }


}