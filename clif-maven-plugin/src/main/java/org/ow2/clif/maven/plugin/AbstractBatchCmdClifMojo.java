/*
 * CLIF is a Load Injection Framework
 * Copyright (C) 2004, 2008 France Telecom R&D
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 *
 * Contact: clif@ow2.org
 */
package org.ow2.clif.maven.plugin;

import org.apache.commons.lang.StringUtils;
import org.apache.maven.plugin.MojoExecutionException;

/**
 * Abstract class for all related Clif mojos that need a testplan name
 *
 * @author Julien Coste
 */
public abstract class AbstractBatchCmdClifMojo
    extends AbstractClifMojo
{
    /**
     * The testplan name to use
     *
     * @parameter expression="${clif.testplan.name}"
     * @required
     */
    protected String testplanName;

    /**
     * Checks that the <code>testplanName</code> property is set.<p/>
     *
     * @throws MojoExecutionException if not set
     */
    @Override
    protected void checkParameters()
        throws MojoExecutionException
    {
        super.checkParameters();
        if ( StringUtils.isBlank( this.testplanName ) )
        {
            throw new MojoExecutionException( "testplanName property not set" );
        }
    }

    /**
     * Default execution that adds the testplanName in argument
     *
     * @throws MojoExecutionException if any error occurs
     */
    @Override
    protected void doExecute()
        throws MojoExecutionException
    {
        JavaCommand cmd = getCommand();
        cmd.arg( this.testplanName ).execute();
    }

    public final String getJavaClassname()
    {
        String c = this.getClass().getSimpleName();
        c = c.substring( 0, c.length() - 4 );
        return "org.ow2.clif.console.lib.batch." + c + "Cmd";
    }
}
