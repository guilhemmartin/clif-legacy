package org.ow2.clif.maven.plugin;

import org.apache.maven.plugin.MojoExecutionException;
import org.junit.Test;

import java.util.Arrays;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;

/**
 * @author Julien Coste
 */
public class StartStopSuspendResumeCollecJointMojoTest
    extends AbstractClifMojoTest
{

    List<AbstractBatchCmdClifMojo> mojos =
        Arrays.asList( new StartMojo(), new StopMojo(), new SuspendMojo(), new ResumeMojo(), new CollectMojo(), new JoinMojo() );

    @Test
    public void testDoExecute()
        throws Exception
    {
        for ( AbstractBatchCmdClifMojo mojo : mojos )
        {
            BufferStreamConsumer bscOut = new BufferStreamConsumer();
            BufferStreamConsumer bscErr = new BufferStreamConsumer();

            mojo.err = bscErr;
            mojo.out = bscOut;

            mojo.clifhome = getFileResource( "goodInstallation" );
            mojo.testplanName = "dummy";

            mojo.timeOut = DEFAULT_TIMEOUT;

            try
            {
                mojo.execute();
                fail();
            }
            catch ( MojoExecutionException mee )
            {
                assertEquals( "Forked JVM has been killed on time-out after 10 seconds", mee.getMessage() );
                System.out.println( "out = " + bscOut.getOutput() );
                System.err.println( "err = " + bscErr.getOutput() );
                assertEquals( "", bscOut.getOutput() );
                assertEquals( "", bscErr.getOutput() );
            }
        }
    }
}
