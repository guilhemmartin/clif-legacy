/*
 * CLIF is a Load Injection Framework
 * Copyright (C) 2016 Orange SA
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 *
 * Contact: clif@ow2.org
 */

package org.ow2.isac.plugin.svninjector;

import java.io.OutputStream;
import java.math.BigInteger;
import org.tmatesoft.svn.core.SVNCommitInfo;
import org.tmatesoft.svn.core.SVNException;
import org.tmatesoft.svn.core.SVNPropertyValue;
import org.tmatesoft.svn.core.io.ISVNEditor;
import org.tmatesoft.svn.core.io.diff.SVNDiffWindow;

/**
 * Implementation of checkout listener that simply count
 * the number of checked-out files and directories,
 * as well as the amount of checked-out bytes.
 * 
 * @author Bruno Dillenseger
 */
public class CheckoutListener implements ISVNEditor
{
	BigInteger bytes = BigInteger.ZERO;
	BigInteger files = BigInteger.ZERO;

	/**
	 * Get the number of checked-out bytes
	 * @return the number of checked-out bytes
	 */
	public BigInteger getBytes()
	{
		return bytes;
	}

	/**
	 * Get the number of checked-out files and directories
	 * @return the number of checked-out files and directories
	 */
	public BigInteger getFiles()
	{
		return files;
	}

	////////////////////////////////////////////
	// implementation of interface ISVNEditor //
	////////////////////////////////////////////

	@Override
	public void applyTextDelta(String path, String baseChecksum)
	throws SVNException
	{
	}

	/**
	 * Adds the number of received bytes to the total amount of checked-out bytes
	 */
	@Override
	public OutputStream textDeltaChunk(String path, SVNDiffWindow diffWindow)
	throws SVNException
	{
		bytes = bytes.add(BigInteger.valueOf(diffWindow.getNewDataLength()));
		return null;
	}

	@Override
	public void textDeltaEnd(String arg0)
	throws SVNException
	{
	}

	@Override
	public void abortEdit()
	throws SVNException
	{
	}

	@Override
	public void absentDir(String arg0)
	throws SVNException
	{	
	}

	@Override
	public void absentFile(String arg0)
	throws SVNException
	{
	}

	/**
	 * Increments the number of checked-out files
	 */
	@Override
	public void addDir(String path, String fromPath, long revision)
	throws SVNException
	{
		files = files.add(BigInteger.ONE);
	}

	@Override
	public void changeDirProperty(String arg0, SVNPropertyValue arg1)
	throws SVNException
	{
	}

	@Override
	public void changeFileProperty(String arg0, String arg1, SVNPropertyValue arg2)
	throws SVNException
	{
	}

	@Override
	public void closeDir()
	throws SVNException
	{
	}

	@Override
	public SVNCommitInfo closeEdit()
	throws SVNException
	{
		return null;
	}

	@Override
	public void closeFile(String arg0, String arg1)
	throws SVNException
	{
	}

	@Override
	public void deleteEntry(String arg0, long arg1)
	throws SVNException
	{
	}

	@Override
	public void openDir(String arg0, long arg1)
	throws SVNException
	{
	}

	@Override
	public void openFile(String arg0, long arg1)
	throws SVNException
	{
	}

	@Override
	public void openRoot(long arg0)
	throws SVNException
	{
	}

	@Override
	public void targetRevision(long arg0)
	throws SVNException
	{
	}

	/**
	 * Increments the number of checked-out files
	 */
	@Override
	public void addFile(String arg0, String arg1, long arg2)
	throws SVNException
	{
		files = files.add(BigInteger.ONE);
	}
}
