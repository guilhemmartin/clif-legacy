/*
 * CLIF is a Load Injection Framework
 * Copyright (C) 2016 Orange SA
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 *
 * Contact: clif@ow2.org
 */
package org.ow2.isac.plugin.svninjector;

import java.io.IOException;
import java.io.InputStream;

/**
 * This input stream generates a given number of bytes before reaching end of stream.
 * It supports mark/reset operations.
 *  
 * @author Bruno Dillenseger
 */
public class DummyInputStream extends InputStream
{
	long size;
	long mark = -1;
	int readLimit = -1;

	/**
	 * Creates a new input stream that will generate a given number of bytes.
	 * @param size the number of bytes to generate before reaching end of stream.
	 * Any value strictly less than 1 is considered as 0 (empty input stream).
	 */
	public DummyInputStream(long size)
	{
		if (size < 1)
		{
			this.size = 0;
		}
		else
		{
			this.size = size;
		}
	}

	/**
	 * @throws IOException is never thrown
	 */
	@Override
	public synchronized int read() throws IOException
	{
		if (readLimit > 0)
		{
			--readLimit;
		}
		if (size > -1)
		{
			return (int)(--size % 256);
		}
		else
		{
			return -1;
		}
	}

	@Override
	public int available()
	{
		if (size >= Integer.MAX_VALUE)
		{
			return Integer.MAX_VALUE;
		}
		else
		{
			return (int)size;
		}
	}

	@Override
	public synchronized void mark(int readLimit)
	{
		mark = size;
		this.readLimit = readLimit;
	}

	/**
	 * @throws IOException if there is no active mark
	 */
	@Override
	public synchronized void reset()
	throws IOException
	{
		if (mark > 0 && readLimit > 0)
		{
			size = mark;
		}
		else
		{
			throw new IOException("No active mark for DummyInputStream.");
		}
	}

	/**
	 * @return true
	 */
	@Override
	public boolean markSupported()
	{
		return true;
	}

	@Override
	public synchronized long skip(long n)
	{
		if (size >= n)
		{
			size -= n;
			return n;
		}
		else
		{
			n = size;
			size = 0;
			return n;
		}
	}
}
