/*
 * CLIF is a Load Injection Framework
 * Copyright (C) 2016 Orange SA
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 *
 * Contact: clif@ow2.org
 */
package org.ow2.isac.plugin.svninjector;

import java.io.File;
import java.math.BigInteger;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.SortedMap;
import java.util.TreeMap;
import org.tmatesoft.svn.core.SVNCommitInfo;
import org.tmatesoft.svn.core.SVNException;
import org.tmatesoft.svn.core.io.ISVNEditor;
import org.tmatesoft.svn.core.io.SVNRepository;
import org.tmatesoft.svn.core.io.diff.SVNDeltaGenerator;

/**
 * Instances of this class enable preparing a commit by adding files and directories,
 * and finally committing them. An instance shall not be further used while a commit has
 * been performed. Files content is generated on the fly according to a given file size.
 * 
 * @author Bruno Dillenseger
 */
public class Committer
{
	SVNRepository repo;
	SortedMap<String,Map<String,Long>> dirs = new TreeMap<String,Map<String,Long>>();
	BigInteger writtenBytes = BigInteger.ZERO;
	BigInteger committedFiles = BigInteger.ZERO;
	Map<String,Long> currentDir = new HashMap<String,Long>();
	Set<String> createDirs = new HashSet<String>();

	/**
	 * Creates a new committer object to add files and directories
	 * @param repo the SVN repository
	 */
	Committer(SVNRepository repo)
	{
		this.repo = repo;
		dirs.put("", currentDir);
	}

	/**
	 * Sets current directory, possibly adding it
	 * if it does not exist yet.
	 * @param path the new current directory specified as a full path
	 * relative to the repository path
	 * @param create when true, the directory is created
	 * if it does not exist yet
	 */
	void setDir(String path, boolean create)
	{
		if (path != null
			&& ! path.isEmpty()
			&& ! path.equals("."))
		{
			if (dirs.containsKey(path))
			{
				currentDir = dirs.get(path);
			}
			else
			{
				currentDir = new HashMap<String,Long>();
				dirs.put(path, currentDir);
				if (create)
				{
					createDirs.add(path);
				}
			}
		}
	}

	/**
	 * Adds a file to current directory.
	 * @param name the file name
	 * @param size the file size in bytes (content will be generated)
	 */
	void addFile(String name, Long size)
	{
		currentDir.put(name, size);
	}

	/**
	 * Commits added files and directories
	 * @param message the commit message
	 * @return a record of information about this committed revision
	 * @throws SVNException commit failed
	 */
	SVNCommitInfo commit(String message)
	throws SVNException
	{
		ISVNEditor editor = repo.getCommitEditor(message, null);
		editor.openRoot(-1);
		File prevDir = null;
		for (Map.Entry<String,Map<String,Long>> dirEntry : dirs.entrySet())
		{
			File parentDir = new File(dirEntry.getKey()).getParentFile();
			while (prevDir != null && !prevDir.equals(parentDir))
			{
				editor.closeDir();
				prevDir = prevDir.getParentFile();
			}
			if (dirEntry.getKey() != "")
			{
				if (createDirs.contains(dirEntry.getKey()))
				{
					editor.addDir(dirEntry.getKey(), null, -1);
				}
				else
				{
					editor.openDir(dirEntry.getKey(), -1);
				}
				prevDir = new File(dirEntry.getKey());
			}
			for (Map.Entry<String,Long> fileEntry : dirEntry.getValue().entrySet())
			{
				editor.addFile(fileEntry.getKey(), null, -1);
				editor.applyTextDelta(fileEntry.getKey(), null);
				SVNDeltaGenerator deltaGen = new SVNDeltaGenerator();
				String checksum = deltaGen.sendDelta(fileEntry.getKey(), new DummyInputStream(fileEntry.getValue()), editor, true);
				editor.closeFile(fileEntry.getKey(), checksum);
				writtenBytes = writtenBytes.add(BigInteger.valueOf(fileEntry.getValue()));
				committedFiles = committedFiles.add(BigInteger.ONE);
			}
		}
		while (prevDir != null)
		{
			editor.closeDir();
			prevDir = prevDir.getParentFile();
		}
		editor.closeDir();
		return editor.closeEdit();
	}

	/**
	 * Get the number of committed bytes.
	 * @return 0 as long as the commit method has not been successfully called,
	 * then the number of committed bytes.
	 */
	BigInteger getWrittenBytes()
	{
		return writtenBytes;
	}

	/**
	 * Get the number of committed files
	 * @return 0 as long as the commit method has not been successfully called,
	 * then the number of committed files.
	 */
	BigInteger getCommittedFiles()
	{
		return committedFiles;
	}
}
