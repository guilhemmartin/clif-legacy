package org.ow2.isac.plugin.mqttinjector;

import org.fusesource.mqtt.client.QoS;

public enum ClifQoS {
    AT_MOST_ONCE(0, QoS.AT_MOST_ONCE),
    AT_LEAST_ONCE(1, QoS.AT_LEAST_ONCE),
    EXACTLY_ONCE(2, QoS.EXACTLY_ONCE);
    
    private int value;
    private QoS fuseValue;

	private ClifQoS(int value, QoS fuseValue) {
		this.value = value;
		this.fuseValue = fuseValue;
	}
    
    public static QoS getByValue(int value) throws Exception {
    	QoS fuse = null;
    	if (value == AT_MOST_ONCE.value) {
    		fuse = AT_MOST_ONCE.fuseValue;
    	} else if (value == AT_LEAST_ONCE.value) {
    		fuse = AT_LEAST_ONCE.fuseValue;
    	} else if (value == EXACTLY_ONCE.value) {
    		fuse = EXACTLY_ONCE.fuseValue;
    	} else {
    		throw new Exception("Invalid qos value");
    	}
    	return fuse;
    }
    
}
