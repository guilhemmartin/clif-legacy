/*
 * CLIF is a Load Injection Framework
 * Copyright (C) 2017 Orange SA
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 *
 * Contact: clif@ow2.org
 */
package org.ow2.isac.plugin.mqttinjector;

import java.io.PrintWriter;
import java.io.StringWriter;
import java.io.UnsupportedEncodingException;
import java.net.URISyntaxException;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;

import org.ow2.clif.scenario.isac.exception.IsacRuntimeException;
import org.ow2.clif.scenario.isac.util.ParameterParser;
import org.ow2.clif.storage.api.ActionEvent;
import org.fusesource.mqtt.client.Callback;
import org.fusesource.mqtt.client.Future;
import org.fusesource.mqtt.client.FutureConnection;
import org.fusesource.mqtt.client.MQTT;
import org.fusesource.mqtt.client.Message;
import org.fusesource.mqtt.client.QoS;
import org.fusesource.mqtt.client.Topic;

/**
 * Implementation of a session object for plugin ~MqttFuseInjector~, a ISAC
 * plug-in providing MQTT traffic injection over MQTT 3.1.1 (@see
 * http://docs.oasis-open.org/mqtt/mqtt/v3.1.1/cos02/mqtt-v3.1.1-cos02.html)
 * 
 * Built over FuseSource MQTT Java client library, released under Apache License
 * 2.0
 */
public class SessionObjectFuseImpl extends SessionObject {

	static final String SAMPLE_CONNECT_CLIENT_PORT = "client port";
	static final String SAMPLE_CONNECT_CLIENT_HOST = "client host";
	static final String SAMPLE_GETMESSAGE_ORIGIN_TIME = "origin time";

	private FutureConnection connexion;
	private MQTT client;
	private long time_out = 0;

	/**
	 * Constructor for specimen object. Sets the length of generated client ids
	 * (defaults to 23, as a backward compliance with MQTT 3.1).
	 *
	 * @param params
	 *            may contain the client id size setting
	 */
	public SessionObjectFuseImpl(Map<String, String> params) {
		System.out.println("new sessionObject Fuse");
	}

	/**
	 * Copy constructor (clone specimen object to get session object). Session
	 * object state initialization.
	 * 
	 * @param so
	 *            specimen object to clone
	 */
	private SessionObjectFuseImpl(SessionObjectFuseImpl so) {
		connexion = null;
		clientId = null;
		client = null;
		clientIdLength = so.clientIdLength;
		messagesByTopic = new HashMap<String, MessageQueue>();
		allMessages = new MessageQueue();
		variables = new HashMap<String, String>();
	}

	////////////////////////////////////////
	// SessionObjectAction implementation //
	////////////////////////////////////////

	/**
	 * @see org.ow2.clif.scenario.isac.plugin.SessionObjectAction#createNewSessionObject()
	 */
	@Override
	public Object createNewSessionObject() {
		return new SessionObjectFuseImpl(this);
	}

	/**
	 * @see org.ow2.clif.scenario.isac.plugin.SessionObjectAction#close() Forces
	 *      disconnection of the MQTT client when connected. Clears the session
	 *      object state to enable garbage collection.
	 */
	public void close() {
		if (connexion != null && connexion.isConnected()) {
			try {
				connexion.disconnect();
			} catch (Exception ex) {
				throw new IsacRuntimeException("Warning: mqtt client failed to disconnect", ex);
			} finally {
				connexion = null;
			}
		}
	}

	/////////////////////////////////
	// SampleAction implementation //
	/////////////////////////////////

	/**
	 * @see org.ow2.clif.scenario.isac.plugin.SampleAction#doSample()
	 */
	@Override
	public ActionEvent doSample(int number, Map<String, String> params, ActionEvent report) {
		switch (number) {
		case SAMPLE_GETMESSAGE:
			doSampleGetMessage(params, report);
			break;
		case SAMPLE_UNSUBSCRIBE:
			doSampleUnsubscribe(params, report);
			break;
		case SAMPLE_SUBSCRIBE:
			doSampleSubscribe(params, report);
			break;
		case SAMPLE_DISCONNECT:
			doSampleDisconnect(params, report);
			break;
		case SAMPLE_PUBLISH:
			doSamplePublish(params, report);
			break;
		case SAMPLE_CONNECT:
			doSampleConnect(params, report);
			break;
		default:
			throw new Error("Unable to find this sample in ~MqttFuseInjector~ ISAC plugin: " + number);
		}
		return report;
	}

	/**
	 * Instantiates an MQTT client object and opens a connection with an MQTT
	 * server. An MQTT client id is generated when none is provided (see
	 * {@link SessionObject#genClientId(int)}).
	 * 
	 * @param params
	 *            map giving MQTT server host and port, protocol (tcp or ssl), and
	 *            possibly a client id as well as common samples' action type and
	 *            optional comment. Optionally, a "last will and testament" message
	 *            may be defined (together with a topic, QoS, and flags).
	 * @param report
	 *            initial action report, to be filled with all necessary information
	 *            about the MQTT connection attempt.
	 * @throws IsacRuntimeException
	 *             if this session object was already connected.
	 */
	private void doSampleConnect(Map<String, String> params, final ActionEvent report) throws IsacRuntimeException {

		if (connexion == null || !connexion.isConnected()) {
			// processing of basic parameters
			final String uri = ParameterParser.getCombo(params.get(SAMPLE_CONNECT_PROTOCOL)) + "://"
					+ params.get(SAMPLE_CONNECT_HOST) + ":" + params.get(SAMPLE_CONNECT_PORT);
			String id = params.get(SAMPLE_CONNECT_CLIENTID);
			String username = params.get(SAMPLE_CONNECT_USERNAME);
			String password = params.get(SAMPLE_CONNECT_PASSWORD);
			String timeout_sStr = params.get(SAMPLE_CONNECT_TIMEOUT_S);
			int timeout_s = 0;
			if (timeout_sStr != null) {
				try {
					timeout_s = Integer.parseInt(timeout_sStr);
					time_out = timeout_s;
				} catch (NumberFormatException ex) {
					throw new IsacRuntimeException("Invalid MQTT connection timeout: " + timeout_sStr);
				}
			}
			String keepAlive_sStr = params.get(SAMPLE_CONNECT_KEEPALIVE_S);
			short keepAlive_s = 30; //default value on CONNECT class
			if (keepAlive_sStr != null) {
				try {
					keepAlive_s = Short.parseShort(keepAlive_sStr);
				} catch (NumberFormatException ex) {
					throw new IsacRuntimeException("Invalid MQTT connection keep-alive interval: " + keepAlive_sStr);
				}
			}
			if (id == null || id.trim().isEmpty()) {
				if (clientId == null) {
					clientId = genClientId((char) report.sessionId);
				}
				id = clientId;
			}
			String clientHost = params.get(SAMPLE_CONNECT_CLIENT_HOST);
			String clientPort = params.get(SAMPLE_CONNECT_CLIENT_PORT);
			
			// processing of optional "last will and testament" parameters
			boolean enableLWT = ParameterParser.getRadioGroup(params.get(SAMPLE_CONNECT_ENABLE))
					.equalsIgnoreCase("yes");
			String topic = params.get(SAMPLE_CONNECT_TOPIC);
			String messageStr = null;
			QoS qos = null;
			boolean retained = false;
			if (enableLWT) {
				messageStr = params.get(SAMPLE_CONNECT_MESSAGE);
				String qosStr = ParameterParser.getRadioGroup(params.get(SAMPLE_CONNECT_QOS));

				try {
					int qosInt = Integer.parseInt(qosStr.split("\\D", 2)[0]);
					qos = ClifQoS.getByValue(qosInt);
				} catch (Exception ex) {
					throw new IsacRuntimeException("Bad QoS specification format for MQTT subscription: " + qosStr, ex);
				}
				retained = ParameterParser.getCheckBox(params.get(SAMPLE_CONNECT_FLAGS)).contains("retain");
			}
			// sample execution
			report.setDate(System.currentTimeMillis());

			if (client == null) {
				client = new MQTT();
				client.setConnectAttemptsMax(0);
				client.setReconnectAttemptsMax(0);
				client.setKeepAlive(keepAlive_s);
			}
			try {
				client.setHost(uri);
			} catch (URISyntaxException e) {
				connexion = null;
				report.successful = false;
				report.result = e.getMessage();
				throw new IsacRuntimeException("Invalid address: "+uri);
			}
			client.setClientId(id);
			client.setCleanSession(true);
			if (clientHost != null && !clientHost.isEmpty()) {
				try {
					client.setLocalAddress("tcp://" + clientHost + ":" + clientPort);
				} catch (URISyntaxException e) {
					connexion = null;
					report.successful = false;
					report.result = e.getMessage();
					throw new IsacRuntimeException("Invalid local address: tcp://" + clientHost + ":" + clientPort);
				}
			}
			if (username != null && !username.isEmpty()) {
				client.setUserName(username);
			}
			if (password != null) {
				client.setPassword(password);
			}
			if (enableLWT) {
				client.setWillTopic(topic);
				client.setWillMessage(messageStr);
				client.setWillQos(qos);
				client.setWillRetain(retained);
			}

			if (connexion != null) {
				connexion.disconnect();
			}
			connexion = client.futureConnection();

			final ActionEvent tempReport = new ActionEvent();
			Future<Void> futur = connexion.connect();
			futur.then(new Callback<Void>() {
				public void onSuccess(Void value) {
					tempReport.successful = true;
					tempReport.result = "connected to " + uri;
					tempReport.comment = "connexion success";
				}

				public void onFailure(Throwable value) {
					connexion = null;
					tempReport.successful = false;
					tempReport.result = getStackTrace(value);
					tempReport.comment = "connexion failure";
				}
			});
			try {
				futur.await(timeout_s, TimeUnit.SECONDS);
				synchro(tempReport, report);
				int wait=0;
				while (report.successful && connexion != null && !connexion.isConnected() && wait < timeout_s) {
					try {
						Thread.sleep(1000);
						wait++;
					} catch (InterruptedException e) {

					}
				}
			} catch (TimeoutException te) {
				if (connexion != null) {
					connexion.disconnect();
				}
				connexion = null;
				report.successful = false;
				report.result = "Cannot connect: timeout.";
			} catch (Exception ex) {
				if (connexion != null) {
					connexion.disconnect();
				}
				connexion = null;
				report.successful = false;
				report.result = getStackTrace(ex);
			}

			report.duration = (int) (System.currentTimeMillis() - report.getDate());
			report.type = params.get(SAMPLE_CONNECT_ACTION_TYPE);
			report.comment = params.get(SAMPLE_CONNECT_COMMENT);
			if (report.comment == null) {
				report.comment = "state " + connexion.isConnected();
			}
		} else {
			throw new IsacRuntimeException("Attempt to connect an already connected MQTT client.");
		}
	}

	/**
	 * Publishes a message over current MQTT connection.
	 * 
	 * @param params
	 *            map giving message topic, payload, qos value and retain flag as
	 *            well as common samples' action type and optional comment.
	 * @param report
	 *            initial action report, to be filled with all necessary information
	 *            about the MQTT message publication attempt.
	 * @throws Error
	 *             if current JVM does not support UTF-8 encoding
	 * @throws IsacRuntimeException
	 *             if this session object is not connected to an MQTT server.
	 */
	private void doSamplePublish(Map<String, String> params, final ActionEvent report)
			throws Error, IsacRuntimeException {
		if (connexion != null && connexion.isConnected()) {
			String topic = params.get(SAMPLE_PUBLISH_TOPIC);
			byte payload[] = null;
			try {
				payload = params.get(SAMPLE_PUBLISH_MESSAGE).getBytes("UTF-8");
			} catch (UnsupportedEncodingException ex) {
				throw new Error("Cannot publish MQTT message", ex);
			}
			String qosStr = ParameterParser.getRadioGroup(params.get(SAMPLE_PUBLISH_QOS));
			QoS qos;
			try {
				int qosInt = Integer.parseInt(qosStr.split("\\D", 2)[0]);
				qos = ClifQoS.getByValue(qosInt);
			} catch (Exception ex) {
				throw new IsacRuntimeException("Bad QoS specification format for MQTT subscription: " + qosStr, ex);
			}

			boolean retain = ParameterParser.getCheckBox(params.get(SAMPLE_PUBLISH_FLAGS)).contains("retain");
			report.setDate(System.currentTimeMillis());

			final ActionEvent tempReport = new ActionEvent();
			final int payloadlength = payload.length;
			Future<Void> future = connexion.publish(topic, payload, qos, retain);
			future.then(new Callback<Void>() {
				public void onSuccess(Void value) {
					tempReport.successful = true;
					tempReport.result = "message published (" + payloadlength + " bytes)";
					tempReport.comment = "publish success";
				}

				public void onFailure(Throwable ex) {
					tempReport.successful = false;
					tempReport.result = getStackTrace(ex);
					tempReport.comment = "publish failure";
				}
			});

			try {
				future.await(time_out, TimeUnit.SECONDS);
				synchro(tempReport, report);
			} catch (TimeoutException te) {
				report.successful = false;
				report.result = "Cannot publish: timeout.";
			} catch (Exception ex) {
				report.successful = false;
				report.result = getStackTrace(ex);
			}

			report.duration = (int) (System.currentTimeMillis() - report.getDate());
			report.type = params.get(SAMPLE_PUBLISH_ACTION_TYPE);
			report.comment = params.get(SAMPLE_PUBLISH_COMMENT);
			if (report.comment == null) {
				report.comment = "";
			}
		} else {
			throw new IsacRuntimeException("Attempt to publish a message from an unconnected MQTT client.");
		}
	}

	/**
	 * Closes current MQTT connection.
	 * 
	 * @param params
	 *            map giving common samples' parameters action type and optional
	 *            comment.
	 * @param report
	 *            initial action report, to be filled with all necessary information
	 *            about the MQTT disconnection attempt.
	 * @throws IsacRuntimeException
	 *             if this session object is not connected to an MQTT server.
	 */
	private void doSampleDisconnect(Map<String, String> params, final ActionEvent report) {
		if (connexion != null && connexion.isConnected()) {
			report.successful = true;
			report.setDate(System.currentTimeMillis());

			final ActionEvent tempReport = new ActionEvent();
			Future<Void> future = connexion.disconnect();
			future.then(new Callback<Void>() {
				public void onSuccess(Void value) {
					connexion = null;
					tempReport.successful = true;
					tempReport.result = "disconnected";
					tempReport.comment = "disconnection success";
				}

				public void onFailure(Throwable ex) {
					tempReport.successful = false;
					tempReport.result = getStackTrace(ex);
					tempReport.comment = "disconnection failure";
				}
			});

			try {
				future.await(time_out, TimeUnit.SECONDS);
				synchro(tempReport, report);
			} catch (TimeoutException te) {
				report.successful = false;
				report.result = "Cannot disconnect: timeout.";
			} catch (Exception ex) {
				report.successful = false;
				report.result = getStackTrace(ex);
			}

			report.duration = (int) (System.currentTimeMillis() - report.getDate());
			report.type = params.get(SAMPLE_DISCONNECT_ACTION_TYPE);
			report.comment = params.get(SAMPLE_DISCONNECT_COMMENT);
			if (report.comment == null) {
				report.comment = "";
			}
		} else {
			throw new IsacRuntimeException("Attempt to disconnect an unconnected MQTT client.");
		}
	}

	/**
	 * Subscribes to a topic over an MQTT connection.
	 * 
	 * @param params
	 *            map giving subscription topic and qos value, maximum number of
	 *            stored incoming messages received on this topic (can be zero to
	 *            disable storing) and the message drop policy in case the storage
	 *            capacity is reached (drop the oldest message or the incoming
	 *            message), as well as common samples' action type and optional
	 *            comment.
	 * @param report
	 *            initial action report, to be filled with all necessary information
	 *            about the MQTT topic subscription attempt.
	 * @throws IsacRuntimeException
	 *             if this session object is not connected to an MQTT server.
	 */
	private void doSampleSubscribe(Map<String, String> params, final ActionEvent report) {
		// check state and parameters validity
		if (connexion == null || !connexion.isConnected()) {
			throw new IsacRuntimeException("Attempt to subscribe an unconnected MQTT client.");
		}
		final String topic = params.get(SAMPLE_SUBSCRIBE_TOPIC);
		if (topic == null || topic.isEmpty()) {
			throw new IsacRuntimeException("MQTT client can't subscribe to empty topic.");
		}
		String qosStr = ParameterParser.getRadioGroup(params.get(SAMPLE_SUBSCRIBE_QOS));
		if (qosStr == null || qosStr.isEmpty()) {
			throw new IsacRuntimeException("Missing QoS specification for MQTT subscription.");
		}
		final QoS qos;
		try {
			int qosInt = Integer.parseInt(qosStr.split("\\D", 2)[0]);
			qos = ClifQoS.getByValue(qosInt);
		} catch (Exception ex) {
			throw new IsacRuntimeException("Bad QoS specification format for MQTT subscription: " + qosStr, ex);
		}
		String capacityStr = params.get(SAMPLE_SUBSCRIBE_SIZE);
		if (capacityStr == null || capacityStr.isEmpty()) {
			throw new IsacRuntimeException("Missing message buffer size while subscribing to MQTT topic " + topic);
		}
		int capacity;
		try {
			capacity = Integer.parseInt(capacityStr);
		} catch (Exception ex) {
			throw new IsacRuntimeException("Bad message buffer size specification \"" + capacityStr
					+ "\" while subscribing to MQTT topic " + topic, ex);
		}
		String policyStr = ParameterParser.getCombo(params.get(SAMPLE_SUBSCRIBE_POLICY));
		if (policyStr == null || policyStr.isEmpty()) {
			throw new IsacRuntimeException("Missing drop policy while subscribing to MQTT topic " + topic);
		}
		DropPolicy policy;
		if (policyStr.startsWith("new")) {
			policy = DropPolicy.NEWER_FIRST;
		} else if (policyStr.startsWith("old")) {
			policy = DropPolicy.OLDER_FIRST;
		} else {
			throw new IsacRuntimeException(
					"Bad drop policy specification \"" + policyStr + "\" while subscribing to MQTT topic " + topic
							+ "\n=> expected " + DropPolicy.NEWER_FIRST + " or " + DropPolicy.OLDER_FIRST);
		}
		if (!messagesByTopic.containsKey(topic) && capacity > 0) {
			messagesByTopic.put(topic, new MessageQueue(capacity, policy));
		}
		report.setDate(System.currentTimeMillis());

		final ActionEvent tempReport = new ActionEvent();
		Topic[] topics = { new Topic(topic, qos) };
		Future<byte[]> future = connexion.subscribe(topics);
		future.then(new Callback<byte[]>() {
			public void onSuccess(byte[] value) {
				tempReport.successful = true;
				tempReport.result = "subscribed to " + topic + " with QoS " + qos;
				tempReport.comment = "subscribe success";
			}

			public void onFailure(Throwable ex) {
				tempReport.successful = false;
				tempReport.result = getStackTrace(ex);
				tempReport.comment = "subscribe failure";
			}
		});

		try {
			future.await(time_out, TimeUnit.SECONDS);
			synchro(tempReport, report);
		} catch (TimeoutException te) {
			report.successful = false;
			report.result = "Cannot subscribe: timeout.";
		} catch (Exception ex) {
			report.successful = false;
			report.result = getStackTrace(ex);
		}

		if (capacity > 0) {
			messageArrived();
		}

		report.duration = (int) (System.currentTimeMillis() - report.getDate());
		report.type = params.get(SAMPLE_SUBSCRIBE_ACTION_TYPE);
		report.comment = params.get(SAMPLE_SUBSCRIBE_COMMENT);
		if (report.comment == null) {
			report.comment = "";
		}
	}

	/**
	 * Unsubscribes from a topic over an MQTT connection. Whatever the success
	 * status of this sample, the message store for incoming messages is discarded.
	 * 
	 * @param params
	 *            map giving topic to unsubscribe from, as well as common samples'
	 *            action type and optional comment.
	 * @param report
	 *            initial action report, to be filled with all necessary information
	 *            about the MQTT topic unsubscription attempt.
	 * @throws IsacRuntimeException
	 *             if this session object is not connected to an MQTT server.
	 */
	private void doSampleUnsubscribe(Map<String, String> params, final ActionEvent report) {
		if (connexion != null && connexion.isConnected()) {
			final String[] topic = { params.get(SAMPLE_UNSUBSCRIBE_TOPIC) };
			report.setDate(System.currentTimeMillis());

			final ActionEvent tempReport = new ActionEvent();
			Future<Void> future = connexion.unsubscribe(topic);
			future.then(new Callback<Void>() {
				public void onSuccess(Void value) {
					tempReport.successful = true;
					tempReport.result = "unsubscribed from " + topic[0];
					tempReport.comment = "unsubscribe success";
				}

				public void onFailure(Throwable ex) {
					tempReport.successful = false;
					tempReport.result = getStackTrace(ex);
					tempReport.comment = "unsubscribe failure";
				}
			});

			try {
				future.await(time_out, TimeUnit.SECONDS);
				synchro(tempReport, report);
			} catch (TimeoutException te) {
				report.successful = false;
				report.result = "Cannot unsubscribe: timeout.";
			} catch (Exception ex) {
				report.successful = false;
				report.result = getStackTrace(ex);
			}

			report.duration = (int) (System.currentTimeMillis() - report.getDate());
			report.type = params.get(SAMPLE_UNSUBSCRIBE_ACTION_TYPE);
			report.comment = params.get(SAMPLE_UNSUBSCRIBE_COMMENT);
			if (report.comment == null) {
				report.comment = "";
			}
			MessageQueue mq = messagesByTopic.remove(topic[0]);
			if (mq != null) {
				for (MessageWithTopicAbstract msg : mq) {
					allMessages.remove(msg);
				}
			}
		} else {
			throw new IsacRuntimeException("Attempt to unsubscribe an unconnected MQTT client.");
		}
	}

	/**
	 * Gets next message from the message store associated to one, several or all
	 * current topic subscriptions, possibly waiting for a message to be available.
	 * The payload of the message, as well as the actual topic it was sent to, may
	 * be obtained as a plug-in variable (see {@link #doGet(String)}).
	 * 
	 * @param params
	 *            map giving the list of topics (empty string to read from all
	 *            currently subscribed topics), a time-out (in ms) waiting for a
	 *            message, an optional variable name to make the message payload
	 *            available as a plug-in variable, an optional variable name for the
	 *            actual topic it was sent to, as well as common samples' action
	 *            type and optional comment. A negative timeout value means infinite
	 *            time-out. A zero timeout means no waiting time. The only way to
	 *            know if the timeout has been reached without getting a message is
	 *            to set a variable name and to check if it has been defined (see
	 *            {@link #doTest(int, Map)}.
	 * @param report
	 *            initial action report, to be filled with all necessary information
	 *            about the MQTT message reception attempt.
	 * @throws Error
	 *             if current JVM does not support UTF-8 encoding
	 * @throws IsacRuntimeException
	 *             if this session object has not subscribed to the given MQTT
	 *             topic.
	 */
	private void doSampleGetMessage(Map<String, String> params, ActionEvent report) throws Error {
		List<String> topics = ParameterParser.getNField(params.get(SAMPLE_GETMESSAGE_TOPICS));
		MessageQueue queue = null;
		if (topics.isEmpty()) {
			queue = allMessages;
		} else {
			Iterator<String> topicIter = topics.iterator();
			while (queue == null && topicIter.hasNext()) {
				queue = messagesByTopic.get(topicIter.next());
			}
		}
		if (queue != null) {
			String msgVar = params.get(SAMPLE_GETMESSAGE_MESSAGE_VAR);
			String topicVar = params.get(SAMPLE_GETMESSAGE_TOPIC_VAR);
			String timeoutStr = params.get(SAMPLE_GETMESSAGE_TIMEOUT);
			String originTime = params.get(SAMPLE_GETMESSAGE_ORIGIN_TIME);
			int timeout_ms = 0;
			if (!timeoutStr.isEmpty()) {
				timeout_ms = Integer.parseInt(timeoutStr);
			}
			MessageWithTopic message = null;
			if (msgVar != null && !msgVar.isEmpty()) {
				variables.remove(msgVar);
			}
			if (topicVar != null && !topicVar.isEmpty()) {
				variables.remove(topicVar);
			}
			
			report.setDate(System.currentTimeMillis());
			if (timeout_ms == 0) {
				message = (MessageWithTopic) queue.poll();
			} else {
				try {
					if (timeout_ms < 0) {
						message = (MessageWithTopic) queue.take();
					} else {
						message = (MessageWithTopic) queue.poll(timeout_ms, TimeUnit.MILLISECONDS);
					}
				} catch (InterruptedException ex) {
					message = null;
				}
			}

			report.duration = (int) (System.currentTimeMillis() - report.getDate());
			report.type = params.get(SAMPLE_GETMESSAGE_ACTION_TYPE);
			report.comment = params.get(SAMPLE_GETMESSAGE_COMMENT);
			if (report.comment == null) {
				report.comment = "";
			}
			if (message != null) {
				report.successful = true;
				report.result = "got one message on topic " + message.topic;
				if (msgVar != null && !msgVar.isEmpty()) {
					try {
						variables.put(msgVar, new String(message.message.getPayload(), "UTF-8"));
					} catch (UnsupportedEncodingException ex) {
						throw new Error("~MqttFuseInjector~ requires UTF-8 charset encoding", ex);
					}
				}
				if (topicVar != null && !topicVar.isEmpty()) {
					variables.put(topicVar, message.topic);
				}
				if (queue != allMessages) {
					allMessages.remove(message);
				} else {
					Iterator<Map.Entry<String, MessageQueue>> mqIter = messagesByTopic.entrySet().iterator();
					boolean found = false;
					while (!found && mqIter.hasNext()) {
						Map.Entry<String, MessageQueue> mq = mqIter.next();
						if (matchTopic(mq.getKey(), message.topic)) {
							mq.getValue().remove();
						}
					}
				}
				if (originTime != null && !originTime.isEmpty()) {
					report.setDate(Long.parseLong(originTime));
					report.duration = (int)(message.receiveDate-report.getDate());
				}
			} else {
				report.successful = false;
				report.result = "got no message on topics " + topics.toString();
			}
		} else {
			throw new IsacRuntimeException(
					"Attempt to get an MQTT message from unsubscribed topics " + topics.toString());
		}
	}

	///////////////////////////////
	// TestAction implementation //
	///////////////////////////////

	/**
	 * @see org.ow2.clif.scenario.isac.plugin.TestAction#doTest()
	 */
	@Override
	public boolean doTest(int number, Map<String, String> params) {
		switch (number) {
		case TEST_ISNOTDEFINED:
			return !variables.containsKey(params.get(TEST_ISNOTDEFINED_VARIABLE));
		case TEST_ISDEFINED:
			return variables.containsKey(params.get(TEST_ISDEFINED_VARIABLE));
		case TEST_ISNOTCONNECTED:
			return connexion == null || !connexion.isConnected();
		case TEST_ISCONNECTED:
			return connexion != null && connexion.isConnected();
		default:
			throw new Error("Unable to find this test in ~MqttFuseInjector~ ISAC plugin: " + number);
		}
	}

	////////////////////////////////////
	// interface IMqttMessageListener //
	////////////////////////////////////

	/**
	 * Puts a message to the message store associated to the given topic, or does
	 * nothing if there is no store associated to the topic (since it means the
	 * session object has not subscribed to the topic). Note that the message store
	 * has a limited capacity and may drop a message in case this capacity is
	 * reached.
	 */
	public void messageArrived() {
		if (connexion != null && connexion.isConnected()) {
			Future<Message> futureR = connexion.receive();
			futureR.then(new Callback<Message>() {

				public void onSuccess(Message message) {
					String topic = message.getTopic();
					MessageQueue queue = messagesByTopic.get(topic);
					if (queue == null) {

						Iterator<Map.Entry<String, MessageQueue>> queueIter = messagesByTopic.entrySet().iterator();
						while (queue == null && queueIter.hasNext()) {
							Map.Entry<String, MessageQueue> entry = queueIter.next();
							if (matchTopic(entry.getKey(), topic)) {
								queue = entry.getValue();
							}
						}
					}
					if (queue != null) {
						MessageWithTopic msg = new MessageWithTopic(topic, message);
						queue.put(msg);
						allMessages.put(msg);
					}
					message.ack();

					messageArrived();
				}

				public void onFailure(Throwable value) {
					// do nothing
				}
			});
		} else {
			throw new IsacRuntimeException("Attempt to receive a message from an unconnected MQTT client.");
		}
		
	}

	/////////////////////////////////////////////////////////////////////
	// inner class for queuing incoming messages with limited capacity //
	/////////////////////////////////////////////////////////////////////

	/**
	 * Inner class for associating an MQTT message with a topic
	 */
	class MessageWithTopic extends MessageWithTopicAbstract{
		final String topic;
		final Message message;
		final long receiveDate;

		MessageWithTopic(String topic, Message message) {
			this.topic = topic;
			this.message = message;
			this.receiveDate = System.currentTimeMillis();
		}
	}

	private static String getStackTrace(final Throwable throwable) {
		final StringWriter sw = new StringWriter();
		final PrintWriter pw = new PrintWriter(sw, true);
		throwable.printStackTrace(pw);
		return sw.getBuffer().toString();
	}

	/**
	 * Check the supplied topic name and filter match inspired by paho
	 *
	 * @param topicFilter
	 *            topic filter: wildcards allowed
	 * @param topicName
	 *            topic name: wildcards not allowed
	 * @return true if the topic matches the filter
	 * @throws IllegalArgumentException
	 *             if the topic name or filter is invalid
	 */
	private boolean matchTopic(String topicFilter, String topicName) {
		int curn = 0, curf = 0;
		int curn_end = topicName.length();
		int curf_end = topicFilter.length();

		if (topicFilter.equals(topicName)) {
			return true;
		}

		while (curf < curf_end && curn < curn_end) {
			if (topicName.charAt(curn) == '/' && topicFilter.charAt(curf) != '/')
				break;
			if (topicFilter.charAt(curf) != '+' && topicFilter.charAt(curf) != '#'
					&& topicFilter.charAt(curf) != topicName.charAt(curn))
				break;
			if (topicFilter.charAt(curf) == '+') { // skip until we meet the next separator, or end of string
				int nextpos = curn + 1;
				while (nextpos < curn_end && topicName.charAt(nextpos) != '/')
					nextpos = ++curn + 1;
			} else if (topicFilter.charAt(curf) == '#')
				curn = curn_end - 1; // skip until end of string
			curf++;
			curn++;
		}
		;

		return (curn == curn_end) && (curf == curf_end);
	}
	
	private void synchro(ActionEvent tempReport, ActionEvent report) {
		int wait = 0;
		while ((tempReport.comment == null || tempReport.comment.isEmpty()) && wait < time_out) {
			try {
				Thread.sleep(1000);
				wait++;
			} catch (InterruptedException e) {

			}
		}
		report.successful = tempReport.successful;
		report.result = tempReport.result;
	}
}
