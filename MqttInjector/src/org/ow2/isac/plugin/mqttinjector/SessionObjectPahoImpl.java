/*
 * CLIF is a Load Injection Framework
 * Copyright (C) 2017 Orange SA
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 *
 * Contact: clif@ow2.org
 */
package org.ow2.isac.plugin.mqttinjector;

import org.eclipse.paho.client.mqttv3.IMqttMessageListener;
import org.eclipse.paho.client.mqttv3.MqttClient;
import org.eclipse.paho.client.mqttv3.MqttConnectOptions;
import org.eclipse.paho.client.mqttv3.MqttException;
import org.eclipse.paho.client.mqttv3.MqttMessage;
import org.eclipse.paho.client.mqttv3.MqttTopic;
import java.io.UnsupportedEncodingException;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;
import org.ow2.clif.scenario.isac.exception.IsacRuntimeException;
import org.ow2.clif.scenario.isac.util.ParameterParser;
import org.ow2.clif.storage.api.ActionEvent;


/**
 * Implementation of a session object for plugin ~MqttInjector~,
 * a ISAC plug-in providing MQTT traffic injection over MQTT 3.1.1
 * (@see http://docs.oasis-open.org/mqtt/mqtt/v3.1.1/cos02/mqtt-v3.1.1-cos02.html)
 * 
 * Built over Eclipse Paho MQTT Java client library, released under
 * Eclipse Distribution License 1.0 and Eclipse Public License 1.0 
 */
public class SessionObjectPahoImpl extends SessionObject implements IMqttMessageListener {
	private MqttClient client;

	/**
	 * Constructor for specimen object.
	 * Sets the length of generated client ids
	 * (defaults to 23, as a backward compliance
	 * with MQTT 3.1).
	 *
	 * @param params may contain the client id size setting
	 */
	public SessionObjectPahoImpl(Map<String,String> params)
	{
		System.out.println("new sessionObject Paho");
	}


	/**
	 * Copy constructor (clone specimen object to get session object).
	 * Session object state initialization.
	 * @param so specimen object to clone
	 */
	private SessionObjectPahoImpl(SessionObjectPahoImpl so)
	{
		client = null;
		clientId = null;
		clientIdLength = so.clientIdLength;
		messagesByTopic = new HashMap<String,MessageQueue>();
		allMessages = new MessageQueue();
		variables = new HashMap<String,String>();
	}


	////////////////////////////////////////
	// SessionObjectAction implementation //
	////////////////////////////////////////


	/**
	 * @see org.ow2.clif.scenario.isac.plugin.SessionObjectAction#createNewSessionObject()
	 */
	@Override
	public Object createNewSessionObject()
	{
		return new SessionObjectPahoImpl(this);
	}


	/**
	 * @see org.ow2.clif.scenario.isac.plugin.SessionObjectAction#close()
	 * Forces disconnection of the MQTT client when connected.
	 * Clears the session object state to enable garbage collection.
	 */
	public void close()
	{	
		if (client != null && client.isConnected())
		{
			try
			{
				client.disconnectForcibly(0, 1000);
			}
			catch (Exception ex)
			{
				throw new IsacRuntimeException("Warning: mqtt client failed to disconnect", ex);
			}
			finally
			{
				client = null;
			}
		}
	}

	/////////////////////////////////
	// SampleAction implementation //
	/////////////////////////////////


	/**
	 * @see org.ow2.clif.scenario.isac.plugin.SampleAction#doSample()
	 */
	@Override
	public ActionEvent doSample(int number, Map<String, String> params, ActionEvent report)
	{
		switch (number)
		{
			case SAMPLE_GETMESSAGE:
				doSampleGetMessage(params, report);
				break;
			case SAMPLE_UNSUBSCRIBE:
				doSampleUnsubscribe(params, report);
				break;
			case SAMPLE_SUBSCRIBE:
				doSampleSubscribe(params, report);
				break;
			case SAMPLE_DISCONNECT:
				doSampleDisconnect(params, report);
				break;
			case SAMPLE_PUBLISH:
				doSamplePublish(params, report);
				break;
			case SAMPLE_CONNECT:
				doSampleConnect(params, report);
				break;
			default:
				throw new Error(
					"Unable to find this sample in ~MqttInjector~ ISAC plugin: "
					+ number);
		}
		return report;
	}


	/**
	 * Instantiates an MQTT client object and opens a connection with an MQTT server.
	 * An MQTT client id is generated when none is provided
	 * (see {@link SessionObjectPahoImpl#genClientId(int)}).
	 * @param params map giving MQTT server host and port, protocol (tcp or ssl), and
	 * possibly a client id as well as common samples' action type and optional comment.
	 * Optionally, a "last will and testament" message may be defined (together with a
	 * topic, QoS, and flags).
	 * @param report initial action report, to be filled with all necessary information
	 * about the MQTT connection attempt.
	 * @throws IsacRuntimeException if this session object was already connected.
	 */
	private void doSampleConnect(Map<String, String> params, ActionEvent report)
	throws IsacRuntimeException
	{
		if (client == null || ! client.isConnected())
		{
			// processing of basic parameters
			String uri =
				ParameterParser.getCombo(params.get(SAMPLE_CONNECT_PROTOCOL))
				+ "://" + params.get(SAMPLE_CONNECT_HOST)
				+ ":" + params.get(SAMPLE_CONNECT_PORT);
			String id = params.get(SAMPLE_CONNECT_CLIENTID);
			String username = params.get(SAMPLE_CONNECT_USERNAME);
			String password = params.get(SAMPLE_CONNECT_PASSWORD);
			String timeout_sStr = params.get(SAMPLE_CONNECT_TIMEOUT_S);
			String keepAlive_sStr = params.get(SAMPLE_CONNECT_KEEPALIVE_S);
			int timeout_s = 0;
			if (timeout_sStr != null)
			{
				try
				{
					timeout_s = Integer.parseInt(timeout_sStr);
				}
				catch (NumberFormatException ex)
				{
					throw new IsacRuntimeException("Invalid MQTT connection timeout: " + timeout_sStr);
				}
			}
			int keepAlive_s = 30;
			if (keepAlive_sStr != null)
			{
				try
				{
					keepAlive_s = Integer.parseInt(keepAlive_sStr);
				}
				catch (NumberFormatException ex)
				{
					throw new IsacRuntimeException("Invalid MQTT connection keep-alive interval: " + keepAlive_sStr);
				}
			}
			if (id == null || id.trim().isEmpty())
			{
				if (clientId == null)
				{
					clientId = genClientId((char)report.sessionId);
				}
				id = clientId;
			}
			// processing of optional "last will and testament" parameters
			boolean enableLWT = ParameterParser.getRadioGroup(params.get(SAMPLE_CONNECT_ENABLE)).equalsIgnoreCase("yes");
			String topic = params.get(SAMPLE_CONNECT_TOPIC);
			byte payload[] = null;
			int qos = -1;
			boolean retained = false;
			if (enableLWT)
			{
				String messageStr = params.get(SAMPLE_CONNECT_MESSAGE);
				if (messageStr != null)
				{
					try
					{
						payload = messageStr.getBytes("UTF-8"); 
					}
					catch (UnsupportedEncodingException ex)
					{
						throw new Error("Cannot set Last Will and Testament message", ex);
					}
				}
				String qosStr = ParameterParser.getRadioGroup(params.get(SAMPLE_CONNECT_QOS));
				qos = Integer.parseInt(qosStr.split("\\D", 2)[0]);
				retained = ParameterParser.getCheckBox(params.get(SAMPLE_CONNECT_FLAGS)).contains("retain");
			}
			// sample execution
			report.setDate(System.currentTimeMillis());
			try
			{
				client = new MqttClient(uri, id, null);
				MqttConnectOptions opts = new MqttConnectOptions();
		        opts.setCleanSession(true);
		        if (username != null && !username.isEmpty())
		        {
		        	opts.setUserName(username);
		        }
		        if (password != null)
		        {
		        	opts.setPassword(password.toCharArray());
		        }
		        opts.setConnectionTimeout(timeout_s);
		        opts.setKeepAliveInterval(keepAlive_s);
		        if (enableLWT)
		        {
		        	opts.setWill(topic, payload, qos, retained);
		        }
		        client.connect(opts);
				report.successful = true;
				report.result = "connected to " + uri;
			}
			catch (MqttException ex)
			{
				client = null;
				report.successful = false;
				report.result = ex.getReasonCode() + " " + ex.getMessage();
			}
			report.duration = (int) (System.currentTimeMillis() - report.getDate());
			report.type = params.get(SAMPLE_CONNECT_ACTION_TYPE);
			report.comment = params.get(SAMPLE_CONNECT_COMMENT);
			if (report.comment == null)
			{
				report.comment = "";
			} 
		}
		else
		{
			throw new IsacRuntimeException("Attempt to connect an already connected MQTT client.");
		}
	}


	/**
	 * Publishes a message over current MQTT connection.
	 * @param params map giving message topic, payload, qos value and retain flag
	 * as well as common samples' action type and optional comment.
	 * @param report initial action report, to be filled with all necessary information
	 * about the MQTT message publication attempt.
	 * @throws Error if current JVM does not support UTF-8 encoding
	 * @throws IsacRuntimeException if this session object is not connected to an MQTT
	 * server.
	 */
	private void doSamplePublish(Map<String, String> params, ActionEvent report)
	throws Error, IsacRuntimeException
	{
		if (client != null && client.isConnected())
		{
			String topic = params.get(SAMPLE_PUBLISH_TOPIC);
			byte payload[] = null;
			try
			{
				payload = params.get(SAMPLE_PUBLISH_MESSAGE).getBytes("UTF-8");
			}
			catch (UnsupportedEncodingException ex)
			{
				throw new Error("Cannot publish MQTT message", ex);
			}
			String qosStr = ParameterParser.getRadioGroup(params.get(SAMPLE_PUBLISH_QOS));
			int qos = Integer.parseInt(qosStr.split("\\D", 2)[0]);
			boolean retain = ParameterParser.getCheckBox(params.get(SAMPLE_PUBLISH_FLAGS)).contains("retain");
			report.setDate(System.currentTimeMillis());
			try
			{
				client.publish(topic, payload, qos, retain);
				report.successful = true;
				report.result = "message published (" + payload.length + " bytes)";
			}
			catch (MqttException ex)
			{
				report.successful = false;
				report.result = ex.getReasonCode() + " " + ex.getMessage();
			}
			report.duration = (int) (System.currentTimeMillis() - report.getDate());
			report.type = params.get(SAMPLE_PUBLISH_ACTION_TYPE);
			report.comment = params.get(SAMPLE_PUBLISH_COMMENT);
			if (report.comment == null)
			{
				report.comment = "";
			}
		}
		else
		{
			throw new IsacRuntimeException("Attempt to publish a message from an unconnected MQTT client.");
		}
	}


	/**
	 * Closes current MQTT connection.
	 * @param params map giving common samples' parameters action type and
	 * optional comment.
	 * @param report initial action report, to be filled with all necessary
	 * information about the MQTT disconnection attempt.
	 * @throws IsacRuntimeException if this session object is not connected
	 * to an MQTT server.
	 */
	private void doSampleDisconnect(Map<String, String> params, ActionEvent report)
	{
		if (client != null && client.isConnected())
		{
			report.successful = true;
			report.setDate(System.currentTimeMillis());		
			try
			{
				client.disconnect();
				client = null;
				report.result = "disconnected";
			}
			catch (MqttException ex)
			{
				report.successful = false;
				report.result = ex.getReasonCode() + " " + ex.getMessage();
			}
			report.duration = (int) (System.currentTimeMillis() - report.getDate());
			report.type = params.get(SAMPLE_DISCONNECT_ACTION_TYPE);
			report.comment = params.get(SAMPLE_DISCONNECT_COMMENT);
			if (report.comment == null)
			{
				report.comment = "";
			}
		}
		else
		{
			throw new IsacRuntimeException("Attempt to disconnect an unconnected MQTT client.");
		}
	}


	/**
	 * Subscribes to a topic over an MQTT connection.
	 * @param params map giving subscription topic and qos value, maximum number of
	 * stored incoming messages received on this topic (can be zero to disable storing)
	 * and the message drop policy in case the storage capacity is reached (drop the
	 * oldest message or the incoming message),  
	 * as well as common samples' action type and optional comment.
	 * @param report initial action report, to be filled with all necessary information
	 * about the MQTT topic subscription attempt.
	 * @throws IsacRuntimeException if this session object is not connected to an MQTT
	 * server.
	 */
	private void doSampleSubscribe(Map<String, String> params, ActionEvent report)
	{
		// check state and parameters validity
		if (client == null || !client.isConnected())
		{
			throw new IsacRuntimeException("Attempt to subscribe an unconnected MQTT client.");
		}
		String topic = params.get(SAMPLE_SUBSCRIBE_TOPIC);
		if (topic == null || topic.isEmpty())
		{
			throw new IsacRuntimeException("MQTT client can't subscribe to empty topic.");
		}
		String qosStr = ParameterParser.getRadioGroup(params.get(SAMPLE_SUBSCRIBE_QOS));
		if (qosStr == null || qosStr.isEmpty())
		{
			throw new IsacRuntimeException("Missing QoS specification for MQTT subscription.");
		}
		int qos;
		try
		{
			qos = Integer.parseInt(qosStr.split("\\D", 2)[0]);
		}
		catch (Exception ex)
		{
			throw new IsacRuntimeException(
				"Bad QoS specification format for MQTT subscription: " + qosStr,
				ex);
		}
		String capacityStr = params.get(SAMPLE_SUBSCRIBE_SIZE);
		if (capacityStr == null || capacityStr.isEmpty())
		{
			throw new IsacRuntimeException(
				"Missing message buffer size while subscribing to MQTT topic " + topic);
		}
		int capacity;
		try
		{
			capacity = Integer.parseInt(capacityStr);
		}
		catch (Exception ex)
		{
			throw new IsacRuntimeException(
				"Bad message buffer size specification \"" + capacityStr + "\" while subscribing to MQTT topic "
				+ topic,
				ex);
		}
		String policyStr = ParameterParser.getCombo(params.get(SAMPLE_SUBSCRIBE_POLICY));
		if (policyStr == null || policyStr.isEmpty())
		{
			throw new IsacRuntimeException("Missing drop policy while subscribing to MQTT topic " + topic);
		}
		DropPolicy policy;
		if (policyStr.startsWith("new"))
		{
			policy = DropPolicy.NEWER_FIRST;
		}
		else if (policyStr.startsWith("old"))
		{
			policy = DropPolicy.OLDER_FIRST;
		}
		else
		{
			throw new IsacRuntimeException(
				"Bad drop policy specification \"" + policyStr + "\" while subscribing to MQTT topic "
				+ topic
				+ "\n=> expected " + DropPolicy.NEWER_FIRST + " or " + DropPolicy.OLDER_FIRST);
		}
		if (! messagesByTopic.containsKey(topic) && capacity > 0)
		{
			messagesByTopic.put(topic, new MessageQueue(capacity, policy));
		}
		report.setDate(System.currentTimeMillis());
		try
		{
			if (capacity > 0)
			{
				client.subscribe(topic, qos, this);
			}
			else
			{
				client.subscribe(topic, qos);
			}
			report.successful = true;
			report.result = "subscribed to " + topic + " with QoS " + qos;
		}
		catch (MqttException ex)
		{
			report.successful = false;
			report.result = ex.getReasonCode() + " " + ex.getMessage();
		}
		report.duration = (int) (System.currentTimeMillis() - report.getDate());
		report.type = params.get(SAMPLE_SUBSCRIBE_ACTION_TYPE);
		report.comment = params.get(SAMPLE_SUBSCRIBE_COMMENT);
		if (report.comment == null)
		{
			report.comment = "";
		}
	}


	/**
	 * Unsubscribes from a topic over an MQTT connection.
	 * Whatever the success status of this sample, the message store
	 * for incoming messages is discarded.
	 * @param params map giving topic to unsubscribe from,
	 * as well as common samples' action type and optional comment.
	 * @param report initial action report, to be filled with all necessary information
	 * about the MQTT topic unsubscription attempt.
	 * @throws IsacRuntimeException if this session object is not connected to an MQTT
	 * server.
	 */
	private void doSampleUnsubscribe(Map<String, String> params, ActionEvent report)
	{
		if (client != null && client.isConnected())
		{
			String topic = params.get(SAMPLE_UNSUBSCRIBE_TOPIC);
			report.setDate(System.currentTimeMillis());
			try
			{
				client.unsubscribe(topic);
				report.successful = true;
				report.result = "unsubscribed from " + topic;
			}
			catch (MqttException ex)
			{
				report.successful = false;
				report.result = ex.getReasonCode() + " " + ex.getMessage();
			}
			report.duration = (int) (System.currentTimeMillis() - report.getDate());
			report.type = params.get(SAMPLE_UNSUBSCRIBE_ACTION_TYPE);
			report.comment = params.get(SAMPLE_UNSUBSCRIBE_COMMENT);
			if (report.comment == null)
			{
				report.comment = "";
			}
			MessageQueue mq = messagesByTopic.remove(topic);
			if (mq != null)
			{
				for (MessageWithTopicAbstract msg : mq)
				{
					allMessages.remove(msg);
				}
			}
		}
		else
		{
			throw new IsacRuntimeException("Attempt to unsubscribe an unconnected MQTT client.");
		}
	}


	/**
	 * Gets next message from the message store associated to one, several or
	 * all current topic subscriptions, possibly waiting for a message to be available.
	 * The payload of the message, as well as the actual topic it was sent to, may be
	 * obtained as a plug-in variable (see {@link #doGet(String)}).
	 * @param params map giving the list of topics (empty string to read from all
	 * currently subscribed topics), a time-out (in ms) waiting for a message,
	 * an optional variable name to make the message payload available as a plug-in
	 * variable, an optional variable name for the actual topic it was sent to,
	 * as well as common samples' action type and optional comment. A negative timeout
	 * value means infinite time-out. A zero timeout means no waiting time.
	 * The only way to know if the timeout has been reached without getting a message
	 * is to set a variable name and to check if it has been defined
	 * (see {@link #doTest(int, Map)}.
	 * @param report initial action report, to be filled with all necessary information
	 * about the MQTT message reception attempt.
	 * @throws Error if current JVM does not support UTF-8 encoding
	 * @throws IsacRuntimeException if this session object has not subscribed to the
	 * given MQTT topic.
	 */
	private void doSampleGetMessage(Map<String, String> params, ActionEvent report)
	throws Error
	{
		List<String> topics = ParameterParser.getNField(params.get(SAMPLE_GETMESSAGE_TOPICS));
		MessageQueue queue = null;
		if (topics.isEmpty())
		{
			queue = allMessages;
		}
		else
		{
			Iterator<String> topicIter = topics.iterator();
			while (queue == null && topicIter.hasNext())
			{
				queue = messagesByTopic.get(topicIter.next());
			}
		}
		if (queue != null)
		{
			String msgVar = params.get(SAMPLE_GETMESSAGE_MESSAGE_VAR);
			String topicVar = params.get(SAMPLE_GETMESSAGE_TOPIC_VAR);
			String timeoutStr = params.get(SAMPLE_GETMESSAGE_TIMEOUT);
			int timeout_ms = 0;
			if (! timeoutStr.isEmpty())
			{
				timeout_ms = Integer.parseInt(timeoutStr);
			}
			MessageWithTopic message = null;
			if (msgVar != null && !msgVar.isEmpty())
			{
				variables.remove(msgVar);
			}
			if (topicVar != null && !topicVar.isEmpty())
			{
				variables.remove(topicVar);
			}
			report.setDate(System.currentTimeMillis());
			if (timeout_ms == 0)
			{
				message = (MessageWithTopic) queue.poll();
			}
			else
			{
				try
				{
					if (timeout_ms < 0)
					{
						message = (MessageWithTopic) queue.take();
					}
					else
					{
						message = (MessageWithTopic) queue.poll(timeout_ms, TimeUnit.MILLISECONDS);
					}
				}
				catch (InterruptedException ex)
				{
					message = null;
				}
			}
			report.duration = (int) (System.currentTimeMillis() - report.getDate());
			report.type = params.get(SAMPLE_GETMESSAGE_ACTION_TYPE);
			report.comment = params.get(SAMPLE_GETMESSAGE_COMMENT);
			if (report.comment == null)
			{
				report.comment = "";
			}
			if (message != null)
			{
				report.successful = true;
				report.result = "got one message on topic " + message.topic;
				if (msgVar != null && !msgVar.isEmpty())
				{
					try
					{
						variables.put(msgVar, new String(message.message.getPayload(), "UTF-8"));
					}
					catch (UnsupportedEncodingException ex)
					{
						throw new Error("~MqttInjector~ requires UTF-8 charset encoding", ex);
					}
				}
				if (topicVar != null && !topicVar.isEmpty())
				{
					variables.put(topicVar, message.topic);
				}
				if (queue != allMessages)
				{
					allMessages.remove(message);
				}
				else
				{
					Iterator<Map.Entry<String,MessageQueue>> mqIter = messagesByTopic.entrySet().iterator();
					boolean found = false;
					while (!found && mqIter.hasNext())
					{
						Map.Entry<String,MessageQueue> mq = mqIter.next();
						if (MqttTopic.isMatched(mq.getKey(), message.topic))
						{
							mq.getValue().remove();
						}
					}
				}
			}
			else
			{
				report.successful = false;
				report.result = "got no message on topics " + topics.toString();
			}
		}
		else
		{
			throw new IsacRuntimeException("Attempt to get an MQTT message from unsubscribed topics " + topics.toString());
		}
	}

	///////////////////////////////
	// TestAction implementation //
	///////////////////////////////


	/**
	 * @see org.ow2.clif.scenario.isac.plugin.TestAction#doTest()
	 */
	@Override
	public boolean doTest(int number, Map<String, String> params)
	{
		switch (number)
		{
			case TEST_ISNOTDEFINED:
				return ! variables.containsKey(params.get(TEST_ISNOTDEFINED_VARIABLE));
			case TEST_ISDEFINED:
				return variables.containsKey(params.get(TEST_ISDEFINED_VARIABLE));
			case TEST_ISNOTCONNECTED:
				return client == null || ! client.isConnected();
			case TEST_ISCONNECTED:
				return client != null && client.isConnected();
			default:
				throw new Error(
					"Unable to find this test in ~MqttInjector~ ISAC plugin: "
					+ number);
		}
	}

	////////////////////////////////////
	// interface IMqttMessageListener //
	////////////////////////////////////


	/**
	 * Puts a message to the message store associated to the given topic,
	 * or does nothing if there is no store associated to the topic
	 * (since it means the session object has not subscribed to the topic).
	 * Note that the message store has a limited capacity and may drop a message
	 * in case this capacity is reached.
	 */
	@Override
	public void messageArrived(String topic, MqttMessage message)
	{
		MessageQueue queue = messagesByTopic.get(topic);
		if (queue == null)
		{
			Iterator<Map.Entry<String,MessageQueue>> queueIter = messagesByTopic.entrySet().iterator();
			while (queue == null && queueIter.hasNext())
			{
				Map.Entry<String,MessageQueue> entry = queueIter.next();
				if (MqttTopic.isMatched(entry.getKey(), topic))
				{
					queue = entry.getValue();
				}
			}
		}
		if (queue != null)
		{
			MessageWithTopic msg = new MessageWithTopic(topic, message);
			queue.put(msg);
			allMessages.put(msg);
		}
	}


	/////////////////////////////////////////////////////////////////////
	// inner class for queuing incoming messages with limited capacity //
	/////////////////////////////////////////////////////////////////////

	/**
	 * Inner class for associating an MQTT message with a topic
	 */
	class MessageWithTopic extends MessageWithTopicAbstract
	{
		final String topic;
		final MqttMessage message;

		MessageWithTopic(String topic, MqttMessage message)
		{
			this.topic = topic;
			this.message = message;
		}
	}
}
