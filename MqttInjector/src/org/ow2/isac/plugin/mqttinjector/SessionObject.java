/*
 * CLIF is a Load Injection Framework
 * Copyright (C) 2017 Orange SA
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 *
 * Contact: clif@ow2.org
 */
package org.ow2.isac.plugin.mqttinjector;

import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.ThreadLocalRandom;
import org.ow2.clif.scenario.isac.plugin.SampleAction;
import org.ow2.clif.scenario.isac.plugin.SessionObjectAction;
import org.ow2.clif.scenario.isac.exception.IsacRuntimeException;
import org.ow2.clif.scenario.isac.plugin.TestAction;
import org.ow2.clif.scenario.isac.plugin.DataProvider;
import org.ow2.clif.storage.api.ActionEvent;
import org.ow2.clif.scenario.isac.plugin.ControlAction;


/**
 * Implementation of a session object for plugin ~MqttInjector~,
 * a ISAC plug-in providing MQTT traffic injection over MQTT 3.1.1
 * (@see http://docs.oasis-open.org/mqtt/mqtt/v3.1.1/cos02/mqtt-v3.1.1-cos02.html)
 */
public class SessionObject
	implements
		SessionObjectAction,
		SampleAction,
		TestAction,
		DataProvider,
		ControlAction
{
	static final String PLUGIN_CLIENT_ID_SIZE = "client id size";
	static final String PLUGIN_LIBRARY = "library";

	static final int SAMPLE_CONNECT = 0;
	static final String SAMPLE_CONNECT_KEEPALIVE_S = "keepalive_s";
	static final String SAMPLE_CONNECT_ENABLE = "enable";
	static final String SAMPLE_CONNECT_FLAGS = "flags";
	static final String SAMPLE_CONNECT_TOPIC = "topic";
	static final String SAMPLE_CONNECT_QOS = "qos";
	static final String SAMPLE_CONNECT_MESSAGE = "message";
	static final String SAMPLE_CONNECT_TIMEOUT_S = "timeout_s";
	static final String SAMPLE_CONNECT_PASSWORD = "password";
	static final String SAMPLE_CONNECT_USERNAME = "username";
	static final String SAMPLE_CONNECT_CLIENTID = "clientid";
	static final String SAMPLE_CONNECT_COMMENT = "comment";
	static final String SAMPLE_CONNECT_ACTION_TYPE = "action type";
	static final String SAMPLE_CONNECT_PROTOCOL = "protocol";
	static final String SAMPLE_CONNECT_PORT = "port";
	static final String SAMPLE_CONNECT_HOST = "host";

	static final int SAMPLE_PUBLISH = 1;
	static final String SAMPLE_PUBLISH_FLAGS = "flags";
	static final String SAMPLE_PUBLISH_COMMENT = "comment";
	static final String SAMPLE_PUBLISH_ACTION_TYPE = "action type";
	static final String SAMPLE_PUBLISH_TOPIC = "topic";
	static final String SAMPLE_PUBLISH_QOS = "qos";
	static final String SAMPLE_PUBLISH_MESSAGE = "message";

	static final int SAMPLE_DISCONNECT = 4;
	static final String SAMPLE_DISCONNECT_COMMENT = "comment";
	static final String SAMPLE_DISCONNECT_ACTION_TYPE = "action type";

	static final int SAMPLE_SUBSCRIBE = 5;
	static final String SAMPLE_SUBSCRIBE_POLICY = "policy";
	static final String SAMPLE_SUBSCRIBE_SIZE = "size";
	static final String SAMPLE_SUBSCRIBE_COMMENT = "comment";
	static final String SAMPLE_SUBSCRIBE_ACTION_TYPE = "action type";
	static final String SAMPLE_SUBSCRIBE_QOS = "qos";
	static final String SAMPLE_SUBSCRIBE_TOPIC = "topic";

	static final int SAMPLE_UNSUBSCRIBE = 6;
	static final String SAMPLE_UNSUBSCRIBE_COMMENT = "comment";
	static final String SAMPLE_UNSUBSCRIBE_ACTION_TYPE = "action type";
	static final String SAMPLE_UNSUBSCRIBE_TOPIC = "topic";

	static final int SAMPLE_GETMESSAGE = 7;
	static final String SAMPLE_GETMESSAGE_MESSAGE_VAR = "message var";
	static final String SAMPLE_GETMESSAGE_TOPIC_VAR = "topic var";
	static final String SAMPLE_GETMESSAGE_COMMENT = "comment";
	static final String SAMPLE_GETMESSAGE_ACTION_TYPE = "action type";
	static final String SAMPLE_GETMESSAGE_TOPICS = "topics";
	static final String SAMPLE_GETMESSAGE_TIMEOUT = "timeout";

	static final int TEST_ISCONNECTED = 2;
	static final int TEST_ISNOTCONNECTED = 3;

	static final int TEST_ISDEFINED = 8;
	static final String TEST_ISDEFINED_VARIABLE = "variable";

	static final int TEST_ISNOTDEFINED = 9;
	static final String TEST_ISNOTDEFINED_VARIABLE = "variable";

	static final int CONTROL_CLEAR = 10;
	static final String CONTROL_CLEAR_VARIABLE = "variable";

	private SessionObject sessionObjectImpl;
	protected String clientId;
	protected int clientIdLength = 23;
	// FIFO queues for messages indexed by their subscription topic
	protected Map<String,MessageQueue> messagesByTopic;
	// FIFO queue for all messages
	protected MessageQueue allMessages;
	protected Map<String,String> variables;

	/**
	 * Constructor for specimen object.
	 * Sets the length of generated client ids
	 * (defaults to 23, as a backward compliance
	 * with MQTT 3.1).
	 *
	 * @param params may contain the client id size setting
	 */
	public SessionObject(Map<String,String> params)
	{
		String library = params.get(PLUGIN_LIBRARY);
		if (library != null && library.equals("FuseSource")) {
			sessionObjectImpl = new SessionObjectFuseImpl(params);
		} else {
			/* default value */
			sessionObjectImpl = new SessionObjectPahoImpl(params);
		}
		String idSizeStr = params.get(PLUGIN_CLIENT_ID_SIZE);
		if (idSizeStr != null)
		{
			try
			{
				clientIdLength = Integer.parseInt(idSizeStr);
			}
			catch (NumberFormatException ex)
			{
				throw new IsacRuntimeException("Invalid length for generated MQTT client ids: " + idSizeStr);
			}
		}
	}


	public SessionObject() {
		// do nothing, used by PahoImpl and FuseImpl
	}


	////////////////////////////////////////
	// SessionObjectAction implementation //
	////////////////////////////////////////


	/**
	 * @see org.ow2.clif.scenario.isac.plugin.SessionObjectAction#createNewSessionObject()
	 */
	@Override
	public Object createNewSessionObject()
	{
		return sessionObjectImpl.createNewSessionObject();
	}


	/**
	 * @see org.ow2.clif.scenario.isac.plugin.SessionObjectAction#close()
	 * Forces disconnection of the MQTT client when connected.
	 * Clears the session object state to enable garbage collection.
	 */
	@Override
	public void close()
	{
		messagesByTopic = null;
		allMessages = null;
		variables = null;
		clientId = null;
		sessionObjectImpl.close();
	}


	/**
	 * @see org.ow2.clif.scenario.isac.plugin.SessionObjectAction#reset()
	 * Call {@link #close()} and initializes the session object state.
	 */
	@Override
	public void reset()
	{
		try
		{
			close();
		}
		finally
		{
			messagesByTopic = new HashMap<String,MessageQueue>();
			allMessages = new MessageQueue();
			variables = new HashMap<String,String>();
		}
	}


	/////////////////////////////////
	// SampleAction implementation //
	/////////////////////////////////


	/**
	 * @see org.ow2.clif.scenario.isac.plugin.SampleAction#doSample()
	 */
	@Override
	public ActionEvent doSample(int number, Map<String, String> params, ActionEvent report)
	{
		return sessionObjectImpl.doSample(number, params, report);
	}


	/**
	 * Generates a client id compliant with MQTT 3.1.1 specifications,
	 * i.e. composed of a number of characters among 0-9 A-Z a-z.
	 * The number of such characters is defined by attribute
	 * {@link #clientIdLength}, making the number of possible client ids
	 * equal to 62^clientIdLength.
	 * The id is forged from the system time in nanoseconds, the given alea
	 * parameter, and the #{@link java.util.concurrent.ThreadLocalRandom}
	 * random generator.
	 * @param alea a contribution to the id randomness (suggestion: use the virtual
	 * user id allocated by the ISAC execution engine via the provided action event).
	 * @return the client id, which is very likely to be globally unique provided
	 * the client id length is sufficient compared to the number of clients.
	 */
	protected String genClientId(int alea)
	{
		StringBuilder idsb = new StringBuilder(clientIdLength + (clientIdLength % 23));
		char aleaChar = (char) (alea + System.nanoTime());
		byte[] randomBytes = new byte[23];
		while (idsb.length() < clientIdLength)
		{
			ThreadLocalRandom.current().nextBytes(randomBytes);
			idsb.append(new String(randomBytes));
		}
		idsb.setLength(clientIdLength);
		for (int i=0 ; i < clientIdLength ; ++i)
		{
			char character = (char)('0' + ((idsb.charAt(i) + aleaChar) % 62));
			if (character > '9')
			{
				character += 7;
			}
			if (character > 'Z')
			{
				character += 6;
			}
			if (character > 'z')
			{
				character -= '0';
			}
			idsb.setCharAt(i, character);
		}
		return idsb.toString();
	}


	///////////////////////////////
	// TestAction implementation //
	///////////////////////////////


	/**
	 * @see org.ow2.clif.scenario.isac.plugin.TestAction#doTest()
	 */
	@Override
	public boolean doTest(int number, Map<String, String> params)
	{
		return sessionObjectImpl.doTest(number, params);
	}


	//////////////////////////////////
	// ControlAction implementation //
	//////////////////////////////////


	/**
	 * @see org.ow2.clif.scenario.isac.plugin.ControlAction#doControl()
	 */
	public void doControl(int number, Map<String, String> params)
	{
		switch (number)
		{
			case CONTROL_CLEAR:
				variables.remove(params.get(CONTROL_CLEAR_VARIABLE));
				break;
			default:
				throw new Error(
					"Unable to find this control in ~MqttInjector~ ISAC plugin: "
					+ number);
		}
	}


	/////////////////////////////////
	// DataProvider implementation //
	/////////////////////////////////


	/**
	 * @see org.ow2.clif.scenario.isac.plugin.DataProvider#doGet()
	 */
	@Override
	public String doGet(String var)
	{
		if (var.equalsIgnoreCase("#"))
		{
			return clientId;
		}
		else if (variables.containsKey(var))
		{
			return variables.get(var);
		}
		throw new IsacRuntimeException("Unknown variable in ~MqttInjector~ ISAC plugin: " + var);
	}

	/////////////////////////////////////////////////////////////////////
	// inner class for queuing incoming messages with limited capacity //
	/////////////////////////////////////////////////////////////////////


	public enum DropPolicy {
		OLDER_FIRST, // drop the message at the head of the queue
		NEWER_FIRST // drop incoming message
	};

	/**
	 * This queue implementation changes the behavior of its parent
	 * implementation by disabling any blocking put operation.
	 * Instead, when the queue capacity is reached, the put operation
	 * either drops the head element and puts the new element at the
	 * end (OLDER_FIRST drop policy), or just drops the new element
	 * (NEWER_FIRST drop policy).
	 */
	@SuppressWarnings("serial")
	class MessageQueue extends LinkedBlockingQueue<MessageWithTopicAbstract>
	{
		DropPolicy policy;

		/**
		 * Creates a message store for MQTT messages
		 * with maximum capacity and #{@link SessionObject.DropPolicy#NEWER_FIRST}
		 * drop policy
		 */
		MessageQueue()
		{
			super();
			policy = DropPolicy.NEWER_FIRST;
		}


		/**
		 * Creates a new message store for MQTT messages
		 * @param capacity maximum of number messages this store
		 * can hold before applying the drop policy
		 * @param policy message drop policy to be used when
		 * the message store is full
		 */
		MessageQueue(int capacity, DropPolicy policy)
		{
			super(capacity);
			this.policy = policy;
		}

		/**
		 * Puts the provided message at the end of the message store,
		 * possibly dropping the message at the head of the store if
		 * the store is full and the drop policy is OLDER_FIRST, or just
		 * drops it if the store is full and the drop policy is NEWER_FIRST 
		 */
		@Override
		public synchronized void put(MessageWithTopicAbstract message)
		{
			if (!offer(message) && policy.equals(DropPolicy.OLDER_FIRST))
			{
				poll();
				offer(message);
			}
		}
	}


	/**
	 * Inner class for associating an MQTT message with a topic
	 */
	abstract class MessageWithTopicAbstract {}
}
