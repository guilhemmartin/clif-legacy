/*
 * CLIF is a Load Injection Framework
 * Copyright (C) 2006, 2008-2009, 2012 France Telecom
 * Copyright (C) 2017, 2019 Orange SA
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 *
 * Contact: clif@ow2.org
 */

package org.ow2.isac.plugin.stringhandler;

import org.ow2.clif.scenario.isac.plugin.SessionObjectAction;
import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.net.URLEncoder;
import java.nio.charset.Charset;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import org.ow2.clif.scenario.isac.plugin.DataProvider;
import org.ow2.clif.scenario.isac.exception.IsacRuntimeException;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.Base64;
import org.ow2.clif.scenario.isac.plugin.TestAction;
import org.ow2.clif.scenario.isac.plugin.ControlAction;
import org.ow2.clif.scenario.isac.util.ParameterParser;
import org.ow2.clif.util.Random;
import org.ow2.clif.util.StringHelper;

/**
 * Implementation of a session object for plugin ~StringHandler~
 * @author Bruno Dillenseger
 */
public class SessionObject implements SessionObjectAction, DataProvider, TestAction, ControlAction
{
	static final String PLUGIN_DEFAULT = "default";
	static final String CONTROL_RANDOMSET_SIZE = "size";
	static final int CONTROL_SET = 0;
	static final String CONTROL_SET_VALUE = "value";
	static final int CONTROL_REPLACEALL = 1;
	static final String CONTROL_REPLACEALL_REPLACE = "replace";
	static final String CONTROL_REPLACEALL_SEARCH = "search";
	static final int CONTROL_RANDOMSET = 2;
	static final int CONTROL_SETPATTERN = 3;
	static final String CONTROL_SETPATTERN_PATTERN = "pattern";
	static final int CONTROL_RESETMATCHING = 4;
	static final int CONTROL_MATCH = 5;
	static final String CONTROL_MATCH_SKIP = "skip";
	static final int CONTROL_CAPTURE = 7;
	static final String CONTROL_CAPTURE_VARIABLE = "variable";
	static final String CONTROL_CAPTURE_LEVEL = "level";
	static final int CONTROL_CUT = 8;
	static final String CONTROL_CUT_TAIL = "tail";
	static final String CONTROL_CUT_HEAD = "head";
	static final int CONTROL_TRUNCATE = 11;
	static final String CONTROL_TRUNCATE_LENGTH = "length";
	static final int CONTROL_REPLACEALLMATCHES = 12;
	static final String CONTROL_REPLACEALLMATCHES_REPLACE = "replace";
	static final int CONTROL_WRITE = 13;
	static final String CONTROL_WRITE_STRING = "string";
	static final int CONTROL_APPEND = 14;
	static final String CONTROL_APPEND_STRING = "string";
	static final int CONTROL_INSERT = 15;
	static final String CONTROL_INSERT_STRING = "string";
	static final int CONTROL_SLICE = 16;
	static final String CONTROL_SLICE_OPTIONS = "options";
	static final int CONTROL_NEXTSLICE = 17;
	static final int CONTROL_BASE64DECODE = 20;
	static final String CONTROL_BASE64DECODE_URLSAFE = "urlsafe";
	static final String CONTROL_BASE64DECODE_CHARSET = "charset";
	static final int CONTROL_BASE64ENCODE = 21;
	static final String CONTROL_BASE64ENCODE_OPTIONS = "options";
	static final String CONTROL_BASE64ENCODE_CHARSET = "charset";
	static final int CONTROL_DIGEST = 22;
	static final String CONTROL_DIGEST_ALGO = "algo";
	static final int CONTROL_URLDECODE = 39;
	static final String CONTROL_URLDECODE_CHARSET = "charset";
	static final int CONTROL_URLENCODE = 40;
	static final String CONTROL_URLENCODE_CHARSET = "charset";
	static final int TEST_CONTAINS = 0;
	static final String TEST_CONTAINS_SEARCH = "search";
	static final int TEST_MATCHED = 6;
	static final int TEST_CONTAINSNOT = 9;
	static final String TEST_CONTAINSNOT_SEARCH = "search";
	static final int TEST_MATCHEDNOT = 10;
	static final int TEST_HASMORESLICE = 18;
	static final int TEST_HASNOMORESLICE = 19;
	static final int TEST_ISEMPTY = 23;
	static final int TEST_ISNOTEMPTY = 24;
	static final int TEST_EQUALS = 25;
	static final String TEST_EQUALS_STRING = "string";
	static final int TEST_EQUALSNOT = 26;
	static final String TEST_EQUALSNOT_STRING = "string";
	static final int TEST_EQUALSIGNORECASE = 27;
	static final String TEST_EQUALSIGNORECASE_STRING = "string";
	static final int TEST_EQUALSNOTIGNORECASE = 28;
	static final String TEST_EQUALSNOTIGNORECASE_STRING = "string";
	static final int TEST_LENGTHIS = 29;
	static final String TEST_LENGTHIS_LENGTH = "length";
	static final int TEST_LENGTHISNOT = 30;
	static final String TEST_LENGTHISNOT_LENGTH = "length";
	static final int TEST_LENGTHISGT = 31;
	static final String TEST_LENGTHISGT_LENGTH = "length";
	static final int TEST_LENGTHISLT = 32;
	static final String TEST_LENGTHISLT_LENGTH = "length";
	static final int TEST_LENGTHISGTE = 33;
	static final String TEST_LENGTHISGTE_LENGTH = "length";
	static final int TEST_LENGTHISLTE = 34;
	static final String TEST_LENGTHISLTE_LENGTH = "length";
	static final int TEST_ISGT = 35;
	static final String TEST_ISGT_STRING = "string";
	static final int TEST_ISLT = 36;
	static final String TEST_ISLT_STRING = "string";
	static final int TEST_ISGTE = 37;
	static final String TEST_ISGTE_STRING = "string";
	static final int TEST_ISLTE = 38;
	static final String TEST_ISLTE_STRING = "string";
	static private final String GET_LENGTH = "#";
	static private final String GET_STRING = "";
	static private final String GET_SLICE = "slice";
	static private final String GET_SLICE_NUMBER = "slice#";
	static private final String GET_NEXT_SLICE = "slice++";

	static private Random random = new Random();

	private String defaultValue;
	private StringBuilder value;
	private Pattern pattern;
	private Matcher matcher;
	private Map<String,String> variables = new HashMap<String,String>();
	private String[] slices;
	private int sliceIndex;
	private boolean matched = false;


	/**
	 * Constructor for specimen object.
	 *
	 * @param params key-value pairs for plugin parameters
	 */
	public SessionObject(Map<String,String> params)
	{
		defaultValue = params.get(PLUGIN_DEFAULT);
		if (defaultValue == null)
		{
			defaultValue = "";
		}
	}


	/**
	 * Copy constructor (clone specimen object to get session object).
	 *
	 * @param so specimen object to clone
	 */
	private SessionObject(SessionObject so)
	{
		defaultValue = so.defaultValue;
		value = new StringBuilder(defaultValue);
	}


	/**
	 * Must be called each time the string value changes
	 * to update a number of dependencies.
	 */
	private void valueChanged()
	{
		// reset pattern matcher
		if (pattern != null)
		{
			matcher.reset(value);
			slices = null;
		}
	}


	////////////////////////////////////////
	// SessionObjectAction implementation //
	////////////////////////////////////////

	/**
	 * @see org.ow2.clif.scenario.isac.plugin.SessionObjectAction#createNewSessionObject()
	 */
	public Object createNewSessionObject() {
		return new SessionObject(this);
	}

	/**
	 * @see org.ow2.clif.scenario.isac.plugin.SessionObjectAction#close()
	 */
	public void close() {
		pattern = null;
		matcher = null;
		variables = null;
		value = null;
		slices = null;
		defaultValue = null;
	}

	/**
	 * @see org.ow2.clif.scenario.isac.plugin.SessionObjectAction#reset()
	 */
	public void reset() {
		pattern = null;
		matcher = null;
		variables = new HashMap<String,String>();
		matched = false;
		value = new StringBuilder(defaultValue);
		slices = null;
	}


	/////////////////////////////////
	// DataProvider implementation //
	/////////////////////////////////


	/**
	 * @see org.ow2.clif.scenario.isac.plugin.DataProvider#doGet()
	 */
	public String doGet(String arg)
	{
		String result = null;
		if (arg.equals(GET_STRING))
		{
			result = value.toString();
		}
		else if (arg.equals(GET_LENGTH))
		{
			result = Integer.toString(value.length());
		}
		else if (arg.equals(GET_SLICE_NUMBER))
		{
			if (slices != null)
			{
				result = String.valueOf(slices.length);
			}
		}
		else if (arg.startsWith(GET_SLICE_NUMBER))
		{
			if (slices != null)
			{
				int index = Integer.valueOf(arg.substring(GET_SLICE_NUMBER.length()));
				if (index >= 0 && index < slices.length)
				{
					result = slices[index];
				}
			}
		}
		else if (arg.equals(GET_SLICE))
		{
			if (slices != null && slices.length > 0 && sliceIndex >= 0 && sliceIndex < slices.length)
			{
				result = slices[sliceIndex];
			}
		}
		else if (arg.equals(GET_NEXT_SLICE))
		{
			doControl(CONTROL_NEXTSLICE, null);
			if (slices != null && slices.length > 0 && sliceIndex >= 0 && sliceIndex < slices.length)
			{
				result = slices[sliceIndex];
			}
		}
		else
		{
			 result = variables.get(arg);
		}
		if (result == null)
		{
			throw new IsacRuntimeException("Variable not available in ~StringHandler~ ISAC plugin: " + arg);
		}
		else
		{
			return result;
		}
	}


	///////////////////////////////
	// TestAction implementation //
	///////////////////////////////


	/**
	 * @see org.ow2.clif.scenario.isac.plugin.TestAction#doTest()
	 */
	public boolean doTest(int number, Map<String,String> params)
	{
		switch (number)
		{
			case TEST_ISLTE:
				return value.toString().compareTo(params.get(TEST_ISLTE_STRING)) <= 0;
			case TEST_ISGTE:
				return value.toString().compareTo(params.get(TEST_ISGTE_STRING)) >= 0;
			case TEST_ISLT:
				return value.toString().compareTo(params.get(TEST_ISLT_STRING)) < 0;
			case TEST_ISGT:
				return value.toString().compareTo(params.get(TEST_ISGT_STRING)) > 0;
			case TEST_LENGTHISLTE:
				return value.length() <= Integer.parseInt(params.get(TEST_LENGTHISLTE_LENGTH));
			case TEST_LENGTHISGTE:
				return value.length() >= Integer.parseInt(params.get(TEST_LENGTHISGTE_LENGTH));
			case TEST_LENGTHISLT:			
				return value.length() < Integer.parseInt(params.get(TEST_LENGTHISLT_LENGTH));
			case TEST_LENGTHISGT:
				return value.length() > Integer.parseInt(params.get(TEST_LENGTHISGT_LENGTH));
			case TEST_LENGTHISNOT:
				return value.length() != Integer.parseInt(params.get(TEST_LENGTHISNOT_LENGTH));
			case TEST_LENGTHIS:
				return value.length() == Integer.parseInt(params.get(TEST_LENGTHIS_LENGTH));
			case TEST_EQUALSNOTIGNORECASE:
				return ! value.toString().equalsIgnoreCase(params.get(TEST_EQUALSNOTIGNORECASE_STRING));
			case TEST_EQUALSIGNORECASE:
				return value.toString().equalsIgnoreCase(params.get(TEST_EQUALSIGNORECASE_STRING));
			case TEST_EQUALSNOT:
				return ! value.toString().equals(params.get(TEST_EQUALSNOT_STRING));
			case TEST_EQUALS:
				return value.toString().equals(params.get(TEST_EQUALS_STRING));
			case TEST_ISNOTEMPTY:
				return value.length() > 0;
			case TEST_ISEMPTY:
				return value.length() == 0;
			case TEST_HASNOMORESLICE:
				return ! doTest(TEST_HASMORESLICE, params);
			case TEST_HASMORESLICE:
				if (slices == null)
				{
					throw new IsacRuntimeException("StringHandler's slice control must be called prior to testing slice availability.");
				}
				return sliceIndex+1 < slices.length;
			case TEST_MATCHEDNOT:
				return !matched;
			case TEST_CONTAINSNOT:
				return value.indexOf(params.get(TEST_CONTAINSNOT_SEARCH)) == -1; 
			case TEST_MATCHED:
				return matched;
			case TEST_CONTAINS:
				return value.indexOf(params.get(TEST_CONTAINS_SEARCH)) != -1;
			default:
				throw new Error("Unable to find this test in ~StringHandler~ ISAC plugin: " + number);
		}
	}

	
	//////////////////////////////////
	// ControlAction implementation //
	//////////////////////////////////

	/**
	 * @see org.ow2.clif.scenario.isac.plugin.ControlAction#doControl()
	 */
	public void doControl(int number, Map<String,String> params)
	{
		switch (number)
		{
			case CONTROL_URLENCODE:
				try
				{
					String charset = getCharset(params.get(CONTROL_URLENCODE_CHARSET));
					setValue(new StringBuilder(URLEncoder.encode(value.toString(), charset)));
				}
				catch (UnsupportedEncodingException ex)
				{
					throw new IsacRuntimeException(
						"Plugin ~StringHandler~ could not perform URL encoding: unsupported charset "
							+ params.get(CONTROL_URLENCODE_CHARSET),
						ex);
				}
				break;
			case CONTROL_URLDECODE:
				try
				{
					String charset = getCharset(params.get(CONTROL_URLDECODE_CHARSET));
					setValue(new StringBuilder(URLDecoder.decode(value.toString(), charset)));
				}
				catch (UnsupportedEncodingException ex)
				{
					throw new IsacRuntimeException(
						"Plugin ~StringHandler~ could not perform URL decoding: unsupported charset "
							+ params.get(CONTROL_URLDECODE_CHARSET),
						ex);
				}
				break;
			case CONTROL_DIGEST:
				try
				{
					String algoStr = params.get(CONTROL_DIGEST_ALGO);
					MessageDigest md = MessageDigest.getInstance(algoStr);
					md.update(value.toString().getBytes());
					setValue(new StringBuilder(new String(md.digest())));
				}
				catch (NoSuchAlgorithmException ex)
				{
					throw new IsacRuntimeException(
						"Plugin ~StringHandler~ could not perform digest: unsupported algorithm "
							+ params.get(CONTROL_DIGEST_ALGO),
						ex);
				}
				break;
			case CONTROL_BASE64ENCODE:
				try
				{
					String charset = params.get(CONTROL_BASE64ENCODE_CHARSET);
					List<String> options = ParameterParser.getCheckBox(params.get(CONTROL_BASE64ENCODE_OPTIONS));
					boolean urlSafe = options.contains("URL-safe");
					boolean discardPadding = options.contains("Discard padding");
					Base64.Encoder encoder;
					if (urlSafe)
					{
						encoder = Base64.getUrlEncoder();
					}
					else
					{
						encoder = Base64.getEncoder();
					}
					StringBuilder b64 = new StringBuilder(
						new String(
							encoder.encode(this.value.toString().getBytes(charset)),
							charset));
					if (discardPadding)
					{
						b64.setLength(b64.indexOf("="));
					}
					setValue(b64);
				}
				catch (UnsupportedEncodingException ex)
				{
					throw new IsacRuntimeException(
						"Plugin ~StringHandler~ could not perform base64 encoding: unsupported charset "
							+ params.get(CONTROL_BASE64ENCODE_CHARSET),
						ex);
				}
				break;
			case CONTROL_BASE64DECODE:
				try
				{
					String charset = params.get(CONTROL_BASE64DECODE_CHARSET);
					String urlSafeStr = params.get(CONTROL_BASE64DECODE_URLSAFE);
					Base64.Decoder decoder;
					if (urlSafeStr != null && StringHelper.isEnabled(urlSafeStr))
					{
						decoder = Base64.getUrlDecoder();
					}
					else
					{
						decoder = Base64.getDecoder();
					}
					setValue(
						new StringBuilder(
							new String(
								decoder.decode(this.value.toString().getBytes(charset)),
								charset)));
				}
				catch (IllegalArgumentException ex)
				{
					throw new IsacRuntimeException("Plugin ~StringHandler~ could not decode base64 input", ex);
				}
				catch (UnsupportedEncodingException ex)
				{
					throw new IsacRuntimeException(
						"Plugin ~StringHandler~ could not perform base64 decoding: unsupported charset "
							+ params.get(CONTROL_BASE64DECODE_CHARSET),
						ex);
				}
				break;
			case CONTROL_NEXTSLICE:
				if (slices != null && slices.length > 0 && sliceIndex < slices.length)
				{
					++sliceIndex;
				}
				else
				{
					throw new IsacRuntimeException("StringHandler has no next slice.");
				}
				break;
			case CONTROL_SLICE:
				if (pattern != null)
				{
					List<String> options = ParameterParser.getCheckBox(params.get(CONTROL_SLICE_OPTIONS));
					if (options.contains("Discard empty strings"))
					{
						slices = pattern.split(value, 0);
						List<String> realSlicesList = new ArrayList<String>(slices.length);
						for (String str : slices)
						{
							if (! str.isEmpty())
							{
								realSlicesList.add(str);
							}
						}
						slices = realSlicesList.toArray(new String[realSlicesList.size()]);
					}
					else
					{
						slices = pattern.split(value, -1);
					}
					sliceIndex = -1;
				}
				else
				{
					throw new IsacRuntimeException("StringHandler can't slice when no pattern is set.");
				}
				break;
			case CONTROL_INSERT:
				value.insert(0, params.get(CONTROL_INSERT_STRING));
				valueChanged();
				break;
			case CONTROL_APPEND:
				value.append(params.get(CONTROL_APPEND_STRING));
				valueChanged();
				break;
			case CONTROL_WRITE:
				String overwrite = params.get(CONTROL_WRITE_STRING);
				value.replace(0, overwrite.length(), overwrite);
				valueChanged();
				break;
			case CONTROL_REPLACEALLMATCHES:
				if (pattern != null)
				{
					setValue(new StringBuilder(matcher.replaceAll(params.get(CONTROL_REPLACEALLMATCHES_REPLACE))));
				}
				else
				{
					throw new IsacRuntimeException("StringHandler can't replace matches when no pattern is set.");
				}
				break;
			case CONTROL_TRUNCATE:
				int length = Integer.parseInt(params.get(CONTROL_TRUNCATE_LENGTH));
				if (length < value.length())
				{
					value.delete(length, value.length());
				}
				valueChanged();
				break;
			case CONTROL_CUT:
				int head = Integer.parseInt(params.get(CONTROL_CUT_HEAD));
				int tail = Integer.parseInt(params.get(CONTROL_CUT_TAIL));
				if (head > 0)
				{
					value.delete(0, head);
				}
				if (tail > 0)
				{
					value.delete(value.length()-tail, value.length());
				}
				valueChanged();
				break;
			case CONTROL_CAPTURE:
				if (matched)
				{
					variables.put(
						params.get(CONTROL_CAPTURE_VARIABLE),
						matcher.group(
							Integer.parseInt(params.get(CONTROL_CAPTURE_LEVEL))));
				}
				else
				{
					variables.remove(params.get(CONTROL_CAPTURE_VARIABLE));
					throw new IsacRuntimeException("StringHandler plug-in can't capture an unmatched pattern.");
				}
				break;
			case CONTROL_MATCH:
				int i = Integer.parseInt(params.get(CONTROL_MATCH_SKIP));
				matched = false;
				while (i > 0 && matcher.find())
				{
					--i;
				}
				if (i == 0)
				{
					matched = matcher.find();
				}
				break;
			case CONTROL_RESETMATCHING:
				matcher.reset();
				matched = false;
				break;
			case CONTROL_SETPATTERN:
				pattern = Pattern.compile(params.get(CONTROL_SETPATTERN_PATTERN));
				matcher = pattern.matcher(value);
				matched = false;
				break;
			case CONTROL_RANDOMSET:
				setValue(random.nextStringBuilder(Integer.parseInt(params.get(CONTROL_RANDOMSET_SIZE))));
				break;
			case CONTROL_REPLACEALL:
				String search = params.get(CONTROL_REPLACEALL_SEARCH);
				String replace = params.get(CONTROL_REPLACEALL_REPLACE);
				int pos = 0;
				while (pos < value.length() && (pos = value.indexOf(search, pos)) != -1)
				{
					value.replace(pos, pos + search.length(), replace);
					pos += replace.length();
				}
				valueChanged();
				break;
			case CONTROL_SET:
				setValue(new StringBuilder((params.get(CONTROL_SET_VALUE))));
				break;
			default:
				throw new Error("Unable to find this control in ~StringHandler~ ISAC plugin: " + number);
		}
	}

	private void setValue(StringBuilder newValue)
	{
		value = newValue;
		valueChanged();
	}

	private String getCharset(String charset)
	{
		if (charset == null || charset.isEmpty() || charset.equalsIgnoreCase("default"))
		{
			return Charset.defaultCharset().name();
		}
		else
		{
			return charset;
		}
	}
}
